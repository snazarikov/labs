#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <queue>
#include <iomanip>

using namespace std;

const int NMAX = 505;
const int INF = 1e9 + 1;

int vertexAmount;

int net[NMAX][NMAX];
int threadVolume[NMAX][NMAX];
int throughputLeft[NMAX][NMAX];

void getData() {
    cin >> vertexAmount;

    int u, v, c;
    while (cin >> u >> v >> c) {
        net[u][v] = c;
        net[v][u] = 0;
        throughputLeft[u][v] = c;
    }
}

int EdmondsKarp(int source, int sink) {
    int isUsed[NMAX];
    int parent[NMAX];
    int topVertex;
    int isPathFinded;

    // searching path in net
    while (1) {
        memset(parent, 0, sizeof parent);
        memset(isUsed, 0, sizeof parent);
        queue<int> q;

        isPathFinded = 0;

        q.push(source);
        isUsed[source] = 1;

        // bfs
        while (!q.empty()) {
            topVertex = q.front();
            q.pop();

            for (int i = 1; i <= vertexAmount; ++i) {
                if (!isUsed[i] && throughputLeft[topVertex][i] > 0) {
                    parent[i] = topVertex;
                    isUsed[i] = 1;
                    q.push(i);

                    if (i == sink) {
                        isPathFinded = 1;
                        break;
                    }
                }
            }
        }

        if (!isPathFinded) {
            break;
        }

        // searching min thread
        int minThread = INF;
        int v = sink, u;
        while (v != source) {
            u = parent[v];
            if (minThread > throughputLeft[u][v]) {
                minThread = throughputLeft[u][v];
            }
            v = u;
        }

        // updating values
        v = sink;
        while (v != source) {
            u = parent[v];
            threadVolume[u][v] += minThread;
            threadVolume[v][u] = -threadVolume[u][v];
            throughputLeft[u][v] -= minThread;
            throughputLeft[v][u] += minThread;
            v = u;
        }
    }

    int result = 0;
    for (int i = 1; i <= vertexAmount; ++i) {
        result += threadVolume[source][i];
    }
    return result;
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    getData();
    cout << EdmondsKarp(1, vertexAmount) << endl;

    for (int i = 1; i <= vertexAmount; ++i) {
        for (int j = 1; j <= vertexAmount; ++j) {
            cout << setw(3) << threadVolume[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}
