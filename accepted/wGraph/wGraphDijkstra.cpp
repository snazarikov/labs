#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

const int NMAX = 105;
const int INF = 1e9 + 5;

int vertexAmount, startVertex;

struct graphElem {
    int vertex;
    int weight;
};

vector<graphElem> graph[NMAX];

void getData() {
    cin >> vertexAmount;

    int u, v, w;
    while (cin >> u >> v >> w) {
        struct graphElem e;
        e.vertex = v;
        e.weight = w;
        graph[u].push_back(e);
        e.vertex = u;
        e.weight = w;
        graph[v].push_back(e);
    }

    startVertex = vertexAmount;
}

void dijkstra(int start) {
    vector<int> dist(vertexAmount + 5, INF);
    vector<int> parent(vertexAmount + 5);
    vector<int> isUsed(vertexAmount + 5);

	dist[start] = 0;

	for (int i = 0; i < vertexAmount; ++i) {
		int v = -1;
		for (int j = 1; j <= vertexAmount; ++j) {
            if (!isUsed[j] && (v == -1 || dist[j] < dist[v])) {
                v = j;
			}
		}

        if (dist[v] == INF) {
            break;
		}

		isUsed[v] = 1;
		for (int j = 0; j < graph[v].size(); ++j) {
			int to = graph[v][j].vertex;
			int w = graph[v][j].weight;

			if (dist[v] + w < dist[to]) {
				dist[to] = dist[v] + w;
				parent[to] = v;
			}
		}
	}

    for (int i = 1; i < vertexAmount; ++i) {
        int path[NMAX * NMAX];
        int counter = 0;
        int v = i;

        path[counter++] = v;
        int flag = 1;
        while (parent[v] != start) {
            path[counter++] = parent[v];
            v = parent[v];
            if (!v) {
                flag = 0;
                break;
            }
        }

        if (!flag) {
            cout << "no path" << endl;
            continue;
        }

        cout << start;
        for (int i = counter - 1; i >= 0; --i) {
            cout << " " << path[i];
        }
        cout << endl;
    }
}

int main() {
    freopen ("input.txt", "r", stdin);
    freopen ("output.txt", "w", stdout);
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    getData();
    dijkstra(startVertex);
    return 0;
}
