#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const int VERTEX_AMOUNT = 1e5 + 5;
const int EDGE_AMOUNT = 1e6 + 5;

int unionSize[VERTEX_AMOUNT];
int parent[VERTEX_AMOUNT];
int isUsed[VERTEX_AMOUNT];

int vertexAmount;
int edgeAmount = 0;

struct Edge {
    int u;
    int v;
    int w;
    Edge(int one = 0, int two = 0, int three = 0) : u(one), v(two), w(three) {}
};

vector<Edge> edges(EDGE_AMOUNT);

int getPresentor(int u) {
    if (u == parent[u]) {
        return u;
    }
    parent[u] = getPresentor(parent[u]);
    return parent[u];
}

void makeUnion(int u, int v) {
    int uPresentor = getPresentor(u);
    int vPresentor = getPresentor(v);

    if (uPresentor == vPresentor) {
        return;
    }
    if (unionSize[u] > unionSize[v]) {
        unionSize[uPresentor] += unionSize[vPresentor];
        parent[vPresentor] = uPresentor;
    }
    else {
        unionSize[vPresentor] += unionSize[uPresentor];
        parent[uPresentor] = vPresentor;
    }
}

void getData() {
    cin >> vertexAmount;
    int u, v, w;
    while (cin >> u >> v >> w) {
        edges[edgeAmount++] = Edge(u, v, w);
    }
}

void printData() {
    cout << vertexAmount << endl;
    for (int i = 0; i < edgeAmount; ++i) {
        cout << edges[i].u << " " << edges[i].v << " " << edges[i].w << endl;
    }
    cout << endl;
}

bool myCmp(Edge a, Edge b) {
    return a.w < b.w;
}

void Kruscal() {
    vector<Edge> MST(EDGE_AMOUNT);
    int MSTamount = 0;
    int MSTweight = 0;
    for (int i = 0; i < edgeAmount; ++i) {
        int u = edges[i].u;
        int v = edges[i].v;
        if (getPresentor(u) != getPresentor(v)) {
            MST[MSTamount++] = edges[i];
            MSTweight += edges[i].w;
            makeUnion(getPresentor(u), getPresentor(v));
        }
    }

    cout << MSTweight << endl;
    for (int i = 0; i < MSTamount; ++i) {
        cout << MST[i].u << " " << MST[i].v << endl;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    getData();

    // DSU init
    for (int i = 1; i <= vertexAmount; i++) {
        unionSize[i] = 1;
        parent[i] = i;
    }

    sort(edges.begin(), edges.begin() + edgeAmount, myCmp);

    Kruscal();

    return 0;
}
