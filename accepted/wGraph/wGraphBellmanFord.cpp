#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

const int NMAX = 105;
const int INF = 1e9 + 5;

int vertexAmount, edgeAmount = 0, startVertex;

struct Edge {
    int u;
    int v;
    int weight;
};

vector<Edge> graph(2 * NMAX * NMAX);

void getData() {
    cin >> vertexAmount;

    int u, v, w;
    while (cin >> u >> v >> w) {
        graph[edgeAmount++] = Edge{u, v, w};
        graph[edgeAmount++] = Edge{v, u, w};
    }

    startVertex = vertexAmount;
}

void bellmanford(int start) {
    vector<int> dist(vertexAmount + 5, INF);
    vector<int> parent(vertexAmount + 5, 0);

    dist[start] = 0;

    int u, v, w;
    for (int i = 0; i < vertexAmount - 1; ++i) {
        for (int j = 0; j < edgeAmount; ++j) {
            u = graph[j].u;
            v = graph[j].v;
            w = graph[j].weight;

            if (dist[v] > dist[u] + w) {
                dist[v] = dist[u] + w;
                parent[v] = u;
            }
        }
    }

    for (int i = 1; i < vertexAmount; ++i) {
        int path[NMAX * NMAX];
        int counter = 0;
        int v = i;

        path[counter++] = v;
        int flag = 1;
        while (parent[v] != start) {
            path[counter++] = parent[v];
            v = parent[v];
            if (!v) {
                flag = 0;
                break;
            }
        }

        if (!flag) {
            cout << "no path" << endl;
            continue;
        }

        cout << start;
        for (int i = counter - 1; i >= 0; --i) {
            cout << " " << path[i];
        }
        cout << endl;
    }
}

int main() {
    freopen ("input.txt", "r", stdin);
    freopen ("output.txt", "w", stdout);
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    getData();
    bellmanford(startVertex);
    return 0;
}
