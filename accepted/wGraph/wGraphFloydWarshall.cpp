#include <cstdio>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const int VERTEX_AMOUNT = 1e2 + 5;
const int INF = 1e3 + 5;

int graph[VERTEX_AMOUNT][VERTEX_AMOUNT];
int dist[VERTEX_AMOUNT][VERTEX_AMOUNT];
int parent[VERTEX_AMOUNT][VERTEX_AMOUNT];

int vertexAmount;

void getData() {
    cin >> vertexAmount;
    int u, v, w;
    while (cin >> u >> v >> w) {
        graph[u][v] = w;
        graph[v][u] = w;
    }
}

void FloydWarshall() {
    memcpy(dist, graph, sizeof graph);

    for (int i = 0; i < VERTEX_AMOUNT; ++i) {
        for (int j = 0; j < VERTEX_AMOUNT; ++j) {
            if (!dist[i][j] && i != j) {
                dist[i][j] = INF;
            }
        }
    }

    for (int k = 1; k <= vertexAmount; ++k) {
        for (int i = 1; i <= vertexAmount; ++i) {
            for (int j = 1; j <= vertexAmount; ++j) {
                if (dist[i][k] + dist[k][j] < dist[i][j]) {
                    dist[i][j] = dist[i][k] + dist[k][j];
                }
            }
        }
    }

    for (int i = 0; i < VERTEX_AMOUNT; ++i) {
        for (int j = 0; j < VERTEX_AMOUNT; ++j) {
            if (dist[i][j] == INF) {
                dist[i][j] = -1;
            }
        }
    }

    for (int i = 1; i <= vertexAmount; ++i) {
        for (int j = 1; j <= vertexAmount; ++j) {
            cout << setw(4) << dist[i][j] << " ";
        }
        cout << endl;
    }
}


void buildParent() {
    for (int i = 1; i <= vertexAmount; ++i) {
        for (int j = 1; j <= vertexAmount; ++j) {
            if (dist[i][j] == graph[i][j]) {
                continue;
            }

            int minDist = INF;
            int p = 0;
            for (int k = 1; k <= vertexAmount; ++k) {
                if (k != i && k != j && dist[i][j] == dist[i][k] + dist[k][j] && dist[k][j] < minDist) {
                    minDist = dist[k][j];
                    p = k;
                }
            }
            parent[i][j] = p;
        }
    }

    for (int i = 1; i <= vertexAmount; ++i) {
        for (int j = 1; j <= vertexAmount; ++j) {
            cout << setw(4) << parent[i][j] << " ";
        }
        cout << endl;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    getData();
    FloydWarshall();
    cout << endl;
    buildParent();

    return 0;
}
