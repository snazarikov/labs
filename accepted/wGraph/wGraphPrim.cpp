#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <set>

#define pii pair<int, int>

using namespace std;

const int VERTEX_AMOUNT = 1e5 + 5;
const int INF = 1e9 + 5;

int vertexAmount;

vector<pii> graph[VERTEX_AMOUNT];

void getData() {
    cin >> vertexAmount;
    int u, v, w;
    while (cin >> u >> v >> w) {
        graph[u].push_back(pii(w, v));
        graph[v].push_back(pii(w, u));
    }
}

void Prim() {
    vector<int> minWeight(VERTEX_AMOUNT, INF);
    vector<int> minVertex(VERTEX_AMOUNT, 0);
    vector<int> isUsed(VERTEX_AMOUNT, 0);

    minWeight[1] = 0;
    set<pii> myQueue;
    myQueue.insert(pii(0, 1));

    for (int i = 1; i <= vertexAmount; ++i) {
        if (myQueue.empty()) {
            break;
        }

        int v = myQueue.begin()->second;
        isUsed[v] = 1;
        myQueue.erase(myQueue.begin());

        int u, weight;
        for (int j = 0; j < graph[v].size(); ++j) {
            u = graph[v][j].second;
            weight = graph[v][j].first;
            if (weight < minWeight[u] && !isUsed[u]) {
                myQueue.erase(pii(minWeight[u], u));
                minWeight[u] = weight;
                minVertex[u] = v;
                myQueue.insert(pii(minWeight[u], u));
            }
        }
    }

    int MSTweight = 0;
    for (int i = 2; i <= vertexAmount; ++i) {
        MSTweight += minWeight[i];
    }

    cout << MSTweight << endl;
    for (int i = 2; i <= vertexAmount; ++i) {
        cout << i << " " << minVertex[i] << endl;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    getData();
    Prim();

    return 0;
}
