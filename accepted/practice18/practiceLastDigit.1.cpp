#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>

using namespace std;

const int NMAX = 1e6 + 5;

int n, k;

int globalDivisor[NMAX];
int localDivisor[NMAX];

void erato(int numeral) {
    for (int i = 2; i <= numeral; ++i) {
        localDivisor[i] = i;
    }

    for (int i = 2; i <= numeral; ++i) {
        if (localDivisor[i] == i) {
            for (int j = 2 * i; j <= numeral; j += i) {
                localDivisor[j] = i;
            }
        }
    }
}

void getDivisors(int numeral, int incrementOrDecrement) {
    while (numeral != 1) {
        globalDivisor[localDivisor[numeral]] += incrementOrDecrement;
        numeral /= localDivisor[numeral];
    }
}

int modProduct(long long a, long long b, long long d) {
    return (((a % d) * (b % d)) % d);
}

void C(int n, int k) {
	for (int i = n - k + 1; i <= n; ++i) {
        getDivisors(i, 1);
	}
	for (int i = 2; i <= k; ++i) {
        getDivisors(i, -1);
	}

	long long d = 10;
	int pow = 1;
	while (pow <= min(globalDivisor[2], globalDivisor[5])) {
        d *= 10;
        pow++;
	}

    int value = 1;
    for (int i = 2; i <= n; ++i) {
        while (globalDivisor[i]) {
            value = modProduct(value, i, d);
            globalDivisor[i]--;
        }
    }

    d /= 10;
    cout << value / d;
}

int main() {
//    freopen ("input.txt", "r", stdin);
    freopen ("cnm.in", "r", stdin);
    freopen ("cnm.out", "w", stdout);

    cin >> n >> k;
    erato(n);
    C(n, k);

    return 0;
}
