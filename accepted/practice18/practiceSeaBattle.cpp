#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <vector>

using namespace std;

const int NMAX = 15;
const int FIELD_SIZE = 10;
//                        i   j
const pair<int, int> WEST(0, -1);
const pair<int, int> EAST(0, 1);
const pair<int, int> NORTH(-1, 0);
const pair<int, int> SOUTH(1, 0);

char gameField[NMAX][NMAX];
int someInfo[NMAX][NMAX];

int ships[NMAX];

void getData() {
    for (int i = 0; i < FIELD_SIZE; ++i) {
        for (int j = 0; j < FIELD_SIZE; ++j) {
            cin >> gameField[i][j];
        }
    }
}

void GG() {
    cout << "incorrect";
    exit(0);
}

bool checkDiagonal(int ii, int jj) {
    if ((ii + 1 < FIELD_SIZE && jj + 1 < FIELD_SIZE && gameField[ii + 1][jj + 1] == '1') ||
        (ii + 1 < FIELD_SIZE && jj - 1 >= 0 && gameField[ii + 1][jj - 1] == '1') ||
        (ii - 1 >= 0 && jj + 1 < FIELD_SIZE && gameField[ii - 1][jj + 1] == '1') ||
        (ii - 1 >= 0 && jj - 1 >= 0 && gameField[ii - 1][jj - 1] == '1')) {
        return false;
    }
    return true;
}

bool checkBorder(int ii, int jj) {
    if (ii == 0 || ii == FIELD_SIZE - 1 || jj == 0 || jj == FIELD_SIZE - 1) {
        return true;
    }
    return false;
}

bool checkShip(int ii, int jj) {
    if (checkBorder(ii, jj)) {
        return false;
    }
    someInfo[ii][jj] = 1;
    pair<int, int> direction(0, 0);
    if (ii - 1 >= 0 && gameField[ii - 1][jj] == '1') {
        direction = NORTH;
    }
    else if (ii + 1 < FIELD_SIZE && gameField[ii + 1][jj] == '1') {
        direction = SOUTH;
    }
    else if (jj - 1 >= 0 && gameField[ii][jj - 1] == '1') {
        direction = WEST;
    }
    else if (jj + 1 < FIELD_SIZE && gameField[ii][jj + 1] == '1') {
        direction = EAST;
    }
    else {
        ships[1]++;
        if (ships[1] > 4 || !checkDiagonal(ii, jj)) {
            return false;
        }
        return true;
    }

    int i = ii, j = jj, shipLen = 0;
    while (i >= 0 && i < FIELD_SIZE && j >= 0 && j < FIELD_SIZE && gameField[i][j] == '1') {
        someInfo[i][j] = 1;
        shipLen++;
        if (checkBorder(i, j) || !checkDiagonal(i, j)) {
            return false;
        }
        i += direction.first;
        j += direction.second;
    }


    if (shipLen > 4) {
        return false;
    }

    ships[shipLen]++;

    if (ships[shipLen] > 5 - shipLen) {
        return false;
    }

    return true;
}

bool checkShipAmount() {
    for (int i = 1; i <= 4; ++i) {
        if (ships[i] != 5 - i) {
            return false;
        }
    }
    return true;
}

int main() {
//    freopen("input.txt", "r", stdin);
    freopen("battle.in", "r", stdin);
    freopen("battle.out", "w", stdout);

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    getData();

    for (int i = 0; i < FIELD_SIZE; ++i) {
        for (int j = 0; j < FIELD_SIZE; ++j) {
            if (!someInfo[i][j] && gameField[i][j] == '1' && !checkShip(i, j)) {
                GG();
            }
        }
    }

    if (!checkShipAmount()) {
        GG();
    }

    cout << "correct" << endl;

    return 0;
}
