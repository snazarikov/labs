#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>

using namespace std;

const int NMAX = 505;

string ATsequence;

void solution() {
    cin >> ATsequence;

    int pos = 2;
    while (pos < ATsequence.size()) {
        string command;

        // header significant part
        if (ATsequence[pos] >= 'A' && ATsequence[pos] <= 'Z') {
            command.push_back(ATsequence[pos++]);
            if (ATsequence[pos] >= 'A' && ATsequence[pos] <= 'Z') {
                command.push_back(ATsequence[pos++]);
            }
        }
        else if (ATsequence[pos] == '&' || ATsequence[pos] == '#' || ATsequence[pos] == '@') {
            command.push_back(ATsequence[pos++]);
            command.push_back(ATsequence[pos++]);
        }

        // header non-significant part
        if (ATsequence[pos] >= '0' && ATsequence[pos] <= '9') {
            while (ATsequence[pos] >= '0' && ATsequence[pos] <= '9') {
                command.push_back(ATsequence[pos++]);
            }
        }

        // value
        if (ATsequence[pos] == '=') {
            command.push_back(ATsequence[pos++]);
             while (ATsequence[pos] >= '0' && ATsequence[pos] <= '9') {
                command.push_back(ATsequence[pos++]);
            }
        }

        cout << command << endl;
    }
}

int main (void) {
//	freopen("input.txt", "r", stdin);
	freopen("modem.in", "r", stdin);
	freopen("modem.out", "w", stdout);

    solution();

	return 0;
}
