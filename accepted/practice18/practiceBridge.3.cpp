#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <vector>

using namespace std;

bool myCmp(int a, int b) {
    return a > b;
}

int main() {
//    freopen("input.txt", "r", stdin);
    freopen("bridge.in", "r", stdin);
    freopen("bridge.out", "w", stdout);

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n, sum = 0, tmp;

    vector<int> left;

    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> tmp;
        left.push_back(tmp);
    }

    if (n == 1) {
        cout << tmp;
        return 0;
    }

    sort(left.begin(), left.end(), myCmp);

    int fastOne = left.back();
    left.pop_back();
    int fastTwo = left.back();
    left.pop_back();

    sum += fastTwo;

    while (!left.empty()) {
        if (left.size() == 1) {
            sum += (left[0] + fastOne);
            left.erase(left.begin());
            continue;
        }

        if (2 * fastOne + left[0] + left[1] > fastOne + left[0] + fastTwo * 2) {
            sum += (fastOne + left[0] + fastTwo * 2);
        }
        else {
            sum += (2 * fastOne + left[0] + left[1]);
        }

        left.erase(left.begin());
        left.erase(left.begin());
    }

    cout << sum;

    return 0;
}
