#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <vector>
#include <iomanip>

using namespace std;

const int NMAX = 15;

int optimal[NMAX];
int optimalCopy[NMAX];

int n, startExp, flag = 0;

struct Quest {
    int a;
    int b;
    int exp;
    Quest (int one = 0, int two = 0, int three = 0) : a(one), b(two), exp(three) {}
};

Quest task[NMAX];
double maxProbability = 0;

void getData() {
    cin >> n >> startExp;
    int a, b, exp;
    for (int i = 1; i <= n; ++i) {
        cin >> a >> b >> exp;
        task[i] = Quest(a, b, exp);
    }
}

void backtracking(int order, double probability, int currExp) {
    if (order > n) {
        if (probability > maxProbability) {
            flag = 1;
            maxProbability = probability;
            memcpy(optimal, optimalCopy, sizeof optimalCopy);
        }
        return;
    }

    for (int i = 1; i <= n; ++i) {
        if (!optimalCopy[i] && currExp >= task[i].a) {
            optimalCopy[i] = order;
            double value = (currExp >= task[i].b) ? ((double)1) : (((double)currExp - (double)task[i].a) / ((double)task[i].b - (double)task[i].a));
            backtracking(order + 1, probability * value, currExp + task[i].exp);
            optimalCopy[i] = 0;
        }
    }
}

int main() {
//    freopen("input.txt", "r", stdin);
    freopen("rpg.in", "r", stdin);
    freopen("rpg.out", "w", stdout);

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    getData();
    backtracking(1, (double)1, startExp);

    cout << fixed << setprecision(3) << maxProbability << endl;

    if (!flag) {
        for (int i = 1; i <= n; ++i) {
            cout << i << ' ';
        }
        return 0;
    }

    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            if (optimal[j] == i) {
                cout << j << " ";
                break;
            }
        }
    }

    return 0;
}

