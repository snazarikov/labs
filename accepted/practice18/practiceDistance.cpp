#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <iomanip>

using namespace std;

const double AVG_LEN = 111.2;
const double R = 6370;
const double PI = 3.14159265358979323846;

double angleDegPlusMin(int deg, int minute) {
    return (double)deg + (double)minute / (double)60;
}

double getRadFromDeg(double angle) {
    return angle * PI / (double)180;
}

void fixAngle(double& angle, char direction) {
    if (direction == 'S' || direction == 'W') {
        angle *= (double)-1;
    }
}

int main() {
//    freopen("input.txt", "r", stdin);
    freopen("globe.in", "r", stdin);
    freopen("globe.out", "w", stdout);

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int latOne, latMinOne, lonOne, lonMinOne;
    int latTwo, latMinTwo, lonTwo, lonMinTwo;
    char latDirOne, lonDirOne;
    char latDirTwo, lonDirTwo;

    cin >> latOne >> latMinOne >> latDirOne >> lonOne >> lonMinOne >> lonDirOne;
    cin >> latTwo >> latMinTwo >> latDirTwo >> lonTwo >> lonMinTwo >> lonDirTwo;

    double latAngleOne = angleDegPlusMin(latOne, latMinOne);
    double latAngleTwo = angleDegPlusMin(latTwo, latMinTwo);
    double lonAngleOne = angleDegPlusMin(lonOne, lonMinOne);
    double lonAngleTwo = angleDegPlusMin(lonTwo, lonMinTwo);

    fixAngle(latAngleOne, latDirOne);
    fixAngle(latAngleTwo, latDirTwo);
    fixAngle(lonAngleOne, lonDirOne);
    fixAngle(lonAngleTwo, lonDirTwo);

    double radAngleLatOne = getRadFromDeg(latAngleOne);
    double radAngleLonOne = getRadFromDeg(lonAngleOne);
    double radAngleLatTwo = getRadFromDeg(latAngleTwo);
    double radAngleLonTwo = getRadFromDeg(lonAngleTwo);

//    double result = acos(sin(radAngleLatOne) * sin(radAngleLatTwo) + cos(radAngleLatOne) * cos(radAngleLatTwo) * cos(radAngleLonTwo - radAngleLonOne));
    double result = 2 * asin(sqrt(pow(sin((radAngleLatTwo - radAngleLatOne) / 2), 2) + cos(radAngleLatOne) * cos(radAngleLatTwo) * pow(sin((radAngleLonTwo - radAngleLonOne) / 2), 2)));

    cout << fixed << setprecision(3) << result * R;

    return 0;
}
