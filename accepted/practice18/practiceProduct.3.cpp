#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <vector>

using namespace std;

int n, k;
vector<int> numerals;

void getData() {
    cin >> n >> k;
    int tmp;
    for (int i = 0; i < n; ++i) {
        cin >> tmp;
        numerals.push_back(tmp);
    }
}

void printNums() {
    cout << "All: " << endl;
    for (int i = 0; i < numerals.size(); ++i) {
        cout << numerals[i] << " ";
    }
    cout << endl;
}

bool myCmp(int a, int b) {
    return a > b;
}

bool absCmp(int a, int b) {
    if (abs(a) == abs(b)) {
        return a > b;
    }
    return abs(a) > abs(b);
}

void stupidSolution() {
    vector<int> answer;

    int leftPosAmount = 0;
    int leftNegAmount = 0;
    int leftMinIndPos = -1;
    int leftMinIndNeg = -1;

    for (int i = 0; i < k; ++i) {
        if (numerals[i] > 0) {
            leftPosAmount++;
            leftMinIndPos = i;
        }
        else if (numerals[i] < 0) {
            leftNegAmount++;
            leftMinIndNeg = i;
        }
    }

    if (!(leftNegAmount % 2) || !numerals[k - 1]) {
        for (int i = 0; i < k; ++i) {
            answer.push_back(numerals[i]);
        }
    }
    else if (leftMinIndPos != -1) {
        int rightMaxIndPos = -1;
        int rightMaxIndNeg = -1;

        for (int i = k; i < numerals.size(); ++i) {
            if (rightMaxIndNeg != -1 && rightMaxIndPos != -1) {
                break;
            }
            if (numerals[i] > 0 && rightMaxIndPos == -1) {
                rightMaxIndPos = i;
            }
            else if (numerals[i] < 0 && rightMaxIndNeg == -1) {
                rightMaxIndNeg = i;
            }
        }

        int flag = 0;
        if (rightMaxIndNeg != -1 && rightMaxIndPos != -1) {
            if ((double)numerals[leftMinIndPos] / (double)abs(numerals[rightMaxIndNeg]) < (double)abs(numerals[leftMinIndNeg]) / (double)numerals[rightMaxIndPos]) {
                numerals[leftMinIndPos] = numerals[rightMaxIndNeg];
            }
            else {
                numerals[leftMinIndNeg] = numerals[rightMaxIndPos];
            }
        }
        else if (rightMaxIndNeg != -1) {
            numerals[leftMinIndPos] = numerals[rightMaxIndNeg];
        }
        else if (rightMaxIndPos != -1) {
            numerals[leftMinIndNeg] = numerals[rightMaxIndPos];
        }
        else {
            flag = 1;
        }

        if (!flag) {
            for (int i = 0; i < k; ++i) {
                answer.push_back(numerals[i]);
            }
        }
        else {
            for (int i = numerals.size() - 1; i >= 0 && k; --i) {
                answer.push_back(numerals[i]);
                k--;
            }
        }
    }
    else {
        int rightMaxIndPos = -1;

        for (int i = k; i < numerals.size(); ++i) {
            if (numerals[i] > 0) {
                rightMaxIndPos = i;
                break;
            }
        }

        if (rightMaxIndPos != -1) {
            numerals[leftMinIndNeg] = numerals[rightMaxIndPos];
            for (int i = 0; i < k; ++i) {
                answer.push_back(numerals[i]);
            }
        }
        else {
            for (int i = numerals.size() - 1; i >= 0 && k; --i) {
                answer.push_back(numerals[i]);
                k--;
            }
        }
    }

    sort(answer.begin(), answer.end(), myCmp);

    for (int i = 0; i < answer.size(); ++i) {
        cout << answer[i] << " ";
    }
}

int main() {
//    freopen("input.txt", "r", stdin);
    freopen("product.in", "r", stdin);
    freopen("product.out", "w", stdout);

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    getData();

    sort(numerals.begin(), numerals.end(), absCmp);

//    printNums();
    stupidSolution();

    return 0;
}
