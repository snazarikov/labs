#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

const int NMAX = 30;

int row, column, k, startUnderAttack;

struct PositionModel {
    int ii;
    int jj;
    PositionModel(int one = 0, int two = 0) : ii(one), jj(two) {}
};

vector<PositionModel> queenPosition;
int gameField[NMAX][NMAX];

void getData() {
    cin >> row >> column >> k;
    char a;
    int b;
    for (int i = 0; i < k; ++i) {
        cin >> a >> b;
        queenPosition.push_back(PositionModel(a - 'a', b - 1));
        gameField[a - 'a'][b - 1] = 1;
    }
}

int stupidCheck(int i, int j, int value) {
    if (gameField[i][j] != 2 && gameField[i][j] != value) {
        gameField[i][j] = value;
        return 1;
    }
    return 0;
}

int markUnderAttack(int ii, int jj, int value) {
    int counter = 0;
    for (int i = ii + 1; i < row; ++i) {
        if (gameField[i][jj] == 1) {
            break;
        }
        counter += stupidCheck(i, jj, value);
    }
    for (int i = ii - 1; i >= 0; --i) {
        if (gameField[i][jj] == 1) {
            break;
        }
        counter += stupidCheck(i, jj, value);
    }

    for (int j = jj + 1; j < column; ++j) {
        if (gameField[ii][j] == 1) {
            break;
        }
        counter += stupidCheck(ii, j, value);
    }
    for (int j = jj - 1; j >= 0; --j) {
        if (gameField[ii][j] == 1) {
            break;
        }
        counter += stupidCheck(ii, j, value);
    }

    for (int i = ii + 1, j = jj + 1; i < row && j < column; ++i, ++j) {
        if (gameField[i][j] == 1) {
            break;
        }
        counter += stupidCheck(i, j, value);
    }
    for (int i = ii + 1, j = jj - 1; i < row && j >= 0; ++i, --j) {
        if (gameField[i][j] == 1) {
            break;
        }
        counter += stupidCheck(i, j, value);
    }
    for (int i = ii - 1, j = jj + 1; i >= 0 && j < column; --i, ++j) {
        if (gameField[i][j] == 1) {
            break;
        }
        counter += stupidCheck(i, j, value);
    }
    for (int i = ii - 1, j = jj - 1; i >= 0 && j >= 0; --i, --j) {
        if (gameField[i][j] == 1) {
            break;
        }
        counter += stupidCheck(i, j, value);
    }
    return counter;
}

int startCalc(vector<PositionModel>& queens) {
    int underAttackAmount = 0;
    for (int i = 0; i < queens.size(); ++i) {
        underAttackAmount += markUnderAttack(queens[i].ii, queens[i].jj, 2);
    }

    return underAttackAmount;
}

bool myCmp(pair<int, string> a, pair<int, string> b) {
    if (a.first == b.first) {
        return a.second < b.second;
    }
    return a.first > b.first;
}

void solution() {
    vector<pair<int, string> > data;
    int value = 3;
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < column; ++j) {
            if (gameField[i][j] != 1) {
                int flag = 0;
                if (gameField[i][j] != 2) {
                    gameField[i][j] = value;
                    flag = 1;
                }
                int current = markUnderAttack(i, j, value++);

                pair<int, string> elem;
                elem.first = row * column - startUnderAttack - current - flag - k;
                elem.second.resize(2);
                elem.second[0] = i + 'a';
                if (j > 8) {
                    elem.second.resize(3);
                    elem.second[2] = ((j + 1) % 10) + '0';
                    elem.second[1] = ((j + 1) / 10) + '0';
                }
                else {
                    elem.second[1] = (j + 1) + '0';
                }

                data.push_back(elem);
            }
        }
    }

    sort(data.begin(), data.end(), myCmp);

    cout << data[0].second << endl;
    cout << data[0].first;
}

int main() {
//    freopen("input.txt", "r", stdin);
    freopen("queen.in", "r", stdin);
    freopen("queen.out", "w", stdout);

    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    getData();

    startUnderAttack = startCalc(queenPosition);

    solution();

    return 0;
}
