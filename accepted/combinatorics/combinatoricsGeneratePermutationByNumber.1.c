#include <stdio.h>
#include <stdlib.h>

#define NMAX 25

int isUsed[NMAX];
long long fact[NMAX];

long long n, number;

void getData() {
	scanf("%I64d %I64d", &n, &number);
}

void printData() {
	printf("%I64d %I64d\n", n, number);
}

void makeFact(int num) {
	int i;
	
	fact[0] = 1;
	fact[1] = 1;
	for (i = 2; i <= num; i++) {
		fact[i] = fact[i - 1] * i;
	}
}

void getResult() {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			if (isUsed[j] == i + 1) {
				printf("%d ", j + 1);
				break;
			}
		}
	}
}

void solve(long long n) {
	long long help, counter = 0;
	int i, j, save;
	
	makeFact(n - 1);
	
	number--;
	
	for (i = 0; i < n; i++) {
		help = number / fact[n - 1 - i];
		
		if (help == 0) {
			for (j = 0; j < n; j++) {
				if (isUsed[j] == 0) {
					isUsed[j] = i + 1;
					break;
				}
			}
		}
		
		else {
			counter = help;
	
			for (j = 0; j < n; j++) {
				if (isUsed[j] == 0 && counter != -1) {
					counter--;
					save = j;
				}
			}
		
			isUsed[save] = i + 1;
		}
		
		
		number %= fact[n - 1 - i];
	}
	
	getResult();
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    getData();
	solve(n);

    return 0;
}
