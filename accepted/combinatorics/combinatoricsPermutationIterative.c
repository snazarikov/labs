#include <stdio.h>
#include <stdlib.h>

#define NMAX 15

int A[NMAX];

int n;

void getData() {
	int i;
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		A[i] = i + 1;
	}
}

void printData() {
	int i;
	printf("%d\n", n);
	for (i = 0; i < n; i++) {
		printf("%d ", i + 1);
	}
	printf("\n");
}

void reverse(int* A, int a, int b) {
	int i, tmp;
	for (i = a; i <= (a + b) / 2; i++) {
		tmp = A[i];
		A[i] = A[b - i + a];
		A[b - i + a] = tmp;
	}
}

int searchPermutations() {
	int i, j, minInd, minVal, tmp;
	
	for (i = n - 2; i >= 0; i--) {
		if (A[i] < A[i + 1]) {
			minInd = i + 1;
			minVal = A[i + 1];
			for (j = i + 2; j < n; j++) {
				if (A[j] > A[i] && A[j] < minVal) {
					minVal = A[j];
					minInd = j;	
				}
			}
			
			tmp = A[i];
			A[i] = A[minInd];
			A[minInd] = tmp;
			
			reverse(A, i + 1, n - 1);
			
			return 1;
		}
	}	
	return 0;
}

void solve() {
	int i;
	do {
		for (i = 0; i < n; i++) {
			printf("%d ", A[i]);
		}
		printf("\n");
	}
	while (searchPermutations());
}

int main() {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);
    
    getData();
	solve();

    return 0;
}
