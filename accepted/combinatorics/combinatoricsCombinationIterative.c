#include <stdio.h>
#include <stdlib.h>

#define NMAX 21

int A[NMAX];
int comb[200000][NMAX];

int n, m;

void getData() {
	int i;
	scanf("%d %d", &n, &m);
	for (i = n - 1; i > n - m - 1; i--) {
//	for (i = 0; i < m; i++) {
		A[i] = 1;
	}
}

void printData() {
	int i;
	printf("%d %d\n", n, m);
	for (i = 0; i < n; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

int searchPermutations() {
	int i, j, pos, tmp, oneCount = 0, zeroCount = 0;
	
	if (A[n - 1]) {
		oneCount++;
	}
	
	for (i = n - 2; i >= 0; i--) {
		if (A[i]) {
			oneCount++;
		}
		
		if (A[i] < A[i + 1]) {
			pos = i + 1;
			
			tmp = A[i];
			A[i] = A[i + 1];
			A[i + 1] = tmp;
			
			zeroCount = n - pos + 1 - oneCount;
			
//			printf("%d %d %d\n", oneCount, zeroCount, pos);
			
			for (i = pos; i < n; i++) {
				if (i < pos + zeroCount) {
					A[i] = 0;
				}
				else {
					A[i] = 1;
				}
			}
			
			return 1;
		}
		
	}	
	return 0;
}

int searchPermutations1() {
	int i, j, pos, tmp, oneCount = 0, zeroCount = 0;
	
//	if (!A[0]) {
//		zeroCount++;
//	}
	
	for (i = 0; i < n - 1; i++) {
		if (!A[i]) {
			zeroCount++;
		}
		
		if (A[i] > A[i + 1]) {
			pos = i + 1;
			
			tmp = A[i];
			A[i] = A[i + 1];
			A[i + 1] = tmp;
			
			oneCount = pos - 1 - zeroCount;
			
//			printf("%d %d %d\n", oneCount, zeroCount, pos);
			
			for (i = 0; i < pos; i++) {
				if (i < oneCount) {
					A[i] = 1;
				}
				else {
					A[i] = 0;
				}
			}
			
			return 1;
		}
		
	}	
	return 0;
}

void solve() {
	int i, j, counter = 0;
	
	do {
		for (i = 0; i < n; i++) {
			if (A[i]) {
//				printf("%d ", i + 1);
				comb[counter][i] = i + 1;
			}
		}
//		for (i = n - 1; i >= 0; i--) {
//			if (A[i]) {
//				printf("%d ", n - i);
//			}
//		}
//		printf("\n");
		counter++;
	}
	while (searchPermutations());
	
	for (i = counter - 1; i >= 0; i--) {
		for (j = 0; j < n; j++) {
			if (comb[i][j]) {
				printf("%d ", j + 1);
			}
		}
		printf("\n");
	}
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    getData();
	solve();

    return 0;
}
