#include <stdio.h>
#include <stdlib.h>

#define NMAX 25

int A[NMAX];
int isUsed[NMAX];
unsigned long long fact[NMAX];

int n;

void getData() {
	int i;
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		scanf("%d", &A[i]);
	}
}

void printData() {
	int i;
	printf("%d\n", n);
	for (i = 0; i < n; i++) {
		printf("%d ", A[i]);
	};
	printf("\n");
}

void makeFact(int num) {
	int i;
	
	fact[0] = 1;
	fact[1] = 1;
	for (i = 2; i <= num; i++) {
		fact[i] = fact[i - 1] * i;
	}
}

void solve(int n) {
	int i, j, help;
	unsigned long long res = 1;
	
	makeFact(n);
	
	for (i = 0; i < n - 1; i++) {
		isUsed[A[i] - 1] = 1;
		help = 0;
		for (j = 0; j < A[i]; j++) {
			if (isUsed[j] == 0) {
				help++;
			}
		}
		res += fact[n - 1 - i] * help;
	}
	
	printf("%lld", res);
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    getData();
	solve(n);

    return 0;
}
