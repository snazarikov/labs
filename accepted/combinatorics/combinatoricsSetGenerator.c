#include <stdio.h>
#include <stdlib.h>

#define NMAX 25

int A[NMAX];

int n, m;

void getData() {
	int i;
	scanf("%d", &n);
}

void solve() {
	int i;

	while (A[n] != 1) {
		for (i = 0; i < n; i++) {
			if (A[i]) {
				printf("%d ", i + 1);
			}
		}
		printf("\n");

		i = 0;
		while(A[i]) {
			A[i] = 0;
			i++;
		}

		A[i] = 1;
	}
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    getData();
	solve();

    return 0;
}
