#include <stdio.h>
#include <stdlib.h>

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

#define NMAX 10005

int topA = 0, tailA = -1, topB = 0, tailB = -1, topC = 0, tailC = -1, num, prime1, prime2, prime3;
int queueA[NMAX], queueB[NMAX], queueC[NMAX];

void getData() {
	scanf("%d %d %d %d", &num, &prime1, &prime2, &prime3);
}

void printData() {
	printf("%d %d %d %d\n", num, prime1, prime2, prime3);
}

int isEmpty(int top, int tail) {
	if (tail < top) {
		return 1;
	}
	return 0;
}

int getTop(int* A, int top) {
	return A[top];
}

void enqueue(int key, int element) {
	if (key == 1) {
		queueA[++tailA] = element;
	}
	else if (key == 2) {
		queueB[++tailB] = element;
	}
	else if (key == 3) {
		queueC[++tailC] = element;
	}
}

int dequeue(int key) {
	if (key == 1) {
		return queueA[++topA - 1];
	}
	else if (key == 2) {
		return queueB[++topB - 1];
	}
	else if (key == 3) {
		return queueC[++topC - 1];
	} 
}

int solve() {
	int k, m = 1, i;
	
	enqueue(1, prime1);
	enqueue(2, prime2);
	enqueue(3, prime3);
	
	k = 1;
		
	while (k < num) {
		m = min(getTop(queueA, topA), min(getTop(queueB, topB), getTop(queueC, topC)));
		
//		printf("k: %d m: %d\n", k, m);
		
		if (getTop(queueA, topA) == m) {
			dequeue(1);
		}
		if (getTop(queueB, topB) == m) {
			dequeue(2);
		}
		if (getTop(queueC, topC) == m) {
			dequeue(3);
		}
		
		enqueue(1, prime1 * m);
		enqueue(2, prime2 * m);
		enqueue(3, prime3 * m);
		
//		for (i = topA; i <= tailA; i++) {
//			printf("%d ", queueA[i]);
//	
//		printf("\n");
//		for (i = topB; i <= tailB; i++) {
//			printf("%d ", queueB[i]);
//		}
//		printf("\n");
//		for (i = topC; i <= tailC; i++) {
//			printf("%d ", queueC[i]);
//		}
//		printf("\n\n");
		
		k++;
	}
	
//	printf("\n\n%d %d %d\n\n", getTop(queueA, topA), getTop(queueB, topB), getTop(queueC, topC));
	
	return m;
}

int main(void) {
//	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);
	
	getData();
//	printData();
	printf("%d", solve());
	
	return 0;
}
