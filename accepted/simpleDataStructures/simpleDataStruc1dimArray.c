#include <stdio.h>
#include <stdlib.h>

#define NMAX 1000005

int B[NMAX], C[NMAX];
long long A[NMAX], S[NMAX];
int num, count;

void getData() {
	int i;
	
	scanf("%d %d", &num, &count);
	
	for (i = 0; i < num; i++) {
		scanf("%I64d", &A[i]);
	}
	
	for (i = 0; i < count; i++) {
		scanf("%d %d", &B[i], &C[i]);
	}
}

void printData() {
	int i;
	
	printf("%d %d\n", num, count);
	
	for (i = 0; i < num; i++) {
		printf("%I64d ", A[i]);
	}
	
	printf("\n");
	
	for (i = 0; i < count; i++) {
		printf("%d %d\n", B[i], C[i]);
	}
}
void solve() {
	int i;
	
	S[0] = A[0];
//	printf("%d ", S[0]);
	for (i = 1; i < num; i++) {
		S[i] +=  S[i - 1] + A[i];
//		printf("%d ", S[i]);
	}
	
//	printf("\n");
	
	for (i = 0; i < count; i++) {
		printf("%I64d ", S[C[i] - 1] - S[B[i] - 1] + A[B[i] - 1]);
	}
}

int main(void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	getData();
//	printData();
	
	solve();
	
	return 0;
}
