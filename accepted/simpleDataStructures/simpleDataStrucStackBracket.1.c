#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NMAX 1000005

char stack[NMAX];
char symbol;
int top;

void make() {
	top = -1;
}

int isEmpty() {
	if (top == -1) {
		return 1;
	}
	return 0;
}

char getTop() {
	return stack[top];
}

void push(char element) {
	stack[++top] = element;
}

void pop() {
	--top;
//	return stack[--top + 1];
}

int solve() {	
	make();

	while (scanf("%c", &symbol) != EOF) {
		if (symbol == '(' || symbol == '{' || symbol == '[') {
			push(symbol);
			continue;
		}
		
//		if (symbol == ')' || symbol == '}' || symbol == ']') {
		else {
			if (isEmpty() == 1) {
				return 0;
			}
			
			if ( (symbol == ')' && getTop() == '(') ||
			 	 (symbol == ']' && getTop() == '[') ||
			 	 (symbol == '}' && getTop() == '{') ) {
				pop();
			}
			else {
				return 0;
			}
		}		
	}
	
	if (isEmpty() == 0) {
		return 0;
	}
	
	return 1;
}

int main(void) {
	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);
	
	if (solve() == 0) {
		printf("no");
	}
	else {
		printf("yes");
	}
	
	return 0;
}
