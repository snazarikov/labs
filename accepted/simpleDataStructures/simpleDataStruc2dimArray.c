#include <stdio.h>
#include <stdlib.h>

#define NMAX 1000005
#define NMAX2 1005

int A[NMAX], B[NMAX], C[NMAX], D[NMAX];
long long N[NMAX2][NMAX2], S[NMAX2][NMAX2];
int n, m, count;

void getData() {
	int i, j;

	scanf("%d %d %d", &n, &m, &count);

	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			scanf("%I64d", &N[i][j]);
		}
		scanf("\n");
	}

	for (i = 0; i < count; i++) {
		scanf("%d %d %d %d", &A[i], &B[i], &C[i], &D[i]);
	}
}

void printData() {
	int i, j;

	printf("%d %d %d\n", n, m, count);

	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			printf("%I64d ", N[i][j]);
		}
		printf("\n");
	}

	for (i = 0; i < count; i++) {
		printf("%d %d %d %d\n", A[i], B[i], C[i], D[i]);
	}
	printf("\n");
}

void solve() {
	int i, j;
	long long res;

	S[0][0] = N[0][0];

	for (i = 1; i < n; i++) {
		S[i][0] += S[i - 1][0] + N[i][0];
	}

	for (i = 1; i < m; i++) {
		S[0][i] += S[0][i - 1] + N[0][i];
	}

	for (i = 1; i < n; i++) {
		for (j = 1; j < m; j++) {
			S[i][j] = S[i - 1][j] + S[i][j - 1] - S[i - 1][j - 1] + N[i][j];
		}
	}

//	for (i = 0; i < n; i++) {
//		for (j = 0; j < m; j++) {
//			printf("%I64d ", S[i][j]);
//		}
//		printf("\n");
//	}

	for (i = 0; i < count; i++) {
		res = S[B[i] - 1][D[i] - 1];

		if (A[i] - 2 >= 0) {
			res -= S[A[i] - 2][D[i] - 1];
		}
		if (C[i] - 2 >= 0) {
			res -= S[B[i] - 1][C[i] - 2];
		}
		if (A[i] - 2 >= 0 && C[i] - 2 >= 0) {
			res += S[A[i] - 2][C[i] - 2];
		}

		printf("%I64d ", res);
	}
}

int main(void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
//	printData();

	solve();

	return 0;
}
