#include <cstdio>
#include <iostream>

#define NMAX 45

using namespace std;

int power, size;

class Matrix {
public:
    
    Matrix() {
        for(int i = 0; i < size; i++) {
        	for(int j = 0; j < size; j++) {
        		matrix[i][j] = 0;
			}                
		}            
    }
    
    void print() {
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
            	printf("%.6lf ", matrix[i][j]);
			}
            printf("\n");
        }
    }
    
    double matrix[NMAX][NMAX];
};

Matrix A, E;

void getData() {
	
	scanf("%d %d", &power, &size);
	
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			scanf("%lf", &A.matrix[i][j]);
			if (i == j) {
				E.matrix[i][j] = 1;
			}
		}
		scanf("\n");
	}
}

void printData() {
	printf("%d %d\n", power, size);
	A.print();
}

Matrix multMatrix (Matrix A, Matrix B) {
	Matrix C;
	
	for(int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
        	for(int k = 0; k < size; k++) {
        		C.matrix[i][j] += A.matrix[i][k] * B.matrix[k][j];
			}
    	}
	}
	
	return C;
}

Matrix makePow (Matrix A, int power) {
	if (power == 0) {
		return E;
	}
	
	if (power % 2 == 0) {
		return makePow(multMatrix(A, A), power / 2);
	}
	else {
		return multMatrix(A, makePow(A, power - 1));
	}
}


int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    getData();
//	printData();

	Matrix solve = makePow(A, power);
	solve.print();

    return 0;
}
