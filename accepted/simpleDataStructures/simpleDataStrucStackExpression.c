#include <stdio.h>
#include <stdlib.h>

#define NMAX 1005

long long stack[NMAX];
char symbol;
long long value = 1, num = 0;
int top, i;

void make() {
	top = -1;
}

int isEmpty() {
	if (top == -1) {
		return 1;
	}
	return 0;
}

long long getTop() {
	return stack[top];
}

void push(long long element) {
	stack[++top] = element;
}

void pop() {
	--top;
//	return stack[--top + 1];
}

long long solve() {	
	make();

	while (scanf("%I64d", &num)) {
//		printf("%I64d", num);
		if (scanf("%c", &symbol) != EOF) {
//			printf("%c", symbol);
			value *= num;
			if (symbol == '+' || symbol == '-') {
				push(value);
				if (symbol == '-') {
					value = -1;
				}
				else {
					value = 1;
				}
			}
		}
		else {
			break;
		}
	}
	
	if (symbol != 10) {
		push(value * num);
	}
	else {
		push(value);
	}
	
	
	value = 0;
	for (i = 0; i <= top; i++) {
		value += stack[i];
	}
	
	return value;
}

int main(void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	printf("%I64d", solve());
	
	return 0;
}
