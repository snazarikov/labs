#include <stdio.h>
#include <stdlib.h>


long long A, B, C, X = 1, Y = 1;

void getData() {
	scanf("%I64d %I64d %I64d", &A, &B, &C);
}

void printData() {
	printf("%I64d %I64d %I64d", A, B, C);
}

long long GCDExtended(long long a, long long b, long long *x, long long *y) {
	long long help;
	if (b == 0) {
		*x = 1;
		*y = 0;
		return a;
	}
	else {
		long long GCD = GCDExtended(b, a % b, x, y);
		help = *y;
		*y = *x - (a / b) * (*y);
		*x = help;
		return GCD;
	}
}

void solve() {
	long long GCD = GCDExtended(A, B, &X, &Y);
	if (C % GCD != 0) {
		printf("-1");
	}
	else {
		printf("%I64d %I64d", (C / GCD) * X, (C / GCD) * Y);
//		printf("\n%I64d %I64d %I64d", X, Y, GCD);
//		cin >> c * x / GCD >> c * y / GCD;
	}
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    getData();
//	printData();

	solve();

    return 0;
}
