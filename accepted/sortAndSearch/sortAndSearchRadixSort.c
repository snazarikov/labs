#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define NMAX ((int)1e6 + 5)

int L = 7;
int B = 10;

int deques[11][NMAX];
int amount[11];

int A[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void radixSort(int* A) {
    int p, i, j, k;
    int help1 = 1, help2 = 0;
    for (p = 1; p <= L; ++p) {
        help2 = help1;
        help1 *= 10;

        for (i = 0; i < count; ++i) {
            int digit = (A[i] % help1) / help2;
            deques[digit][amount[digit]] = A[i];
            amount[digit]++;
        }

        i = 0;
        for (j = 0; j < B; ++j) {
            for (k = 0; k < amount[j]; ++k) {
                A[i] = deques[j][k];
                deques[j][k] = 0;
                i++;
            }
            amount[j] = 0;
        }
    }
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int i;

	getData();
	radixSort(A);
	printData();

	return 0;
}
