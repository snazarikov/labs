#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double a, b, c, d;

void getData() {
    scanf("%lf %lf %lf %lf", &a, &b, &c, &d);
}

void printData() {
    printf("%lf %lf %lf %lf\n", a, b, c, d);
}

double functionValue(double x) {
    return a * log(b + x) - (c / (d + x));
}

double bisection(double a, double b) {
    while (1) {
        double m = (a + b) / 2;

        if (m - a == 0 || m - b == 0) {
            break;
        }

        if (functionValue(a) * functionValue(m) < 0) {
            b = m;
        }
        else {
            a = m;
        }
    }

    return a;
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

    getData();
    printf("%.6lf\n", bisection(0, 1e19));
//    printData();

	return 0;
}
