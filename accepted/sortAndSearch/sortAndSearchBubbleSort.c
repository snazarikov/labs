#include <stdio.h>
#include <math.h>

#define NMAX 100005

int A[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void bubbleSort(int* A, const int count) {
	int i, j, tmp;

	for (i = 1; i < count; i++) {
		j = i - 1;

		while(j >= 0 && A[j] > A[j + 1]) {
			tmp =  A[j];
			A[j] = A[j + 1];
			A[j + 1] = tmp;

			j--;
		}
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	bubbleSort(A, count);
	printData();

	return 0;
}
