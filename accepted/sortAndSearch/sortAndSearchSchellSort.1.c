#include <stdio.h>
#include <math.h>

#define NMAX 1000005

int A[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void schellSort(int* A, const int count) {
	int i, j, k, tmp, h = 1, magicConst = 11;
	while(h < count / magicConst) {
		h = 3 * h + 1;
	}
//	printf("H %d\n", h);
	while(h > 0) {
		for (k = 1; k <= h; k++) {
			i = k + h - 1;

			while(i < count) {
				if (A[i] >= A[i - h]) {
					i += h;
					continue;
				}

				tmp = A[i];
				j = i - h;

				while (j >= 0 && A[j] > tmp) {
					A[j + h] = A[j];
					j -= h;
				}

				A[j + h] = tmp;
				i += h;
			}
		}
		h /= 3;
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	schellSort(A, count);
	printData();

	return 0;
}
