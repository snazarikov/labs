#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define NMAX 1000005

int A[NMAX];
int buf[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void mergeSort(int* A, int a, int b) {
	if (a >= b) {
		return;
	}
	int m = (a + b) / 2;

	mergeSort(A, a, m);
	mergeSort(A, m + 1, b);

	int i = a;
	int j = m + 1;

	int k;
	for (k = a; k <= b; k++) {
		if (j > b || i <= m && A[i] < A[j]) {
			buf[k] = A[i];
			i++;
		}
		else {
			buf[k] = A[j];
			j++;
		}
	}

	for (k = a; k <= b; k++) {
		A[k] = buf[k];
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	mergeSort(A, 0, count - 1);
	printData();

	return 0;
}
