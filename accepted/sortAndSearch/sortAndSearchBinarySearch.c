#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define NMAX ((int)1e6 + 5)

int A[NMAX];
char invariant;
int needToFind, count = 0;

void getData() {
    scanf("%c %d\n", &invariant, &needToFind);
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	printf("%c %d\n", invariant, needToFind);
	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

int binarySearchEqual(int* A, int a, int b) {
    if (needToFind < A[0]) {
        return 0;
    }
    if (needToFind > A[count - 1]) {
        return 0;
    }
    if (needToFind == A[0]) {
        return 1;
    }

    while (a + 1 < b) {
        int m = (a + b) / 2;

        if (A[m] < needToFind) {
            a = m;
        }
        else {
            b = m;
        }
    }

    if (A[b] == needToFind) {
        return b + 1;
    }
    else {
        return 0;
    }
}

int binarySearchLess(int* A, int a, int b) {
    if (needToFind < A[0]) {
        return 0;
    }
    if (needToFind >= A[count - 1]) {
        return count;
    }
    if (needToFind == A[0]) {
        return 1;
    }

    while (a + 1 < b) {
        int m = (a + b) / 2;

        if (A[m] <= needToFind) {
            a = m;
        }
        else {
            b = m;
        }
    }

    return a + 1;
}

int binarySearchGreater(int* A, int a, int b) {
    if (needToFind <= A[0]) {
        return 1;
    }
    if (needToFind > A[count - 1]) {
        return 0;
    }

    while (a + 1 < b) {
        int m = (a + b) / 2;

        if (A[m] < needToFind) {
            a = m;
        }
        else {
            b = m;
        }
    }

    return b + 1;
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

    getData();
    if (invariant == '=') {
        printf("%d", binarySearchEqual(A, 0, count - 1));
    }
    else if (invariant == '>') {
        printf("%d", binarySearchGreater(A, 0, count - 1));
    }
    else {
        printf("%d", binarySearchLess(A, 0, count - 1));
    }

//    printData();

	return 0;
}
