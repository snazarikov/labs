#include <stdio.h>
#include <math.h>

#define NMAX 100005

int A[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void swapSort(int* A, const int count) {
	int i, j, tmp;

	for (i = 0; i < count - 1; i++) {
		for (j = i + 1; j < count; j++) {
			if (A[i] > A[j]) {
				tmp = A[i];
				A[i] = A[j];
				A[j] = tmp;
			}
		}
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	swapSort(A, count);
	printData();

	return 0;
}
