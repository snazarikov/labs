#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define NMAX ((int)1e6 + 5)

int A[NMAX];
int B[NMAX];
int C[NMAX];

int count = 0, maxKey = -1e7, minKey = 1e7;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
        if (A[count] > maxKey) {
            maxKey = A[count];
        }
        if (A[count] < minKey) {
            minKey = A[count];
        }
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
//	printf("%d %d\n", minKey, maxKey);
}

void countingSort(int* A) {
    int i, j;

    for (j = 0; j < count; ++j) {
        C[A[j]]++;
    }

    for (i = minKey + 1; i <= maxKey; ++i) {
        C[i] += C[i - 1];
    }

    for (j = 0; j < count; ++j) {
        C[A[j]]--;
        B[C[A[j]]] = A[j];
    }

    for (j = 0; j < count; ++j) {
        A[j] = B[j];
    }
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	countingSort(A);
	printData();

	return 0;
}
