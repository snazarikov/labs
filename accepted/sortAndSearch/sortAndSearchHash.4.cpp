#include <cstdio>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <cstring>

#define NMAX 10000
#define P 251 //251
#define MAX_LENGTH 11

#define NOT_EMPTY -1
#define EMPTY -2
#define DELETED -3
#define MAX_SIZE 8191

using namespace std;

long long pows[NMAX];
long long step[] = {1, 11, 37, 83, 281, 313, 587, 809, 929, 1103};

int flag = 0, s = 9;

pair<string, long long> myHash[NMAX];

void makePows(long long* pows) {
    pows[0] = 1;
    for (int i = 1; i < MAX_LENGTH; ++i) {
        pows[i] = pows[i - 1] * P;
    }
    for (int i = 1; i < MAX_LENGTH; ++i) {
        pows[i] = abs(pows[i]);
    }
}

long long thirdHashFunction(long long help, long long index, int s) {
    if (index + (step[s] * help) >= MAX_SIZE) {
        flag = 1;
    }
    return (index + (step[s] * help)) % MAX_SIZE;
}

//long long secondHashFunction(string str, long long help, long long index) {
//    long long hash = 0;
//    for (int i = 0; i < str.size(); ++i) {
//        hash += (unsigned char)str[i] * pows[i];
//    }
//    return (hash + (step[index % 10] * help)) % MAX_SIZE;
//}

long long firstHashFunction(string str) {
    long long hash = 1, a = 127;
    for (int i = 0; i < str.size(); ++i) {
        hash = (a * hash + (unsigned char)str[i]) % MAX_SIZE;
    }
    return hash;
}

long long search(string element) {
    long long i = firstHashFunction(element);
    long long help = 1;

    s = 9;

    while(1) {
        if (myHash[i].second == EMPTY) {
            return -1;
        }
        else if (myHash[i].first == element && myHash[i].second == NOT_EMPTY) {
            return i;
        }
        i = thirdHashFunction(help, i, s);
        if (flag) {
            s--;
            flag = 0;
            help = 0;
        }
        if (s < 0) {
            s = 0;
        }
        help++;
    }
}

void put(string element) {
    if (search(element) >= 0) {
        return;
    }

    long long i = firstHashFunction(element);
    long long help = 1;

    s = 9;

    while(1) {
        if (myHash[i].second == EMPTY || myHash[i].second == DELETED) {
            myHash[i].first = element;
            myHash[i].second = NOT_EMPTY;
            return;
        }
        i = thirdHashFunction(help, i, s);
        if (flag) {
            s--;
            flag = 0;
            help = 0;
        }
        if (s < 0) {
            s = 0;
        }
        help++;
    }
}

void remove(string element) {
    long long i = search(element);
    if (i != -1) {
        myHash[i].second = DELETED;
    }
}

void solve() {
    char action;

    makePows(pows);
    for (int i = 0; i < NMAX; ++i) {
        myHash[i].second = EMPTY;
    }

    cin >> action;
    while (action != 'E') {
        string str;
        cin >> str;
        if (action == '+') {
            put(str);
        }
        else if (action == '-') {
            remove(str);
        }
        else if (action == '?') {
            int status = search(str);
            if (status > 0) {
                cout << "+" << myHash[status].first << "\n";
            }
            else if (status == -1) {
                cout << "-" << str << "\n";
            }
        }
        cin >> action;
    }
}

int main() {
    freopen ("input.txt", "r", stdin);
    freopen ("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    solve();

    return 0;
}
