#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#define NMAX 1000005

int A[NMAX];
int buf[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void quickSort(int* A, int a, int b) {
	if (a >= b) {
		return;
	}

	int m = a + rand() % (b - a + 1);

    int l = a - 1;
    int r = b + 1;
    int key = A[m];

    while (1) {
        while (A[++l] < key);
        while (A[--r] > key);

        if (l >= r) {
            break;
        }

        int tmp = A[l];
        A[l] = A[r];
        A[r] = tmp;
    }

    quickSort(A, a, r);
    quickSort(A, r + 1, b);
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	srand(time(NULL));
	quickSort(A, 0, count - 1);
	printData();

	return 0;
}
