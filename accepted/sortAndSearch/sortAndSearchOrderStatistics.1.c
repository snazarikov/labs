#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#define NMAX 1000005

int A[NMAX];
int buf[NMAX];
int count = 0, k;

void getData() {
    scanf("%d\n", &k);
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

//	printf("%d\n", k);
	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

int orderStatistics(int* A, int a, int b, int k) {
    if (a == b) {
//        printf("got %d\n", a);
//        printData();
        return A[a];
    }

	int m = a + rand() % (b - a + 1);
    int l = a - 1;
    int r = b + 1;
    int key = A[m];

//    printf("%d %d\n", m, key);
//    printf("%d %d %d %d\n", a, l, r, b);
//    printData();

    while (1) {
        while (A[++l] < key);
        while (A[--r] > key);

        if (l >= r) {
//            printf("break %d %d\n", l, r);
            break;
        }

        int tmp = A[l];
        A[l] = A[r];
        A[r] = tmp;
    }

//    printData();

    if (k <= r) {
        return orderStatistics(A, a, r, k);
    }
    else {
        return orderStatistics(A, r + 1, b, k);
    }
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	srand(time(NULL));
	printf("%d", orderStatistics(A, 0, count - 1, k - 1));
//	printData();

	return 0;
}
