#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#define NMAX 1000005

#define maximum(a, b) (((a) > (b)) ? (a) : (b))

int A[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

int ternarySearch(int* A, int a, int b) {
    int max = maximum(A[a], A[b]), i;
//    printf("%d %d %d\n", max, A[a], A[b]);
    while (a + 1 < b) {
//        printf("LEL %d %d\n", a, b);
        int h = (b - a) / 3;
        int a1 = a + h;
        int b1 = b - h;

        if (a == a1 || b == b1) {
            for (i = a; i <= b; ++i) {
                if (A[i] > max) {
                    max = A[i];
                }
            }
            return max;
        }

        if (A[a1] < A[b1]) {
            a = a1;
        }
        else {
            b = b1;
        }
    }

//    printf("%d %d\n", a, b);

    return max;
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	printf("%d", ternarySearch(A, 0, count - 1));
//	printData();

	return 0;
}
