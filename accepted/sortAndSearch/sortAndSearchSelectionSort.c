#include <stdio.h>
#include <math.h>

#define NMAX 100005

int A[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void selectionSort(int* A, const int count) {
	int i, j, tmp, min;

	for (i = 0; i < count - 1; i++) {
		min = i;

		for (j = i + 1; j < count; j++) {
			if (A[j] < A[min]) {
				min = j;
			}
		}

		if (min != i) {
			tmp = A[i];
			A[i] = A[min];
			A[min] = tmp;
		}
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	selectionSort(A, count);
	printData();

	return 0;
}
