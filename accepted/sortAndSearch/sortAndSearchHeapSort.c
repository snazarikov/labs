#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define NMAX 1000005

int* makeHeap(const int size) {
	int* heap = malloc(sizeof(int) * (size + 1));
	memset(heap, 0, sizeof(int) * size);
	return heap;
}

int isEmpty(const int heapLength) {
	if (heapLength < 0) {
		return 1;
	}
	return 0;
}

void swap(int* a, int* b) {
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void lift(int* heap, int childIndex) {
	int parentIndex;

	while(childIndex > 1) {
		parentIndex = childIndex / 2;

		if (heap[parentIndex] >= heap[childIndex]) {
			return;
		}

		swap(&heap[childIndex], &heap[parentIndex]);
		childIndex = parentIndex;
	}
}

void sift(int* heap, int parentIndex) {
	int childIndex;

	while (2 * parentIndex <= heap[0]) {
		childIndex = 2 * parentIndex;

		if (childIndex < heap[0] && heap[childIndex + 1] > heap[childIndex]) {
			childIndex++;
		}

		if (heap[parentIndex] >= heap[childIndex]) {
			return;
		}

		swap(&heap[childIndex], &heap[parentIndex]);
		parentIndex = childIndex;
	}
}

void put(int* heap, int data) {
	heap[0]++;
	heap[heap[0]] = data;
	lift(heap, heap[0]);
}

int extractMax(int* heap) {
	int max;
	if (!isEmpty(heap[0])) {
		max = heap[1];
		heap[1] = heap[heap[0]];
		heap[0]--;
		sift(heap, 1);

		return max;
	}
	printf("Heap is empty");
	return 0;
}

int getData(int* heap) {
	int count = 0;
	while (scanf("%d", &heap[++count]) != EOF);
	return count - 1;
}

void printData(int* heap, int size) {
	int i;

	for (i = 1; i <= size; i++) {
		printf("%d ", heap[i]);
	}
}

void heapSort(int* heap, int size) {
	int i;

	heap[0] = 1;

	for (i = 2; i <= size; i++) {
		put(heap, heap[i]);
	}

	for (i = size; i >= 2; i--) {
		heap[i] = extractMax(heap);
	}
}

void solve() {
	int* heap = makeHeap(NMAX);
	int count;

	count = getData(heap);
	heapSort(heap, count);
	printData(heap, count);

	free(heap);
}

int main (void) {
	freopen ("input.txt", "r", stdin);
	freopen ("output.txt", "w", stdout);

	solve();

	return 0;
}

