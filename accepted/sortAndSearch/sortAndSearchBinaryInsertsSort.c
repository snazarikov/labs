#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NMAX 100005

int A[NMAX];
int count = 0;

void getData() {
	while(scanf("%d", &A[count]) != EOF) {
		count++;
	}
}

void printData() {
	int i;

	for (i = 0; i < count; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

void swapSort(int* A, const int count) {
	int i, j, tmp, index, a, b, m;

	for (i = 1; i < count; i++) {
		if (A[i] >= A[i - 1]) {
			continue;
		}

		if (A[i] < A[0]) {
			index = 0;
//			tmp = A[i];
		}

		else {
			a = 0;
			b = i - 1;

			while(a + 1 < b) {
				m = (a + b) / 2;

				if (A[m] < A[i]) {
					a = m;
				}

				else {
					b = m;
				}
			}

			index = b;
//			tmp = A[i];
		}

		tmp = A[i];
		memmove(A + index + 1, A + index, (i - index) * sizeof(int));
//		for (j = i - 1; j >= index; j--) {
//			A[j + 1] = A[j];
//		}
		A[index] = tmp;
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
	swapSort(A, count);
	printData();

	return 0;
}
