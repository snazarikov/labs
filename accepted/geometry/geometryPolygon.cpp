#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

using namespace std;

#define NMAX 105

struct point{
    int x;
    int y;
};

string movement;

int pointsAmount;

point P[NMAX];

void getData() {
    cin >> movement;

    pointsAmount = movement.size() + 1;

    P[0].x = 0;
    P[0].y = 0;

    for (int i = 1; i < pointsAmount; ++i) {
        if (movement[i - 1] == 'U') {
            P[i].x = P[i - 1].x;
            P[i].y = P[i - 1].y + 1;
        }
        else if (movement[i - 1] == 'D') {
            P[i].x = P[i - 1].x;
            P[i].y = P[i - 1].y - 1;
        }
        else if (movement[i - 1] == 'R') {
            P[i].x = P[i - 1].x + 1;
            P[i].y = P[i - 1].y;
        }
        else if (movement[i - 1] == 'L') {
            P[i].x = P[i - 1].x - 1;
            P[i].y = P[i - 1].y;
        }
    }
}

void printData() {
    for (int i = 0; i < pointsAmount; ++i) {
        cout << P[i].x << " " << P[i].y << "\n";
    }
}

int calcTriangleOrSquare(point p1, point p2, point p3) {
    return ((p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y));
}

int solve() {
    int result = 0;
    point zero;

    zero.x = 0;
    zero.y = 0;

    for (int i = 0; i < pointsAmount - 1; ++i) {
        result += calcTriangleOrSquare(zero, P[i], P[i + 1]);
    }
    return abs(result / 2);
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    freopen ("polygon.in", "r", stdin);
    freopen ("polygon.out", "w", stdout);

    getData();
//    printData();
    cout << solve();

    return 0;
}
