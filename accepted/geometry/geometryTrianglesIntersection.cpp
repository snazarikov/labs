#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

#define NMAX 5

using namespace std;

struct point{
    long long x;
    long long y;
};

point trianle1[NMAX];
point trianle2[NMAX];

void getData() {
    for (int i = 0; i < 3; ++i) {
        cin >> trianle1[i].x >> trianle1[i].y;
    }
    trianle1[3] = trianle1[0];
    for (int i = 0; i < 3; ++i) {
        cin >> trianle2[i].x >> trianle2[i].y;
    }
    trianle2[3] = trianle2[0];
}

void printData() {
    for (int i = 0; i < 4; ++i) {
        cout << trianle1[i].x << " " << trianle1[i].y << "\n";
    }
    cout << "\n";
    for (int i = 0; i < 4; ++i) {
        cout << trianle2[i].x << " " << trianle2[i].y << "\n";
    }
}

int signOfDistance(point p1, point p2, point p3) {
    long long r = (p2.y - p1.y) * p3.x + (p1.x - p2.x) * p3.y + p1.y * p2.x - p1.x * p2.y;
    if (r > 0) {
        return 1;
    }
    if (r < 0) {
        return -1;
    }
    return 0;
}

int isRectIntersect(point p1, point p2, point p3, point p4) {
    point rec1Left, rec1Right, rec2Left, rec2Right;

    if (p1.x > p2.x) {
        rec1Left = p2;
        rec1Right = p1;
    }
    else {
        rec1Left = p1;
        rec1Right = p2;
    }
    if (rec1Left.y > rec1Right.y) {
        long long tmp = rec1Left.y;
        rec1Left.y = rec1Right.y;
        rec1Right.y = tmp;
    }

    if (p3.x > p4.x) {
        rec2Left = p4;
        rec2Right = p3;
    }
    else {
        rec2Left = p3;
        rec2Right = p4;
    }
    if (rec2Left.y > rec2Right.y) {
        long long tmp = rec2Left.y;
        rec2Left.y = rec2Right.y;
        rec2Right.y = tmp;
    }

    long long maxLeft = max(rec1Left.x, rec2Left.x);
    long long minRight = min(rec1Right.x, rec2Right.x);
    long long minTop = min(rec1Right.y, rec2Right.y);
    long long maxDown = max(rec1Left.y, rec2Left.y);

    if (maxLeft > minRight || maxDown > minTop) {
        return 0;
    }
    return 1;
}

int isTriangleIntersect(point* firstTriangle, point* secondTriangle) {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (signOfDistance(firstTriangle[i], firstTriangle[i + 1], secondTriangle[j]) * signOfDistance(firstTriangle[i], firstTriangle[i + 1], secondTriangle[j + 1]) <= 0 && signOfDistance(secondTriangle[j], secondTriangle[j + 1], firstTriangle[i]) * signOfDistance(secondTriangle[j], secondTriangle[j + 1], firstTriangle[i + 1]) <= 0 && isRectIntersect(firstTriangle[i], firstTriangle[i + 1], secondTriangle[j], secondTriangle[j + 1])) {
                return 1;
            }
        }
    }

    for (int i = 0; i < 3; ++i) {
        int sign = signOfDistance(firstTriangle[0], firstTriangle[1], secondTriangle[i]);
        int flag = 1;
        for (int j = 1; j < 3; ++j) {
            if (signOfDistance(firstTriangle[j], firstTriangle[j + 1], secondTriangle[i]) == sign) {
                flag++;
            }
        }
        if (flag == 3) {
            return 1;
        }
    }

    for (int i = 0; i < 3; ++i) {
        int sign = signOfDistance(secondTriangle[0], secondTriangle[1], firstTriangle[i]);
        int flag = 1;
        for (int j = 1; j < 3; ++j) {
            if (signOfDistance(secondTriangle[j], secondTriangle[j + 1], firstTriangle[i]) == sign) {
                flag++;
            }
        }
        if (flag == 3) {
            return 1;
        }
    }

    return 0;
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    freopen("triangles.in", "r", stdin);
    freopen("triangles.out", "w", stdout);

    getData();

    if (isTriangleIntersect(trianle1, trianle2)) {
        cout << "yes";
    }
    else {
        cout << "no";
    }
//    cout << solve();

    return 0;
}
