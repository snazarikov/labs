#include <cstdio>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iomanip>

using namespace std;

double thickness, AB, BC, CD, AD, AC;

void getData() {
    cin >> thickness >> AB >> BC >> CD >> AD >> AC;
}

void printData() {
    cout << thickness << " " << AB << " " << BC << " " << CD << " " << AD << " " << AC << "\n";
}

double getAdditionalOuterValue(double angle) {
    return thickness * cos(angle / 2) / sin(angle / 2);
}

double solve() {
    double ADC, DCB, CBA, BAD;

    ADC = acos((CD * CD + AD * AD - AC * AC) / (2 * CD * AD));
    CBA = acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB));
    DCB = acos((AC * AC + CD * CD - AD * AD) / (2 * AC * CD)) + acos((BC * BC + AC * AC - AB * AB) / (2 * BC * AC));
    BAD = acos((AB * AB + AC * AC - BC * BC) / (2 * AB * AC)) + acos((AC * AC + AD * AD - CD * CD) / (2 * AC * AD));

//    cout << ADC << " " << CBA << " " << DCB << " " << BAD << "\n";

    double additionalOuterADCD, additionalOuterADAB, additionalOuterABBC, additionalOuterBCCD;

    additionalOuterADCD = getAdditionalOuterValue(ADC);
    additionalOuterADAB = getAdditionalOuterValue(BAD);
    additionalOuterABBC = getAdditionalOuterValue(CBA);
    additionalOuterBCCD = getAdditionalOuterValue(DCB);

//    cout << additionalOuterADCD << " " << additionalOuterADAB << " " << additionalOuterABBC << " " << additionalOuterBCCD << "\n";

    double result = AB + additionalOuterABBC + additionalOuterADAB +
                    BC + additionalOuterABBC + additionalOuterBCCD +
                    CD + additionalOuterADCD + additionalOuterBCCD +
                    AD + additionalOuterADAB + additionalOuterADCD;

    return result;
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    freopen ("baguette.in", "r", stdin);
    freopen ("baguette.out", "w", stdout);

    getData();
//    printData();
    printf("%.3lf", solve()) ;

    return 0;
}
