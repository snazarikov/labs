#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>
#include <algorithm>

#define NMAX 2005

using namespace std;

int doorsOSAmount, panixOSAmount, startDoors, startPanix, doorsConvexPointAmount, panixConvexPointAmount;

struct point{
    int x;
    int y;
    int position;
};

point startPoint;

point doorsOS[NMAX];
point panixOS[NMAX];

point doorsOSConvexHall[NMAX];
point panixOSConvexHall[NMAX];

void getData() {
    cin >> doorsOSAmount >> panixOSAmount;

    point point1, point2;

    point startPointDoors;
    startPointDoors.x = 1e5;
    startPointDoors.y = 1e5;

    for (int i = 0; i < doorsOSAmount; ++i) {
        cin >> point1.x >> point1.y >> point2.x >> point2.y;

        if (point1.x < startPointDoors.x || (point1.x == startPointDoors.x && point1.y < startPointDoors.y)) {
            startPointDoors.x = point1.x;
            startPointDoors.y = point1.y;
            startDoors = 4 * i;
        }

        doorsOS[4 * i] = point1;

        doorsOS[4 * i + 1] = point1;
        doorsOS[4 * i + 1].y = point2.y;

        doorsOS[4 * i + 2] = point2;

        doorsOS[4 * i + 3] = point2;
        doorsOS[4 * i + 3].y = point1.y;
    }

    point startPointPanix;
    startPointPanix.x = 1e5;
    startPointPanix.y = 1e5;

    for (int i = 0; i < panixOSAmount; ++i) {
        cin >> point1.x >> point1.y >> point2.x >> point2.y;

        if (point1.x < startPointPanix.x || (point1.x == startPointPanix.x && point1.y < startPointPanix.y)) {
            startPointPanix.x = point1.x;
            startPointPanix.y = point1.y;
            startPanix = 4 * i;
        }

        panixOS[4 * i] = point1;

        panixOS[4 * i + 1] = point1;
        panixOS[4 * i + 1].y = point2.y;

        panixOS[4 * i + 2] = point2;

        panixOS[4 * i + 3] = point2;
        panixOS[4 * i + 3].y = point1.y;
    }
}

void printData() {
    cout << "doorsAmount " << doorsOSAmount << " panixAmount " << panixOSAmount << "\n";
    for (int i = 0; i < doorsOSAmount * 4; ++i) {
        cout << doorsOS[i].x << " " << doorsOS[i].y << "\n";
    }

    cout << "startDoorsPoint " << doorsOS[startDoors].x << " " << doorsOS[startDoors].y << "\n\n";

    for (int i = 0; i < panixOSAmount * 4; ++i) {
        cout << panixOS[i].x << " " << panixOS[i].y << "\n";
    }

    cout << "startPanixPoint " << panixOS[startPanix].x << " " << panixOS[startPanix].y << "\n";
}

int calcTriangleOrSquare(point p1, point p2, point p3) {
    return ((p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y));
}

int calcDistance(point p1, point p2) {
    return ((p1.y - p2.y) * (p1.y - p2.y) + (p1.x - p2.x) * (p1.x - p2.x));
}

bool myCmp(point p1, point p2) {
    if ((p2.x - startPoint.x != 0 && p1.x - startPoint.x != 0 && ((p1.y - startPoint.y) * (p2.x - startPoint.x) == (p1.x - startPoint.x) * (p2.y - startPoint.y))) || (p2.x - startPoint.x == 0 && p1.x - startPoint.x == 0)){
        return calcDistance(p1, startPoint) < calcDistance(p2, startPoint);
    }
    else if (p2.x - startPoint.x != 0 && p1.x - startPoint.x != 0) {
        return (p1.y - startPoint.y) * (p2.x - startPoint.x) > (p1.x - startPoint.x) * (p2.y - startPoint.y);
    }
    else if (p2.x - startPoint.x == 0) {
        return false;
    }
    else if (p1.x - startPoint.x == 0) {
        return true;
    }
}

int graham(point* polygonPoints, point* convexHall, int start, int pointsAmount) {
    point tmp = polygonPoints[0];
    polygonPoints[0] = polygonPoints[start];
    polygonPoints[start] = tmp;

    startPoint = polygonPoints[0];

    sort(polygonPoints + 1, polygonPoints + pointsAmount, myCmp);

    convexHall[0] = polygonPoints[0];
    convexHall[1] = polygonPoints[1];
    convexHall[2] = polygonPoints[2];

    int convexPointAmount = 3;

    for (int i = 3; i < pointsAmount; i++) {
        while (calcTriangleOrSquare(convexHall[convexPointAmount - 2], convexHall[convexPointAmount - 1], polygonPoints[i]) > 0) {
            convexPointAmount--;
        }

        convexHall[convexPointAmount] = polygonPoints[i];
        convexPointAmount++;
    }

    return convexPointAmount;
}

int signOfDistance(point p1, point p2, point p3) {
    long long r = (p2.y - p1.y) * p3.x + (p1.x - p2.x) * p3.y + p1.y * p2.x - p1.x * p2.y;
    if (r > 0) {
        return 1;
    }
    if (r < 0) {
        return -1;
    }
    return 0;
}

int isRectIntersect(point p1, point p2, point p3, point p4) {
    point rec1Left, rec1Right, rec2Left, rec2Right;

    if (p1.x > p2.x) {
        rec1Left = p2;
        rec1Right = p1;
    }
    else {
        rec1Left = p1;
        rec1Right = p2;
    }
    if (rec1Left.y > rec1Right.y) {
        int tmp = rec1Left.y;
        rec1Left.y = rec1Right.y;
        rec1Right.y = tmp;
    }

    if (p3.x > p4.x) {
        rec2Left = p4;
        rec2Right = p3;
    }
    else {
        rec2Left = p3;
        rec2Right = p4;
    }
    if (rec2Left.y > rec2Right.y) {
        int tmp = rec2Left.y;
        rec2Left.y = rec2Right.y;
        rec2Right.y = tmp;
    }

    int maxLeft = max(rec1Left.x, rec2Left.x);
    int minRight = min(rec1Right.x, rec2Right.x);
    int minTop = min(rec1Right.y, rec2Right.y);
    int maxDown = max(rec1Left.y, rec2Left.y);

    if (maxLeft > minRight || maxDown > minTop) {
        return 0;
    }
    return 1;
}

int isConvexHallIntersect(point* firstConvexHall, point* secondConvexHall, int firstConvexPointAmount, int secondConvexPointAmount) {
    for (int i = 0; i < firstConvexPointAmount - 1; ++i) {
        for (int j = 0; j < secondConvexPointAmount - 1; ++j) {
            if (signOfDistance(firstConvexHall[i], firstConvexHall[i + 1], secondConvexHall[j]) * signOfDistance(firstConvexHall[i], firstConvexHall[i + 1], secondConvexHall[j + 1]) <= 0 && signOfDistance(secondConvexHall[j], secondConvexHall[j + 1], firstConvexHall[i]) * signOfDistance(secondConvexHall[j], secondConvexHall[j + 1], firstConvexHall[i + 1]) <= 0 && isRectIntersect(firstConvexHall[i], firstConvexHall[i + 1], secondConvexHall[j], secondConvexHall[j + 1])) {
                return 1;
            }
        }
    }

    for (int i = 0; i < secondConvexPointAmount - 1; ++i) {
        int sign = signOfDistance(firstConvexHall[0], firstConvexHall[1], secondConvexHall[i]);
        int flag = 1;
        for (int j = 1; j < firstConvexPointAmount - 1; ++j) {
            if (signOfDistance(firstConvexHall[j], firstConvexHall[j + 1], secondConvexHall[i]) == sign) {
                flag++;
            }
        }
        if (flag == firstConvexPointAmount - 1) {
            return 1;
        }
    }

    for (int i = 0; i < firstConvexPointAmount - 1; ++i) {
        int sign = signOfDistance(secondConvexHall[0], secondConvexHall[1], firstConvexHall[i]);
        int flag = 1;
        for (int j = 1; j < secondConvexPointAmount - 1; ++j) {
            if (signOfDistance(secondConvexHall[j], secondConvexHall[j + 1], firstConvexHall[i]) == sign) {
                flag++;
            }
        }
        if (flag == secondConvexPointAmount - 1) {
            return 1;
        }
    }

    return 0;
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    freopen ("wall.in", "r", stdin);
    freopen ("wall.out", "w", stdout);

    getData();
//    printData();

    doorsConvexPointAmount = graham(doorsOS, doorsOSConvexHall, startDoors, doorsOSAmount * 4);
    panixConvexPointAmount = graham(panixOS, panixOSConvexHall, startPanix, panixOSAmount * 4);

    doorsOSConvexHall[doorsConvexPointAmount] = doorsOSConvexHall[0];
    doorsConvexPointAmount++;

    panixOSConvexHall[panixConvexPointAmount] = panixOSConvexHall[0];
    panixConvexPointAmount++;

    if (isConvexHallIntersect(doorsOSConvexHall, panixOSConvexHall, doorsConvexPointAmount, panixConvexPointAmount)) {
        cout << "No";
    }
    else {
        cout << "Yes";
    }

    return 0;
}
