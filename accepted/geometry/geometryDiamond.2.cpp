#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>
#include <algorithm>

#define NMAX 105

#define max(a, b) (((a) > (b)) ? (a) : (b))

using namespace std;

int p, q, pointsAmount, pos, a = 0, b = 0, convexPointAmount = 0;

struct point{
    int x;
    int y;
    int position;
};

point P[NMAX];
point convexHall[NMAX];
int isDent[NMAX];

void getData() {
    cin >> p >> q >> pointsAmount;
    point startPoint;
    startPoint.x = 1e4;
    startPoint.y = 1e4;

    for (int i = 0; i < pointsAmount; ++i) {
        cin >> P[i].x >> P[i].y;

        if (P[i].x < startPoint.x || (P[i].x == startPoint.x && P[i].y < startPoint.y)) {
            startPoint.x = P[i].x;
            startPoint.y = P[i].y;
            pos = i;
        }
    }

    for (int i = pos; i < pointsAmount; ++i) {
        P[i].position = i - pos;
    }

    for (int i = 0; i < pos; ++i) {
        P[i].position = pointsAmount - pos + i;
    }
}

void printData() {
    for (int i = 0; i < pointsAmount; ++i) {
        cout << P[i].x << " " << P[i].y << "\n";
    }

    cout << "\nstartPoint " << P[pos].x << " " << P[pos].y << "\n";
}

int calcTriangleOrSquare(point p1, point p2, point p3) {
    return ((p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y));
}

int calcDistance(point p1, point p2) {
    return ((p1.y - p2.y) * (p1.y - p2.y) + (p1.x - p2.x) * (p1.x - p2.x));
}

bool myCmp(point p1, point p2) {
    if ((p2.x - P[0].x != 0 && p1.x - P[0].x != 0 && ((p1.y - P[0].y) * (p2.x - P[0].x) == (p1.x - P[0].x) * (p2.y - P[0].y))) || (p2.x - P[0].x == 0 && p1.x - P[0].x == 0)){
        return calcDistance(p1, P[0]) < calcDistance(p2, P[0]);
    }
    else if (p2.x - P[0].x != 0 && p1.x - P[0].x != 0) {
        return (p1.y - P[0].y) * (p2.x - P[0].x) > (p1.x - P[0].x) * (p2.y - P[0].y);
    }
    else if (p2.x - P[0].x == 0) {
        return false;
    }
    else if (p1.x - P[0].x == 0) {
        return true;
    }
}

void graham() {
    point tmp = P[0];
    P[0] = P[pos];
    P[pos] = tmp;

    sort(P + 1, P + pointsAmount, myCmp);

    convexHall[0] = P[0];
    convexHall[1] = P[1];
    convexHall[2] = P[2];

    convexPointAmount = 3;

    for (int i = 3; i < pointsAmount; ++i) {
        while (calcTriangleOrSquare(convexHall[convexPointAmount - 2], convexHall[convexPointAmount - 1], P[i]) > 0) {
            convexPointAmount--;
        }

        convexHall[convexPointAmount] = P[i];
        convexPointAmount++;
    }
}

void solve() {
    graham();

    for (int i = 0; i < convexPointAmount; ++i) {
        isDent[(convexHall[i].position + pointsAmount + pos) % pointsAmount] = 1;
    }

    isDent[pointsAmount] = isDent[0];
    for (int i = 0; i < pointsAmount; ++i) {
        if (isDent[i] && isDent[i + 1]) {
            b++;
        }
    }

    isDent[pointsAmount] = 0;
    for (int i = 0; i < pointsAmount; ++i) {
        isDent[i] = !isDent[i];
    }

    int flag = 0;
    for (int i = 0; i <= pointsAmount; ++i) {
        if (isDent[i]) {
            flag = 1;
        }
        else {
            a += flag;
            flag = 0;
        }
    }
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    freopen ("diamond.in", "r", stdin);
    freopen ("diamond.out", "w", stdout);

    getData();
    solve();

//    cout << "\nb " << b << " a " << a << "\n";
    cout << max(0, -a * p + b * q);

    return 0;
}
