#include <cstdio>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <string>

using namespace std;

struct treeElement {
    int vertexAmount;
    int yKey;
    treeElement* leftChild;
    treeElement* rightChild;
    string data;
};

treeElement* root = NULL;
int amount = 0;

void printTree(treeElement* root, int depth, int side) {
    // 1 - left 2 - right 0 - start root
    if (root != NULL) {
        if (root->leftChild != NULL) {
            printTree(root->leftChild, depth + 1, 1);
        }
        cout << root->vertexAmount << "," << root->data << "(" << depth << "," << side << ") ";
        if (root->rightChild != NULL) {
            printTree(root->rightChild, depth + 1, 2);
        }
    }
}

int getRandomKey() {
    return rand();
}

int getVertexAmount(treeElement* u) {
    return ((u == NULL) ? (0) : (u->vertexAmount));
}

treeElement* merge(treeElement* left, treeElement* right) {
    if (left == NULL) {
        return right;
    }
    if (right == NULL) {
        return left;
    }
    treeElement* u;
    if (left->yKey >= right->yKey) {
        left->rightChild = merge(left->rightChild, right);
        u = left;
    }
    else {
        right->leftChild = merge(left, right->leftChild);
        u = right;
    }
    u->vertexAmount = getVertexAmount(u->leftChild) + getVertexAmount(u->rightChild) + 1;
    return u;
}

void split(treeElement* u, int ind, treeElement*& left, treeElement*& right) {
    if (u == NULL) {
        left = right = NULL;
        return;
    }
    if (ind < getVertexAmount(u->leftChild)) {
        split(u->leftChild, ind, left, u->leftChild);
        right = u;
    }
    else {
        split(u->rightChild, ind - 1 - getVertexAmount(u->leftChild), u->rightChild, right);
        left = u;
    }
    u->vertexAmount = getVertexAmount(u->leftChild) + getVertexAmount(u->rightChild) + 1;
}

treeElement* pos(treeElement* u, int k) {
    if (u == NULL || k > getVertexAmount(u)) {
        return NULL;
    }
    int leftAmount = getVertexAmount(u->leftChild);
    if (k == leftAmount) {
        return u;
    }
    else if (k < leftAmount) {
        return pos(u->leftChild, k);
    }
    else {
        return pos(u->rightChild, k - leftAmount - 1);
    }
}

void ins(int ind, string data) {
    treeElement* left = NULL;
    treeElement* right = NULL;

    split(root, ind - 1, left, right);

    treeElement* v = new treeElement;
    v->vertexAmount = 1;
    v->yKey = getRandomKey();
    v->data = data;
    v->leftChild = NULL;
    v->rightChild = NULL;

    left = merge(left, v);
    root = merge(left, right);
}

void add(string data) {
    treeElement* v = new treeElement;
    v->vertexAmount = 1;
    v->yKey = getRandomKey();
    v->data = data;
    v->leftChild = NULL;
    v->rightChild = NULL;

    root = merge(root, v);
}

void del(int indLeft, int indRight) {
    treeElement* left = NULL;
    treeElement* right = NULL;
    treeElement* deletedSubtree = NULL;

    split(root, indRight, left, right);
    split(left, indLeft - 1, left, deletedSubtree);

    root = merge(left, right);
}

void solve() {
    string action, data;
    int a, b;

    cin >> action;
    while (action != "END") {
        if (action == "ADD") {
            cin >> data;
            if (root == NULL) {
                root = new treeElement;
                root->yKey = getRandomKey();
                root->data = data;
                root->leftChild = NULL;
                root->rightChild = NULL;
                root->vertexAmount = 1;
            }
            else {
                add(data);
            }
        }
        else if (action == "DEL") {
            cin >> a >> b;
            del(a - 1, b - 1);
        }
        else if (action == "INS") {
            cin >> a >> data;
            if (root == NULL) {
                root = new treeElement;
                root->yKey = getRandomKey();
                root->data = data;
                root->leftChild = NULL;
                root->rightChild = NULL;
                root->vertexAmount = 1;
            }
            else {
                ins(a - 1, data);
            }
        }
        else if (action == "KEY") {
            cin >> a;
            treeElement* finded = pos(root, a - 1);
            cout << ((finded == NULL) ? ("OUT OF RANGE") : (finded->data)) << '\n';
        }

        cin >> action;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    srand(time(NULL));

    solve();

    return 0;
}
