#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

const int ALPHABET_SIZE = 2;

struct treeElement {
    int data;
    treeElement* children[ALPHABET_SIZE];
};

treeElement* makeVertex() {
    treeElement* v = new treeElement;
    v->data = 0;
    for (int i = 0; i < ALPHABET_SIZE; ++i) {
        v->children[i] = NULL;
    }
    return v;
}

treeElement* root = makeVertex();

int search(string key) {
    treeElement* u = root;
    for (int i = 0; i < key.size(); ++i) {
        if (u->children[key[i] - '0'] == NULL) {
            return 0;
        }
        u = u->children[key[i] - '0'];
    }
    return u->data;
}

void add(string key, int data) {
    treeElement* u = root;
    for (int i = 0; i < key.size(); ++i) {
        if (u->children[key[i] - '0'] == NULL) {
            u->children[key[i] - '0'] = makeVertex();
        }
        u = u->children[key[i] - '0'];
    }
    if (!u->data) {
        u->data = data;
    }
}

void del(string key) {
    treeElement* u = root;
    for (int i = 0; i < key.size(); ++i) {
        if (u->children[key[i] - '0'] == NULL) {
            return;
        }
        u = u->children[key[i] - '0'];
    }
    u->data = 0;
}

void solve() {
    string action;

    cin >> action;
    while (action != "END") {
        string key;
        cin >> key;
        if (action == "ADD") {
            int data;
            cin >> data;
            add(key, data);
        }
        else if (action == "DEL") {
            del(key);
        }
        else if (action == "FIND") {
            cout << search(key) << endl;
        }
        cin >> action;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    solve();

    return 0;
}
