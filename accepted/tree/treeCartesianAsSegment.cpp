#include <cstdio>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <string>

using namespace std;

struct treeElement {
    int xKey;
    int yKey;
    treeElement* leftChild;
    treeElement* rightChild;
    int data;
    int maximum;
};

treeElement* root = NULL;

void printTree(treeElement* root, int depth, int side) {
    // 1 - left 2 - right 0 - start root
    if (root != NULL) {
        if (root->leftChild != NULL) {
            printTree(root->leftChild, depth + 1, 1);
        }
        cout << root->xKey << "," << root->data << "(" << depth << "," << side << "," << root->maximum << ") ";
        if (root->rightChild != NULL) {
            printTree(root->rightChild, depth + 1, 2);
        }
    }
}

int getRandomKey() {
    return rand();
}

int getMaximum(treeElement* u) {
    return ((u == NULL) ? (0) : (u->maximum));
}

treeElement* merge(treeElement* left, treeElement* right) {
    if (left == NULL) {
        return right;
    }
    if (right == NULL) {
        return left;
    }
    treeElement* u;
    if (left->yKey >= right->yKey) {
        left->rightChild = merge(left->rightChild, right);
        u = left;
    }
    else {
        right->leftChild = merge(left, right->leftChild);
        u = right;
    }
    u->maximum = max(max(getMaximum(u->leftChild), getMaximum(u->rightChild)), u->data);
    return u;
}

void split(treeElement* u, int xKeyCut, treeElement*& left, treeElement*& right) {
    if (u == NULL) {
        left = right = NULL;
        return;
    }
    if (xKeyCut < u->xKey) {
        split(u->leftChild, xKeyCut, left, u->leftChild);
        right = u;
    }
    else {
        split(u->rightChild, xKeyCut, u->rightChild, right);
        left = u;
    }
    u->maximum = max(max(getMaximum(u->leftChild), getMaximum(u->rightChild)), u->data);
}

int search(int xKeySearch) {
    treeElement* u = root;
    int depth = 1;

    if (u == NULL) {
        return 0;
    }

    while ((xKeySearch < u->xKey && u->leftChild != NULL) || (xKeySearch > u->xKey && u->rightChild != NULL)) {
        if (xKeySearch < u->xKey) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
        depth++;
    }
    if (u->xKey == xKeySearch) {
        return depth;
    }
    return 0;
}

void add(int xKeyAdd, int data) {
    treeElement* left = NULL;
    treeElement* right = NULL;

    split(root, xKeyAdd, left, right);

    treeElement* v = new treeElement;
    v->xKey = xKeyAdd;
    v->yKey = getRandomKey();
    v->data = data;
    v->leftChild = NULL;
    v->rightChild = NULL;
    v->maximum = data;

    left = merge(left, v);
    root = merge(left, right);
}

void del(int xKeyDel) {
    treeElement* left = NULL;
    treeElement* right = NULL;
    treeElement* deletedVertex = NULL;

    split(root, xKeyDel, left, right);
    split(left, xKeyDel - 1, left, deletedVertex);

    root = merge(left, right);
}

int calcMax(int xKeyLeft, int xKeyRight) {
    treeElement* left = NULL;
    treeElement* center = NULL;
    treeElement* right = NULL;

    treeElement* u = root;

    split(u, xKeyLeft - 1, left, center);
    split(center, xKeyRight, center, right);

    int res = getMaximum(center);

    center = merge(center, right);
    root = merge(left, center);

    return res;
}

void solve() {
    char action;
    int xKey, data;

    cin >> action;
    while (action != 'E') {
        cin >> xKey;
        if (action == '+') {
            cin >> data;
            if (root == NULL) {
                root = new treeElement;
                root->xKey = xKey;
                root->yKey = getRandomKey();
                root->data = data;
                root->leftChild = NULL;
                root->rightChild = NULL;
                root->maximum = data;
            }
            else {
                if (!search(xKey)) {
                    add(xKey, data);
                }
            }
        }
        else if (action == '-') {
            del(xKey);
        }
        else if (action == 'm') {
            cin >> data;
            cout << calcMax(xKey, data) << endl;
        }
        cin >> action;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    srand(time(NULL));

    solve();

    return 0;
}
