#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>

using namespace std;

struct treeElement {
    string data;
    treeElement* leftChild;
    treeElement* rightChild;
};

treeElement* root = NULL;

int search(string data) {
    treeElement* u = root;
    int depth = 1;

    if (u == NULL) {
        return 0;
    }

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
        depth++;
    }
    if (u->data == data) {
        return depth;
    }
    return 0;
}

void add(string data) {
    treeElement* u = root;

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
    }
    if (u->data == data) {
        return;
    }

    treeElement* v = new treeElement;
    v->data = data;
    v->leftChild = NULL;
    v->rightChild = NULL;

    if (v->data > u->data) {
        u->rightChild = v;
    }
    else {
        u->leftChild = v;
    }
}

void del(string data) {
    treeElement* u = root;

    if (u == NULL) {
        return;
    }

    treeElement* uParent = NULL;
    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        uParent = u;
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
    }

    if (u->data != data) {
        return;
    }

    // 1
    if (u->leftChild == NULL && u->rightChild == NULL) {
        if (u == root) {
            root = NULL;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = NULL;
        }
        else if (uParent->rightChild == u) {
            uParent->rightChild = NULL;
        }
    }

    //2
    else if (u->leftChild == NULL) {
        if (u == root) {
            root = u->rightChild;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = u->rightChild;
        }
        if (uParent->rightChild == u) {
            uParent->rightChild = u->rightChild;
        }
    }
    else if (u->rightChild == NULL) {
        if (u == root) {
            root = u->leftChild;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = u->leftChild;
        }
        else if (uParent->rightChild == u) {
            uParent->rightChild = u->leftChild;
        }
    }

    //3
    else if (u->leftChild != NULL && u->rightChild != NULL){
        treeElement* tmpParent = u;
        treeElement* tmp = u->rightChild;
        while (tmp->leftChild != NULL) {
            tmpParent = tmp;
            tmp = tmp->leftChild;
        }
        u->data = tmp->data;

        if (tmpParent->leftChild == tmp) {
            tmpParent->leftChild = tmp->rightChild;
        }
        else if (tmpParent->rightChild == tmp) {
            tmpParent->rightChild = tmp->rightChild;
        }
    }
}

void solve() {
    char action;

    cin >> action;
    while (action != 'E') {
        string word;
        cin >> word;
//        cout << action << word << '\n';
        if (action == '+') {
            if (root == NULL) {
                root = new treeElement;
                root->data = word;
                root->leftChild = NULL;
                root->rightChild = NULL;
            }
            else {
                add(word);
            }
        }
        else if (action == '-') {
            del(word);
        }
        else if (action == '?') {
            int depth = search(word);
            cout << word << ' ' << depth << '\n';
        }
        cin >> action;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    solve();

    return 0;
}
