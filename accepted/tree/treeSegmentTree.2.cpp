#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

const int MAX_ELEMENT_AMOUNT = 131072;

int segmentTree[MAX_ELEMENT_AMOUNT * 2 + 5];

void change(int number, int addition) {
    int ind = number + MAX_ELEMENT_AMOUNT - 1;
    while (ind) {
        segmentTree[ind] += addition;
        ind /= 2;
    }
}

int calcSum(int amount) {
    int ind = amount + MAX_ELEMENT_AMOUNT - 1, sum = segmentTree[ind];
    while (ind) {
        if (((ind / 2) * 2) + 1 == ind) {
            sum += segmentTree[ind -  1];
        }
        ind /= 2;
    }
    return sum;
}

int getSum(int start, int finish) {
    return calcSum(finish) - calcSum(start) + segmentTree[start + MAX_ELEMENT_AMOUNT - 1];
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    char symbol;
    int a, b;

    while (1) {
        cin >> symbol;
        if (symbol == 'E') {
            break;
        }
        cin >> a >> b;

        if (symbol == 'C') {
            change(a, b);
        }
        if (symbol == 'S') {
            cout << getSum(a, b) << '\n';
        }
    }

    return 0;
}


