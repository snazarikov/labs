#include <cstdio>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <string>

using namespace std;

struct treeElement {
    int vertexAmount;
    int yKey;
    treeElement* leftChild;
    treeElement* rightChild;
    long long data;
};

treeElement* root = NULL;
long long res = 0;

void printTree(treeElement* root, int depth, int side) {
    // 1 - left 2 - right 0 - start root
    if (root != NULL) {
        if (root->leftChild != NULL) {
            printTree(root->leftChild, depth + 1, 1);
        }
        cout << root->vertexAmount << "," << root->data << "(" << depth << "," << side << ") ";
        if (root->rightChild != NULL) {
            printTree(root->rightChild, depth + 1, 2);
        }
    }
}

int getRandomKey() {
    return rand();
}

treeElement* createVertex(long long data) {
    treeElement* v = new treeElement;
    v->vertexAmount = 1;
    v->yKey = getRandomKey();
    v->data = data;
    v->leftChild = NULL;
    v->rightChild = NULL;
    return v;
}

int getVertexAmount(treeElement* u) {
    return ((u == NULL) ? (0) : (u->vertexAmount));
}

treeElement* merge(treeElement* left, treeElement* right) {
    if (left == NULL) {
        return right;
    }
    if (right == NULL) {
        return left;
    }
    treeElement* u;
    if (left->yKey >= right->yKey) {
        left->rightChild = merge(left->rightChild, right);
        u = left;
    }
    else {
        right->leftChild = merge(left, right->leftChild);
        u = right;
    }
    u->vertexAmount = getVertexAmount(u->leftChild) + getVertexAmount(u->rightChild) + 1;
    return u;
}

void split(treeElement* u, int ind, treeElement*& left, treeElement*& right) {
    if (u == NULL) {
        left = right = NULL;
        return;
    }
    if (ind < getVertexAmount(u->leftChild)) {
        split(u->leftChild, ind, left, u->leftChild);
        right = u;
    }
    else {
        split(u->rightChild, ind - 1 - getVertexAmount(u->leftChild), u->rightChild, right);
        left = u;
    }
    u->vertexAmount = getVertexAmount(u->leftChild) + getVertexAmount(u->rightChild) + 1;
}

int addToPos(treeElement* u, int k, long long addition) {
    if (u == NULL || k > getVertexAmount(u)) {
        return -1;
    }
    int leftAmount = getVertexAmount(u->leftChild);
    if (k == leftAmount) {
        res -= u->data * u->data;
        u->data += addition;
        res += u->data * u->data;
        return u->data;
    }
    else if (k < leftAmount) {
        return addToPos(u->leftChild, k, addition);
    }
    else {
        return addToPos(u->rightChild, k - leftAmount - 1, addition);
    }
}

void ins(int ind, long long data) {
    treeElement* left = NULL;
    treeElement* right = NULL;

    split(root, ind - 1, left, right);

    treeElement* v = createVertex(data);

    left = merge(left, v);
    root = merge(left, right);
}

void add(long long data) {
    treeElement* v = createVertex(data);
    root = merge(root, v);
}

treeElement* del(int ind) {
    treeElement* left = NULL;
    treeElement* right = NULL;
    treeElement* deletedVertex = NULL;

    split(root, ind, left, right);
    split(left, ind - 1, left, deletedVertex);

    root = merge(left, right);

    return deletedVertex;
}

int n, changeAmount;

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);
    freopen("river.in", "r", stdin);
    freopen("river.out", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    srand(time(NULL));

    cin >> n;

    long long num;
    for (int i = 0; i < n; ++i) {
        cin >> num;
        res += num * num;
        add(num);
    }

    cin >> changeAmount;

    cout << res << endl;

    long long type, position, firstAddition, secondAddition;
    treeElement* deletedVertex;
    for (int i = 0; i < changeAmount; ++i) {
        cin >> type >> position;

        int flag = 0;
        if (position == root->vertexAmount) {
            flag = 1;
        }

        deletedVertex = del(position - 1);
        firstAddition = (deletedVertex->data / 2);
        secondAddition = (deletedVertex->data / 2) + (deletedVertex->data % 2);

        res -= deletedVertex->data * deletedVertex->data;

        if (type == 1) {
            if (position == 1) {
                addToPos(root, position - 1, deletedVertex->data);
            }
            else if (flag == 1) {
                addToPos(root, root->vertexAmount - 1, deletedVertex->data);
            }
            else {
                addToPos(root, position - 2, firstAddition);
                addToPos(root, position - 1, secondAddition);
            }
        }
        else if (type == 2) {
            res += firstAddition * firstAddition + secondAddition * secondAddition;
            ins(position - 1, secondAddition);
            ins(position - 1, firstAddition);
        }

        cout << res << endl;
    }

    return 0;
}
