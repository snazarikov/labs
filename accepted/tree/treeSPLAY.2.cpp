#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <cmath>
#include <vector>
#include <stack>

using namespace std;

const int SOME_MAX = 5;

struct treeElement {
    string data;
    treeElement* leftChild;
    treeElement* rightChild;
};

treeElement* root = NULL;

void printTree(treeElement* root, int depth, int side) {
    // 1 - left 2 - right 0 - start root
    if (root != NULL) {
        if (root->leftChild != NULL) {
            printTree(root->leftChild, depth + 1, 1);
        }
        cout << root->data << "(" << depth << "," << side << ") ";
        if (root->rightChild != NULL) {
            printTree(root->rightChild, depth + 1, 2);
        }
    }
}

void BLR(treeElement* a) {
/*
           a               c(a)
          / \              /  \
         b   S           b    a(c)
        / \      --->   / \    / \
       P   c           P   Q  R   S
          / \
         Q   R
*/
    // before rotation
    treeElement* b = a->leftChild;
    treeElement* c = b->rightChild;
    treeElement* P = b->leftChild;
    treeElement* Q = c->leftChild;
    treeElement* R = c->rightChild;
    treeElement* S = a->rightChild;

    // rotation
    a->rightChild = c;
    string tmpData = a->data;
    a->data = c->data;
    c->data = tmpData;

    b->rightChild = Q;

    c->leftChild = R;
    c->rightChild = S;
}

void BRR(treeElement* a) {
/*
           a                    c(a)
          / \                   /   \
         P   b                a(c)   b
            / \      --->     / \   / \
           c   S             P   Q R   S
          / \
         Q   R
*/
    // before rotation
    treeElement* b = a->rightChild;
    treeElement* c = b->leftChild;
    treeElement* P = a->leftChild;
    treeElement* Q = c->leftChild;
    treeElement* R = c->rightChild;
    treeElement* S = b->rightChild;

    // rotation
    a->leftChild = c;
    string tmpData = a->data;
    a->data = c->data;
    c->data = tmpData;

    c->leftChild = P;
    c->rightChild = Q;

    b->leftChild = R;
}

void SLR(treeElement* a) {
/*
           a               b(a)
          / \              /  \
         b   R            P   a(b)
        / \      --->          / \
       P   Q                  Q   R

*/
    //before rotation
    treeElement* b = a->leftChild;
    treeElement* P = b->leftChild;
    treeElement* Q = b->rightChild;
    treeElement* R = a->rightChild;

    // rotation
    a->rightChild = b;
    string tmpData = a->data;
    a->data = b->data;
    b->data = tmpData;

    a->leftChild = P;
    b->leftChild = Q;
    b->rightChild = R;
}

void SRR(treeElement* a) {
/*
           a                    b(a)
          / \                   /   \
         P   b                a(b)   R
            / \      --->     / \
           Q   R             P   Q
*/
    // before rotation
    treeElement* b = a->rightChild;
    treeElement* P = a->leftChild;
    treeElement* Q = b->leftChild;
    treeElement* R = b->rightChild;

    // rotation
    a->leftChild = b;
    string tmpData = a->data;
    a->data = b->data;
    b->data = tmpData;

    b->leftChild = P;
    b->rightChild = Q;
    a->rightChild = R;
}

void splay(treeElement* a, stack<treeElement*>& ancestors) {
    treeElement* p;
    treeElement* pp;

    while (ancestors.size()) {
        p = ancestors.top();
        ancestors.pop();

        if (ancestors.size()) {
            pp = ancestors.top();
            ancestors.pop();

            if (pp->leftChild == p && p->leftChild == a) {
                SLR(pp);
                SLR(pp);
            }
            else if (pp->rightChild == p && p->rightChild == a) {
                SRR(pp);
                SRR(pp);
            }
            else if (pp->leftChild == p && p->rightChild == a) {
                BLR(pp);
            }
            else if (pp->rightChild == p && p->leftChild == a) {
                BRR(pp);
            }
            a = pp;
        }
        else {
            if (p->leftChild == a) {
                SLR(p);
            }
            else if (p->rightChild == a) {
                SRR(p);
            }
            a = p;
        }
    }
}

int search(string& data) {
    treeElement* u = root;
    int depth = 1;

    stack<treeElement*> ancestors;

    if (u == NULL) {
        return 0;
    }

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        ancestors.push(u);
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
        depth++;
    }
    if (u->data == data) {
        splay(u, ancestors);
        return depth;
    }
    return 0;
}

void add(string& data) {
    treeElement* u = root;
    stack<treeElement*> ancestors;
    ancestors.push(u);

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
        ancestors.push(u);
    }

    if (u->data == data) {
        return;
    }

    treeElement* v = new treeElement;
    v->data = data;
    v->leftChild = NULL;
    v->rightChild = NULL;

    if (v->data > u->data) {
        u->rightChild = v;
        splay(u->rightChild, ancestors);
    }
    else {
        u->leftChild = v;
        splay(u->leftChild, ancestors);
    }
}

void del(string& data) {
    treeElement* u = root;
    stack<treeElement*> ancestors;

    if (u == NULL) {
        return;
    }

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        ancestors.push(u);
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
    }

    if (u->data != data) {
        return;
    }

    treeElement* uParent;
    if (!ancestors.size()) {
        uParent = NULL;
    }
    else {
        uParent = ancestors.top();
        ancestors.pop();
    }

    // 1
    if (u->leftChild == NULL && u->rightChild == NULL) {
        if (u == root) {
            root = NULL;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = NULL;
        }
        else if (uParent->rightChild == u) {
            uParent->rightChild = NULL;
        }
    }

    //2
    else if (u->leftChild == NULL) {
        if (u == root) {
            root = u->rightChild;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = u->rightChild;
        }
        if (uParent->rightChild == u) {
            uParent->rightChild = u->rightChild;
        }
    }
    else if (u->rightChild == NULL) {
        if (u == root) {
            root = u->leftChild;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = u->leftChild;
        }
        else if (uParent->rightChild == u) {
            uParent->rightChild = u->leftChild;
        }
    }

    //3
    else if (u->leftChild != NULL && u->rightChild != NULL){
        treeElement* tmpParent = u;
        treeElement* tmp = u->rightChild;
        while (tmp->leftChild != NULL) {
            tmpParent = tmp;
            tmp = tmp->leftChild;
        }
        u->data = tmp->data;

        if (tmpParent->leftChild == tmp) {
            tmpParent->leftChild = tmp->rightChild;
        }
        else if (tmpParent->rightChild == tmp) {
            tmpParent->rightChild = tmp->rightChild;
        }
    }
    splay(uParent, ancestors);
}

void solve() {
    char action;

    cin >> action;
    while (action != 'E') {
        string word;
        cin >> word;
        if (action == '+') {
            if (root == NULL) {
                root = new treeElement;
                root->data = word;
                root->leftChild = NULL;
                root->rightChild = NULL;
            }
            else {
                add(word);
            }
        }
        else if (action == '-') {
            del(word);
        }
        else if (action == '?') {
            int depth = search(word);
            cout << word << ' ' << depth << '\n';
        }
        cin >> action;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    solve();

    return 0;
}
