#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const int VERTEX_AMOUNT = 1e4 + 5;
const int EMPTY = -1;

int vertexAmount, edgeAmount;

struct treeElement {
    int parent;
    int leftChild;
    int rightNeighbor;
    treeElement(int p = -1, int lC = -1, int rN = -1) : parent(p), leftChild(lC), rightNeighbor(rN) {}
} tree[VERTEX_AMOUNT];

vector<int> vertexPow(VERTEX_AMOUNT, EMPTY);
vector<int> vertexDepth(VERTEX_AMOUNT, EMPTY);
vector<int> vertexAmountLevelDepth(VERTEX_AMOUNT, 0);

int maxPow = EMPTY, height = EMPTY, root;

void add(int prnt, int chld) {
    tree[chld] = treeElement(prnt, tree[chld].leftChild, tree[prnt].leftChild);
    tree[prnt].leftChild = chld;
}

void del(int u) {
    int prnt = tree[u].parent;
    if (tree[prnt].leftChild == u) {
        tree[prnt].leftChild = tree[u].rightNeighbor;
        return;
    }
    int v = tree[prnt].leftChild;
    while (tree[v].rightNeighbor != u) {
        v = tree[v].rightNeighbor;
    }
    tree[v].rightNeighbor = tree[u].rightNeighbor;
}

void getData() {
    cin >> vertexAmount;
    int parent, child;
    for (int i = 0; i < vertexAmount - 1; ++i) {
        cin >> parent >> child;
        add(parent, child);
    }
}

void pass(int u, int depth) {
    height = max(height, depth);
    vertexDepth[u] = depth;
    vertexAmountLevelDepth[depth]++;
    int v = tree[u].leftChild;
    int counter = 0;
    while (v != EMPTY) {
        pass(v, depth + 1);
        v = tree[v].rightNeighbor;
        counter++;
    }
    vertexPow[u] = counter;
    maxPow = max(maxPow, counter);
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();

    for (int i = 1; i <= vertexAmount; ++i) {
        if (tree[i].parent == EMPTY) {
            root = i;
            break;
        }
    }

    pass(root, 1);

    cout << maxPow << '\n';
    for (int i = 1; i <= vertexAmount; ++i) {
        if (vertexPow[i] == maxPow) {
            cout << i << ' ';
        }
    }
    cout << '\n';

    int leafAmount = 0;
    for (int i = 1; i <= vertexAmount; ++i) {
        if (tree[i].leftChild == EMPTY) {
            leafAmount++;
        }
    }
    cout << leafAmount << '\n';

    cout << height << '\n';
    for (int i = 1; i <= height; ++i) {
        cout << vertexAmountLevelDepth[i] << ' ';
    }

    return 0;
}
