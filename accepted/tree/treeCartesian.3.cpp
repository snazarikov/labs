#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>

using namespace std;

struct treeElement {
    int xKey;
    int yKey;
    treeElement* leftChild;
    treeElement* rightChild;
    int vertexAmount;
};

treeElement* root = NULL;

void printTree(treeElement* root, int depth, int side) {
    // 1 - left 2 - right 0 - start root
    if (root != NULL) {
        if (root->leftChild != NULL) {
            printTree(root->leftChild, depth + 1, 1);
        }
        cout << root->xKey << "," << root->yKey << "(" << depth << "," << side << ") ";
        if (root->rightChild != NULL) {
            printTree(root->rightChild, depth + 1, 2);
        }
    }
}

int getVertexAmount(treeElement* u) {
    return ((u == NULL) ? (0) : (u->vertexAmount));
}

treeElement* merge(treeElement* left, treeElement* right) {
    if (left == NULL) {
        return right;
    }
    if (right == NULL) {
        return left;
    }
    treeElement* u;
    if (left->yKey >= right->yKey) {
        left->rightChild = merge(left->rightChild, right);
        u = left;
    }
    else {
        right->leftChild = merge(left, right->leftChild);
        u = right;
    }
    u->vertexAmount = getVertexAmount(u->leftChild) + getVertexAmount(u->rightChild) + 1;
    return u;
}

void split(treeElement* u, int xKeyCut, treeElement*& left, treeElement*& right) {
    if (u == NULL) {
        left = right = NULL;
        return;
    }
    if (xKeyCut < u->xKey) {
        split(u->leftChild, xKeyCut, left, u->leftChild);
        right = u;
    }
    else {
        split(u->rightChild, xKeyCut, u->rightChild, right);
        left = u;
    }
    u->vertexAmount = getVertexAmount(u->leftChild) + getVertexAmount(u->rightChild) + 1;
}

treeElement* pos(treeElement* u, int k) {
    if (u == NULL || k > getVertexAmount(u)) {
        return NULL;
    }
    int leftAmount = getVertexAmount(u->leftChild);
    if (k == leftAmount) {
        return u;
    }
    else if (k < leftAmount) {
        return pos(u->leftChild, k);
    }
    else {
        return pos(u->rightChild, k - leftAmount - 1);
    }
}

int search(int xKeySearch) {
    treeElement* u = root;
    int depth = 1;

    if (u == NULL) {
        return 0;
    }

    while ((xKeySearch < u->xKey && u->leftChild != NULL) || (xKeySearch > u->xKey && u->rightChild != NULL)) {
        if (xKeySearch < u->xKey) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
        depth++;
    }
    if (u->xKey == xKeySearch) {
        return depth;
    }
    return 0;
}

void add(int xKeyAdd, int yKeyAdd) {
    treeElement* left = NULL;
    treeElement* right = NULL;

    split(root, xKeyAdd, left, right);

    treeElement* v = new treeElement;
    v->xKey = xKeyAdd;
    v->yKey = yKeyAdd;
    v->leftChild = NULL;
    v->rightChild = NULL;
    v->vertexAmount = 1;

    left = merge(left, v);
    root = merge(left, right);
}

void del(int xKeyDel) {
    treeElement* left = NULL;
    treeElement* right = NULL;
    treeElement* deletedVertex = NULL;

    split(root, xKeyDel, left, right);
    split(left, xKeyDel - 1, left, deletedVertex);

    root = merge(left, right);
}

void solve() {
    char action;
    int xKey, yKey;

    cin >> action;
    while (action != 'E') {
        cin >> xKey;
        if (action == '+') {
            cin >> yKey;
            if (root == NULL) {
                root = new treeElement;
                root->xKey = xKey;
                root->yKey = yKey;
                root->leftChild = NULL;
                root->rightChild = NULL;
                root->vertexAmount = 1;
            }
            else {
                if (!search(xKey)) {
                    add(xKey, yKey);
                }
            }
        }
        else if (action == '-') {
            del(xKey);
        }
        else if (action == '?') {
            int depth = search(xKey);
            cout << xKey << ' ' << depth << '\n';
        }
        else if (action == 'K') {
            treeElement* finded = pos(root, xKey - 1);
            cout << ((finded == NULL) ? (-1) : (finded->xKey)) << '\n';

        }
        cin >> action;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    solve();

    return 0;
}
