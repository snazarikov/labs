#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <cmath>
#include <vector>

using namespace std;

const int SOME_MAX = 5;

int heightChange[SOME_MAX][SOME_MAX][SOME_MAX];

struct treeElement {
    string data;
    treeElement* leftChild;
    treeElement* rightChild;
    int heightDifference;
};

treeElement* root = NULL;
treeElement* p = NULL;

void fillHeightChangeMatrix() {
    heightChange[1][0][1] = heightChange[1][1][0] = heightChange[2][2][1] =
    heightChange[2][0][1] = heightChange[0][1][2] = heightChange[0][1][0] = 0;

    heightChange[1][2][1] = heightChange[1][1][2] =
    heightChange[2][1][2] = heightChange[0][2][1] = 1;

    heightChange[2][1][0] = heightChange[0][0][1] = -1;
}

void BLR(treeElement* a) {
/*
           a               c(a)
          / \              /  \
         b   S           b    a(c)
        / \      --->   / \    / \
       P   c           P   Q  R   S
          / \
         Q   R
*/
    // before rotation
    treeElement* b = a->leftChild;
    treeElement* c = b->rightChild;
    treeElement* P = b->leftChild;
    treeElement* Q = c->leftChild;
    treeElement* R = c->rightChild;
    treeElement* S = a->rightChild;

    // height calculation before rotation (subtree - const height)
    int heightQ = 0;
    int heightR = c->heightDifference; // heightR = heightQ + c->heightDifference
    int heightP = 1 + max(heightQ, heightR) - b->heightDifference;
    int heightS = 1 + max(heightP, 1 + max(heightQ, heightR)) + a->heightDifference;

    // rotation
    a->rightChild = c;
    string tmpData = a->data;
    a->data = c->data;
    c->data = tmpData;

    b->rightChild = Q;

    c->leftChild = R;
    c->rightChild = S;

    // calculation height difference after rotation
    b->heightDifference = heightQ - heightP;
    c->heightDifference = heightS - heightR;
    a->heightDifference = max(heightR, heightS) - max(heightP, heightQ);
}

void BRR(treeElement* a) {
/*
           a                    c(a)
          / \                   /   \
         P   b                a(c)   b
            / \      --->     / \   / \
           c   S             P   Q R   S
          / \
         Q   R
*/
    // before rotation
    treeElement* b = a->rightChild;
    treeElement* c = b->leftChild;
    treeElement* P = a->leftChild;
    treeElement* Q = c->leftChild;
    treeElement* R = c->rightChild;
    treeElement* S = b->rightChild;

    // height calculation before rotation
    int heightQ = 0;
    int heightR = c->heightDifference; // heightR = heightQ + c->heightDifference
    int heightS = 1 + max(heightQ, heightR) + b->heightDifference;
    int heightP = 1 + max(heightS, 1 + max(heightQ, heightR)) - a->heightDifference;

    // rotation
    a->leftChild = c;
    string tmpData = a->data;
    a->data = c->data;
    c->data = tmpData;

    c->leftChild = P;
    c->rightChild = Q;

    b->leftChild = R;

    // calculation height difference after rotation
    c->heightDifference = heightQ - heightP;
    b->heightDifference = heightS - heightR;
    a->heightDifference = max(heightR, heightS) - max(heightP, heightQ);
}

void SLR(treeElement* a) {
/*
           a               b(a)
          / \              /  \
         b   R            P   a(b)
        / \      --->          / \
       P   Q                  Q   R

*/
    //before rotation
    treeElement* b = a->leftChild;
    treeElement* P = b->leftChild;
    treeElement* Q = b->rightChild;
    treeElement* R = a->rightChild;

    // height calculation before rotation
    int heightP = 0;
    int heightQ = b->heightDifference;
    int heightR = 1 + max(heightQ, heightP) + a->heightDifference;

    // rotation
    a->rightChild = b;
    string tmpData = a->data;
    a->data = b->data;
    b->data = tmpData;

    a->leftChild = P;
    b->leftChild = Q;
    b->rightChild = R;

    // calculation height difference after rotation
    b->heightDifference = heightR - heightQ;
    a->heightDifference = 1 + max(heightQ, heightR) - heightP;
}

void SRR(treeElement* a) {
/*
           a                    b(a)
          / \                   /   \
         P   b                a(b)   R
            / \      --->     / \
           Q   R             P   Q
*/
    // before rotation
    treeElement* b = a->rightChild;
    treeElement* P = a->leftChild;
    treeElement* Q = b->leftChild;
    treeElement* R = b->rightChild;

    // height calculation before rotation
    int heightQ = 0;
    int heightR = b->heightDifference;
    int heightP = 1 + max(heightQ, heightR) - a->heightDifference;

    // rotation
    a->leftChild = b;
    string tmpData = a->data;
    a->data = b->data;
    b->data = tmpData;

    b->leftChild = P;
    b->rightChild = Q;
    a->rightChild = R;

    // calculation height difference after rotation
    b->heightDifference = heightQ - heightP;
    a->heightDifference = heightR - (1 + max(heightQ, heightP));
}

int balance(treeElement* a) {
    if (a->heightDifference == -2) {
        treeElement* b = a->leftChild;
        if (b->heightDifference == 0) {
            SLR(a);
            return 0;
        }
        else if (b->heightDifference == -1) {
            SLR(a);
            return -1;
        }
        else if (b->heightDifference == 1) {
            BLR(a);
            return -1;
        }
    }
    else if (a->heightDifference == 2) {
        treeElement* b = a->rightChild;
        if (b->heightDifference == 0) {
            SRR(a);
            return 0;
        }
        else if (b->heightDifference == -1) {
            BRR(a);
            return -1;
        }
        else if (b->heightDifference == 1) {
            SRR(a);
            return -1;
        }
    }
    else {
        return 0;
    }
}

void fix(treeElement* u, vector<treeElement*>& ancestors, int leftSubtreeHeightChange, int rightSubtreeHeightChange) {
    int currHeightChange = heightChange[(u->heightDifference) + 1][leftSubtreeHeightChange + 1][rightSubtreeHeightChange + 1];
    u->heightDifference += (rightSubtreeHeightChange - leftSubtreeHeightChange);

    if (abs(u->heightDifference) > 1) {
        currHeightChange += balance(u);
    }

    if (!currHeightChange) {
        return;
    }

    if (!ancestors.size()) {
        p = NULL;
    }
    else {
        p = ancestors.back();
        ancestors.pop_back();
    }

    if (p == NULL) {
        return;
    }
    if (p->leftChild == u) {
        fix(p, ancestors, currHeightChange, 0);
    }
    else {
        fix(p, ancestors, 0, currHeightChange);
    }
}

int search(string data) {
    treeElement* u = root;
    int depth = 1;

    if (u == NULL) {
        return 0;
    }

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
        depth++;
    }
    if (u->data == data) {
        return depth;
    }
    return 0;
}

void add(string data) {
    treeElement* u = root;
    vector<treeElement*> ancestors;
    ancestors.push_back(u);

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
        ancestors.push_back(u);
    }
    if (u->data == data) {
        return;
    }

    treeElement* v = new treeElement;
    v->data = data;
    v->heightDifference = 0;
    v->leftChild = NULL;
    v->rightChild = NULL;

    treeElement* parent;
    if (!ancestors.size()) {
        parent = NULL;
    }
    else {
        parent = ancestors.back();
        ancestors.pop_back();
    }

    if (v->data > u->data) {
        u->rightChild = v;
        fix(parent, ancestors, 0, 1);
    }
    else {
        u->leftChild = v;
        fix(parent, ancestors, 1, 0);
    }
}

void del(string data) {
    treeElement* u = root;
    vector<treeElement*> ancestors;

    if (u == NULL) {
        return;
    }

    while ((data < u->data && u->leftChild != NULL) || (data > u->data && u->rightChild != NULL)) {
        ancestors.push_back(u);
        if (data < u->data) {
            u = u->leftChild;
        }
        else {
            u = u->rightChild;
        }
    }

    if (u->data != data) {
        return;
    }

    treeElement* uParent;
    if (!ancestors.size()) {
        uParent = NULL;
    }
    else {
        uParent = ancestors.back();
        ancestors.pop_back();
    }

    // 1
    if (u->leftChild == NULL && u->rightChild == NULL) {
        if (u == root) {
            root = NULL;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = NULL;
            fix(uParent, ancestors, -1, 0);
        }
        else if (uParent->rightChild == u) {
            uParent->rightChild = NULL;
            fix(uParent, ancestors, 0, -1);
        }
    }

    //2
    else if (u->leftChild == NULL) {
        if (u == root) {
            root = u->rightChild;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = u->rightChild;
            fix(uParent, ancestors, -1, 0);
        }
        if (uParent->rightChild == u) {
            uParent->rightChild = u->rightChild;
            fix(uParent, ancestors, 0, -1);
        }
    }
    else if (u->rightChild == NULL) {
        if (u == root) {
            root = u->leftChild;
            return;
        }
        if (uParent->leftChild == u) {
            uParent->leftChild = u->leftChild;
            fix(uParent, ancestors, -1, 0);
        }
        else if (uParent->rightChild == u) {
            uParent->rightChild = u->leftChild;
            fix(uParent, ancestors, 0, -1);
        }
    }

    //3
    else if (u->leftChild != NULL && u->rightChild != NULL){
    	if (uParent != NULL) {
    		ancestors.push_back(uParent);
		}
        treeElement* tmpParent = u;
        treeElement* tmp = u->rightChild;
        while (tmp->leftChild != NULL) {
            ancestors.push_back(tmpParent);
            tmpParent = tmp;
            tmp = tmp->leftChild;
        }
        u->data = tmp->data;

        if (tmpParent->leftChild == tmp) {
            tmpParent->leftChild = tmp->rightChild;
            fix(tmpParent, ancestors, -1, 0);
        }
        else if (tmpParent->rightChild == tmp) {
            tmpParent->rightChild = tmp->rightChild;
            fix(tmpParent, ancestors, 0, -1);
        }
    }
}

void printTree(treeElement* root, int depth, int side) {
    // 1 - left 2 - right 0 - start root
    if (root != NULL) {
        if (root->leftChild != NULL) {
            printTree(root->leftChild, depth + 1, 1);
        }
        cout << root->data << "(" << depth << "," << side << ") ";
        if (root->rightChild != NULL) {
            printTree(root->rightChild, depth + 1, 2);
        }
    }
}

void solve() {
    char action;

    cin >> action;
    while (action != 'E') {
        string word;
        cin >> word;
        if (action == '+') {
            if (root == NULL) {
                root = new treeElement;
                root->data = word;
                root->leftChild = NULL;
                root->rightChild = NULL;
                root->heightDifference = 0;
            }
            else {
                add(word);
            }
        }
        else if (action == '-') {
            del(word);
        }
        else if (action == '?') {
//            printTree(root, 1, 0);
            int depth = search(word);
            cout << word << ' ' << depth << '\n';
        }
        cin >> action;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

	fillHeightChangeMatrix();
    solve();

    return 0;
}
