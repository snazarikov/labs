#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iomanip>

using namespace std;

const int NMAX = 1024;

class MyPair {
public:
    int first;
    int second;

    MyPair(int f = 0, int s = 0) : first(f), second(s) {}

    MyPair& operator+=(const MyPair& obj) {
        first += obj.first;
        second += obj.second;
        return *this;
    }

    MyPair& operator-=(const MyPair& obj) {
        first -= obj.first;
        second -= obj.second;
        return *this;
    }

    MyPair& operator=(const MyPair& obj){
        if (this == &obj) {
            return *this;
        }
        first = obj.first;
        second = obj.second;
        return *this;
    }

    MyPair operator+(const MyPair& obj){
        MyPair tmp(*this);
        tmp += obj;
        return tmp;
    }

    bool operator==(const MyPair& obj){
        if(first == obj.first && second == obj.second) {
            return true;
        }
        return false;
    }
};

MyPair PLUS_LETTER_A = MyPair(1, 0);
MyPair PLUS_LETTER_B = MyPair(0, 1);
MyPair A_REPLACE_B = MyPair(1, -1);
MyPair B_REPLACE_A = MyPair(-1, 1);
MyPair ZERO = MyPair(0, 0);

MyPair segmentTree[NMAX * 2 + 5][NMAX * 2 + 5];

void change(int ii, int jj, MyPair addition) {
    ii += (NMAX - 1);
    jj += (NMAX - 1);

    int startJ = jj;

    segmentTree[ii][jj] = ZERO;
    if (addition == A_REPLACE_B) {
        segmentTree[ii][jj] = PLUS_LETTER_B;
    }
    else if (addition == B_REPLACE_A) {
        segmentTree[ii][jj] = PLUS_LETTER_A;
    }

    while (ii) {
        jj = startJ;
        while (jj) {
            segmentTree[ii][jj] += addition;
            jj /= 2;
        }
        ii /= 2;
    }
}

MyPair getOneDimSum(int ii, int jj) {
    MyPair res = segmentTree[ii][jj];
    while (ii) {
        if (ii % 2) {
            res += segmentTree[ii - 1][jj];
        }
        ii /= 2;
    }
    return res;
}

MyPair getSum(int ii, int jj) {
    ii += (NMAX - 1);
    jj += (NMAX - 1);

    MyPair res = getOneDimSum(ii, jj);

    while (jj) {
        if (jj % 2) {
            res += getOneDimSum(ii, jj - 1);
        }
        jj /= 2;
    }
    return res;
}

MyPair getSubmatrixSum(int a, int b, int c, int d) {
    MyPair res = getSum(c, d);
    if (a - 1 > 0) {
        res -= getSum(a - 1, d);
    }
    if (b - 1 > 0) {
        res -= getSum(c, b - 1);
    }
    if (a - 1 > 0 && b - 1 > 0) {
        res += getSum(a - 1, b - 1);
    }
    return res;
}

void fillSegmentTree() {
    for (int i = 1; i <= NMAX; ++i) {
        for (int j = 1; j <= NMAX; ++j) {
            if (i % 2) {
                if (j % 2) {
                    change(i, j, PLUS_LETTER_A);
                }
                else {
                    change(i, j, PLUS_LETTER_B);
                }
            }
            else {
                if (j % 2) {
                    change(i, j, PLUS_LETTER_B);
                }
                else {
                    change(i, j, PLUS_LETTER_A);
                }
            }
        }
    }
}

void printSegmentTree(int nn, int mm) {
    for (int i = 1; i < nn; ++i) {
        for (int j = 1; j < mm; ++j) {
            cout << setw(3) << "(" << segmentTree[i][j].first << ' ' << segmentTree[i][j].second << ") ";
        }
        cout << endl;
    }
}

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);
    freopen("election.in", "r", stdin);
    freopen("election.out", "w", stdout);

    ios_base::sync_with_stdio(false);

    fillSegmentTree();

    int n;
    cin >> n;

    int a, b, c, d;
    char command;
    for (int i = 0; i < n; ++i) {
        cin >> command >> a >> b;
        if (command == 'A') {
            if (segmentTree[NMAX - 1 + a][NMAX - 1 + b] == PLUS_LETTER_B) {
                change(a, b, A_REPLACE_B);
            }
        }
        else if (command == 'B') {
            if (segmentTree[NMAX - 1 + a][NMAX - 1 + b] == PLUS_LETTER_A) {
                change(a, b, B_REPLACE_A);
            }
        }
        else if (command == 'R') {
            cin >> c >> d;
            MyPair res = getSubmatrixSum(a, b, c, d);
            cout << res.first << ' ' << res.second << endl;
        }

//        printSegmentTree(NMAX * 2, NMAX * 2);
//        cout << endl;
    }

    return 0;
}



