#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

const int MAX_ELEMENT_AMOUNT = 1e5;

int data[MAX_ELEMENT_AMOUNT + 5];
int treePart[MAX_ELEMENT_AMOUNT + 5];

void change(int number, int addition) {
    data[number] += addition;
    while (number <= MAX_ELEMENT_AMOUNT) {
        treePart[number] += addition;
        number = (number | (number + 1));
    }
}

int calcSum(int amount) {
    int sum = 0;
    while (amount > 0) {
        sum += treePart[amount];
        amount =(amount & (amount + 1)) - 1;
    }
    return sum;
}

int getSum(int start, int finish) {
    return calcSum(finish) - calcSum(start) + data[start];
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    char symbol;
    int a, b;

    while (1) {
        cin >> symbol;
        if (symbol == 'E') {
            break;
        }
        cin >> a >> b;

        if (symbol == 'C') {
            change(a, b);
        }
        if (symbol == 'S') {
            cout << getSum(a, b) << '\n';
        }
    }

    return 0;
}


