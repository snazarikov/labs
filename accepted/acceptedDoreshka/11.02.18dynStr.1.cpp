#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>

using namespace std;

const int NMAX = 1e3 + 5;
const int EMPTY = -1;

string first, second;

int table[NMAX][NMAX];

int dynamic(int prefixA, int prefixB) {
//    cout << "CHEBYREK " << prefixA << " " << prefixB << endl;
    if (prefixA == first.size() && prefixB == second.size()) {
        return 0;
    }
    if ((prefixA > first.size() || prefixB > second.size()) ) {
        return 1e9;
    }
    if (table[prefixA][prefixB] != 1e9) {
//        cout << "LOL " << prefixA << " " << prefixB << endl;
        return table[prefixA][prefixB];
    }

    if (first[prefixA] == second[prefixB]) {
//        cout << "LUL " << prefixA << " " << prefixB << endl;
        table[prefixA][prefixB] = min(table[prefixA][prefixB], dynamic(prefixA + 1, prefixB + 1));
    }
    else {
//        cout << "KEK " << prefixA << " " << prefixB << endl;
        table[prefixA][prefixB] = min(table[prefixA][prefixB], min(dynamic(prefixA + 1, prefixB + 1) + 1, min(dynamic(prefixA, prefixB + 1) + 1, dynamic(prefixA + 1, prefixB) + 1)));
    }

    return table[prefixA][prefixB];
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("jumps.in", "r", stdin);
//    freopen ("jumps.out", "w", stdout);

    cin >> first >> second;
//    cout << first << endl << second << endl;

//    memset(table, EMPTY, sizeof(table));

    for (int i = 0; i < NMAX; ++i) {
        for (int j = 0; j < NMAX; ++j) {
            table[i][j] = 1e9;
        }
    }

//    cout << "size " << first.size() << " " << second.size() << endl;

    cout << dynamic(0, 0);

    return 0;
}
