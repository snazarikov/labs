#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <bitset>

using namespace std;

const int MAX_LENGTH = 2005;
const int NMAX = 105;
const int MAX_AMOUNT = 1030;

const int NORTH = 0;
const int EAST = 1;
const int SOUTH = 2;
const int WEST = 3;

int m, n;

struct point {
    int x;
    int y;
    point(int xx = 0, int yy = 0) : x(xx), y(yy) {}
};

struct brick {
    point leftDown;
    point rightUp;
    brick(point p1 = point(0, 0), point p2 = point(0, 0)) : leftDown(p1), rightUp(p2) {}
};

brick upperLayer[NMAX];
brick lowerLayer[NMAX];

int kakTebeTakoeElonMusk; // mask
bool usedMask[MAX_AMOUNT];

void getData() {
    cin >> m >> n;
    int x1, y1, x2, y2;
    for (int i = 0; i < m; ++i) {
        cin >> x1 >> y1 >> x2 >> y2;
        lowerLayer[i] = brick(point(x1 * 2, y1 * 2), point(x2 * 2, y2 * 2));
    }

    for (int i = 0; i < n; ++i) {
        cin >> x1 >> y1 >> x2 >> y2;
        upperLayer[i * 4] = brick(point(x1 * 2, y1 * 2), point((x2 * 2 + x1 * 2) / 2, y2 * 2));
        upperLayer[i * 4 + 1] = brick(point((x2 * 2 + x1 * 2) / 2, y1 * 2), point(x2 * 2, y2 * 2));
        upperLayer[i * 4 + 2] = brick(point(x1 * 2, (y2 * 2 + y1 * 2) / 2), point(x2 * 2, y2 * 2));
        upperLayer[i * 4 + 3] = brick(point(x1 * 2, y1 * 2), point(x2 * 2, (y2 * 2 + y1 * 2) / 2));
    }
}

void printData() {
    cout << m << ' ' << n << '\n';
    cout << "Lower layer\n";
    for (int i = 0; i < m; ++i) {
        cout << lowerLayer[i].leftDown.x << ' ' << lowerLayer[i].leftDown.y << ' ' << lowerLayer[i].rightUp.x << ' ' << lowerLayer[i].rightUp.y << '\n';
    }

    cout << "Upper layer\n";
    for (int i = 0; i < 4 * n; ++i) {
        cout << upperLayer[i].leftDown.x << ' ' << upperLayer[i].leftDown.y << ' ' << upperLayer[i].rightUp.x << ' ' << upperLayer[i].rightUp.y << '\n';
    }
}

bool isIntersect(brick& brickOne, brick& brickTwo) {
    int maxLeft = max(brickOne.leftDown.x, brickTwo.leftDown.x);
    int minRight = min(brickOne.rightUp.x, brickTwo.rightUp.x);
    int minTop = min(brickOne.rightUp.y, brickTwo.rightUp.y);
    int maxDown = max(brickOne.leftDown.y, brickTwo.leftDown.y);

    if (maxLeft >= minRight || maxDown >= minTop) {
        return false;
    }
    return true;
}

bool isPossiblePullOut(int brickNumber) {
    brick possibleDirection[4];
    for (int i = NORTH; i <= WEST; ++i) {
        possibleDirection[i] = lowerLayer[brickNumber];
    }
    possibleDirection[NORTH].rightUp.y = MAX_LENGTH;
    possibleDirection[EAST].rightUp.x = MAX_LENGTH;
    possibleDirection[SOUTH].leftDown.y = -MAX_LENGTH;
    possibleDirection[WEST].leftDown.x = -MAX_LENGTH;

    for (int i = NORTH; i <= WEST; ++i) {
        bool flag = false;
        for (int j = 0; j < m; ++j) {
            if (j != brickNumber && !(kakTebeTakoeElonMusk & (1 << j)) && isIntersect(possibleDirection[i], lowerLayer[j])) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            return true;
        }
    }
    return false;
}

bool isSmthDropped() {
    for (int i = 0; i < 4 * n; ++i) {
        bool flag = false;
        for (int j = 0; j < m; ++j) {
            if (!(kakTebeTakoeElonMusk & (1 << j)) && isIntersect(upperLayer[i], lowerLayer[j])) {
                flag = true;
            }
        }

        if (!flag) {
            return true;
        }
    }

    return false;
}

int res = 0;

void etoReshitVseMoiProblemi(int amount) {
    if (amount > res) {
        res = amount;
    }

    if (!usedMask[kakTebeTakoeElonMusk]) {
        usedMask[kakTebeTakoeElonMusk] = true;
        for (int i = 0; i < m; ++i) {
            if (!(kakTebeTakoeElonMusk & (1 << i)) && isPossiblePullOut(i)) {
                kakTebeTakoeElonMusk = (kakTebeTakoeElonMusk | (1 << i));
                if (!isSmthDropped()) {
                    etoReshitVseMoiProblemi(amount + 1);
                }
                kakTebeTakoeElonMusk = (kakTebeTakoeElonMusk & ~(1 << i));
            }
        }
    }
}

int main(void) {
//    freopen("input.txt", "r", stdin);
    freopen("bricks.in", "r", stdin);
    freopen("bricks.out", "w", stdout);

    getData();
//    printData();

    etoReshitVseMoiProblemi(0);

    cout << res << endl;

    return 0;
}
