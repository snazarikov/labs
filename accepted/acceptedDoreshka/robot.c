#include <stdio.h>
#include <math.h>
#include <string.h>
#define NMAX 10005

char F[NMAX];
char S[NMAX];

int maximum (int firstNumeral, int secondNumeral) {
	if (firstNumeral > secondNumeral) return firstNumeral;
	else return secondNumeral;
}

int minimum (int firstNumeral, int secondNumeral) {
	if (firstNumeral < secondNumeral) return firstNumeral;
	else return secondNumeral;
}

main (void) {
	//freopen("inputE.txt", "r", stdin);
	freopen("robot.in", "r", stdin);
	freopen("robot.out", "w", stdout);
	int i, flag = 0, wholeF, wholeS, lengthF, lengthS, pointF = -1, pointS = -1, maxLength, minLength;
	char symbol;
	
	gets(F);
	gets(S);
	
	if (F[0] == '-' && S[0] == '-') flag = -1;
	else if (F[0] != '-' && S[0] == '-') {
		printf(">");
		return 0;
	} 
	else if (F[0] == '-' && S[0] != '-') {
		printf("<");
		return 0;
	}
	
	lengthF = strlen(F);	
	for (i=0; i<lengthF; i++) {
		if (F[i] == '.') {
			pointF = i;
			break;
		}
	}
	
	lengthS = strlen(S);
	for (i=0; i<lengthS; i++) {
		if (S[i] == '.') {
			pointS = i;
			break;
		}
	}
	
	
	
	if (pointF == -1) wholeF = lengthF;
	else wholeF = pointF;
	if (pointS == -1) wholeS = lengthS;
	else wholeS = pointS;
	
	if (flag == 0 && wholeF>wholeS) {
		printf(">");
		return 0;
	}
	else if (flag == 0 && wholeF<wholeS) {
		printf("<");
		return 0;
	}
	else if (flag == -1 && wholeF>wholeS) {
		printf("<");
		return 0;
	}
	else if (flag == -1 && wholeF<wholeS) {
		printf(">");
		return 0;
	}
	
	maxLength = maximum(lengthF, lengthS);
	minLength = minimum(lengthF, lengthS);
	
	if (wholeF==wholeS) {
		for (i=0; i<wholeF; i++) {
			if (flag == 0 && F[i]<S[i]) {
				printf("<");
				return 0;
			}
			else if (flag == 0 && F[i]>S[i]) {
				printf(">");
				return 0;
			}
			else if (flag == -1 && F[i]<S[i]) {
				printf(">");
				return 0;
			}
			else if (flag == -1 && F[i]>S[i]) {
				printf("<");
				return 0;
			}
		}
		
		for (i=minLength; i<maxLength; i++) {
			if (S[i] == '0') S[i]-=48;
			if (F[i] == '0') F[i]-=48;
		}
		
		for (i=wholeF+1; i<maxLength; i++) {
			if (flag == 0 && F[i]<S[i]) {
				printf("<");
				return 0;
			}
			else if (flag == 0 && F[i]>S[i]) {
				printf(">");
				return 0;
			}
			else if (flag == -1 && F[i]<S[i]) {
				printf(">");
				return 0;
			}
			else if (flag == -1 && F[i]>S[i]) {
				printf("<");
				return 0;
			}
		}
		printf("=");
	}
	
//	printf("%d %d", pointF, pointS);
//	printf("\n%d %d", wholeF, wholeS);
	
	return 0;
}
