#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iomanip>

using namespace std;

const int NMAX = 105;

int n;

double oddFactorial[NMAX];
double evenFactorial[NMAX];

void fillOddFactorial() {
    oddFactorial[1] = 1;
    for (int i = 2; i <= 98; ++i) {
        oddFactorial[i] = ((i % 2) ? (oddFactorial[i - 1]) : (oddFactorial[i - 1] * (double)i));
    }
}

void fillEvenFactorial() {
    evenFactorial[1] = 1;
    for (int i = 2; i <= 98; ++i) {
        evenFactorial[i] = ((i % 2) ? (evenFactorial[i - 1] * (double)i) : (evenFactorial[i - 1]));
    }
}

int main(){
//	freopen("input.txt", "r", stdin);

	freopen("knots.in", "r", stdin);
	freopen("knots.out", "w", stdout);

    fillEvenFactorial();
    fillOddFactorial();

    while (cin >> n) {
        if (n == 2) {
            cout << 1 << endl;
            continue;
        }
        if (n == 100) {
            cout << setprecision(5) << fixed << oddFactorial[n - 4] * 98 / evenFactorial[n - 3] / 99 << endl;
            continue;
        }
        cout << setprecision(5) << fixed << oddFactorial[n - 2] / evenFactorial[n - 1] << endl;
    }

	return 0;
}

