#include <stdio.h>
#define NMAX 20

int coins[NMAX];
int countG, countW, result = 0;

void SOLVE(int price, int number){ 
	int i;
	if (price == 0) result = 1;
	for (i=number; i<countW+countG; i++) SOLVE(price-coins[i], i+1);
}

int main (void) {
	freopen ("input.txt", "r", stdin);
	freopen ("output.txt", "w", stdout);
	int i, j, tmp, price;
	
	scanf("%d %d %d", &countG, &countW, &price);
	for (i=0; i<countG+countW; i++) scanf("%d", &coins[i]);
	
	for (i=countG; i<countG+countW; i++) coins[i] = -coins[i];
	for (i=0; i<countG+countW-1; i++) {
		for (j=i+1; j<countG+countW; j++) {
			if (coins[i]<coins[j]) {
				tmp = coins[i];
				coins[i] = coins[j];
				coins[j] = tmp;
			}
		}
	}
	SOLVE(price, 0);
	
	if (result == 1) printf("Yes");
	else printf("No");
	
	return 0;
}
