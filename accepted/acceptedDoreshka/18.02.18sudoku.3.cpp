#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <ctime>
#include <cstring>

using namespace std;

const int NMAX = 10;

//clock_t tStart = clock();

int table[NMAX][NMAX];

int row[NMAX][NMAX];
int column[NMAX][NMAX];
int square[NMAX][NMAX];

void getData() {
    char symbol;
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            scanf("%c", &symbol);
            table[i][j] = symbol - '0';
            if (table[i][j]) {
                row[i][table[i][j]] = 1;
                column[j][table[i][j]] = 1;
                int line = (i / 3) * 3;
                int clmn = (j / 3);
                square[line + clmn][table[i][j]] = 1;
            }
        }
        scanf("\n");
    }
}

void printData() {
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            cout << table[i][j];
        }
        cout << '\n';
    }
//    cout << '\n';
}

void backtracking(int ii, int jj) {
//    cout << "IT " << ii << ' ' << jj << '\n';
    if (ii > 8) {
//        cout << "GG\n";
        printData();
//        printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
        exit(0);
    }

    int jjj;
    int iii;

    if (jj + 1 > 8) {
        iii = ii + 1;
        jjj = 0;
    }
    else {
        jjj = jj + 1;
        iii = ii;
    }

    if (table[ii][jj]) {
        backtracking(iii, jjj);
    }
    else {
        for (int i = 1; i < 10; ++i) {
            int line = (ii / 3) * 3;
            int clmn = (jj / 3);
            if (!row[ii][i] && !column[jj][i] && !square[line + clmn][i]) {
                row[ii][i] = 1;
                column[jj][i] = 1;
                square[line + clmn][i] = 1;
                table[ii][jj] = i;
                backtracking(iii, jjj);
                row[ii][i] = 0;
                column[jj][i] = 0;
                square[line + clmn][i] = 0;
                table[ii][jj] = 0;
            }
        }
    }
}

void printMatrix(int A[][NMAX]) {
    for (int i = 0; i < 9; ++i) {
        for (int j = 1; j < 10; ++j) {
            cout << A[i][j] << ' ';
        }
        cout << '\n';
    }
    cout << '\n';
}

int main() {
    freopen ("input.txt", "r", stdin);
    freopen ("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();
//    printData();

//    printMatrix(row);
//    printMatrix(column);
//    printMatrix(square);

    backtracking(0, 0);

    return 0;
}

