#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

using namespace std;

#define VERTICES_AMOUNT 500
#define EDGES_AMOUNT 125000
#define INF 1e9

int verticesAmount, edgesAmount, startVertex, finishVertex;

struct graphElem {
    int nextVertex;
    int weight;
};

vector<graphElem> g[VERTICES_AMOUNT + 1];
vector<int> isMined(VERTICES_AMOUNT + 1, INF);

void getData() {
    cin >> verticesAmount >> edgesAmount >> startVertex >> finishVertex;

    for (int i = 0; i < verticesAmount; ++i) {
        cin >> isMined[i];
        if (!isMined[i]) {
            isMined[i] = INF;
        }
    }

    for (int i = 0; i < edgesAmount; i++) {
        int u, v, len;
        cin >> u >> v >> len;
        struct graphElem e;
        e.nextVertex = v;
        e.weight = len;
        g[u].push_back(e);
        e.nextVertex = u;
        e.weight = len;
        g[v].push_back(e);
    }
}

void printData() {
    cout << "** GRAPH **\n";
    cout << verticesAmount << " " << edgesAmount << " " << startVertex << " " << finishVertex << "\n";

    for (int i = 0; i < verticesAmount; ++i) {
        cout << isMined[i] << "\n";
    }

    for (int i = 1; i <= verticesAmount; ++i) {
        for (int j = 0; j < g[i].size(); ++j) {
            cout << i << " " << g[i][j].nextVertex << " " << g[i][j].weight << "\n";
        }
    }

    cout << "** GRAPH **\n";
    cout << "\n";
}


void dijkstra(int stVertex) {
    vector<int> d (verticesAmount + 1, INF);
    vector<int> p (verticesAmount + 1);
    vector<char> isUsed (verticesAmount + 1);

	d[stVertex] = 0;

	for (int i = 0; i < verticesAmount; ++i) {
		int currVertex = -1;

		for (int j = 1; j <= verticesAmount; ++j) {
            if (!isUsed[j] && (currVertex == -1 || d[j] < d[currVertex])) {
                currVertex = j;
			}
		}
        if (d[currVertex] == INF) {
            break;
		}
		isUsed[currVertex] = true;

		for (int j = 0; j < g[currVertex].size(); ++j) {
			int to = g[currVertex][j].nextVertex;
			int len = g[currVertex][j].weight;

//            cout << currVertex << " " << d[currVertex] << " " << d[currVertex] + len << " " << d[to] << " " << isMined[to - 1] << "\n";
			if (d[currVertex] + len < d[to] && d[currVertex] + len < isMined[to - 1]) {
//                cout << "KEK " << currVertex << " " << d[currVertex] << " " << d[currVertex] + len << " " << d[to] << " " << isMined[to - 1] << "\n";
				d[to] = d[currVertex] + len;
				p[to] = currVertex;
			}
		}
	}

//	for (int i = 1; i < d.size(); ++i) {
//        cout << i << " " << d[i] << "\n";
//    }
//    cout << "\n";
//
//    vector<int> path;
//    for (int v=finishVertex; v!=stVertex; v=p[v])
//        path.push_back (v);
//    path.push_back (stVertex);
//    reverse (path.begin(), path.end());
//    for (vector<int>::iterator i = path.begin(); i != path.end(); i++) {
//        cout << *i << " ";
//    }


    if (d[finishVertex] == INF) {
        cout << "-1";
    }
    else {
        cout << d[finishVertex];
    }
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    freopen ("bombs.in", "r", stdin);
    freopen ("bombs.out", "w", stdout);
    getData();
//    printData();
    dijkstra(startVertex);
    return 0;
}
