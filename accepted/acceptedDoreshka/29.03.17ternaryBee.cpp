#include <cstdio>
#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

const int NMAX = 1000;

struct point {
    double x;
    double y;
};

point start, finish;
point p1, p2; // start stream, finish stream

double calcSmth(point p1, point p2) {
    return sqrt((((double)p2.x - (double)p1.x) * ((double)p2.x - (double)p1.x) + ((double)p2.y - (double)p1.y) * ((double)p2.y - (double)p1.y)));
}

double value(double t) {
    double x = p1.x + (p2.x - p1.x) * t;
    double y = p1.y + (p2.y - p1.y) * t;
    point p = {x, y};

    return calcSmth(p, start) + calcSmth(p, finish);
}

double ternarySearch(double left, double right) {
	double a, b;

	while (1) {
		a = left + (right - left) / 3;
		b = right - (right - left) / 3;

		if (a == left || b == right) {
			return value((left + right) / 2);
		}

		if (value(b) > value(a)) {
            right = b;
		}
		else {
            left = a;
		}
	}
}

int main(void) {
//	freopen("input.txt", "r", stdin);
	freopen("bee.in", "r", stdin);
	freopen("bee.out", "w", stdout);

    cin >> start.x >> start.y;
    cin >> p1.x >> p1.y >> p2.x >> p2.y;
    cin >> finish.x >> finish.y;

    cout << fixed << setprecision(3) << ternarySearch(0, 1);

	return 0;
}
