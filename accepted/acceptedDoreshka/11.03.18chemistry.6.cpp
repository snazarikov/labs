#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

const int NMAX = 3005;

int r, p, kostil = 0, zeroPos;

struct Atom {
    char firstLetter;
    char secondLetter;
    int pos;
    int amountStart;
    int mult;
    int amountFull;
};

string equation[NMAX];
int coef[NMAX];
vector<Atom> elementToAtom[NMAX];
vector<Atom> bracketLevel[NMAX];

void getData() {
    cin >> r >> p;
    for (int i = 0; i < r; ++i) {
        cin >> equation[i];
    }
    for (int i = 0; i < p; ++i) {
        cin >> equation[i + r];
    }
    for (int i = 0; i < r; ++i) {
        cin >> coef[i];
        if (coef[i] == 0) {
            zeroPos = i;
        }
    }
    for (int i = 0; i < p; ++i) {
        cin >> coef[i + r];
        if (coef[i + r] == 0) {
            zeroPos = i + r;
        }
    }
}

int getNum(string& element, int& pos) {
    vector<int> num;

    while (element[pos] >= '0' && element[pos] <= '9') {
        num.push_back(element[pos++] - '0');
    }

    int amount = 0, mult = 1, flag = 0;
    while (!num.empty()) {
        flag = 1;
        amount += (num.back() * mult);
        mult *= 10;
        num.pop_back();
    }

    if (!flag) {
        amount = 1;
    }

    return amount;
}

Atom getAtom(string& element, int& pos) {
    Atom atom = {0, 0, 0, 0};
    if (element[pos] >= 'A' && element[pos] <= 'Z') {
        atom.pos = pos;
        atom.firstLetter = element[pos++];
        if (element[pos] >= 'a' && element[pos] <= 'z') {
            atom.secondLetter = element[pos++];
        }

        atom.amountStart = getNum(element, pos);
        atom.mult = 1;
    }

    return atom;
}

int findUniqueAtom(vector<Atom>& atomVect, Atom atom) {
    for (int i = 0; i < atomVect.size(); ++i) {
        if (atomVect[i].firstLetter == atom.firstLetter && atomVect[i].secondLetter == atom.secondLetter && atomVect[i].pos == atom.pos) {
            return i;
        }
    }
    return -1;
}

void parser(string& element, int &pos, int level, vector<Atom>* bracketLevel, vector<Atom>& elementToAtom) {
    Atom atom;

    while (pos < element.size()) {

        if (element[pos] == '(') {
            pos++;
            parser(element, pos, level + 1, bracketLevel, elementToAtom);
        }

        if (element[pos] == ')') {
            pos++;
            kostil = 1;
            int multiplier = getNum(element, pos);
            while (!bracketLevel[level].empty()) {
                int ind = findUniqueAtom(elementToAtom, bracketLevel[level].back());
                elementToAtom[ind].mult *= multiplier;
                bracketLevel[level].pop_back();
            }
            return;
        }

        if (kostil) {
            kostil = 0;
            continue;
        }

        atom = getAtom(element, pos);
        elementToAtom.push_back(atom);

        bracketLevel[level].push_back(atom);
        if (level > 1) {
            for (int i = 1; i < level; ++i) {
                bracketLevel[i].push_back(atom);
            }
        }
    }
}

int findAtom(vector<Atom>& atomVect, Atom atom) {
    for (int i = 0; i < atomVect.size(); ++i) {
        if (atomVect[i].firstLetter == atom.firstLetter && atomVect[i].secondLetter == atom.secondLetter) {
            return i;
        }
    }
    return -1;
}

void fillAtomSum(vector<Atom>& atomSum, int a, int b) {
    for (int i = a; i < a + b; ++i) {
        for (int j = 0; j < elementToAtom[i].size(); ++j) {
            if (i != zeroPos) {
                int ind = findAtom(atomSum, elementToAtom[i][j]);
                if (ind == -1) {
                    atomSum.push_back(elementToAtom[i][j]);
                    atomSum[atomSum.size() - 1].amountFull = atomSum[atomSum.size() - 1].amountStart * atomSum[atomSum.size() - 1].mult * coef[i];
                }
                else {
                    atomSum[ind].amountFull += (elementToAtom[i][j].amountStart * elementToAtom[i][j].mult * coef[i]);
                }
            }
        }
    }
}

void constructVector(vector<Atom>& zeroElement, vector<Atom>& atomSum, vector<Atom>& healedSide, int k) {
    for (int i = 0; i < zeroElement.size(); ++i) {
        int ind = findAtom(atomSum, zeroElement[i]);
        healedSide.push_back(zeroElement[i]);
        if (ind == -1) {
            healedSide[healedSide.size() - 1].amountFull *= k;
        }
        else {
            healedSide[healedSide.size() - 1].amountFull *= k;
            healedSide[healedSide.size() - 1].amountFull += atomSum[ind].amountFull;
        }
    }
    for (int i = 0; i < atomSum.size(); ++i) {
        int ind = findAtom(healedSide, atomSum[i]);
        if (ind == -1) {
            healedSide.push_back(atomSum[i]);;
        }
    }
}

void getAnswer(vector<Atom>& zeroElement, vector<Atom>& atomSumFull, vector<Atom>& atomSumBad, int k) {
    vector<Atom> healedSide;
    constructVector(zeroElement, atomSumBad, healedSide, k);
    int flag = 0;
    for (int i = 0; i < atomSumFull.size(); ++i) {
        int ind = findAtom(healedSide, atomSumFull[i]);
        if (ind == -1) {
            cout << '0';
            exit(0);
        }
        if (atomSumFull[i].amountFull < healedSide[ind].amountFull) {
            cout << '0';
            exit(0);
        }
        else if (atomSumFull[i].amountFull != healedSide[ind].amountFull) {
            flag = 1;
            break;
        }
    }
    if (!flag) {
        cout << k;
        exit(0);
    }
}

void solve() {
    vector<Atom> atomSumLeft;
    vector<Atom> atomSumRight;

    fillAtomSum(atomSumLeft, 0, r);
    fillAtomSum(atomSumRight, r, p);

    vector<Atom> zeroElement;

    for (int i = 0; i < elementToAtom[zeroPos].size(); ++i) {
        int ind = findAtom(zeroElement, elementToAtom[zeroPos][i]);
        if (ind == -1) {
            zeroElement.push_back(elementToAtom[zeroPos][i]);
            zeroElement[zeroElement.size() - 1].amountFull = zeroElement[zeroElement.size() - 1].amountStart * zeroElement[zeroElement.size() - 1].mult;
        }
        else {
            zeroElement[ind].amountFull += (elementToAtom[zeroPos][i].amountStart * elementToAtom[zeroPos][i].mult);
        }
    }

    for (int k = 1; k <= 32767; ++k) {
        if (zeroPos < r) {
            getAnswer(zeroElement, atomSumRight, atomSumLeft, k);
        }
        else {
            getAnswer(zeroElement, atomSumLeft, atomSumRight, k);
        }
    }
}

int main() {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    freopen("chemistry.in", "r", stdin);
    freopen("chemistry.out", "w", stdout);

    getData();
    int position;

    for (int i = 0; i < r; ++i) {
        position = 0;
        for (int j = 0; j < NMAX; ++j) {
            bracketLevel[j].clear();
        }
        parser(equation[i], position, 0, bracketLevel, elementToAtom[i]);
    }

    for (int i = 0; i < p; ++i) {
        position = 0;
        for (int j = 0; j < NMAX; ++j) {
            bracketLevel[j].clear();
        }
        parser(equation[i + r], position, 0, bracketLevel, elementToAtom[i + r]);
    }

    solve();

    return 0;
}


