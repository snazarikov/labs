#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#define NAME_LENGTH_MAX 35
#define NMAX 105
#define MMAX 25

char name[NMAX][NAME_LENGTH_MAX];
int time[NMAX][MMAX];
int fine[NMAX][MMAX];
int accepted[NMAX][MMAX];
int acceptedFinal[NMAX];
int timeFinal[NMAX];

int teamCount, problemCount, playerInTeamCount, tryCount;

void getName() {
	scanf("%d %d %d\n", &teamCount, &problemCount, &playerInTeamCount);
	for (int i = 0; i < teamCount; i++) {
		gets(name[i]);
	}
}

int rotate(char* A, char* B){
	char *p1, *p2;
	char t1, t2;
	
	for (p1 = A, p2 = B; *p1 || *p2; p1++, p2++){
		t1 = *p1;
		t2 = *p2;
		
		if (t1 < '�') {
			t1 = t1 - '�' + '�';
		}
		if (t2 < '�') {
			t2 = t2 - '�' + '�';
		}
		
		if  (t1 == 0) {
			return 1;
		}
		if (t2 == 0) {
			return 0;
		}
		
		if (t1 < t2) {
			return 1;
		}
		else if (t1 > t2) {
			return 0;
		}
	}
	
	return 0;
}

void SOLVE() {
	int i, j, timeFine, currProblem, pos, tmp1, tmp2, problemCountCopy, teamCountCopy;
	char result;
	char S[NAME_LENGTH_MAX];
	
	problemCountCopy = problemCount;
	teamCountCopy = teamCount;
	
	for (i = 0; i < teamCount; i++) {
		for (j = 0; j < problemCount; j++) {
			time[i][j] = INT_MAX;
		}
	}
	
	scanf("%d\n", &tryCount);
	
	for (i = 0; i < tryCount; i++) {
		scanf("%s", &S);
		scanf(" %d %d %c\n", &timeFine, &currProblem, &result);
		
		for (j = 0; j < teamCount; j++) {
			if (strncmp(S, name[j], strlen(name[j])) == 0) {
				pos = j;
				break;
			}
		}
		
		if (result == 'A' && timeFine < time[pos][currProblem - 1]) {
			time[pos][currProblem - 1] = timeFine;
			accepted[pos][currProblem - 1] = 1;
		}		
	}
	
	freopen("contest.in", "r", stdin);
	getName();
	
	scanf("%d\n", &tryCount);
	
	for (i = 0; i < tryCount; i++) {
		scanf("%s", &S);
		scanf(" %d %d %c\n", &timeFine, &currProblem, &result);
		
		for (j = 0; j < teamCount; j++) {
			if (strncmp(S, name[j], strlen(name[j])) == 0) {
				pos = j;
				break;
			}
		}
		
		if (result == 'R' && timeFine < INT_MAX) {
			fine[pos][currProblem - 1] += 20;
		}
	}
	
	
	
	for (i = 0; i < teamCount; i++) {
		for (j = 0; j < problemCount; j++) {
			//printf("%d %d %d %d\n", teamCount, problemCount, i, j);
			if (time[i][j] != INT_MAX) {
				timeFinal[i] += (time[i][j] + fine[i][j]);
			}
			acceptedFinal[i] += accepted[i][j];		
		}
	}

	for (i = 0; i < teamCount - 1; i++) {
		for (j = i + 1; j < teamCount; j++) {
			if (acceptedFinal[i] < acceptedFinal[j]) {
				tmp1 = acceptedFinal[i];
	    	    acceptedFinal[i] = acceptedFinal[j];
    		    acceptedFinal[j] = tmp1;
        
	        	tmp2 = timeFinal[i];
    	    	timeFinal[i] = timeFinal[j];
        		timeFinal[j] = tmp2;
        
    	    	strcpy(S, name[i]);
	        	strcpy(name[i], name[j]);
        		strcpy(name[j], S);
			}
			else if (acceptedFinal[i] == acceptedFinal[j] && timeFinal[i] > timeFinal[j]) {
				tmp1 = acceptedFinal[i];
	    	    acceptedFinal[i] = acceptedFinal[j];
    		    acceptedFinal[j] = tmp1;
        
	        	tmp2 = timeFinal[i];
    	    	timeFinal[i] = timeFinal[j];
        		timeFinal[j] = tmp2;
        
    	    	strcpy(S, name[i]);
	        	strcpy(name[i], name[j]);
        		strcpy(name[j], S);
			}
			else if (acceptedFinal[i] == acceptedFinal[j] && timeFinal[i] == timeFinal[j] && rotate(name[i], name[j]) == 0) {
				tmp1 = acceptedFinal[i];
	    	    acceptedFinal[i] = acceptedFinal[j];
    		    acceptedFinal[j] = tmp1;
        
	        	tmp2 = timeFinal[i];
    	    	timeFinal[i] = timeFinal[j];
        		timeFinal[j] = tmp2;
        
    	    	strcpy(S, name[i]);
	        	strcpy(name[i], name[j]);
        		strcpy(name[j], S);
			}
		} 
	}
	
	for (i = 0; i < teamCount; i++) {
		printf("%d %s %d %d\n", i + 1, name[i], acceptedFinal[i], timeFinal[i]);
	}
	
}

int main (void) {
	freopen("contest.in", "r", stdin);
	freopen("contest.out", "w", stdout);
	
	getName();
	SOLVE();
	
	return 0;
}

