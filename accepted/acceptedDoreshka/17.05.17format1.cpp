#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#define NMAX 205

char S[NMAX];
char line[NMAX];

int count, lineLength = 0, i;

void SOLVE() {
	scanf("%d\n", &count);
	
	while (scanf("%s", &S) != EOF) {
		if (lineLength + strlen(S) + 1 <= count) {
			if (lineLength != 0) {
				line[strlen(line)] = ' '; 
				lineLength++;
			}
			strcpy(line + lineLength, S);
			lineLength += strlen(S);
		}
		else if (lineLength + strlen(S) + 1 > count) {
			if (lineLength != 0) {
				for (i = 0; i < count - lineLength; i++) {
					printf(" ");
				}
				puts(line);
			}
			memset(line, 0, NMAX * sizeof(char));
			lineLength = strlen(S);
			strcpy(line, S);
		}
	}
	
	for (i = 0; i < count - lineLength; i++) {
		printf(" ");
	}
	printf("%s", line);
}

int main (void) {
	freopen("format.in", "r", stdin);
	freopen("format.out", "w", stdout);
	
	SOLVE();
	
	return 0;
}
