#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <cstring>

using namespace std;

const int NMAX = 1e3 + 5;
const int NOT_CHECKED = 0;
const int CHECKING = 1;
const int CHECKED = 2;

vector<int> graph[NMAX];
int vertexAmount, isCyclic = 0, isSpinePath = 1;

vector<int> status(NMAX, 0);
vector<int> isChildHasChildren(NMAX, 0);

void getData() {
    cin >> vertexAmount;
//    cout << vertexAmount << '\n';
    int u, v;
    while (cin >> u >> v) {
        graph[u].push_back(v);
        graph[v].push_back(u);
//        cout << u << ' ' << v << '\n';
    }
}

void printGraph() {
    for (int i = 1; i <= vertexAmount; ++i) {
        cout << i << " - ";
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << graph[i][j] << ' ';
        }
        cout << '\n';
    }
}

void dfs(int u, int parent) {
    if (isCyclic) {
        return;
    }
    status[u] = CHECKING;

    int v;
    for (int i = 0; i < graph[u].size(); ++i) {
        v = graph[u][i];
        if (v != parent && status[v] == CHECKING) {
            isCyclic = 1;
//            cout << "u v " << u << ' ' << v << endl;
//            cout << "status " << status[u] << ' ' << status[v] << endl;
            return;
        }
        if (status[v] == NOT_CHECKED) {
            dfs(v, u);
        }
    }
    status[u] = CHECKED;
}

bool isTree() {
    dfs(1, -1);
    if (isCyclic) {
//        cout << "Cycle there\n";
        return false;
    }
    for (int i = 1; i <= vertexAmount; ++i) {
//        cout << status[i] << ' ';
        if (!status[i]) {
//            cout << "It's not connected\n";
            return false;
        }
    }
//    cout << '\n';
    return true;
}

void checkSpinePath(int u, int parent) {
    if (!isSpinePath) {
        return;
    }

    status[u] = CHECKING;

    if (graph[u].size() > 1) {
        if (isChildHasChildren[parent]) {
            isSpinePath = 0;
            return;
        }
        isChildHasChildren[parent] = 1;
    }

    int v;
    for (int i = 0; i < graph[u].size(); ++i) {
        v = graph[u][i];
        if (v != parent && isSpinePath && status[v] == NOT_CHECKED) {
            checkSpinePath(v, u);
        }
    }

    status[u] = CHECKED;
}

bool isCaterpillar() {
    if (!isTree()) {
        return false;
    }

    for (int i = 1; i <= vertexAmount; ++i) {
        isSpinePath = 1;
        isChildHasChildren = vector<int>(NMAX, 0);
        status = vector<int>(NMAX, NOT_CHECKED);
        checkSpinePath(i, vertexAmount + 1);
        if (isSpinePath) {
//            cout << i << '\n';
            return true;
        }
    }

    return false;
}

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);
    freopen("caterpil.in", "r", stdin);
    freopen("caterpil.out", "w", stdout);

    getData();
//    printGraph();
    if (isCaterpillar()) {
        cout << "Graph is a caterpillar.";
    }
    else {
        cout << "Graph is not a caterpillar.";
    }

    return 0;
}


