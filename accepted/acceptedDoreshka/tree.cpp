#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

const int NMAX = 1e5 + 5;
const int EMPTY = -1;

int n, t;
int root = 0;

struct treeElement {
    int leftChild;
    int rightNeighbor;
    int parent;
    int isLeaf;
    int childAmount;
    treeElement(int lC = -1, int rN = -1, int p = -1, int iL = 0, int c = 0) : leftChild(lC), rightNeighbor(rN), parent(p), isLeaf(iL), childAmount(c) {}
};

treeElement BWtree[NMAX];
int needRemove[NMAX];

void add(int child, int parent) {
    BWtree[child] = treeElement(BWtree[child].leftChild, BWtree[parent].leftChild, parent, 1, BWtree[child].childAmount);
    BWtree[parent].leftChild = child;
    BWtree[parent].isLeaf = 0;
    BWtree[parent].childAmount++;
}

int idkWhatIsThat(int u) {
//    cout << "KEK " << u << '\n';
    if (BWtree[u].isLeaf) {
        return 1;
    }
    if (needRemove[u] != EMPTY) {
        return needRemove[u];
    }

//    cout << "MEK " << BWtree[u].childAmount << ' ' << t << endl;
    int needBlack = (BWtree[u].childAmount * t) / 100 + (((BWtree[u].childAmount * t) % 100) ? (1) : (0));
    vector<int> results;

    int v = BWtree[u].leftChild;
    while (v != EMPTY) {
        results.push_back(idkWhatIsThat(v));
        v = BWtree[v].rightNeighbor;
    }

//    cout << "PEK " << u << ' ' << needBlack << endl;
//    for (int i = 0; i < results.size(); ++i) {
//        cout << results[i] << ' ';
//    }
//    cout << endl;

    sort(results.begin(), results.begin() + results.size());

    needRemove[u] = 0;
    for (int i = 0; i < needBlack; ++i) {
        needRemove[u] += results[i];
    }

    return needRemove[u];
}

int main() {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    freopen("bwtree.in", "r", stdin);
    freopen("bwtree.out", "w", stdout);

    while (cin >> n >> t) {
        if (!n && !t) {
            break;
        }

        memset(needRemove, EMPTY, sizeof needRemove);

//        cout << n << ' ' << t << '\n';

        int parent;
        for (int i = 1; i <= n; ++i) {
            cin >> parent;
//            cout << parent << ' ';
            add(i, parent);
        }
//        cout << '\n';

//        for (int i = 0; i <= n; ++i) {
//            cout << BWtree[i].childAmount << ' ';
//        }
//        cout << endl;
//
//        for (int i = 0; i <= n; ++i) {
//            cout << BWtree[i].isLeaf << ' ';
//        }
//        cout << endl;

        cout << idkWhatIsThat(0) << endl;

        for (int i = 0; i <= n; ++i) {
            BWtree[i] = treeElement();
        }
    }

    return 0;
}
