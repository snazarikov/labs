#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <cstring>

using namespace std;

const int NMAX = 1e5 + 5;

int A[NMAX], B[NMAX], square[NMAX];

void scanNumeral(int* A) {
	char S[NMAX];
	int i, j;

	scanf("%s", S);
	A[0] = strlen(S);

	for (i = A[0] - 1, j = 1; i >= 0; i--, j++) {
		A[j] = S[i] - '0';
	}
}

void printNumeral(int* A) {
	int i;

	while (A[A[0]] == 0 && A[0] != 1) {
        A[0]--;
	}

	for (i = A[0]; i; i--) {
		printf("%c", A[i] + '0');
	}
	printf("\n");
}

int findRemainder(int* A, int shortNum) {
	int d = 0;

	for (int i = A[0]; i; i--) {
		d = d * 10 + A[i];
		d %= shortNum;
	}

	return d;
}

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);
    freopen("crypto.in", "r", stdin);
    freopen("crypto.out", "w", stdout);

    int supremum;
    scanNumeral(A);
    cin >> supremum;

    for (int i = 2; i < supremum; ++i) {
        if (!findRemainder(A, i)) {
            cout << "BAD " << i;
            return 0;
        }
    }
    cout << "GOOD";

    return 0;
}


