#include <stdio.h>
#include <math.h>
#define NMAX 1000
int point[NMAX];
int number[NMAX];

int main(void) {
	freopen("rating.in", "r", stdin);
	freopen("rating.out", "w", stdout);
	int countT, countP, level, countPl, tmp, i ,j, position, t;
	
	scanf("%d %d", &countT, &countP);
	
	for (i=0; i<countP; i++) {
		 number[i] = i;
		 point[i] = 0;
	}
	
	for (i=0; i<countT; i++) {
		scanf("%d %d", &level, &countPl);
		//printf("number of tournament %d level %d\n", i+1, level);
		for (j=0; j<countPl; j++) {
			scanf("%d", &position);
			//printf("priviousPoints %d ", point[position-1]);
			if (j==0) point[position-1] += (15*level);
			else if (j==1) point[position-1] += (12*level);
			else if (j==2) point[position-1] += (9*level);
			else if (j==3) point[position-1] += (7*level);
			else if (j==4) point[position-1] += (6*level);
			else if (j==5) point[position-1] += (5*level);
			else if (j==6) point[position-1] += (4*level);
			else if (j==7) point[position-1] += (3*level);
			else if (j==8) point[position-1] += (2*level);
			else if (j==9) point[position-1] += (1*level);
			else if (j==12) point[position-1] -= (level*10);
		//	printf("place %d position %d point %d\n", j+1, position, point[position-1]);
		}
		//printf("\n\n");
	}
	
	for (i=0; i<countP-1; i++) {
		for (j=i+1; j<countP; j++) {
			if (point[i]<point[j]) {
				tmp = point[i];
				point[i] = point[j];
				point[j] = tmp;
				t = number[i];
				number[i] = number[j];
				number[j] = t;
			}
			else if (point[i]==point[j] && number[i]>number[j]) {
				tmp = number[i];
				number[i] = number[j];
				number[j] = tmp;
			}
		}
	}
	
	if (countP>10) countP = 10;
	
	for (i=0; i<countP; i++) {
		if (point[i]>0) {
			printf("%d %d %d", i+1, number[i]+1, point[i]);
			printf("\n");
		}
	}
	
	return 0;
}
