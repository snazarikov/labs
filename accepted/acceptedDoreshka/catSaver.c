#include <stdio.h>
#include <string.h>
#define NMAX 55

char A[NMAX][NMAX][NMAX];
int v[NMAX][NMAX][NMAX];
int q[4][NMAX*NMAX*NMAX];
int save[2][NMAX*NMAX];
int dj[] = {-1, 0, 0, 1, 0, 0};
int di[] = {0, -1, 0, 0, 1, 0};
int dk[] = {0, 0, -1, 0, 0, 1};

int h, n, m;

main (void) {
	//freopen("input.txt", "r", stdin);
	freopen("save.in", "r", stdin);
	freopen("save.out", "w", stdout);
	
	int i, j, k, ii, jj, kk, d, top = 0, tail = 0, flagU, flagD, toRoofCount = 0, result = 1000000;
	
	scanf("%d %d %d", &h, &n, &m);
	
	
	for (k=0; k<h; k++) {
		for (i=0; i<n; i++) {
			scanf("\n");
			for (j=0; j<m; j++) {
				scanf("%c", &A[k][i][j]);
				v[k][i][j] = -1;
				if (A[k][i][j] == 'E') {
					v[k][i][j] = 1;
					q[0][tail] = 1;
					q[1][tail] = j;
					q[2][tail] = i;
					q[3][tail] = k;
					tail++;
					//printf("KEEEEEEEEEEEEEK v:%d value:%d x:%d y:%d z:%d top:%d tail:%d\n",v[k][i][j], q[0][tail], q[1][tail], q[2][tail], q[3][tail], tail, top);
				}
				if (k==h-1 && A[k][i][j] == 'U') {
					save[0][toRoofCount] = j;
					save[1][toRoofCount] = i;
					toRoofCount++;
				}
			}
		}
		scanf("\n");
	}
	
	while (top != tail) {
		for(d = 0; d < 6; d++) {
			flagU = 0;
			flagD = 0;
			jj = q[1][top] + dj[d];
			ii = q[2][top] + di[d];
			kk = q[3][top] + dk[d];
			if (A[kk][ii][jj] != '#' && v[kk][ii][jj] == -1 && ii >= 0 && ii < n && jj >= 0 && jj < m && kk >= 0 && kk < h) {
				if (kk > q[3][top] && (A[kk][ii][jj] != 'D' || A[q[3][top]][ii][jj] != 'U')) {
					flagU = 1;
				}
				if (kk < q[3][top] && (A[kk][ii][jj] != 'U' || A[q[3][top]][ii][jj] != 'D')) {
					flagD = 1;
				}
				if (flagU == 0 && flagD == 0) {
					v[kk][ii][jj] = q[0][top] + 1;
					q[0][tail] = q[0][top] + 1;
					q[1][tail] = jj;
					q[2][tail] = ii;
					q[3][tail] = kk;
					tail++;
					//printf("v:%d value:%d x:%d y:%d z:%d top:%d tail:%d\n",v[jj][ii][kk], q[0][tail], q[1][tail], q[2][tail], q[3][tail], tail, top);
				}
			}
		}
		top++;
	}
	
	for (i=0; i<toRoofCount; i++) {
		if (v[h-1][save[1][i]][save[0][i]] != -1 && v[h-1][save[1][i]][save[0][i]] < result) result = v[h-1][save[1][i]][save[0][i]];
	}
	
	if (result == 1000000) printf("888");
	else printf("%d", result);
	
//	for (k=0; k<h; k++) {
//		for (i=0; i<n; i++) {
//			for (j=0; j<m; j++) {
//				printf("%d ", v[k][i][j]);
//			}
//			printf("\n");
//		}
//		printf("\n");
//	}	
	
	return 0;
}
