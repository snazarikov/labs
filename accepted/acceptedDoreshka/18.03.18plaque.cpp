#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <cstring>

using namespace std;

const int NMAX = 1e2 + 5;
const int EMPTY = -1;

int n, width, boxMaxHeight, boxCurrHeight = 0;
int boxes[NMAX];
int boxAmount = 0;

char box[NMAX][NMAX];
char plaque[NMAX][NMAX];

void getPlaque(int h) {
    memset(plaque, '.', sizeof plaque);
    for (int i = h - 1; i >= 0; --i) {
        for (int j = 0; j < width; ++j) {
            scanf("%c", &plaque[i][j]);
        }
        scanf("\n");
    }
}

void printBox() {
    for (int i = 0; i < boxCurrHeight; ++i) {
        for (int j = 0; j < width; ++j) {
            printf("%c", box[i][j]);
        }
        cout << " " << i;
        printf("\n");
    }
}

void printPlaque(int h) {
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < width; ++j) {
            printf("%c", plaque[i][j]);
        }
        cout << " " << i;
        printf("\n");
    }
}

int findStartPosToAdd(int currBoxHeight, char* partOfPlaque) {
    int height = 0u;

    for (int i = currBoxHeight; i >= 0; --i) {
        int flag = 0;
        for (int j = 0; j < width; ++j) {
            if (box[i][j] == 'X' && partOfPlaque[j] == 'X') {
//                cout << "POSITION " << i << ' ' << j << ' ' << box[i][j] << partOfPlaque[j] << endl;
                height = i + 1;
                flag = 1;
                break;
            }
        }
        if (flag) {
            break;
        }
    }

    return height;
}

int addPlaqueToBox(int plaqueHeight, int startHeight) {
    if (startHeight + plaqueHeight > boxMaxHeight) {
        return 0;
    }

    for (int i = startHeight; i < startHeight + plaqueHeight; ++i) {
        for (int j = 0; j < width; ++j) {
            if (box[i][j] == 'X' && plaque[i - startHeight][j] == 'X') {
                return 0;
            }
        }
    }

    for (int i = startHeight; i < startHeight + plaqueHeight; ++i) {
        for (int j = 0; j < width; ++j) {
            if (box[i][j] == '.') {
                box[i][j] = plaque[i - startHeight][j];
            }
        }
    }
    return 1;
}

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);
    freopen("plaque.in", "r", stdin);
    freopen("plaque.out", "w", stdout);

    scanf("%d %d %d\n", &n, &width, &boxMaxHeight);
//    printf("%d %d %d\n", n, width, boxMaxHeight);

    memset(box, '.', sizeof box);
    int height;
    for (int i = 0; i < n; ++i) {
        scanf("%d\n", &height);
//        printf("%d\n", height);
        getPlaque(height);
//        printPlaque(height);

//        cout << "boxHeight " << boxCurrHeight << endl;
        int startPosition = findStartPosToAdd(boxCurrHeight, plaque[0]);
//        cout << "startPos " << startPosition << endl;

        int isAdded = 0;
        for (int i = startPosition; i <= boxCurrHeight; ++i) {
            if (addPlaqueToBox(height, i)) {
                isAdded = 1;
                boxCurrHeight = height + i;
//                cout << "plague added in box " << i << ' ' << boxCurrHeight << endl;
                break;
            }
        }

        if (!isAdded) {
//            cout << "boxAmount " << boxAmount << endl;
//            cout << "KEK " << boxCurrHeight << endl;
            boxes[boxAmount++] = boxCurrHeight;
            memset(box, '.', sizeof box);
            boxCurrHeight = 0;
//            cout << "plague added in new box " << i << ' ' << boxCurrHeight << endl;
//            cout << "is succsess " << addPlaqueToBox(height, 0) << endl;
            addPlaqueToBox(height, 0);
            boxCurrHeight = height;
//            cout << "boxAmount " << boxAmount << endl;
        }

//        printBox();
//        cout << endl;
    }
    boxes[boxAmount++] = boxCurrHeight;

//    cout << boxAmount << endl;
    for (int i = 0; i < boxAmount; ++i) {
        cout << boxes[i] << ' ';
    }
    return 0;
}


