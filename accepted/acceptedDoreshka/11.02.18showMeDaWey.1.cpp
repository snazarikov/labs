#include <cstdio>
#include <iostream>
#include <queue>
#include <algorithm>

using namespace std;

const int NMAX = 1e3 + 5;

const int RIGHT = 0;
const int DOWN = 1;
const int LEFT = 2;
const int UP = 3;

int n, m;

struct elemModel {
    char element;
    int i;
    int j;
    int dist;
    int direction;
    int right;
    int down;
    int left;
    int up;
};

int di[] = {0, 1, 0, -1};
int dj[] = {1, 0, -1, 0};

elemModel A[NMAX][NMAX];

elemModel startPos, finishPos;

void getData() {
    char symbol;
    scanf("%d %d\n", &n, &m);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            scanf("%c", &symbol);
            if (symbol == 'X') {
                elemModel e = {'X', i, j, 0, -1, 0, 0, 0, 0};
                A[i][j] = e;
            }
            else if (symbol == 'S') {
                elemModel e = {'S', i, j, 0, -1, 0, 0, 0, 0};
                A[i][j] = e;

                startPos = e;
            }
            else if (symbol == 'F') {
                elemModel e = {'F', i, j, 0, -1, 0, 0, 0, 0};
                A[i][j] = e;

                finishPos = e;
            }
            else {
                elemModel e = {'E', i, j, 0, -1, 0, 0, 0, 0};
                A[i][j] = e;
            }
        }
        scanf("\n");
    }
}

void printData() {
    cout << endl;
    cout << n << " " << m << endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%c", A[i][j].element);
        }
        cout << endl;
    }
}

int someSearch(elemModel start, elemModel finish) {
    queue<elemModel> q;

    for (int d = 0; d < 4; ++d) {
        int ii = start.i + di[d];
        int jj = start.j + dj[d];

        if (ii >= 0 && ii < n && jj >= 0 && jj < m && A[ii][jj].element != 'X') {
            elemModel e = {A[ii][jj].element, ii, jj, 1, d, 0, 0, 0, 0};
            switch (d) {
                case RIGHT: {
                    e.right = 1;
                    A[ii][jj].right = 1;
                    break;
                }
                case DOWN: {
                    e.down = 1;
                    A[ii][jj].down = 1;
                    break;
                }
                case LEFT: {
                    e.left = 1;
                    A[ii][jj].left = 1;
                    break;
                }
                case UP: {
                    e.up = 1;
                    A[ii][jj].up = 1;
                    break;
                }
            }
            q.push(e);
            if (A[ii][jj].element == 'F') {
                return 1;
            }
        }
    }

    while (!q.empty()) {
        elemModel frontElem = q.front();
        q.pop();

        for (int d = frontElem.direction; d < frontElem.direction + 2; ++d) {
            int ii = frontElem.i + di[d % 4];
            int jj = frontElem.j + dj[d % 4];

            if (ii >= 0 && ii < n && jj >= 0 && jj < m && A[ii][jj].element != 'X') {
                int flag = 0;

                switch (d % 4) {
                    case RIGHT: {
                        (A[ii][jj].right) ? (flag = 1) : (A[ii][jj].right = 1);
                        break;
                    }
                    case DOWN: {
                        (A[ii][jj].down) ? (flag = 1) : (A[ii][jj].down = 1);
                        break;
                    }
                    case LEFT: {
                        (A[ii][jj].left) ? (flag = 1) : (A[ii][jj].left = 1);
                        break;
                    }
                    case UP: {
                        (A[ii][jj].up) ? (flag = 1) : (A[ii][jj].up = 1);
                        break;
                    }
                }
                if (flag) {
                    continue;
                }

                elemModel e = A[ii][jj];
                e.dist = frontElem.dist + 1;
                e.direction = d % 4;
                q.push(e);
                if (A[ii][jj].element == 'F') {
                    return e.dist;
                }
            }
        }
    }
}

int main() {
//    freopen ("input.txt", "r", stdin);
    freopen ("noleft.in", "r", stdin);
    freopen ("noleft.out", "w", stdout);

    getData();

    cout << someSearch(startPos, finishPos);

    return 0;
}
