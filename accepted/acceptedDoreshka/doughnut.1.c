#include <stdio.h>
#include <math.h>

main (void) {
	freopen("doughnut.in", "r", stdin);
	freopen("doughnut.out", "w", stdout);
	double w;
	
	scanf("%lf", &w);
	printf("%.5lf", log(M_PI)*w);
	
	return 0;
}
