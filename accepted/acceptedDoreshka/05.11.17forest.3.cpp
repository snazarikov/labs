#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <queue>

using namespace std;

#define NMAX 105

int di[] = {1, -1, 0, 0};
int dj[] = {0, 0, 1, -1};

int A[NMAX][NMAX];
int tmp[NMAX][NMAX];
int inQueue[NMAX][NMAX];

struct queueElem {
    int si;
    int sj;
    int value;
};

queue<struct queueElem> mainQueue;

int n, m;

void getData() {
    cin >> n >> m;

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cin >> A[i][j];
		}
	}
}

void printData() {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << A[i][j]  << " ";
		}
		cout << "\n";
	}
}

int getStartElements() {
    int ii, jj;
    struct queueElem element;

    for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			for (int d = 0; d < 4; d++) {
                ii = i + di[d];
                jj = j + dj[d];
                if (jj >= 0 && jj < m && ii >= 0 && ii < n)  {
                    if (A[i][j] - A[ii][jj] == -1) {
                        element.si = i;
                        element.sj = j;
                        mainQueue.push(element);
                        break;
                    }
                }
			}
		}
	}
	return 0;
}

int main (void) {
//	freopen ("forest.in", "r", stdin);
//	freopen ("forest.out", "w", stdout);
//	freopen ("output.txt", "w", stdout);
	freopen ("input.txt", "r", stdin);

	ios_base::sync_with_stdio(false);

    getData();

    int res = 0, ii, jj, i, j;

    getStartElements();

    for (int i = 0; i < n; ++i) {
        memcpy(tmp[i], A[i], sizeof(int) * (m + 1));
    }

    while (1) {
        int currSize = mainQueue.size();
        queue<queueElem> updateElements;

        while (currSize) {
            for (int d = 0; d < 4; d++) {
                i = mainQueue.front().si;
                j = mainQueue.front().sj;

                ii = i + di[d];
                jj = j + dj[d];

                if (jj >= 0 && jj < m && ii >= 0 && ii < n)  {
                    if (A[ii][jj] - A[i][j] == 1) {
                        tmp[i][j] = A[i][j] + 1;

                        struct queueElem e2;
                        e2.si = i;
                        e2.sj = j;
                        e2.value = A[i][j] + 1;

                        updateElements.push(e2);

                        inQueue[i][j] = res + 1;

                        for (int k = 0; k < 4; k++) {
                            ii = i + di[k];
                            jj = j + dj[k];

                            if (jj >= 0 && jj < m && ii >= 0 && ii < n && inQueue[ii][jj] != res + 1) {
                                inQueue[ii][jj] = res + 1;

                                struct queueElem e1;
                                e1.si = ii;
                                e1.sj = jj;

                                mainQueue.push(e1);
                            }
                        }

                        mainQueue.push(mainQueue.front());

                        break;
                    }
                }
			}
			mainQueue.pop();
			currSize--;
        }

        while (!updateElements.empty()) {
            struct queueElem e3 = updateElements.front();
            A[e3.si][e3.sj] = e3.value;
            updateElements.pop();
        }

        if (mainQueue.empty()) {
            cout << res << "\n";
            printData();
            return 0;
        }

        res++;
    }

	return 0;
}

