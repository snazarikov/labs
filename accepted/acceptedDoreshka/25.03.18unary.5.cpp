#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace std;

const int NMAX = 5 * 1e3 + 5;
const int EMPTY = 1e9;
const int ADD = 0;
const int MULT = 1;

int n;

int table[NMAX][3];

int dynamic(int num, int lastAction) {
    if (num < 0) {
        return EMPTY + 1;
    }
    if (num < 2) {
        return num;
    }
    if (table[num][lastAction] != EMPTY) {
        return table[num][lastAction];
    }

    table[num][lastAction] = min(table[num][lastAction], num);

    for (int i = 2; i * i <= num; ++i) {
        if (!(num % i)) {
            table[num][lastAction] = min(table[num][lastAction], dynamic(i, MULT) + dynamic(num / i, MULT) + 2);
        }
    }

    if (lastAction != MULT) {
        for (int i = 1; i <= num; ++i) {
            table[num][lastAction] = min(table[num][lastAction], dynamic(i, ADD) + dynamic(num - i, ADD) + 2);
        }
    }

    return table[num][lastAction];
}

int main(){
//	freopen("input.txt", "r", stdin);

	freopen("unary.in", "r", stdin);
	freopen("unary.out", "w", stdout);

    for (int i = 0; i < NMAX; ++i) {
        table[i][0] = EMPTY;
        table[i][1] = EMPTY;
    }

    while (cin >> n) {
//        cout << n << endl;
        if (n < 1) {
            cout << n << endl;
            continue;
        }
        cout << dynamic(n, ADD) << endl;
//        cout << "\n\n";
    }

	return 0;
}
