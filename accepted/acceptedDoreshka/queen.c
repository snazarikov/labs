#include <stdio.h>
#include <math.h>
#define NMAX 8
int A[NMAX][NMAX];
int main(void) {
	freopen("queen.in", "r", stdin);
	freopen("queen.out", "w", stdout);
	int digit,counter=0,n,i,j,secDig, stP1, stP2, sgnToDig;
	char sign, secSign;
	scanf ("%c%d",&sign,&digit);
	stP1=sign - 'a';
	stP2=digit-1;
	scanf ("%d", &n);
	for (i=0; i<n; i++) {
		scanf ("\n");
		scanf ("%c%d", &secSign, &secDig);
		sgnToDig = secSign - 'a';
		A[sgnToDig][secDig-1]=1;		
	}
	for (i=stP1, j=stP2; i>=0 && j<8; i--, j++) {
		if (A[i][j]) {
		   counter++;
		   break;
		}
	}
	for (i=stP1, j=stP2; i>=0 && j>=0; i--, j--) {
		if (A[i][j]) {
		    counter++;
			break;
		}
	}
	for (i=stP1, j=stP2; i<8 && j<8; i++, j++) {
		if (A[i][j]) {
		   counter++;
		   break;
        }
	}
	for (i=stP1, j=stP2; i<8 && j>=0; i++, j--) {
		if (A[i][j]) {
		   counter++;
		   break;
        }
	}
	for (i=stP1; i<8; i++) {
		j=stP2;
		if (A[i][j]) {
			counter++;
			break;
		}
	}
	for (i=stP1; i>=0; i--) {
		j=stP2;
		if (A[i][j]) {
			counter++;
			break;
		}
	}
	for (j=stP2; j<8; j++) {
		i=stP1;
		if (A[i][j]) {
			counter++;
			break;
		}
	}
	for (j=stP2; j>=0; j--) {
		i=stP1;
		if (A[i][j]) {
			counter++;
			break;
		}
	}
	printf ("%d",counter);
	return 0;
}
