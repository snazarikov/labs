#include <stdio.h>
#include <math.h>
#include <string.h>
#define NMAX 1005

int count, countOfRaysPP = 0, countOfRaysPM = 0, countOfRaysMP = 0, countOfRaysMM = 0;
int flagXP = 0, flagXM = 0, flagYP = 0, flagYM = 0;
int diag1 = 0, diag2 = 0, diag3 = 0, diag4 = 0;

int raysPP[NMAX][2];
int raysPM[NMAX][2];
int raysMP[NMAX][2];
int raysMM[NMAX][2];

int ged (int A, int B) {
	if (!B) return A;
	return ged(B, A%B);
}

int isProportional(int a, int b, int x, int y) {
	int gedAB = ged(a, b);
	int gedXY = ged(x, y);
	a /= gedAB;
	b /= gedAB;
	x /= gedXY;
	y /= gedXY;
	if (a == x && b == y) {
		return 1;
	}
	return 0;
}

int SOLVE() {
	int x, y, flag;
	
	scanf("%d", &count);
	for (int i = 0; i < count; i++) {
		flag = 0;
		scanf("%d %d", &x, &y);
		
		if (y == 0 && x > 0) {
			flagXP = 1;
			//printf("%d %d\n", x, y);
			continue;
		}
		if (y == 0 && x < 0) {
			flagXM = 1;
		//	printf("%d %d\n", x, y);
			continue;
		}
		if (x == 0 && y > 0) {
			flagYP = 1;
		//	printf("%d %d\n", x, y);
			continue;
		}
		if (x == 0 && y < 0) {
			flagYM = 1;
		//	printf("%d %d\n", x, y);
			continue;
		}
		
		if (abs(x) == abs(y)) {
			if (x > 0 && y > 0) {
				diag1 = 1;
				continue;
			}
		
			if (x > 0 && y < 0) {
				diag2 = 1;
				continue;
			}
		
			if (x < 0 && y > 0) {
				diag3 = 1;
				continue;
			}
		
			if (x < 0 && y < 0) {
				diag4 = 1;
				continue;
			}
		}
		
		if (x > 0 && y > 0) {
			for (int j = 0; j < countOfRaysPP; j++) {
				if (isProportional(abs(x), abs(y), abs(raysPP[j][0]), abs(raysPP[j][1])) == 1) {
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				raysPP[countOfRaysPP][0] = x;
				raysPP[countOfRaysPP][1] = y;
				countOfRaysPP++;
				//printf("PP %d %d\n", x, y);
			}
			continue;
		}
		
		if (x > 0 && y < 0) {
			for (int j = 0; j < countOfRaysPM; j++) {
				if (isProportional(abs(x), abs(y), abs(raysPM[j][0]), abs(raysPM[j][1])) == 1) {
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				raysPM[countOfRaysPM][0] = x;
				raysPM[countOfRaysPM][1] = y;
				countOfRaysPM++;
				//printf("PM %d %d\n", x, y);
			}
			continue;
		}
		
		if (x < 0 && y > 0) {
			for (int j = 0; j < countOfRaysMP; j++) {
				if (isProportional(abs(x), abs(y), abs(raysMP[j][0]), abs(raysMP[j][1])) == 1) {
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				raysMP[countOfRaysMP][0] = x;
				raysMP[countOfRaysMP][1] = y;
				countOfRaysMP++;
			//	printf("MP %d %d\n", x, y);
			}
			continue;
		}
		
		if (x < 0 && y < 0) {
			for (int j = 0; j < countOfRaysMM; j++) {
				if (isProportional(abs(x), abs(y), abs(raysMM[j][0]), abs(raysMM[j][1])) == 1) {
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				raysMM[countOfRaysMM][0] = x;
				raysMM[countOfRaysMM][1] = y;
				countOfRaysMM++;
				//printf("MM %d %d\n", x, y);
			}
			continue;
		}
	}
	return (countOfRaysPP + countOfRaysPM + countOfRaysMP + countOfRaysMM + flagXP + flagXM + flagYP + flagYM + diag1 + diag2 + diag3 + diag4);
}

main (void) {
//	freopen("input.txt", "r", stdin);
	freopen("points.in", "r", stdin);
	freopen("points.out", "w", stdout);
	
	printf("%d", SOLVE());
	//printf("%d %d %d", isProportional(2, 4, 3, 6), ged(2,4), ged(3, 6));
	
	return 0;
}

