#include <cstdio>
#include <iostream>
#include <vector>
#include <cstring>

#define VERTICES_AMOUNT 500
#define EDGES_AMOUNT 125000

using namespace std;

int verticesAmount, edgesAmount, res = 0, isCyclic = 0;

vector<int> g[VERTICES_AMOUNT + 1];
vector<int> dist(VERTICES_AMOUNT + 1, -1);

void getData() {
    cin >> verticesAmount >> edgesAmount;
    for (int i = 0; i < edgesAmount; i++) {
        int u, v;
        cin >> u >> v;
        g[u].push_back(v);
        g[v].push_back(u);
    }
}

void dfs(int vertex, int color, int prevVertex, vector<int> way) {
    vector<int> currWay = way;

    dist[vertex] = color;

    for (int i = 0; i < g[vertex].size(); i++) {
        if (dist[g[vertex][i]] == -1) {
            currWay.push_back(g[vertex][i]);
            dfs(g[vertex][i], color + 1, currWay[currWay.size() - 2], currWay);
            currWay.pop_back();
        }
        else if (g[vertex][i] != prevVertex){
            if (isCyclic) {
                continue;
            }
            for (int j = 0; j < currWay.size(); j++) {
                if (g[vertex][i] == currWay[j]) {
                    isCyclic = 1;
                    break;
                }
            }
        }
    }
}

void solve() {
    for (int i = 1; i <= verticesAmount; i++) {
        if (dist[i] == -1) {
            vector<int> help;
            help.push_back(i);

            isCyclic = 0;

            res++;
            dfs(i, 0, 0, help);
            res -= isCyclic;
        }
    }

    if (res <= 0) {
        cout << "No trees.";
    }
    else if (res == 1) {
        cout << "There is one tree.";
    }
    else {
        cout << "A forest of " << res << " trees.";
    }
}

int main() {
//    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    freopen ("trees.in", "r", stdin);
    freopen ("trees.out", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();
    solve();

    return 0;
}
