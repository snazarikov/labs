#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

const int NMAX = 21;

vector<int> table[NMAX][NMAX];
int n, m;
int leftColumn, rightColumn, topRow, bottomRow;

void getCards() {
    cin >> n >> m;
    int card;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> card;
            table[i][j].push_back(card);
        }
    }

    leftColumn = 0;
    topRow = 0;
    rightColumn = m - 1;
    bottomRow = n - 1;
}

void Trotation() {
    for (int j = leftColumn; j <= rightColumn; ++j) {
        for (int k = table[topRow][j].size() - 1; k >= 0; --k) {
            table[topRow + 1][j].push_back(-table[topRow][j][k]);
        }
        table[topRow][j].clear();
    }
    topRow++;
}

void Brotation() {
    for (int j = leftColumn; j <= rightColumn; ++j) {
        for (int k = table[bottomRow][j].size() - 1; k >= 0; --k) {
            table[bottomRow - 1][j].push_back(-table[bottomRow][j][k]);
        }
        table[bottomRow][j].clear();
    }
    bottomRow--;
}

void Lrotation() {
    for (int i = topRow; i <= bottomRow; ++i) {
        for (int k = table[i][leftColumn].size() - 1; k >= 0; --k) {
            table[i][leftColumn + 1].push_back(-table[i][leftColumn][k]);
        }
        table[i][leftColumn].clear();
    }
    leftColumn++;
}

void Rrotation() {
    for (int i = topRow; i <= bottomRow; ++i) {
        for (int k = table[i][rightColumn].size() - 1; k >= 0; --k) {
            table[i][rightColumn - 1].push_back(-table[i][rightColumn][k]);
        }
        table[i][rightColumn].clear();
    }
    rightColumn--;
}

void printRes() {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (!table[i][j].empty()) {
                for (int k = 0; k < table[i][j].size(); ++k) {
                    if (table[i][j][k] > 0) {
                        cout << table[i][j][k] << ' ';
                    }
                }
                return;
            }
        }
    }
}

int main() {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    freopen("cards.in", "r", stdin);
    freopen("cards.out", "w", stdout);

    getCards();

    char symbol;
    for (int i = 0; i < n + m - 2; ++i) {
        cin >> symbol;
        if (symbol == 'T') {
            Trotation();
        }
        else if (symbol == 'B') {
            Brotation();
        }
        else if (symbol == 'L') {
            Lrotation();
        }
        else if (symbol == 'R') {
            Rrotation();
        }
    }

    printRes();

    return 0;
}

