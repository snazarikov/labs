#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <queue>
#include <algorithm>
#include <cstring>

using namespace std;

const int NMAX = 25;

const int NORTH = 0;
const int EAST = 1;
const int SOUTH = 2;
const int WEST = 3;

int di[] = {-1, 0, 1, 0};
int dj[] = {0, 1, 0, -1};

int n, m;

struct Model {
    int i;
    int j;
    int isChecked;
    int direction[4];
};

Model field[NMAX][NMAX];

void getData() {
    for (int i = 0; i < NMAX; ++i) {
        for (int j = 0; j < NMAX; ++j) {
            field[i][j] = Model{i, j, 0, {0, 0, 0, 0}};
        }
    }

    cin >> m >> n;
    int amount;
    cin >> amount;
    int x1, y1, x2, y2;
    for (int k = 0; k < amount; ++k) {
        cin >> x1 >> y1 >> x2 >> y2;
        int minY = min(n - y1, n - y2);
        int maxY = max(n - y1, n - y2);
        int minX = min(x1, x2);
        int maxX = max(x1, x2);

        maxY--;
        maxX--;

        for (int i = minX; i <= maxX; ++i) {
            field[minY][i].direction[NORTH] = 1;
            if (minY - 1 >= 0) {
                field[minY - 1][i].direction[SOUTH] = 1;
            }

            field[maxY][i].direction[SOUTH] = 1;
            if (maxY + 1 < n) {
                field[maxY + 1][i].direction[NORTH] = 1;
            }
        }

        for (int i = minY; i <= maxY; ++i) {
            field[i][minX].direction[WEST] = 1;
            if (minX - 1 >= 0) {
                field[i][minX - 1].direction[EAST] = 1;
            }

            field[i][maxX].direction[EAST] = 1;
            if (maxX + 1 < m) {
                field[i][maxX + 1].direction[WEST] = 1;
            }
        }
    }

    for (int i = 0; i < n; ++i) {
        field[i][0].direction[WEST] = 1;
        field[i][m - 1].direction[EAST] = 1;
    }
    for (int i = 0; i < m; ++i) {
        field[0][i].direction[NORTH] = 1;
        field[n - 1][i].direction[SOUTH] = 1;
    }
}

void printData() {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            for (int k = 0; k < 4; ++k) {
                cout << field[i][j].direction[k];
            }
            cout << ' ';
        }
        cout << endl;
    }
}

void bfs(Model start) {
    queue<Model> q;
    q.push(start);

    while(!q.empty()) {
        Model front = q.front();
        q.pop();
        for (int d = 0; d < 4; ++d) {
            int ii = front.i + di[d];
            int jj = front.j + dj[d];

            if (ii >= 0 && jj >= 0 && ii < n && jj < m && !field[ii][jj].isChecked && !front.direction[d]) {
                field[ii][jj].isChecked = 1;
                q.push(field[ii][jj]);
            }
        }
    }

}

int main() {
//    freopen ("input.txt", "r", stdin);
    freopen ("cut.in", "r", stdin);
    freopen ("cut.out", "w", stdout);
//    freopen ("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();
//    printData();

    int res = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (!field[i][j].isChecked) {
                res++;
                field[i][j].isChecked = 1;
                bfs(field[i][j]);
            }
        }
    }

    cout << res;

    return 0;
}
