#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <cstring>
#include <climits>

using namespace std;

const int NMAX = 1e3 + 5;
const int MAX_VENDOR = 35;
const int EMPTY = -1;
const int INF = INT_MAX;

int n, money;
int price[NMAX];

int table[NMAX][MAX_VENDOR][MAX_VENDOR];

int dynamic(int currMoney, int checking, int minVendor) {
    if (currMoney < 0) {
//        cout << "KEK 1 " << currMoney << ' ' << checking << ' ' << minVendor << endl;
        return 0;
    }
    if (checking == n) {
        if (currMoney < price[minVendor] && currMoney != money) {
//            cout << "KEK 2 " << currMoney << ' ' << checking << ' ' << minVendor << endl;
            return 1;
        }
//        cout << "KEK 3 " << currMoney << ' ' << checking << ' ' << minVendor << endl;
        return 0;
    }

    if (table[currMoney][checking][minVendor] != EMPTY) {
        return table[currMoney][checking][minVendor];
    }

    int minIndex = (((min(price[minVendor], price[checking])) == price[minVendor]) ? minVendor : checking);
    table[currMoney][checking][minVendor] = dynamic(currMoney - price[checking], checking + 1, minVendor) + dynamic(currMoney, checking + 1, minIndex);

    return table[currMoney][checking][minVendor];
}

void getData() {
    cin >> n >> money;
    for (int i = 0; i < n; ++i) {
        cin >> price[i];
    }
    price[n] = INF;
}

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);
    freopen("margarit.in", "r", stdin);
    freopen("margarit.out", "w", stdout);

    getData();
    memset(table, EMPTY, sizeof table);
    cout << dynamic(money, 0, n) << endl;

    return 0;
}


