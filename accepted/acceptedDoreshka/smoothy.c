#include <stdio.h>
#include <math.h>
#include <string.h>
#define NMAX 15

long long A[NMAX];
long long B[NMAX];

main (void) {
	//freopen("smooth.in", "r", stdin);
	//freopen("smooth.out", "w", stdout);
	int k, i, j;
	long long res = 0;
	
	scanf("%d", &k);
	
	for (i=1; i<10; i++) {
		A[i] = 1;
		B[i] = 1;
	}
	
	for (i=1 ; i<k; i++) {
		
		for (j=0; j<10; j++) {
			if (j-1==-1) A[j] = B[j] + B[j+1];
			else if (j+1==10) A[j] = B[j] + B[j-1];
			else A[j] = B[j-1] + B[j] + B[j+1]; 
		}
		memcpy(B, A, NMAX * sizeof(long long));
		
	}
	
	for (i=0; i<10; i++) res += A[i];	
	
	printf("%I64d", res);
	
	return 0;
}

