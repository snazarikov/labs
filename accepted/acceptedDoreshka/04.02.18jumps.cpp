#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

using namespace std;

const int NMAX = 255;

int perfomance[NMAX][NMAX];
int wonder[NMAX][NMAX];

int n, m, p;

void getData() {
    cin >> n >> m >> p;

    int res = 0;
    for (int i = 0; i < p; ++i) {
        int numbSp, numbEx;
        cin >> numbSp >> numbEx;
        perfomance[numbSp - 1][numbEx - 1] = 1;
    }
}

void printTable(int (&table)[NMAX][NMAX]) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << table[i][j] << ' ';
        }
        cout << '\n';
    }
    cout << '\n';
}

void fillTable() {
    wonder[0][0] = perfomance[0][0];

    for (int i = 1; i < m; ++i) {
        wonder[0][i] = wonder[0][i - 1] + perfomance[0][i];
    }

    for (int i = 1; i < n; ++i) {
        wonder[i][0] = wonder[i - 1][0] + perfomance[i][0];
    }

    for (int i = 1; i < n; ++i) {
        for (int j = 1; j < m; ++j) {
            wonder[i][j] = wonder[i - 1][j] + wonder[i][j - 1] - wonder[i - 1][j - 1] + perfomance[i][j];
        }
    }
}

int solve() {
//    printTable(perfomance);
    fillTable();
//    printTable(wonder);

    int res = 0;

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (perfomance[i][j]) {
                if (i + 1 < n && j - 1 >= 0) {
                    res += wonder[n - 1][j - 1] - wonder[i][j - 1];
                }
                if (i - 1 >= 0 && j + 1 < m) {
                    res += wonder[i - 1][m - 1] - wonder[i - 1][j];
                }
            }
        }
    }

    return res / 2;
}

int main() {
//    freopen ("input.txt", "r", stdin);
    freopen ("jumps.in", "r", stdin);
    freopen ("jumps.out", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();
    cout << solve();

    return 0;
}

