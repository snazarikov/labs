#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NMAX 10005
#define maximum(a, b) (((a) > (b)) ? (a) : (b))

int count[NMAX];
int help1[NMAX];
int help2[NMAX];
int help3[NMAX];

void scanNumeral (int* A) {
	char S[NMAX];
	int i, j;
	
	gets(S);
	A[0] = strlen(S);
	
	for (i=A[0]-1, j=1; i>=0; i--, j++) {
		A[j] = S[i] - '0';
	}
}

void printNumeral (int* A) {
	int i;
	
	while (A[A[0]]==0 && A[0]!=1) A[0]--;
	
	for (i = A[0]; i; i--) {
		printf("%c", A[i]+'0');
	}
	printf("\n");
}

void addition (int* A, int* B, int* Z) {
	int d, i;
	
    memset(Z, 0, NMAX * sizeof(int));
	Z[0] = maximum(A[0],B[0]);
	d = 0;
	
	for (i=1; i<=Z[0]; i++) {
		d+=(A[i]+B[i]);
		Z[i] = d%10;
		d/=10;
	}
	
	if (d) Z[++Z[0]] = d;
}

void multiplication (int* A, int* B, int* Z) {
	int d, i, j;
	
    memset(Z, 0, NMAX * sizeof(int));
    
    for (i=1; i<=A[0]; i++) {
    	for (j=1; j<=B[0]; j++) {
    		Z[i+j-1]+=A[i]*B[j];
		}
	}
    Z[0] = A[0] + B[0] - 1;
    for (i=1; i<=Z[0]; i++) {
    	Z[i+1]+=Z[i]/10;
    	Z[i]%=10;
	}
	
	d=Z[Z[0]+1];
	while (d) {
		Z[++Z[0]]=d%10;
		d/=10;
	}
}

void divisionOfLongByShort (int* A, int shortNum, int* Z) {
	int d, i;
	
    memset(Z, 0, NMAX * sizeof(int));
	Z[0] = A[0];
	d = 0;
	
	for (i=A[0]; i; i--) {
		d=d*10 + A[i];
		Z[i]=d/shortNum;
		d%=shortNum;
	}
	
	while (Z[0]>1 && !Z[Z[0]]) Z[0]--;
}

main (void) {
	freopen("life.in", "r", stdin);
	freopen("life.out", "w", stdout);
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	
	int i, j;
	long long res = 0;
	
	scanNumeral(count);
	
	while (1) {
		if (count[0] == 1 && count[1] == 1) break;
		
		memset(help1, 0, NMAX * sizeof(int));
		memset(help2, 0, NMAX * sizeof(int));
		memset(help3, 0, NMAX * sizeof(int));
		
		if (count[1] % 2 == 0) {
			res++;
			divisionOfLongByShort(count, 2, help1);
			memcpy(count, help1, NMAX * sizeof(int));
		}
		
		else if (count[1] % 2 == 1){
			res++;
			help2[0] = 1;
			help2[1] = 3;
			help1[0] = 1;
			help1[1] = 1;
			multiplication(count, help2, help3);
			memset(help2, 0, NMAX * sizeof(int));
			addition(help3, help1, help2);
			memcpy(count, help2, NMAX * sizeof(int));
		}
	}
	
	printf("%I64d", res);
	
	return 0;
}
