#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

const int VERTICES_AMOUNT = 1e5 + 5;

int verticesAmount;

vector<pair<int, int> > graph[VERTICES_AMOUNT];

vector<int> powIn(VERTICES_AMOUNT, 0);
vector<int> powOut(VERTICES_AMOUNT, 0);

vector<int> EulerCycle;

void getData() {
    cin >> verticesAmount;
    for (int i = 1; i <= verticesAmount; ++i) {
        int length;
        cin >> length;

        powOut[i] = length;
        for (int j = 0; j < length; ++j) {
            int vertex;
            cin >> vertex;
            powIn[vertex]++;
            graph[i].push_back(pair<int, int> (vertex, 0));
        }
    }
}

void printData() {
    cout << verticesAmount << endl;
    for (int i = 1; i <= verticesAmount; ++i) {
        cout << i;
//        cout << "(" << graph[i].size() << ")";
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << " " << graph[i][j].first;
//            cout << "(" << graph[i][j].second << ")";
        }
        cout << endl;
    }
    cout << endl;
}

void getEulerCycle(int u) {
    for (int i = 0; i < graph[u].size(); ++i) {
        int v = graph[u][i].first;
        if (!graph[u][i].second) {
            graph[u][i].second = 1;
            getEulerCycle(v);
        }
    }
    EulerCycle.push_back(u);
}

bool isEuler() {
    for (int i = 1; i <= verticesAmount; ++i) {
        for (int j = 0; j < graph[i].size(); ++j) {
            if (!graph[i][j].second) {
//                cout << "GG " << i << ' ' << graph[i][j].first << ' ' << j << endl;
                return false;
            }
        }
    }

    if (EulerCycle[0] != EulerCycle[EulerCycle.size() - 1]) {
//        cout << "GG not cycle\n";
        return false;
    }

    return true;
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    getData();
//    printData();

    for (int i = 1; i <= verticesAmount; ++i) {
        if (powIn[i] || powOut[i]) {
            getEulerCycle(i);
            break;
        }
    }

    if (isEuler()) {
        for (int i = EulerCycle.size() - 1; i >= 0; --i) {
            cout << EulerCycle[i] << ' ';
        }
    }
    else {
        cout << -1;
    }

    return 0;
}

