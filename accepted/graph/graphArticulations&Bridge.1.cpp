#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int VERTICES_AMOUNT = 1e5 + 5;

const int NOT_CHECKED = 0;
const int CHECKING = 1;
const int CHECKED = 2;

const int EMPTY = -1;

int verticesAmount;

vector<int> graph[VERTICES_AMOUNT + 1];
vector<int> graphWOBridges[VERTICES_AMOUNT + 1];

vector<int> status(VERTICES_AMOUNT + 1, NOT_CHECKED);
vector<int> low(VERTICES_AMOUNT + 1, EMPTY);
vector<int> parent(VERTICES_AMOUNT + 1, EMPTY);
vector<int> childAmount(VERTICES_AMOUNT + 1, EMPTY);
vector<int> timeIn(VERTICES_AMOUNT + 1, EMPTY);
vector<int> timeOut(VERTICES_AMOUNT + 1, EMPTY);
vector<int> isArticulation(VERTICES_AMOUNT + 1, EMPTY);

vector<int> statusBiconnectedComponent(VERTICES_AMOUNT + 1, NOT_CHECKED);
vector<int> biconnectedComponent;

vector<pair<int, int> > bridges;

int timer = 0;

void getData() {
    cin >> verticesAmount;

    int u, v;
    while (cin >> u) {
        cin >> v;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }

    for (int i = 1; i <= verticesAmount; ++i) {
        graphWOBridges[i] = graph[i];
    }
}

void printData() {
    for (int i = 1; i <= verticesAmount; ++i) {
        cout << i;
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << ' ' << graph[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

void deleteEdge(vector<int>* graph, int u, int v) {
    for (int i = 0; i < graph[u].size(); ++i) {
        if (graph[u][i] == v) {
            graph[u][i] = 0;
            break;
        }
    }
    for (int i = 0; i < graph[v].size(); ++i) {
        if (graph[v][i] == u) {
            graph[v][i] = 0;
            break;
        }
    }
}

void getArticulations(int root, int u) {
    timeIn[u] = low[u] = ++timer;
    status[u] = CHECKING;
    int maxLow = 0;
    childAmount[u] = 0;


    for (int i = 0; i < graph[u].size(); ++i) {
        int v = graph[u][i];

        if (status[v] == NOT_CHECKED) {
            parent[v] = u;
            getArticulations(root, v);
            childAmount[u]++;
            low[u] = min(low[v], low[u]);
            maxLow = max(maxLow, low[v]);

            if (low[v] > timeIn[u]) {
//                cout << "newBridge: " << u << ' ' << v << '\n';
                bridges.push_back(pair<int,int> (min(u, v), max(u, v)));
                deleteEdge(graphWOBridges, u, v);
            }
        }
        else if (status[v] == CHECKING && parent[u] != v) {
            low[u] = min(low[u], timeIn[v]);
        }
    }

    status[u] = CHECKED;
    isArticulation[u] = ((u == root && childAmount[u] > 1) || (u != root && maxLow >= timeIn[u])) ? 1 : 0;
}

void dfs(int u) {
    statusBiconnectedComponent[u] = CHECKING;
    for (int i = 0; i < graphWOBridges[u].size(); ++i) {
        int v = graphWOBridges[u][i];
        if (statusBiconnectedComponent[v] == NOT_CHECKED && v != 0) {
            dfs(v);
        }
    }
    statusBiconnectedComponent[u] = CHECKED;
    biconnectedComponent.push_back(u);
}

bool myCmp(pair<int,int> a, pair<int, int> b) {
    if (min(a.first, a.second) == min(b.first, b.second)) {
        return max(a.first, a.second) < max(b.first, b.second);
    }
    return min(a.first, a.second) < min(b.first, b.second);
}

int main() {
    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();
//    printData();

    for (int i = 1; i <= verticesAmount; ++i) {
        if (timeIn[i] == EMPTY) {
            getArticulations(i, i);
        }
    }

//    cout << "Arts\n";
    for (int i = 1; i <= verticesAmount; ++i) {
        if (isArticulation[i]) {
            cout << i << ' ';
        }
    }
    cout << '\n';

//    cout << "Brids\n";
    sort(bridges.begin(), bridges.begin() + bridges.size(), myCmp);
    for (int i = 0; i < bridges.size(); ++i) {
        cout << bridges[i].first << ' ' << bridges[i].second << '\n';
    }

//    cout << "Block 1\n";
    dfs(1);
    sort(biconnectedComponent.begin(), biconnectedComponent.begin() + biconnectedComponent.size());
    for (int i = 0; i < biconnectedComponent.size(); ++i) {
        cout << biconnectedComponent[i] << ' ';
    }
    cout << endl;

    return 0;
}
