#include <cstdio>
#include <iostream>
#include <vector>
#include <deque>

using namespace std;

const int INF = 1e9;
const int VERTICES_AMOUNT = 1e5 + 5;

const int NOT_CHECKED = 0;
const int CHECKING = 1;
const int CHECKED = 2;

const int EMPTY = -1;

int verticesAmount, edgesAmount;

vector<int> graph[VERTICES_AMOUNT + 1];

vector<int> status(VERTICES_AMOUNT + 1, NOT_CHECKED);

void getData() {
    cin >> verticesAmount;

    int u, v;
    while (cin >> u) {
        cin >> v;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }
}

void printData() {
    for (int i = 1; i <= verticesAmount; ++i) {
        cout << i;
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << " " << graph[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

void dfs(int startVertex) {
    status[startVertex] = CHECKING;

    for (int i = 0; i < graph[startVertex].size(); ++i) {
        int v = graph[startVertex][i];
        if (status[v] == NOT_CHECKED) {
            dfs(v);
        }
    }

    status[startVertex] = CHECKED;
}

int main() {
    freopen ("input.txt", "r", stdin);
//    freopen ("output.txt", "w", stdout);
    getData();
//    printData();

    int res = 0;
    for (int i = 1; i <= verticesAmount; ++i) {
        if (status[i] == NOT_CHECKED) {
            res++;
            dfs(i);
        }
    }

    cout << res;

    return 0;
}
