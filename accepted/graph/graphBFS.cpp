#include <iostream>
#include <vector>
#include <queue>

using namespace std;

const int NMAX = 1005;
const int EMPTY = -1;

int verticesAmount;

vector<int> graph[NMAX];

vector<int> used(NMAX + 1, EMPTY);

void getData() {
    cin >> verticesAmount;
    for (int i = 1; i <= verticesAmount; ++i) {
        int length;
        cin >> length;
        for (int j = 0; j < length; ++j) {
            int vertex;
            cin >> vertex;
            graph[i].push_back(vertex);
        }
    }
}

void printData() {
    cout << verticesAmount << endl;
    for (int i = 1; i <= verticesAmount; ++i) {
        cout << i;
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << " " << graph[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

struct Model {
    int dist;
    int vertex;
    vector<int> way;
};

vector<Model> bfs(int startVertex) {
    queue<Model> q;

    vector<Model> distance(verticesAmount + 1);
    for (int i = 0; i < distance.size(); ++i) {
        distance[i].dist = EMPTY;
    }

    Model start;
    start.dist = 0;
    start.vertex = startVertex;
    start.way = vector<int>(0);

    distance[startVertex].dist = 0;
    distance[startVertex].way = vector<int>(0);

    q.push(start);

    while (!q.empty()) {
        Model u = q.front();

        for (int v = 0; v < graph[u.vertex].size(); ++v) {
            if (distance[graph[u.vertex][v]].dist == EMPTY) {
                used[graph[u.vertex][v]] = 1;

                vector<int> currentWay = u.way;
                currentWay.push_back(graph[u.vertex][v]);

                distance[graph[u.vertex][v]].dist = u.dist + 1;
                distance[graph[u.vertex][v]].way = currentWay;

                Model m;
                m.dist = u.dist + 1;
                m.vertex = graph[u.vertex][v];
                m.way = currentWay;

                q.push(m);
            }
        }

        q.pop();
    }

    return distance;
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    getData();
//    printData();

    used[verticesAmount] = 1;
    vector<Model> res = bfs(verticesAmount);

    for (int i = 1; i < verticesAmount; ++i) {
        if (res[i].dist == EMPTY) {
            cout << 0 << endl;
            continue;
        }
        cout << res[i].dist << " ";
        for (int j = 0; j < res[i].way.size(); ++j) {
            cout << res[i].way[j] << " ";
        }
        cout << endl;
    }

    int componentAmount = (!verticesAmount) ? 0 : 1;

    for (int i = 1; i <= verticesAmount; ++i) {
        if (used[i] == EMPTY) {
            used[i] = 1;
            ++componentAmount;
            bfs(i);
        }
    }

    cout << componentAmount;

    return 0;
}

