#include <cstdio>
#include <iostream>
#include <vector>
#include <deque>

using namespace std;

const int INF = 1e9;
const int VERTICES_AMOUNT = 1e5 + 5;

const int NOT_CHECKED = 0;
const int CHECKING = 1;
const int CHECKED = 2;

const int EMPTY = -1;

int verticesAmount, edgesAmount;

vector<int> graph[VERTICES_AMOUNT + 1];
vector<int> graphTransposed[VERTICES_AMOUNT + 1];

vector<int> status(VERTICES_AMOUNT + 1, NOT_CHECKED);
vector<int> time1(VERTICES_AMOUNT + 1, EMPTY);
vector<int> time2(VERTICES_AMOUNT + 1, EMPTY);

vector<int> time2Vertices;

int timer = 0;

void getData() {
    cin >> verticesAmount;

    for (int u = 1; u <= verticesAmount; ++u) {
        int len;
        cin >> len;
        for (int i = 0; i < len; ++i) {
            int v;
            cin >> v;

            graph[u].push_back(v);
            graphTransposed[v].push_back(u);
        }
    }
}

void printData(vector<int>* graph) {
    for (int i = 1; i <= verticesAmount; ++i) {
        cout << i;
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << " " << graph[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

void dfs(int startVertex) {
    status[startVertex] = CHECKING;
    time1[startVertex] = ++timer;

    for (int i = 0; i < graphTransposed[startVertex].size(); ++i) {
        int v = graphTransposed[startVertex][i];
        if (status[v] == NOT_CHECKED) {
            dfs(v);
        }
    }

    status[startVertex] = CHECKED;
    time2[startVertex] = ++timer;
    time2Vertices.push_back(startVertex);
}

void dfsSmth(int startVertex) {
    status[startVertex] = CHECKING;

    for (int i = 0; i < graph[startVertex].size(); ++i) {
        int v = graph[startVertex][i];
        if (status[v] == NOT_CHECKED) {
            dfsSmth(v);
        }
    }

    status[startVertex] = CHECKED;
}

int main() {
    freopen ("input.txt", "r", stdin);
    freopen ("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();

    for (int i = 1; i <= verticesAmount; ++i) {
        if (status[i] == NOT_CHECKED) {
            dfs(i);
        }
    }

    int res = 0;
    status = vector<int>(VERTICES_AMOUNT + 1, NOT_CHECKED);
    for (int i = time2Vertices.size() - 1; i >= 0; --i) {
        if (status[time2Vertices[i]] == NOT_CHECKED) {
            ++res;
            dfsSmth(time2Vertices[i]);
        }
    }

    cout << res;

    return 0;
}
