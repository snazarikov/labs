#include <cstdio>
#include <iostream>
#include <vector>
#include <stack>

using namespace std;

const int VERTICES_AMOUNT = 1e5 + 5;

const int NOT_CHECKED = 0;
const int CHECKING = 1;
const int CHECKED = 2;

int verticesAmount, edgesAmount, cycle = 0;

vector<int> graph[VERTICES_AMOUNT + 1];
vector<int> status(VERTICES_AMOUNT + 1, NOT_CHECKED);
stack<int> order;

void getData() {
    cin >> verticesAmount >> edgesAmount;

    for (int i = 0; i < edgesAmount; i++) {
        int u, v;
        cin >> u >> v;
        graph[u].push_back(v);
    }
}

void printData() {
    for (int i = 1; i <= verticesAmount; ++i) {
        cout << i << " -";
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << " " << graph[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

void dfs(int u) {
    status[u] = CHECKING;
    for (int i = 0; i < graph[u].size(); ++i) {
        int v = graph[u][i];
        if (status[v] == NOT_CHECKED) {
            dfs(v);
        }
        else if (status[v] == CHECKING) {
            cout << -1;
            exit(0);
        }
    }
    status[u] = CHECKED;
    order.push(u);
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();
//    printData();

    for (int i = 1; i <= verticesAmount; ++i) {
        if (status[i] == NOT_CHECKED) {
            dfs(i);
        }
    }
    while (!order.empty()) {
        cout << order.top() << ' ';
        order.pop();
    }
    return 0;
}
