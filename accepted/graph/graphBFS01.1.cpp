#include <cstdio>
#include <iostream>
#include <vector>
#include <deque>

using namespace std;

const int INF = 1e9;
const int VERTICES_AMOUNT = 1e5 + 5;
const int EDGES_AMOUNT = 1e6 + 5;

int verticesAmount, edgesAmount;

struct Model {
    int nextVertex;
    int weight;
};

vector<Model> graph[VERTICES_AMOUNT + 1];

void getData() {
    cin >> verticesAmount >> edgesAmount;

    for (int i = 0; i < edgesAmount; i++) {
        int u, v, len;
        cin >> u >> v >> len;
        struct Model e;
        e.nextVertex = v;
        e.weight = len;
        graph[u].push_back(e);
        e.nextVertex = u;
        e.weight = len;
        graph[v].push_back(e);
    }
}

void printData() {
    for (int i = 1; i <= verticesAmount; ++i) {
        cout << i;
        for (int j = 0; j < graph[i].size(); ++j) {
            cout << " " << graph[i][j].nextVertex;
        }
        cout << endl;
    }
    cout << endl;
}

int bfs01(int startVertex, int finishVertex) {
    vector<int> dist (verticesAmount + 5, INF);
    deque<int> deq;

    dist[startVertex] = 0;

    deq.push_back(startVertex);

    while(!deq.empty()) {
        int u = deq.front();
        deq.pop_front();

        for (int i = 0; i < graph[u].size(); ++i) {
            int v = graph[u][i].nextVertex;
            if (dist[v] > dist[u] + graph[u][i].weight) {
                dist[v] = dist[u] + graph[u][i].weight;
                if (graph[u][i].weight) {
                    deq.push_back(v);
                }
                else {
                    deq.push_front(v);
                }
            }
        }
    }

    return (dist[finishVertex] == INF) ? -1 : dist[finishVertex];
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    getData();
    cout << bfs01(1, verticesAmount);
    return 0;
}
