#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const int VERTEX_AMOUNT = 1e5 + 5;

int unionSize[VERTEX_AMOUNT];
int parent[VERTEX_AMOUNT];
int isUsed[VERTEX_AMOUNT];

int vertexAmount;

int getPresentor(int u) {
    if (u == parent[u]) {
        return u;
    }
    parent[u] = getPresentor(parent[u]);
    return parent[u];
}

void makeUnion(int u, int v) {
    int uPresentor = getPresentor(u);
    int vPresentor = getPresentor(v);

    if (uPresentor == vPresentor) {
        return;
    }
    if (unionSize[u] > unionSize[v]) {
        unionSize[uPresentor] += unionSize[vPresentor];
        parent[vPresentor] = uPresentor;
    }
    else {
        unionSize[vPresentor] += unionSize[uPresentor];
        parent[uPresentor] = vPresentor;
    }
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);

    cin >> vertexAmount;

    for (int i = 1; i <= vertexAmount; i++) {
        unionSize[i] = 1;
        parent[i] = i;
    }

    int u, v, componentAmount = 0;
    while (cin >> u >> v) {
        makeUnion(u, v);
    }

    for (int i = 1; i <= vertexAmount; ++i) {
        int presentor = getPresentor(i);
        if (!isUsed[presentor]) {
            isUsed[presentor] = 1;
            componentAmount++;
        }
    }
    cout << componentAmount << endl;

    return 0;
}
