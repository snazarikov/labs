#include <stdio.h>
#include <string.h>
#define NMAX1 35
#define NMAX2 25

char words[NMAX1][NMAX2];
char S[NMAX2];
int optimal[NMAX1];
int optimalCopy [NMAX1];

int maxCount = 0, wordsCount;

int SOLVE (int order, int count) {
	int i, j;
	if (count > maxCount) {
		maxCount = count;
		for (j=0; j<wordsCount; j++) optimal[j] = optimalCopy[j];
	}
	for (i=0; i<wordsCount; i++) {
		if (i != order && words[order][words[order][0]] == words[i][1] && optimalCopy[i] == -1) {
			optimalCopy[order] = count;
			optimalCopy[i] = count+1;
			//printf("%s %s %d %d\n", words[order], words[i], order, i);
			SOLVE(i, count+1);
			optimalCopy[i] = -1;
		}
	}
	return count;
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int i, j;
    
    scanf("%d", &wordsCount);
    scanf("\n");
    for (i=0; i<wordsCount; i++) {
    	gets(S);
    	words[i][0] = strlen(S);
		strcpy(words[i]+1,S);
		optimalCopy[i] = -1;
	}
	
	for (i=0; i<wordsCount; i++) {
		for (j=0; j<wordsCount; j++) optimalCopy[j] = -1;
		SOLVE(i,0);		
	}
	
	for (i=0; i<maxCount+1; i++) {
		for (j=0; j<wordsCount; j++) {
			if (optimal[j] == i) {
				printf("%s", words[j]+1);
				printf("\n");
			}
		}
	}
	
	return 0;
}
