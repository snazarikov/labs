#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define NMAX 305

int optimalCopy[NMAX];
char S[NMAX];
char T[NMAX];
char res[NMAX];

void SOLVE (int countOp, int len, int top, int tail) {
	int i;
	if (strncmp(res, T, strlen(T)) == 0) {
		for (i=0; i<countOp; i++) {
			if (optimalCopy[i] == 2) {
				printf("E ");
			}
			else if (optimalCopy[i] == 1) {
				printf("W ");
			}
			else if (optimalCopy[i] == 3) {
				printf("R ");
			}
		}
		exit(0);
	}	
	if (countOp < 150) {
		if (top < tail) {			
			while (S[top] != T[len] && top <= tail) {
				optimalCopy[countOp] = 2;
				countOp++;
				top++;
			}
			if (top <= tail) {
				res[len] = S[top];
				optimalCopy[countOp] = 1;
				SOLVE(countOp+1, len+1, top, tail);		
				countOp++;
				optimalCopy[countOp] = 3;
				SOLVE(countOp+1, len+1, tail, top);
			}			
		}
		else {			
			while (S[top] != T[len] && top >= tail) {
				optimalCopy[countOp] = 2;
				countOp++;
				top--;
			}
			if (top >= tail) {
				res[len] = S[top];
				optimalCopy[countOp] = 1;
				SOLVE(countOp+1, len+1, top, tail);		
				countOp++;
				optimalCopy[countOp] = 3;
				SOLVE(countOp+1, len+1, tail, top);
			}			
		}		
	}	
}

int main (void) {
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	freopen("W_E_R.in", "r", stdin);
	freopen("W_E_R.out", "w", stdout);
	
	int i, j ,k;
	
	gets(S);
	gets(T);
	
	SOLVE(0, 0, 0, strlen(S)-1);
	optimalCopy[0] = 3;
	SOLVE(1, 0, strlen(S)-1, 0);
	
	return 0;
}
