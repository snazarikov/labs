/*
������ 5. "���������" 

��������� ����� N ���������� ��������� (1 <= N <= 30), ������ �� ������� ��������������� ����� ��������� 
vi  (0 < vi <= 10000). �� ����� ���������� M ���������� (1 <= M <= 10), ���������� �������� ��������� ���������. 
������ ���������� ����� ���� ����������� �� ����� ������ ����. ������������� ���������� ��������� �� ��� ��������� 
��������. ���������� ������� �� 2 ����. ����� ���������� ���������� ������� ���� � ������� j ��������� �������� i 
���������� � Dij ��� (���� 1 < Dij ? 100, ���������� �������� ��������� �������������, ��� 0 ? Dij < 1  �����������, 
��� Dij = 1 �������� ����������). ���������� ������� ���� � ������� j �������� ��������� �������� i �� Rij 
(���� Rij > 0, ��������� �������������, ��� Rij < 0 - �����������, ��� Rij = 0 �������� ����������). 
��������� ������ � ������� ��������� ��� ���������� �������� ����, ����� ��������� �������� ��������� ��������� ���� 
������������.

����
��������� ���� WIZARD.IN �������� M + 2 ������. ������ ������ �������� �������� N � M. ��������� ������ �������� 
�������� vi (i = 1, ..., N). �������, ������ �� ��������� M ����� ������������� ������ ����������. ��� ���������� 
������� ���� ��� ������ �������� ������ * � �������� Dij (i = 1, ..., N). ��� ���������� ������� ���� ��� �������� 
������ + � �������� Rij (i = 1, ..., N). ������ � ������� �������� ����� ����������� ����� ��� ����������� ���������.

�����
�������� ������ ���������� � ��������� ���� WIZARD.OUT � �������� ��� ������. ������ ������ �������� ������������ 
��������� ��������� ��������� (� ��������� �� 0.001), ������ - M ����������� ����� �������� ����� tj (j = 1, ..., M), 
��� tj = k, ���� ���������� j ���� ����������� k-� �� �����, � tj = 0, ���� ���������� �� ���� �����������.
*/

#include <stdio.h>
#include <string.h>
#define NMAX1 35
#define NMAX2 15

int optimal[NMAX2];
int optimalCopy[NMAX2];
double magic[NMAX2][NMAX1];
double price[NMAX1];
char type[NMAX2];

int itemCount, spellCount;
double maxPrice = 0;

void makeMagic (int number, double* A) {
	int i;
	if (type[number] == '*') {
		for (i=0; i<itemCount; i++) {
			A[i]*=magic[number][i];
		}
	}
	if (type[number] == '+') {
		for (i=0; i<itemCount; i++) {
			A[i]+=magic[number][i];
		}
	}
}

double getPrice (double* A) {
	int i;
	double sum = 0;
	for (i=0; i<itemCount; i++) sum+=A[i];
	return sum;
}

void SOLVE (int count, double currPrice) {
	int i, j;
	double prev[NMAX1];
	if (currPrice > maxPrice) {
		maxPrice = currPrice;
		for (j=0; j<spellCount; j++) optimal[j] = optimalCopy[j];
	}
	for (i=0; i<spellCount; i++) {
		if (optimalCopy[i] == 0) {
			optimalCopy[i] = count;
			memcpy(prev, price, NMAX1*sizeof(double));
			makeMagic(i, price);
			SOLVE(count+1, getPrice(price));
			memcpy(price, prev, NMAX1*sizeof(double));
			optimalCopy[i] = 0;
		}
	}
}

int main (void) {
	//freopen("input.txt", "r", stdin);
	freopen("wizard.in", "r", stdin);
	freopen("wizard.out", "w", stdout);
	
	int i, j, k;
	
	scanf("%d %d\n", &itemCount, &spellCount);
	
	for (i=0; i<itemCount; i++) scanf("%lf", &price[i]);
	
	for (i=0; i<spellCount; i++) {
		scanf("\n");
		scanf("%c", &type[i]);
		for (j=0; j<itemCount; j++) {
			scanf("%lf", &magic[i][j]);
			if (magic[i][j] == '*') magic[i][j] == 1;
			if (magic[i][j] == '+') magic[i][j] == 0;
		}
	}
    
    SOLVE(1, getPrice(price));
    
	printf("%.3lf\n", maxPrice);
	for (i=0; i<spellCount; i++) printf("%d ", optimal[i]);
	
	return 0;
}
