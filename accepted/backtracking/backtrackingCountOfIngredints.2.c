#include <stdio.h>
#include <string.h>
#define NMAX1 205
#define NMAX2 25

int pairF[NMAX1];
int pairS[NMAX1];
int optimal[NMAX2];

int maxCount = 0, ingCount, pairsCount;

int usable (int firstIng, int secondIng) {
	int i;
	for (i=0; i<pairsCount; i++) {
		if ((pairF[i] == firstIng && pairS[i] == secondIng) || (pairF[i] == secondIng && pairS[i] == firstIng)) return 0;
	}
	return 1;
}

int take (int number) {
	int i;
	for (i=0; i<ingCount; i++) {
		if (optimal[i] == 1) {
			if (usable(number, i)==0) return 0;
		}
	}
	return 1;
}

void SOLVE (int number, int count) {
	if (count > maxCount) {
		maxCount = count;
	}
	if (number<ingCount) {
		if (optimal[number] == 0 && take(number)==1 ) {
			//printf("%d %d\n", number, take(number));
			optimal[number] = 1;
			SOLVE(number+1, count+1);
			optimal[number] = 0;
		}
		SOLVE(number+1, count);
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int i, j, k;
	
	scanf("%d %d", &ingCount, &pairsCount);
	
	for (i=0; i<pairsCount; i++) {
		scanf("%d %d", &pairF[i], &pairS[i]);
		pairF[i]-=1;
		pairS[i]-=1;
	}
    
    //printf("%d %d\n", ingCount, pairsCount);
	SOLVE(0,0);
	
	printf("%d", maxCount);
	return 0;
}
