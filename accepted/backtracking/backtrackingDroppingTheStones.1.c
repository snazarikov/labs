#include <stdio.h>
#include <string.h>
#define NMAX 15

int weight[NMAX];
int price[NMAX];
int optimal[NMAX];

int maxCount = 0, count, difference, maxPrice = 0;

void SOLVE(int weightA, int weightB, int priceB, int from) {
	int i, check = 0;
	for (i = 0; i < count; ++i) {
        if (optimal[i] == 1) {
            check++;
        }
	}
	if (check == count) {
		if (priceB > maxPrice) {
            maxPrice = priceB;
		}
	}
	if (check < count) {
		for (i = 0; i < count; ++i) {
			if (optimal[i] == 0) {
				optimal[i] = 1;
				if (weightA - weightB >= difference && from == 1) {
					SOLVE(weightA, weightB + weight[i], priceB + price[i], 2);
				}
				else if (weightB - weightA >= difference && from == 2) {
					SOLVE(weightA + weight[i], weightB, priceB, 1);
				}
				else if (from == 2) {
					SOLVE(weightA, weightB + weight[i], priceB + price[i], 2);
				}
				else if (from == 1) {
					SOLVE(weightA + weight[i], weightB, priceB, 1);
				}
				optimal[i] = 0;
			}
		}
	}
}

int main(void) {
//	freopen("input.txt", "r", stdin);
	freopen("stones.in", "r", stdin);
	freopen("stones.out", "w", stdout);

	int i, j;

	scanf("%d %d", &count, &difference);

	//printf("%d %d\n", count, difference);

	for (i = 0; i < count; i++) {
		scanf("%d %d", &weight[i], &price[i]);
	//	printf("%d %d\n", weight[i], price[i]);
	}

	for (i = 0; i < count; i++) {
		optimal[i] = 1;
		SOLVE(weight[i], 0, 0, 1);
		optimal[i] = 0;
	}

	printf("%d", maxPrice);

	return 0;
}
