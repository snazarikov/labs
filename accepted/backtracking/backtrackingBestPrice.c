#include <stdio.h>
#include <string.h>
#define NMAX 30

int vol[NMAX];
int price[NMAX];
int optimal[NMAX];

int volume, boxCount, minPrice = 1000000000;

void SOLVE (int number, int count, int sum) {
	if (count>=volume) {
		if (sum<minPrice) minPrice = sum;
	}
	if (number<boxCount) {
		if (optimal[number] == 0) {
			optimal[number] = 1;
			SOLVE(number+1, count+vol[number], sum+price[number]);
			optimal[number] = 0;
		}
		SOLVE(number+1, count, sum);
	}
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int i, j;
	
	scanf("%d %d", &volume, &boxCount);
	for (i=0; i<boxCount; i++) {
		scanf("%d", &vol[i]);
	}
	for (i=0; i<boxCount; i++) {
		scanf("%d", &price[i]);
	}
	
	SOLVE(0,0,0);
	
	if (minPrice == 1000000000) minPrice = -1;
	printf("%d", minPrice);
	return 0;
}

