#include <stdio.h>
#include <string.h>

int gamesCount, difference;
double winF, winS, draw, res = 0;

void SOLVE (int countG, int pointF, int pointS, double value) {
	if (countG == gamesCount || pointF - pointS >= difference || pointS - pointF >= difference) {
		res += countG * value;
	}
	if (countG < gamesCount && pointF - pointS < difference && pointS - pointF < difference) {
		SOLVE(countG + 1, pointF + 1, pointS, value * winF);
		SOLVE(countG + 1, pointF, pointS + 1, value * winS);
		SOLVE(countG + 1, pointF, pointS, value * draw);
	}
}

int main (void) {
	//freopen("input.txt", "r", stdin);
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	
	int i;
	double a, b, c;
	
	scanf("%lf %lf %lf %d %d", &a, &b, &c, &gamesCount, &difference);
	
	winF = a / (a + b + c);
	winS = b / (a + b + c);
	draw = c / (a + b + c);
	
	SOLVE(0, 0, 0, 1);

	printf("%.4lf", res);

	return 0;
}
