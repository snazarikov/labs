#include <stdio.h>
#include <string.h>
#define NMAX 105

int A[NMAX];

void sort(int* A,int a,int b){
	int m, k, l, r, tmp;
    if (a>=b) return;
    m = a+rand()%(b-a+1);
    k = A[m];
    l = a-1;
    r = b+1;
    while (1){
        while (A[++l]<k);
        while (A[--r]>k);
        if (l>=r) break;
        tmp = A[l];
        A[l]=A[r];
        A[r]=tmp;
    }
    sort(A,a,r);
    sort(A,r+1,b);
}

int main (void) {
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	freopen("contest.in", "r", stdin);
	freopen("contest.out", "w", stdout);
	
	int i, j ,k, duration, count, solved = 0, fine = 0, help = 0;
	
	scanf("%d %d", &duration, &count);
	duration*=60;
	for (i=0; i<count; i++) {
		scanf("%d", &A[i]);
	}
	
	sort(A, 0, count-1);
	
	i = 0;
	while (duration - A[i] >= 0 && i < count) {
		solved++;
		fine += help + A[i];
		help += A[i];
		duration -= A[i];
		i++;
	}
	
	printf("%d %d", solved, fine);
	
	return 0;
}
