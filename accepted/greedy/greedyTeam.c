#include <stdio.h>
#include <stdlib.h>
#define NMAX 1005

int A[NMAX];
int B[NMAX];

void sort(int* A,int a,int b){
	int m, k, l, r, tmp;
    if (a>=b) return;
    m = a+rand()%(b-a+1);
    k = A[m];
    l = a-1;
    r = b+1;
    while (1){
        while (A[++l]<k);
        while (A[--r]>k);
        if (l>=r) break;
        tmp = A[l];
        A[l]=A[r];
        A[r]=tmp;
    }
    sort(A,a,r);
    sort(A,r+1,b);
}

int main (void) {
	freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	//freopen("team.in", "r", stdin);
	//freopen("team.out", "w", stdout);
	
	int i, j, count, pos = 0, t, res = 0;
	
	scanf("%d", &count);
	
	for (i=0; i<count; i++) {
		scanf("%d", &A[i]);
	}
	
	for (i=0; i<count; i++) {
		scanf("%d", &B[i]);
	}
	
	sort(A, 0, count-1);
	sort(B, 0, count-1);
	
	
	for (i=0; i<count; i++) {
		for (j=0; j<count; j++) {
			if (B[i] > A[j]) {
				res++;
				A[j] = 10005;
				break;
			}
		}
	}
	
	printf("%d", res);
	
	return 0;
}
