#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define NMAX 105

long long A[NMAX];
long long B[NMAX];
double C[NMAX];

int main (void) {
	freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	//freopen("penalty.in", "r", stdin);
	//freopen("penalty.out", "w", stdout);
	
	long long i, j, count, t, res = 0, pay = 0, min, pos = 0, tmp2, tmp3;
	double tmp1;
	
	scanf("%I64d", &count);
//	printf("%I64d\n", count);
	
	for (i=0; i<count; i++) {
		scanf("%I64d %I64d", &A[i], &B[i]);
		C[i] = (double)A[i]/(double)B[i];
	//	printf("%lf ", C[i]);
		pay+=B[i];
	}	
	//printf("\n%I64d\n\n", pay);
	
	for (i=0; i<count-1; i++) {
		for (j=i+1; j<count; j++) {
			if (C[i] > C[j]) {
				tmp1 = C[i];
				C[i] = C[j];
				C[j] = tmp1;
				tmp2 = B[i];
				B[i] = B[j];
				B[j] = tmp2;
				tmp3 = A[i];
				A[i] = A[j];
				A[j] = tmp3;
			}
		}
	}
	
//	for (i=0; i<count; i++) {
//		printf("%I64d %I64d %lf\n", A[i], B[i], C[i]);
//	}
	
	for (i=0; i<count; i++) {
		pay -= B[i];
		res += (pay * A[i]);
	}
	
	printf("%I64d", res);
	
	return 0;
}

