#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>

long long k, x, y;

void getData() {
	scanf("%I64d", &k);
	scanf("%I64d", &x);
	scanf("%I64d", &y);
}

long long SOLVE() {
	if (x >= y) {
		return x;
	}
	if (y >= (y / x) * x && y <= (y / x) * (x - 1 + k)) {
		return y;
	}
	return x * (y / x + 1);
}

int main (void) {
	//freopen("delivery.in", "r", stdin);
	//freopen("delivery.out", "w", stdout);
	
	getData();
	printf("%I64d", SOLVE());
	
	return 0;
}
