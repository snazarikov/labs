#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define NMAX 200005

long long sign[NMAX];
long long pos[NMAX];
long long time[NMAX];
long long queue[NMAX];
long long dest[NMAX];

int n, m, countProt = -1, destroyed = 0;

void getData() {
	int i;
	long long p, t;
	
	scanf("%d", &n);
	for (i=0; i<n; i++) {
		scanf("%I64d %I64d", &p, &sign[i]);
		pos[i] = p * 2;
	}
	
	scanf("%d", &m);
	for (i=0; i<m; i++) {
		scanf("%I64d", &t);
		time[i] = t * 2;
	}
}

void printData() {
	int i;
	
	printf("%d\n", n);
	for (i=0; i<n; i++) {
		printf("%I64d %I64d\n", pos[i], sign[i]);
	}
	
	printf("%d\n", m);
	for (i=0; i<m; i++) {
		printf("%I64d ", time[i]);
	}
}

void sort(long long* A, int a,int b){
	long long tmp1, tmp2, m, k, l, r;
    if (a>=b) return;
    m = a+rand()%(b-a+1);
    k = A[m];
    l = a-1;
    r = b+1;
    while (1){
        while (A[++l]<k);
        while (A[--r]>k);
        if (l>=r) break;
        tmp1 = A[l];
        A[l]=A[r];
        A[r]=tmp1;
    }
    sort(A,a,r);
    sort(A,r+1,b);
}

void SOLVE() {
	int i, help = 0;
	
	for (i=0; i<n; i++) {
		if (sign[i] == 1) {
			countProt++;
			queue[countProt] = pos[i];
		}
		if (sign[i] == -1) {
			if (countProt >= 0) {
				dest[destroyed] = llabs(queue[countProt] - pos[i])/2;
				destroyed++;
				countProt--;
			}
		}
	}
	
	sort(dest, 0, destroyed - 1);
		
	for (i=0; i<m; i++) {
		while (dest[help] <= time[i] && help != destroyed) {
			n -= 2;
			help++;
		}
		printf("%d\n", n);
	}	
}

int main (void) {
	freopen("linear.in", "r", stdin);
	freopen("linear.out", "w", stdout);
	
	getData();
	SOLVE();
	
//	printf("\n")
//	printData();
	
	return 0;
}

