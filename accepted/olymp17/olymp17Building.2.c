#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define NMAX 1005

long long A[NMAX];
long long B[NMAX];

int main (void) {
	//freopen("inputD.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	freopen("building.in", "r", stdin);
	freopen("building.out", "w", stdout);
	
	long long i, j, floorCount, k, x, y, roomCount, entRoom = 0, help = 0, floorInBlock = 0;
	
	scanf("%I64d %I64d %I64d %I64d\n", &floorCount, &k, &x, &y);
	scanf("%I64d\n", &roomCount);
	for (i=0; i<roomCount; i++) {
		scanf("%I64d", &A[i]);
	}
	
	entRoom = (floorCount / k) * x + (floorCount - (floorCount / k)) * y;
	
	for (i=0; i<roomCount; i++) {
		if (A[i]%entRoom != 0) {
			A[i]%=entRoom;
		}
		else A[i] = entRoom;
		//printf("%I64d ", A[i]);
	}
	
	help = x + (k - 1) * y;
	//printf("\n%I64d\n", help);
	for (i=0; i<roomCount; i++) {
		if (A[i]%help != 0) {
			B[i]=A[i]%help;
		}
		else B[i] = help;
	//	printf("%I64d ", B[i]);
	}
	
	for (i=0; i<roomCount; i++) {
		if (B[i] >  (k-1) * y && B[i] <= (k-1) * y + x) {
			if (A[i] % help == 0) printf("%I64d\n", A[i]/help*k);
			else printf("%I64d\n", ((A[i]/help) + 1)*k);
		}
		else if (B[i] > 0 && B[i] <= y) {
			if (A[i]/help == 0) printf("1\n");
			else printf("%I64d\n", (A[i]/help*k)+1);
		}
		else {
			if (B[i] % y == 0) floorInBlock = B[i]/y;
			else floorInBlock = (B[i]/y) + 1;
			printf("%I64d\n", floorInBlock + k*(A[i]/help));
		}
	}
	
	return 0;
}
