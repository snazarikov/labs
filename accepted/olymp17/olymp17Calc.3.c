#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#define NMAX 65

long long numeral;
long long T[NMAX][NMAX][NMAX];
int a, b, c, count;

int main (void) {
	//freopen("inputD.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	//freopen("calc.in", "r", stdin);
	//freopen("calc.out", "w", stdout);
	
	int i, j, k;
	
	scanf("%I64d %d %d %d", &numeral, &a, &b, &c);
	
	for (i=0; i<=a; i++) {
		for (j=0; j<=b; j++) {
			for (k=0; k<=c; k++) {
				T[i][j][k] = LLONG_MAX;
			}
		}
	}
	T[a][b][c] = numeral;
	
	for (i=a; i>=0; i--) {
		for (j=b; j>=0; j--) {
			for (k=c; k>=0; k--) {
				if (i != 0 && T[i][j][k] / 2 < T[i-1][j][k]) {
					T[i-1][j][k] = T[i][j][k] / 2;
				}
				
				if (j != 0 && (T[i][j][k] + 1) / 2 < T[i][j-1][k]) {
					T[i][j-1][k] = (T[i][j][k] + 1) / 2;
				}					
				
				if (k != 0 && (T[i][j][k] - 1) / 2 < T[i][j][k-1]) {
					T[i][j][k-1] = (T[i][j][k] - 1) / 2;
				}					
			}
		}
	}
	
//	for (i=0; i<=a; i++) {
//		for (j=0; j<=b; j++) {
//			for (k=0; k<=c; k++) {
//				printf("%I64d ", T[i][j][k]);
//			}
//			printf("\n");
//		}
//		printf("\n");
//	}
	
	printf("%I64d", T[0][0][0]);	
	
	return 0;
}
