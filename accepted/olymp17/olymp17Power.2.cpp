#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <set>
#define NMAX 200005
using namespace std;

int posX[NMAX];
int posY[NMAX];

int n, k;

void getData() {
	scanf("%d %d", &n, &k);
	for (int i=0; i<n; i++) {
		scanf("%d %d", &posX[i], &posY[i]);
	}
}

void printData() {
	printf("%d %d\n\n", n, k);
	for (int i=0; i<n; i++) {
		printf("%d %d\n", posX[i], posY[i]);
	}
}

void sort(int* A, int* B, int a,int b){
	int tmp1, tmp2, m, k, l, r;
    if (a>=b) return;
    m = a+rand()%(b-a+1);
    k = A[m];
    l = a-1;
    r = b+1;
    while (1){
        while (A[++l]>k);
        while (A[--r]<k);
        if (l>=r) break;
        tmp1 = A[l];
        A[l]=A[r];
        A[r]=tmp1;
        tmp2 = B[l];
        B[l]=B[r];
        B[r]=tmp2;
    }
    sort(A,B,a,r);
    sort(A,B,r+1,b);
}

multiset <int> itsMagic;

void SOLVE() {
	int i;
	
	for (i=0; i<k; i++) {
		itsMagic.insert(posY[i]);
	}
	
	long long result = (long long)posX[k-1] * (long long)(*itsMagic.begin());
	
	for (i=k; i<n; i++) {
		if (posY[i] > *itsMagic.begin()) {
			itsMagic.erase(itsMagic.begin());
			itsMagic.insert(posY[i]);
			if ((long long)*itsMagic.begin() * (long long)posX[i] > result) {
				result = (long long)*itsMagic.begin() * (long long)posX[i];
			}
		}
	}
	
	printf("%I64d", result);
}

int main (void) {
	freopen("power.in", "r", stdin);
	freopen("power.out", "w", stdout);
	
	getData();
	sort(posX, posY, 0, n-1);
	//printData();
	SOLVE();
	
	return 0;
}

