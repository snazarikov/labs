#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const int VERTEX_AMOUNT = 1e6 + 5;
const int SOME_MAX = 25;

int vertexAmount, requestAmount;

vector<int> trash[VERTEX_AMOUNT];
vector<int> isVisited(VERTEX_AMOUNT, 0);
vector<int> timeIn(VERTEX_AMOUNT, 0);
vector<int> timeOut(VERTEX_AMOUNT, 0);

int timer = 1, upperBound = 1;

int parent[VERTEX_AMOUNT][SOME_MAX];

void getData() {
    cin >> vertexAmount;
    int u, v;
    for (int i = 0; i < vertexAmount - 1; ++i) {
        cin >> u >> v;
        trash[u].push_back(v);
        trash[v].push_back(u);
    }
}

void dfs(int u, int p) {
    if (isVisited[u]) {
        return;
    }

    parent[u][0] = p;
    for (int i = 1; i <= upperBound; ++i) {
        parent[u][i] = parent[parent[u][i - 1]][i - 1];
    }

    isVisited[u] = 1;
    timeIn[u] = timer++;

    int v;
    for (int i = 0; i < trash[u].size(); ++i) {
        v = trash[u][i];
        if (isVisited[v]) {
            continue;
        }
        dfs(v, u);
    }

    timeOut[u] = timer++;
}

int isAncestor(int u, int v) {
    return ((timeIn[v] > timeIn[u]) && (timeOut[v] < timeOut[u]));
}

int getLCA(int u, int v) {
	if (u == v) {
		return u;
	}
    if (isAncestor(u, v)) {
//    	cout << "UEK ";
        return u;
    }
    if (isAncestor(v, u)) {
//    	cout << "VEK ";
        return v;
    }

    for (int i = upperBound; i >= 0; --i) {
        if (!isAncestor(parent[u][i], v) && parent[u][i]) {
            u = parent[u][i];
        }
    }

//	cout << "KEK ";
    return parent[u][0];
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    getData();
    while ((1 << upperBound) <= vertexAmount) {
        upperBound++;
	}
    dfs(1, 1);

    int u, v;
    cin >> requestAmount;
    for (int i = 0; i < requestAmount; ++i) {
        cin >> u >> v;
        cout << getLCA(u, v) << " ";
    }

    return 0;
}
