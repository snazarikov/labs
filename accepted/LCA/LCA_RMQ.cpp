#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const int VERTEX_AMOUNT = 1e6 + 5;
const int INF = 1e9;

int vertexAmount, requestAmount;
int ELEMENT_AMOUNT = 0;

struct treeElement {
    int parent;
    int depth;
    vector<int> children;
};

treeElement tree[VERTEX_AMOUNT];

vector<int> trash[VERTEX_AMOUNT];
int isVisited[VERTEX_AMOUNT];

int A[VERTEX_AMOUNT * 2];
int ACounter = 0;

int first[VERTEX_AMOUNT];

int segmentTree[5 * VERTEX_AMOUNT];

void printTree() {
    for (int i = 1; i <= vertexAmount; ++i) {
        cout << "Vertex: " << i << " parent: " << tree[i].parent << " depth: " << tree[i].depth << endl;
        cout << "Children:" << endl;
        for (int j = 0; j < tree[i].children.size(); ++j) {
            cout << tree[i].children[j] << " ";
        }
        cout << endl;
    }
}

void printSegmentTree() {
    int smth = 1;
    for (int i = 1; i < 2 * ELEMENT_AMOUNT; i *= 2) {
//        cout << "i: " << i << "  ";
        for (int j = 0; j < i; ++j) {
            cout << "[" << segmentTree[smth + j] << "] ";
        }
        cout << endl;
        smth += i;
    }
}

int findPow(int n) {
    int pow = 1;
    while (n > pow) {
        pow *= 2;
    }
    return pow;
}

void getData() {
    cin >> vertexAmount;
    int u, v;
    for (int i = 0; i < vertexAmount - 1; ++i) {
        cin >> u >> v;
        trash[u].push_back(v);
        trash[v].push_back(u);
    }
}

void buildTree(int u, int depth) {
    if (isVisited[u]) {
        return;
    }

    A[ACounter++] = u;

    tree[u].depth = depth;
    isVisited[u] = 1;

    int v;
    for (int i = 0; i < trash[u].size(); ++i) {
        v = trash[u][i];
        if (isVisited[v]) {
            continue;
        }

        tree[u].children.push_back(v);
        tree[v].parent = u;
        buildTree(v, depth + 1);
        A[ACounter++] = u;
    }
}

void buildFirst() {
    for (int i = 0; i < ACounter; ++i) {
        if (!first[A[i]]) {
            first[A[i]] = i + 1;
        }
    }
}

void buildSegmentTree(int* elems, int amount) {
    for (int i = ELEMENT_AMOUNT; i < ELEMENT_AMOUNT + amount; ++i) {
        segmentTree[i] = elems[i - ELEMENT_AMOUNT];
    }

    for (int i = ELEMENT_AMOUNT + amount; i < ELEMENT_AMOUNT * 2; ++i) {
        segmentTree[i] = 0;
    }

    for (int i = ELEMENT_AMOUNT - 1; i; --i) {
        segmentTree[i] = ((min(tree[segmentTree[2 * i]].depth, tree[segmentTree[2 * i + 1]].depth) == tree[segmentTree[2 * i]].depth) ? (segmentTree[2 * i]) : (segmentTree[2 * i + 1]));
    }
}

int getLCA(int currVertex, int leftLeaf, int rightLeaf, int left, int right) {
    if (left > right) {
        return 0;
    }
    if (left == leftLeaf && right == rightLeaf) {
        return segmentTree[currVertex];
    }

    int middle = (leftLeaf + rightLeaf) / 2;

    int minLeft = getLCA(currVertex * 2, leftLeaf, middle, left, min(right, middle));
    int minRight = getLCA(currVertex * 2 + 1, middle + 1, rightLeaf, max(middle + 1, left), right);
    return ((tree[minLeft].depth < tree[minRight].depth) ? (minLeft) : (minRight));
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    getData();

    tree[0].depth = INF;
    tree[1].parent = 0;

    buildTree(1, 0);
    ELEMENT_AMOUNT = findPow(ACounter);
    cout << ELEMENT_AMOUNT << endl;
    buildFirst();
    buildSegmentTree(A, ACounter);

    int u, v, a, b;
    cin >> requestAmount;
    for (int i = 0; i < requestAmount; ++i) {
        cin >> a >> b;
        if (first[a] < first[b]) {
            u = first[a];
            v = first[b];
        }
        else {
            u = first[b];
            v = first[a];
        }
        cout << getLCA(1, 0, ELEMENT_AMOUNT - 1, u - 1, v - 1) << " ";
    }

    return 0;
}
