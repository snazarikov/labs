#include <cstdio>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const int VERTEX_AMOUNT = 1e6 + 5;

int vertexAmount, requestAmount;

struct treeElement {
    int parent;
    int height;
    vector<int> children;
} tree[VERTEX_AMOUNT];

vector<int> trash[VERTEX_AMOUNT];
vector<int> isVisited(VERTEX_AMOUNT, 0);

void getData() {
    cin >> vertexAmount;
    int u, v;
    for (int i = 0; i < vertexAmount - 1; ++i) {
        cin >> u >> v;
        trash[u].push_back(v);
        trash[v].push_back(u);
    }
}

void buildTree(int u, int height) {
    if (isVisited[u]) {
        return;
    }

    tree[u].height = height;
    isVisited[u] = 1;

    int v;
    for (int i = 0; i < trash[u].size(); ++i) {
        v = trash[u][i];
        if (isVisited[v]) {
            continue;
        }

        tree[u].children.push_back(v);
        tree[v].parent = u;
        buildTree(v, height + 1);
    }
}

void printTree() {
    for (int i = 1; i <= vertexAmount; ++i) {
        cout << "Vertex: " << i << " parent: " << tree[i].parent << " height: " << tree[i].height << endl;
        cout << "Children:" << endl;
        for (int j = 0; j < tree[i].children.size(); ++j) {
            cout << tree[i].children[j] << " ";
        }
        cout << endl;
    }
}

int getLCA(int u, int v) {
    while (tree[u].height != tree[v].height) {
        if (tree[u].height < tree[v].height) {
            v = tree[v].parent;
        }
        else if (tree[v].height < tree[u].height) {
            u = tree[u].parent;
        }
    }

    while (u != v) {
        u = tree[u].parent;
        v = tree[v].parent;
    }

    return u;
}

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    getData();
    tree[1].parent = 0;
    buildTree(1, 1);

    int u, v;
    cin >> requestAmount;
    for (int i = 0; i < requestAmount; ++i) {
        cin >> u >> v;
        cout << getLCA(u, v) << " ";
    }

    return 0;
}
