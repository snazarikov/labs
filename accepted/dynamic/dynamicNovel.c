#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define NMAX 105
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

int n, k, res;

int A[NMAX];
int T[NMAX][NMAX];

int sum (int a, int b) {
	int i, result = 0;
	
	for (i=a-1; i<b; i++) {
		result += A[i];
	}
	
	return result;
}

int SEARCH (int chap, int vol) {
	int i, maximum = 0, minimum = INT_MAX;
//	printf(" %d %d KEK\n", chap, vol);
	
	if (T[chap][vol] != 0) {
	//	printf("LEEEEEEEEEEEEEEEEEEEEEEEEEEL %d\n", T[chap][vol]);
		return T[chap][vol];
	}
	
	for (i=vol-1; i<chap; i++) {
	//	printf("SEK\n");
		maximum = max(SEARCH(i, vol-1), sum(i + 1, chap));
		minimum = min(minimum, maximum);
	}
	//printf("YA SDELYAL %d %d\n", chap, vol);
	T[chap][vol] = minimum;
	
	return T[chap][vol];
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int i, j, help = 0;
	
	scanf("%d %d", &n, &k);
	//printf("%d %d\n", n, k);
	
	for (i=0; i<n; i++) {
		scanf("%d", &A[i]);
	//	printf("%d ", A[i]);
	}
	
	//T[n][1] = sum(1, n);
	//T[1][1] = A[0];
	for (i=1; i<=n; i++) {
		T[i][1] = sum(1, i);
	//	printf("summa %d", T[i][1]);
	}

	printf("%d", SEARCH(n, k));
	
	return 0;
}
