#include <stdio.h>
#define NMAX 100

int K[NMAX][NMAX];
int T[NMAX][NMAX];

int empty = 2000000001, n;
long long sum = 0;

int max (int a, int b) {
	if (a>b) return a;
	else return b;
}

int SEARCH (int i, int j) {
	if (T[i][j]!=empty) return T[i][j];
	if (i==n-1) sum = K[n-1][j];
	else sum = maximum(SEARCH(i+1, j), SEARCH(i+1, j+1)) + K[i][j];
	T[i][j] = sum;
	return sum;
}

int main (void) {
	
	freopen ("input.txt", "r", stdin);
	freopen ("output.txt", "w", stdout);
	int count = 1, i, j;
	long long maxSum = -1000001;
	scanf("%d\n", &n);
	for (i=0; i<n; i++) {
		for (j=0; j<count; j++) {
			scanf("%d ", &K[i][j]);
			T[i][j] = empty;
		}
		scanf("\n");
		count++;
	}
	maxSum = SEARCH(0, 0);
	printf("%I64d", maxSum);
	return 0;

}
