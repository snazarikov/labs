#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

const int NMAX = 185;
const int EMPTY = -1;
const int INF = 1e9 + 5;

int n, k, res;

int table[NMAX][NMAX];
int stacks[NMAX];

void getData() {
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> stacks[i];
    }
    cin >> k;
}

void printData() {
    cout << n << endl;
    for (int i = 0; i < n; ++i) {
        cout << stacks[i] << " ";
    }
    cout << k << endl;
}

void printTable() {
    cout << endl;
    for (int i = 0; i <= k; ++i) {
        for (int j = 0; j <= n; ++j) {
            cout << table[i][j] << " ";
        }
        cout << endl;
    }
}

int sums[NMAX];

int getSum(int start, int amount) {
    return sums[start + amount - 1] - sums[start] + stacks[start];
}

int dynamic(int prevAmount, int currAmount) {
    if (currAmount <= 0) {
        return 0;
    }
    if (table[prevAmount][currAmount] != EMPTY) {
        return table[prevAmount][currAmount];
    }

    for (int i = prevAmount; i >= 1; --i) {
        if (i <= currAmount) {
            table[prevAmount][currAmount] = max(table[prevAmount][currAmount], getSum(n - currAmount, i) + (getSum(n - currAmount + i, currAmount - i) - dynamic(i, currAmount - i)));
        }
    }
//    cout << "KEK" << prevAmount << ' ' << currAmount << ' ' << table[prevAmount][currAmount] << '\n';
    return table[prevAmount][currAmount];
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
//	printData();

    sums[0] = stacks[0];
//    cout << sums[0] << " ";
    for (int i = 1; i <= n; ++i) {
        sums[i] = sums[i - 1] + stacks[i];
//        cout << sums[i] << " ";
    }
//    cout << endl;

    memset(table, EMPTY, sizeof(table));

    for (int i = 0; i < n; ++i) {
        table[i][0] = 0;
    }

    cout << dynamic(k ,n);

	return 0;
}
