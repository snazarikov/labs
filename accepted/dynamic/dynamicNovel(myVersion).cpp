#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

const int NMAX = 105;
const int EMPTY = -1;
const int INF = 1e9 + 5;

int n, k, res;

int table[NMAX][NMAX];
int parts[NMAX];

void getData() {
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> parts[i];
    }
    cin >> k;
}

void printData() {
    cout << n << endl;
    for (int i = 0; i < n; ++i) {
        cout << parts[i] << " ";
    }
    cout << endl << k;
}

void printTable() {
    cout << endl;
    for (int i = 0; i <= k; ++i) {
        for (int j = 0; j <= n; ++j) {
            cout << table[i][j] << " ";
        }
        cout << endl;
    }
}

int getSum(int start, int amount) {
    int sum = 0;
    for (int i = start - 1; i > start - 1 - amount; --i) {
        sum += parts[i];
    }
    return sum;
}

int dynamic(int tom, int part) {
    if (table[tom][part] != EMPTY) {
        return table[tom][part];
    }

    int myMax = -1;
    int myMin = INF;

    for (int i = part; i >= 0; --i) {
        int currSum = getSum(part, i);
        myMax = max(currSum, dynamic(tom - 1, part - i));
        myMin = min(myMin, myMax);
    }

    return table[tom][part] = myMin;
}

int main (void) {
//	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);

	int i, j;

	getData();
//	printData();

	memset(table, EMPTY, sizeof(table));
	table[0][0] = 0;

	for (int i = 0; i <= k; ++i) {
        table[i][0] = 0;
	}


	for (int i = 1; i <= n; ++i) {
        table[0][i] = INF;
	}

//	printTable();

//    cout << getSum(3, 3) << endl;

	cout << dynamic(k, n);

//	printTable();

	return 0;
}
