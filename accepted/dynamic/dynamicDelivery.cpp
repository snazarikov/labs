#include <iostream>
#include <cstdlib>
#include <cstring>
#include <climits>

using namespace std;

const int NMAX = 35;
const int EMPTY = INT_MAX;
const int INF = INT_MAX;

int n, z, res;

int table[NMAX][NMAX][NMAX];
int dist[NMAX][NMAX];

void getData() {
    cin >> n;
    for (int i = 0; i < n; ++i) {
       for (int j = i + 1; j < n; ++j) {
            cin >> dist[i][j];
       }
    }
}

void printData() {
    cout << n << '\n';
    for (int i = 0; i < n; ++i) {
       for (int j = 0; j < n; ++j) {
            cout << dist[i][j] << ' ';
       }
       cout << '\n';
    }
}

void setTable() {
    for (int k = 0; k < NMAX; ++k) {
        for (int i = 0; i < NMAX; ++i) {
            for (int j = 0; j < NMAX; ++j) {
                table[k][i][j] = EMPTY;
            }
        }
    }
}

int dynamic(int z, int y, int x) {
//    cout << "KEK " << z << ' ' << y << ' ' << x << '\n';
    if (z >= n || y >= n || x >= n) {
        return 0;
    }
    if (table[z][y][x] != EMPTY) {
//        cout << "GG " <<  z << ' ' << y << ' ' << x << ' ' << table[z][y][x] << '\n';
        return table[z][y][x];
    }

    int myMax = max(max(x, y), z);
    table[z][y][x] = min(min(dynamic(myMax + 1, y, x) + dist[z][myMax + 1], dynamic(z, myMax + 1, x) + dist[y][myMax + 1]), min(table[z][y][x], dynamic(z, y, myMax + 1) + dist[x][myMax + 1]));

//    cout << z << ' ' << y << ' ' << x << '\n';

    return table[z][y][x];
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	getData();
//	printData();

    setTable();

	cout << dynamic(0, 0, 0);

	return 0;
}
