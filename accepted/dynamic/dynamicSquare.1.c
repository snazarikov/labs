#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define NMAX 105

int n, m, res;

int T[NMAX][NMAX];

int SEARCH (int height, int width) {
	int i, help, min;
	
	min = INT_MAX;
	
	if (T[height][width] != -1) return T[height][width];
	
	for (i=1; i<height; i++) {
		help = SEARCH(i, width) + SEARCH(height - i, width) + 1;
		if (help < min) {
			min = help;
		}
	}
	for (i=1; i<width; i++) {
		help = SEARCH(height, i) + SEARCH(height, width - i) + 1;
		if (help < min) {
			min = help;
		}
	}
	
	T[height][width] = min;
	
	return T[height][width];
}

int main (void) {
//	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);
	
	int i, j;
	
	scanf("%d %d", &n, &m);
	
	for (i=1; i<=n; i++) {
		for (j=1; j<=m; j++) {
			if (i == j) {
				T[i][j] = 0;
				continue;
			}
			T[i][j] = -1;
		}
	}
	
	printf("%d", SEARCH(n, m));
	
	return 0;
}
