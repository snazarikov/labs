#include <stdio.h>
#include <math.h>

#define NMAX 1005
#define EMPTY -1
#define CENTER 1002

int n, k;
double a, b, c, winFirst, winSecond, draw;

double table[NMAX][NMAX * 2];

double dynamic(int gameAmount, int difference) {
    if (gameAmount >= n) {
//        cout << "GG " << gameAmount << " " << difference << endl;
//        printf("GG %d %d\n", gameAmount, difference);
        return table[gameAmount][difference + CENTER] = (double)gameAmount;
    }
    if (abs(difference) >= k) {
//        cout << "DIFF " << gameAmount << " " << difference << endl;
//        printf("DIFF %d %d\n", gameAmount, difference);
        return table[gameAmount][difference + CENTER] = (double)gameAmount;
    }
    if (table[gameAmount][difference + CENTER] != EMPTY) {
        return table[gameAmount][difference + CENTER];
    }

    table[gameAmount][difference + CENTER] = dynamic(gameAmount + 1, difference) * draw + dynamic(gameAmount + 1, difference + 1) * winFirst + dynamic(gameAmount + 1, difference - 1) * winSecond;

//    cout << "KEK " << gameAmount << " " << difference << endl;
//    printf("KEK %d %d\n", gameAmount, difference);

    return table[gameAmount][difference + CENTER];
}

int main() {
    freopen ("input.txt", "r", stdin);
    freopen ("output.txt", "w", stdout);

    scanf("%lf %lf %lf %d %d", &a, &b, &c, &n, &k);
//    printf("%lf %lf %lf %d %d\n", a, b, c, n, k);

    winFirst = a / (a + b + c);
    winSecond = b / (a + b + c);
    draw = c / (a + b + c);

//    printf("%lf %lf %lf\n", winFirst, winSecond, draw);

    int i, j;
    for (i = 0; i < NMAX; ++i) {
        for (j = 0; j < NMAX * 2; ++j) {
            table[i][j] = EMPTY;
        }
    }

    printf("%.4lf", dynamic(0, 0));

    return 0;
}
