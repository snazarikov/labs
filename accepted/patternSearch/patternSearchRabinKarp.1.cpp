#include <cstdio>
#include <iostream>
#include <vector>
#include <cstring>
#include <cmath>

const long long ALPHABET_SIZE = 26; //251
const long long SMTH_BIG = 20999999;
const int MAX_LENGTH = 102;

using namespace std;

unsigned long long pows[MAX_LENGTH];

void makePows() {
    pows[0] = 1;
    for (int i = 1; i < MAX_LENGTH; ++i) {
        pows[i] = (pows[i - 1] * ALPHABET_SIZE) % SMTH_BIG;
    }
}

void printPows(int len) {
    for (int i = 0; i <= len; ++i) {
        cout << "i: " << i << " pow: " << pows[i] << endl;
    }
}

unsigned long long hashFunction(string& str, int len) {
    unsigned long long value = 0;
    for (int i = 0; i < len; ++i) {
        value += (pows[len - 1 - i] * (unsigned char)str[i]) % SMTH_BIG;
    }
    return value % SMTH_BIG;
}

int RabinKarp(string& pattern, string& text) {
    int patternSize = pattern.size();
    int textSize = text.size();

    if (patternSize > textSize) {
        return -1;
    }

    makePows();
    unsigned long long patternHash = hashFunction(pattern, patternSize);
    unsigned long long textHash = hashFunction(text, patternSize);

    for (int i = 0; i <= textSize - patternSize; ++i) {
//        cout << "patternHash: " << patternHash << " textLenXHash: " << textHash << endl;
        if (patternHash == textHash) {
            int flag = 1;
            for (int j = i; j < i + patternSize; ++j) {
                if (text[j] != pattern[j - i]) {
                    flag = 0;
                }
            }
            if (flag) {
                return i + 1;
            }
        }
//        textHash = (pows[1] * textHash - pows[patternSize] * (unsigned char)text[i] + (unsigned char)text[i + patternSize]) % SMTH_BIG;
        textHash = (((pows[1] * textHash) % SMTH_BIG) - ((pows[patternSize] * (unsigned char)text[i]) % SMTH_BIG) + SMTH_BIG + (unsigned char)text[i + patternSize]) % SMTH_BIG;
    }

    return -1;
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    string pattern, text;
    cin >> pattern >> text;

    cout << RabinKarp(pattern, text);

    return 0;
}

