#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

const int NMAX = 1e6 + 5;

int prefixFunc[NMAX];

void getPrefixFunc(string& smth) {
	for (int i = 1; i < smth.size(); ++i) {
		int j = prefixFunc[i - 1];
		while (j && smth[i] != smth[j]) {
			j = prefixFunc[j - 1];
		}
		if (smth[i] == smth[j]) {
            ++j;
		}
		prefixFunc[i] = j;
	}
}

int main(void) {
//    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    string smth;
    cin >> smth;
    getPrefixFunc(smth);

    for (int i = 0; i < smth.size(); ++i) {
        cout << prefixFunc[i] << " ";
    }

    return 0;
}
