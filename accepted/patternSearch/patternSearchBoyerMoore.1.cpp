#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

const int NMAX = 1e6 + 105;
const int SOME_MAX = 105;
const int ALPHABET_SIZE = 26;

int stopSymbol[SOME_MAX];
int safeSuffix[SOME_MAX];

int isPrefix(string& pattern, int pos) {
    for (int i = pos, j = 0; i < pattern.size(); ++i, ++j) {
        if (pattern[i] != pattern[j]) {
            return 0;
        }
    }
    return 1;
}

int getSuffixLen(string& pattern, int pos) {
    int len = 0;
    int i = pos;
    int j = pattern.size() - 1;
    while (i >= 0 and pattern[i] == pattern[j]) {
        ++len;
        --i;
        --j;
    }
    return len;
}

void buildSafeSuffix(string& pattern) {
    int lastPrefixPosition = pattern.size();
    int patternSize = pattern.size();
    for (int i = patternSize - 1; i >= 0; --i) {
        if (isPrefix(pattern, i + 1)) {
            lastPrefixPosition = i + 1;
        }
        safeSuffix[patternSize - 1 - i] = lastPrefixPosition - i + patternSize - 1;
    }
    for (int i = 0; i < patternSize - 1; ++i) {
        int suffixLen = getSuffixLen(pattern, i);
        safeSuffix[suffixLen] = patternSize - 1 - i + suffixLen;
    }
}

void buildStopSymbol(string& pattern) {
    for (int i = 0; i < ALPHABET_SIZE; ++i) {
        stopSymbol[i] = pattern.size();
    }
    for (int i = 0; i < pattern.size() - 1; ++i) {
        stopSymbol[pattern[i] - 'a'] = pattern.size() - 1 - i;
    }
}

int boyermoore(string& pattern, string& text) {
    int patternSize = pattern.size();
    int textSize = text.size();
    if (!patternSize) {
//        cout << "KEK 1" << endl;
        return -1;
    }

    buildStopSymbol(pattern);
    buildSafeSuffix(pattern);

    int i = patternSize - 1;
    while (i < textSize) {
        int j = patternSize - 1;
        while (pattern[j] == text[i]) {
            if (!j) {
//                cout << "PEK" << endl;
                return i + 1;
            }
            --i;
            --j;
        }
        i += max(max(safeSuffix[patternSize - 1 - j], stopSymbol[text[i] - 'a']), 1);
    }
//    cout << "KEK 2" << endl;
    return -1;
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    string pattern, text;
    cin >> pattern >> text;

    cout << boyermoore(pattern, text);

//    for (int i = 0; i < ALPHABET_SIZE; ++i) {
//        cout << stopSymbol[i] << " ";
//    }
//    cout << endl;

    return 0;
}
