#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

const int NMAX = 1e6 + 105;

int prefixFunc[NMAX];

void getPrefsixFunc(string& smth) {
	for (int i = 1; i < smth.size(); ++i) {
		int j = prefixFunc[i - 1];
		while (j && smth[i] != smth[j]) {
			j = prefixFunc[j - 1];
		}
		if (smth[i] == smth[j]) {
            ++j;
		}
		prefixFunc[i] = j;
	}
}

int findPattern(string& pattern, string& text) {
    string breakPoint = "#";
    string smth = pattern + breakPoint + text;

    getPrefsixFunc(smth);

    for (int i = 2 * pattern.size(); i < smth.size(); ++i) {
        if (prefixFunc[i] == pattern.size()) {
            return i - 2 * pattern.size() + 1;
        }
    }
    return -1;
}

int main(void) {
    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    string pattern, text;
    cin >> pattern >> text;

    cout << findPattern(pattern, text);

    return 0;
}
