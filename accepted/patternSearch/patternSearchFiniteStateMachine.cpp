#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

const int NMAX = 1e3 + 5;

int prefixFunc[NMAX];
int transitonMatrix[NMAX][NMAX];
int letterNumber[NMAX];
char uniqueLetter[NMAX];
int uniqueAmount = 0;

void getPrefixFunc(string& smth) {
	for (int i = 1; i < smth.size(); ++i) {
		int j = prefixFunc[i - 1];
		while (j && smth[i] != smth[j]) {
			j = prefixFunc[j - 1];
		}
		if (smth[i] == smth[j]) {
            ++j;
		}
		prefixFunc[i] = j;
	}
}

void getTransition(string& pattern) {
    getPrefixFunc(pattern);

    memset(letterNumber, -1, sizeof letterNumber);

    for (int i = 0; i < pattern.size(); ++i) {
        if (letterNumber[pattern[i] - 'a'] == -1) {
            letterNumber[pattern[i] - 'a'] = uniqueAmount;
            uniqueLetter[uniqueAmount++] = pattern[i];
        }
    }

    for (int i = 0; i < pattern.size(); ++i) {
        for (int j = 0; j < uniqueAmount; ++j) {
            if (uniqueLetter[j] == pattern[i]) {
                transitonMatrix[i][uniqueLetter[j] - 'a'] = i + 1;
            }
            else {
                int pref = prefixFunc[i - 1];
                while (pref && pattern[pref] != uniqueLetter[j]) {
                    pref = prefixFunc[pref - 1];
                }

                if (pattern[pref] == uniqueLetter[j]) {
                    pref++;
                }

                transitonMatrix[i][uniqueLetter[j] - 'a'] = pref;
            }
        }
    }
}

int findPattern(string& pattern, string& text) {
    getTransition(pattern);

    int state = 0;
    int patternSize = pattern.size();
    for (int i = 0; i < text.size(); ++i) {
        state = transitonMatrix[state][text[i] - 'a'];
        if (state == patternSize) {
            return i - patternSize + 2;
        }
    }

    return -1;
}

int main(void) {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    string pattern, text;
    cin >> pattern >> text;

    cout << findPattern(pattern, text);

    return 0;
}
