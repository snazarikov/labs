#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>
#include <algorithm>

#include <thread>
#include <future>
#include <mutex>
#include <memory>

#include <stack>
#include <list>
#include <fstream>

using namespace std;

const int THREAD_AMOUNT = thread::hardware_concurrency();

int currThreadAmount = 1;
mutex currThreadAmountMutex;

class threadGuard {
    private:
        thread _t;
    public:
        threadGuard(thread obj) : _t(move(obj)) {}
        ~threadGuard() {
            if (_t.joinable()) {
                _t.join();
            }
        }
        threadGuard(threadGuard &obj) = delete;
        threadGuard& operator=(threadGuard& obj) = delete;

        threadGuard(threadGuard&& other) {
            _t = move(other._t);
        }
};

template<typename Type>
list<Type> sequentialQS(list<Type> input) {
    if (input.empty()) {
        return input;
    }

    list<Type> result;
    result.splice(result.begin(), input, input.begin());

    Type const& pivot = *result.begin();

//    cout << "pivot: " << pivot << endl;

    auto dividePoint = partition(input.begin(), input.end(), [&](Type const& t){return t < pivot;});

//    cout << "divide point: " << *dividePoint << endl;

    list<Type> leftPart;
    leftPart.splice(leftPart.end(), input, input.begin(), dividePoint);

    auto newLower(sequentialQS(move(leftPart)));
    auto newHigher(sequentialQS(move(input)));

    result.splice(result.end(), newHigher);
    result.splice(result.begin(), newLower);

    return result;
}

template<typename Type>
list<Type> cleverSequentialQS(list<Type> input) {
    if (input.empty()) {
        return input;
    }

    list<Type> result;
    result.splice(result.begin(), input, input.begin());

    Type const& pivot = *result.begin();

//    cout << "pivot: " << pivot << endl;

    auto dividePoint = partition(input.begin(), input.end(), [&](Type const& t){return t < pivot;});

//    cout << "divide point: " << *dividePoint << endl;

    list<Type> leftPart;
    leftPart.splice(leftPart.end(), input, input.begin(), dividePoint);

    future<list<Type> > newLower(async(&cleverSequentialQS<Type>, move(leftPart)));
    auto newHigher(cleverSequentialQS(move(input)));

    result.splice(result.end(), newHigher);
    result.splice(result.begin(), newLower.get());

    return result;
}

template<typename Type>
void parallelSequentialQS(list<Type>& input) {
    if (input.empty()) {
        return;
    }

    list<Type> result;
    result.splice(result.begin(), input, input.begin());

    Type const& middleElement = *result.begin();

    auto dividePoint = partition(input.begin(), input.end(), [&](Type const& t){return t < middleElement;});

    list<Type> leftPart;
    leftPart.splice(leftPart.end(), input, input.begin(), dividePoint);

    list<Type> rightPart;
    rightPart.splice(rightPart.end(), input, input.begin(), input.end());

    int flag = 0;
    if (currThreadAmount < THREAD_AMOUNT) {
        lock_guard<mutex> support(currThreadAmountMutex);
        currThreadAmount++;
        flag = 1;
    }

    if (flag) {
        threadGuard{thread(parallelSequentialQS<Type>, ref(leftPart))};
    }
    else {
        parallelSequentialQS(leftPart);
    }
    parallelSequentialQS(rightPart);

    result.splice(result.end(), rightPart);
    result.splice(result.begin(), leftPart);

    input = result;
}

int main(void) {
    srand(time(NULL));

    list<int> listOne;
    list<int> listTwo;
    list<int> listThree;

    for (int i = 0; i < 1e5; ++i) {
        listOne.push_back(1 + rand());
    }
    listTwo = listOne;
    listThree = listOne;

    clock_t startSingleCore = clock();
    sequentialQS<int>(listOne);
    printf("Time taken single core sort: %.2fs\n", (double)(clock() - startSingleCore) / CLOCKS_PER_SEC);

    clock_t startSelskaya = clock();
    parallelSequentialQS<int>(listTwo);
    printf("Time taken sel'skaya sort: %.2fs\n", (double)(clock() - startSelskaya) / CLOCKS_PER_SEC);

    clock_t startMultiCore = clock();
    cleverSequentialQS<int>(listThree);
    printf("Time taken multi core sort: %.2fs\n", (double)(clock() - startMultiCore) / CLOCKS_PER_SEC);

//    for (auto it = myList.begin(); it != myList.end(); it++) {
//        cout << (*it) << " ";
//    }
//    cout << endl << "PEK" << endl;


//    cout << endl << "KEK" << endl;
//    for (auto it = myList.begin(); it != myList.end(); it++) {
//        cout << (*it) << " ";
//    }

    return 0;
}

