#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>

#include <thread>
#include <mutex>
#include <memory>

#include <stack>
#include <fstream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

using std::stack;
using std::ifstream;
using std::ofstream;

using std::thread;
using std::mutex;
using std::lock_guard;

using std::shared_ptr;
using std::ref;
using std::move;

const string INPUT_NAME_ONE = "input1.txt";
const string INPUT_NAME_TWO = "input2.txt";
const string INPUT_NAME_THREE = "input3.txt";

const string OUTPUT_NAME_ONE = "log1.txt";
const string OUTPUT_NAME_TWO = "log2.txt";
const string OUTPUT_NAME_THREE = "log3.txt";

class threadGuard {
    private:
        thread _t;
    public:
        threadGuard(thread obj) : _t(move(obj)) {}
        ~threadGuard() {
            if (_t.joinable()) {
                _t.join();
            }
        }
        threadGuard(threadGuard &obj) = delete;
        threadGuard& operator=(threadGuard& obj) = delete;

        threadGuard(threadGuard&& other) {
            _t = move(other._t);
        }
};

template <typename T>
class stackNeTypoi {
    private:
        stack<T> _stack;
        mutex _m;
    public:
        stackNeTypoi() {}
        stackNeTypoi(stackNeTypoi &obj) = delete;
        stackNeTypoi& operator=(stackNeTypoi& obj) = delete;

        shared_ptr<T> pop_top() {
            lock_guard<mutex> support(_m);
            shared_ptr<T> pointer = make_shared(_stack.top());
            _stack.pop();
            return pointer;
        }

        void pop_top(T& container) {
            lock_guard<mutex> support(_m);
            container = _stack.top();
            _stack.pop();
        }

        void push(T value) {
            lock_guard<mutex> support(_m);
            _stack.push(value);
        }
};

void someFunc(ifstream& in, ofstream& out, stackNeTypoi<int>& myStack) {
    string command;
    while (in >> command) {
        if (command == "pt") {
            int value;
            myStack.pop_top(value);
            out << value << " removed from stack" << endl;
        }
        else if (command == "p") {
            int value;
            in >> value;
            myStack.push(value);
            out << value << " being pushed into stack" << endl;
        }
        else {
            out << "wrong command" << endl;
        }
    }
}

int main(void) {
    srand(time(NULL));

    stackNeTypoi<int> myStack;

    ifstream inputOne(INPUT_NAME_ONE), inputTwo(INPUT_NAME_TWO), inputThree(INPUT_NAME_THREE);
    ofstream outputOne(OUTPUT_NAME_ONE), outputTwo(OUTPUT_NAME_TWO), outputThree(OUTPUT_NAME_THREE);

    threadGuard threadOne = threadGuard{thread(someFunc, ref(inputOne), ref(outputOne), ref(myStack))};
    threadGuard threadTwo = threadGuard{thread(someFunc, ref(inputTwo), ref(outputTwo), ref(myStack))};

    someFunc(inputThree, outputThree, myStack);

    return 0;
}

