#include "weather.h"

Weather::Weather(QObject *parent) : QObject(parent)
{

}

const QString LINK = "/data/2.5/weather?id=";
const QString API_KEY = "fdd59e5a6e5b025f82e0b1814d5910f4";
const QString OUTPUT_PATH = "D:/threads/Qt/lab1/output.txt";

QByteArray Weather::makeRequest(QString& cityId) {
    QString path = LINK + cityId + "&units=metric&appid=" + API_KEY;
    QString request = "GET " + path + " HTTP/1.0\r\n\r\n\r\n";

    return request.toLatin1();
}

QJsonObject Weather::makeJson(QByteArray& jsonByte, QString& timeInfo) {
//    qDebug() << jsonByte;

    QByteArray timePart = jsonByte;
    timePart.replace(0, timePart.indexOf("Date"), QByteArray(""));
    timePart.replace(timePart.indexOf("GMT") + 3, timePart.size(), QByteArray(""));
    timeInfo = timePart;

    qDebug() << timeInfo;

    jsonByte.replace(QByteArray("\r\n"), QByteArray(""));
    jsonByte.replace(0, jsonByte.indexOf('{'), QByteArray(""));

    QJsonDocument document = QJsonDocument::fromJson(jsonByte);

    return document.object();
}

void Weather::printWeather(QJsonObject& rootObject, QTextStream& out) {
    out << (QString)"name" << " : " << rootObject.value("name").toString() << "\n";

    QJsonObject rootSys = rootObject.value("sys").toObject();
    out << (QString)"country" << " : " << rootSys.value("country").toString() << "\n";

    QJsonArray rootWeather = rootObject.value("weather").toArray();
    QJsonObject rootWeatherDescription = rootWeather[0].toObject();
    out << (QString)"description" << " : " <<  rootWeatherDescription.value("description").toString() << "\n";

    QJsonObject rootMain = rootObject.value("main").toObject();
    for (QString key : rootMain.keys()) {
        out << key << " : " << rootMain.value(key).toDouble() << "\n";
    }

    QJsonObject rootWind = rootObject.value("wind").toObject();
    out << (QString)"wind" << " : " << rootWind.value("speed").toDouble() << "\n";

    out << "\n\n";
}

bool Weather::isServerAnswerOk(QByteArray& jsonByte) {
    if (jsonByte.indexOf("}") != -1) {
        return true;
    }
    return false;
}

bool Weather::isServerAnswerOk(QByteArray& jsonByte, int& openAmount, int& closeAmount, int& startPos) {
    for (int i = startPos; i < jsonByte.size(); ++i) {
        if (jsonByte[i] == '{') {
            openAmount++;
        }
        else if (jsonByte[i] == '}') {
            closeAmount++;
        }
    }
    startPos = jsonByte.size();
    if (openAmount == closeAmount) {
        return true;
    }
    return false;
}

void Weather::getWeather(QVector<QString>& city) {
    socket = new QTcpSocket(this);

    QFile fileOut(OUTPUT_PATH);
    if(!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "fileOpenError" << fileOut.error();
    }
    QTextStream out(&fileOut);

    for (int i = 0; i < city.size(); ++i) {
        socket->connectToHost("api.openweathermap.org", 80);

        if(socket->waitForConnected()) {
            qDebug() << "Connected!";

            QString cityId = city[i];
            QByteArray requestByte = makeRequest(cityId);
            const char* requestChar = requestByte.data();

            socket->write(requestChar);
            socket->waitForBytesWritten(1000);

            //
//            int openAmount = 0;
//            int closeAmount = 0;
//            int startPos = 0;
            //

            QByteArray jsonByte;
            do {
                socket->waitForReadyRead();
                qDebug() << "Reading:" << socket->bytesAvailable();
                jsonByte += socket->readAll();
            }
            while (!isServerAnswerOk(jsonByte));
//            while (!isServerAnswerOk(jsonByte, openAmount, closeAmount, startPos));

            socket->close();

            QString timeInfo;
            QJsonObject rootObject = makeJson(jsonByte, timeInfo);


            out << (QString)"request time" << " : " << timeInfo << "\n";
            printWeather(rootObject, out);
        }
        else {
            qDebug() << "Not Connected!";
            qDebug() << socket->error();
        }
    }

    fileOut.close();
}
