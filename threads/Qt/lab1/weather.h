#ifndef WEATHER_H
#define WEATHER_H

#include <QObject>

#include <QTcpSocket>
#include <QDebug>
#include <QVector>
#include <QString>
#include <QFile>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

class Weather : public QObject
{
    Q_OBJECT
public:
    explicit Weather(QObject *parent = nullptr);
    void getWeather(QVector<QString>& city);
signals:

public slots:

private:
    QTcpSocket *socket;
    QByteArray makeRequest(QString& cityId);
    QJsonObject makeJson(QByteArray& jsonByte, QString& timeInfo);
    void printWeather(QJsonObject& rootObject, QTextStream& out);
    bool isServerAnswerOk(QByteArray& jsonByte);
    bool isServerAnswerOk(QByteArray& jsonByte, int& openAmount, int& closeAmount, int& startPos);
};

#endif // WEATHER_H
