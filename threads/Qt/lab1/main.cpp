#include <QCoreApplication>
#include "weather.h"

#include <QFile>
#include <QTextStream>
#include <QVector>

QString INPUT_PATH = "D:/threads/Qt/lab1/input.txt";
QString OUTPUT_PATH = "D:/threads/Qt/lab1/output.txt";

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QVector<QString> city;

    QFile file(INPUT_PATH);
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text)) {
        qDebug() << "fileOpenError" << file.error();
    }

    QFile fileOut(OUTPUT_PATH);
    if(!fileOut.open(QIODevice::ReadWrite | QIODevice::Text)) {
        qDebug() << "fileOpenError" << fileOut.error();
    }

    QTextStream in(&file);
    QTextStream out(&fileOut);
    while(!in.atEnd()) {
        city.push_back(in.readLine());
//        out << city.back() << "\n";
//        qDebug() << city.back().size() << "KEK" << city.back();
    }
    file.close();
    fileOut.close();

    Weather weather;
    weather.getWeather(city);

    return a.exec();
}
