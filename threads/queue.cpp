#include <iostream>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

class threadGuard {
    private:
        thread _t;
    public:
        threadGuard(thread obj) : _t(move(obj)) {}
        ~threadGuard() {
            if (_t.joinable()) {
                _t.join();
            }
        }
        threadGuard(threadGuard &obj) = delete;
        threadGuard& operator=(threadGuard& obj) = delete;

        threadGuard(threadGuard&& other) {
            _t = move(other._t);
        }
};

template <typename T>
class queueGuard {
    private:
        queue<T> _queue;
        mutex _m;
        condition_variable _cv;
    public:
        queueGuard() {}
        queueGuard(queueGuard &obj) = delete;
        queueGuard& operator=(queueGuard& obj) = delete;

        void push(T data) {
            lock_guard<mutex> support(_m);
            _queue.push(data);
            _cv.notify_one();
        }

        void pop_head(T& container) {
            unique_lock<mutex> support(_m);
            _cv.wait(support, [this]{return !_queue.empty();});
            container = _queue.front();
            _queue.pop();
        }

        shared_ptr<T> pop_head() {
            unique_lock<mutex> support(_m);
            _cv.wait(support, [this]{return !_queue.empty();});
            shared_ptr<T> pointer = make_shared(_queue.front());
            _queue.pop();
            return pointer;
        }

        bool try_pop(T& container) {
            lock_guard<mutex> support(_m);
            if (_queue.empty()) {
                return false;
            }
            container = _queue.front();
            _queue.pop();
            return true;
        }

        shared_ptr<T> try_pop() {
            lock_guard<mutex> support(_m);
            if (_queue.empty()) {
                return nullptr;
            }
            shared_ptr<T> pointer = make_shared(_queue.front());
            _queue.pop();
            return pointer;
        }
};

int main(void) {
    queue<int> myQueue;

    for (int i = 0; i < 10; ++i) {
        myQueue.push(i);
        cout << "front: " << myQueue.front() << " back: " << myQueue.back() << endl;
    }

    return 0;
}
