#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>

#include <thread>
#include <mutex>
#include <memory>
#include <chrono>

#include <algorithm>
#include <fstream>
#include <vector>
#include <iomanip>

using namespace std;
using namespace std::chrono;

const int VERTEX_MAX = 1e2 + 5;
const int BUILD_AMOUNT = 100;
const int TEST_AMOUNT = 100;

struct Edge {
    int vertex;
    int weight;
};

struct GraphVertex {
    vector<Edge> adjacentVertices;
    mutex vertexMutex;
};

GraphVertex graph[VERTEX_MAX];
GraphVertex startGraphCondition[VERTEX_MAX];
int vertexAmount, edgeAmount;

class threadGuard {
    private:
        thread _t;
    public:
        threadGuard(thread obj) : _t(move(obj)) {}
        ~threadGuard() {
            if (_t.joinable()) {
                _t.join();
            }
        }
        threadGuard(threadGuard &obj) = delete;
        threadGuard& operator=(threadGuard& obj) = delete;

        threadGuard(threadGuard&& other) {
            _t = move(other._t);
        }
};

int isRepeat(vector<Edge>& vect, int& myVertex) {
    for (int i = 0; i < vect.size(); ++i) {
        if (vect[i].vertex == myVertex) {
            return i;
        }
    }
    return -1;
}

void generateGraph() {
    srand(time(NULL));
    cout << "type vertexAmount: ";
    cin >> vertexAmount;
    cout << endl;
    cout << "type edgeAmount: ";
    cin >> edgeAmount;
    cout << endl;
    for (int i = 0; i < edgeAmount; ++i) {
        int u = 1 + rand() % vertexAmount;
        int v = 1 + rand() % vertexAmount;

        if (isRepeat(graph[u].adjacentVertices, v) == -1 && u != v) {
            graph[u].adjacentVertices.push_back(Edge{v, 1});
        }
    }

    for (int i = 0; i <= vertexAmount; ++i) {
        startGraphCondition[i].adjacentVertices = graph[i].adjacentVertices;
    }
}

void printGraph(GraphVertex* graph) {
    for (int i = 1; i <= vertexAmount; ++i) {
        cout << i << ": ";
        for (int j = 0; j < graph[i].adjacentVertices.size(); ++j) {
            cout << "(" << graph[i].adjacentVertices[j].vertex << ", " << graph[i].adjacentVertices[j].weight << ")  ";
        }
        cout << endl;
    }
    cout << endl<< endl;
}

void randomSearchAndBuild(int threadNumba, ofstream& out, int startVertex, int finishVertex) {
    srand(time(NULL) + threadNumba * 10);

    out << "start config: " << startVertex << " " << finishVertex << endl;

    int currVertex = startVertex;
    int currWeight = 0;
    while (true) {
//        cout << "KEK 1" << endl;
        out << "thread numba #" << threadNumba << " at " << currVertex << " vertex" << endl;
//        out << "curr weight before step: " << currWeight << endl;
        int prevVertex = currVertex;

        graph[currVertex].vertexMutex.lock();
        int nextVertInd = 0 + rand() % graph[currVertex].adjacentVertices.size();
        currVertex = graph[currVertex].adjacentVertices[nextVertInd].vertex;
        //graph[prevVertex].vertexMutex.unlock();

        //graph[prevVertex].vertexMutex.lock();
        currWeight += graph[prevVertex].adjacentVertices[nextVertInd].weight;
        graph[prevVertex].vertexMutex.unlock();
//        out << "done step to: " << currVertex << endl;
//        out << "curr weight after step: " << currWeight << endl;


        if (currVertex == finishVertex) {
            graph[startVertex].vertexMutex.lock();
            int helpfulIndex = isRepeat(graph[startVertex].adjacentVertices, finishVertex);
            if (helpfulIndex == -1) {
                out << "ATTENTION new edge added " << startVertex << " " << finishVertex << " " << currWeight << endl;

                graph[startVertex].adjacentVertices.push_back(Edge{finishVertex, currWeight});
            }
//            else {
//                out << "ATTENTION edge updated to worse" << startVertex << " " << finishVertex << " " << currWeight << endl;
//
//                graph[startVertex].adjacentVertices[helpfulIndex].weight = currWeight;
//            }
            graph[startVertex].vertexMutex.unlock();
            out << endl << endl;
            break;
        }
    }
}

void doDirtyJob(int threadNumba, ofstream& out) {
    srand(time(NULL) + threadNumba * 5);
    for (int i = 0; i < BUILD_AMOUNT; ++i) {
        int u, v;
        do {
            u = 1 + rand() % vertexAmount;
            v = 1 + rand() % vertexAmount;
        }
        while (u == v);

        randomSearchAndBuild(threadNumba, out, u, v);
    }
}

void startBuild() {
    ofstream outputOne("out1.txt"), outputTwo("out2.txt");

    threadGuard threadOne = threadGuard{thread(doDirtyJob, 1, ref(outputOne))};
    threadGuard threadTwo = threadGuard{thread(doDirtyJob, 2, ref(outputTwo))};
}

int randomSearch(GraphVertex* graph, int startVertex, int finishVertex) {
    srand(time(NULL));

    int currVertex = startVertex;
    int currWeight = 0;
    while (true) {
        int prevVertex = currVertex;

        int nextVertInd = 0 + rand() % graph[currVertex].adjacentVertices.size();
        currVertex = graph[currVertex].adjacentVertices[nextVertInd].vertex;
        currWeight += graph[prevVertex].adjacentVertices[nextVertInd].weight;
        if (currVertex == finishVertex) {
            return currWeight;
        }
    }
}

void getDifference() {
    vector<pair<int, int>> test;
    for (int i = 0; i < TEST_AMOUNT; ++i) {
        int u, v;
        do {
            u = 1 + rand() % vertexAmount;
            v = 1 + rand() % vertexAmount;
        }
        while (u == v);
        test.push_back(pair<int, int>(u, v));
    }

    int weightStart = 0;
    int weightAfterJob = 0;
    for (int i = 0; i < test.size(); ++i) {
        weightStart += randomSearch(startGraphCondition, test[i].first, test[i].second);
        weightAfterJob += randomSearch(graph, test[i].first, test[i].second);
    }

    cout << "Medium weights:" << endl;
    cout << "Original graph: " << setprecision(5) << (double)weightStart / (double)test.size() << endl;
    cout << "Modified graph: " << setprecision(5) << (double)weightAfterJob / (double)test.size() << endl;
}

int main(void) {
    generateGraph();

    cout << "start condition: " << endl;
    printGraph(startGraphCondition);

    startBuild();

    cout << "condition after dirty job: " << endl;
    printGraph(graph);

    getDifference();

    return 0;
}

