#include <iostream>
#include <thread>
#include <vector>
#include <ctime>
#include <cstdlib>

using namespace std;

void getData(vector<int>& A, vector<int>& B, int& amount) {
    cin >> amount;
    A.resize(amount);
    B.resize(amount);
    for (int i = 0; i < amount; ++i) {
        cin >> A[i];
    }
    for (int i = 0; i < amount; ++i) {
        cin >> B[i];
    }
}

void printData(vector<int>& A, vector<int>& B, int& amount) {
    cout << amount << '\n';
    for (int i = 0; i < amount; ++i) {
        cout << A[i] << ' ';
    }
    cout << '\n';
    for (int i = 0; i < amount; ++i) {
        cout << B[i] << ' ';
    }
    cout << '\n';
}

void getTest(vector<int>& A, vector<int>& B, int& amount) {
    A.clear();
    B.clear();
    amount = 1 + rand() % 100;
    A.resize(amount);
    B.resize(amount);
    for (int i = 0; i < amount; ++i) {
        A[i] = 1 + rand() % 10;
    }
    for (int i = 0; i < amount; ++i) {
        B[i] = 1 + rand() % 10;
    }
}

void hello() {
    cout << "Preved, ya v onlaine\n";
}

void veryUsefulFunction(vector<int>& A, vector<int>& B, int start, int finish, int& res) {
//    cout << "Ya tyt preved\n";
    for (int i = start; i <= finish; ++i) {
        res += A[i] * B[i];
    }
}

class threadGuard {
    private:
        thread _t;
    public:
        threadGuard(thread obj) : _t(move(obj)) {}
        ~threadGuard() {
            if (_t.joinable()) {
                _t.join();
            }
        }
        threadGuard(threadGuard &obj) = delete;
        threadGuard& operator=(threadGuard& obj) = delete;

        threadGuard(threadGuard&& other) {
            _t = move(other._t);
        }
};

int multiThreadSolution(vector<int>& A, vector<int>& B, int amount) {
    int threadAmount = thread::hardware_concurrency();
    if (threadAmount < 2) {
        threadAmount = 2;
    }

    vector<int> results(threadAmount, 0);
    vector<int> begining(threadAmount, 0);
    vector<int> ending(threadAmount, 0);

    int step = amount / threadAmount;
    for (int i = 0; i < threadAmount; ++i) {
        begining[i] = i * step;
        ending[i] = begining[i] + step - 1;
    }
    ending[threadAmount - 1] = amount - 1;

    vector<threadGuard> vectorThreads;
    for (int i = 0; i < threadAmount; ++i) {
        vectorThreads.push_back(move(threadGuard{thread(veryUsefulFunction, ref(A), ref(B), begining[i], ending[i], ref(results[i]))}));
    }

    cout << "results\n";
    for (int i = 0; i < threadAmount; ++i) {
        cout << results[i] << ' ';
    }
    cout << endl;

    int answer = 0;
    for (int i = 0; i < threadAmount; ++i) {
        answer += results[i];
    }
    return answer;
}

int singleThreadSolution(vector<int>& A, vector<int>& B, int amount) {
    int answer = 0;
    for (int i = 0; i < amount; ++i) {
        answer += A[i] * B[i];
    }
    return answer;
}

int main(void) {
    freopen("input.txt", "r", stdin);
//    freopen("output.txt", "w", stdout);

    srand(time(NULL));

    vector<int> A;
    vector<int> B;

    int amount;
//    getData(A, B, amount);
//    printData(A, B, amount);
    int multi, single;
    do {
        getTest(A, B, amount);
//        printData(A, B, amount);
        multi = multiThreadSolution(A, B, amount);
        single = singleThreadSolution(A, B, amount);
        cout << multi << ' ' << single << endl;
    }
    while (multi == single);
    printData(A, B, amount);

    return 0;
}

