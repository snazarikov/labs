#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>

#include <thread>
#include <mutex>
#include <memory>
#include <chrono>

#include <stack>
#include <algorithm>
#include <fstream>

using namespace std;
using namespace std::chrono;

const int READER_AMOUNT = 10;
const int WRITER_AMOUNT = 1;

class threadGuard {
    private:
        thread _t;
    public:
        threadGuard(thread obj) : _t(move(obj)) {}
        ~threadGuard() {
            if (_t.joinable()) {
                _t.join();
            }
        }
        threadGuard(threadGuard &obj) = delete;
        threadGuard& operator=(threadGuard& obj) = delete;

        threadGuard(threadGuard&& other) {
            _t = move(other._t);
        }
};

void requestImitation() {
    std::this_thread::sleep_for(chrono::milliseconds(100 + rand() % 500));
}

class sharedMutex {
    private:
        mutex _mainMutex;
        mutex _helpMutex;
        int _readerAmount;
    public:
        sharedMutex() : _readerAmount(0) {}

        void lock() {
            _mainMutex.lock();
        }
        void unlock() {
            _mainMutex.unlock();
        }
        void shared_lock() {
            _helpMutex.lock();
            _readerAmount++;
            if (_readerAmount == 1) {
                _mainMutex.lock();
            }
            _helpMutex.unlock();
        }
        void shared_unlock() {
            _helpMutex.lock();
            _readerAmount--;
            if (!_readerAmount) {
                _mainMutex.unlock();
            }
            _helpMutex.unlock();
        }
};

sharedMutex cleverDBprotector;
mutex stupidDBprotector;

void thisIsMyCleverDB(int readRequestAmount, int writeRequestAmount) {
    while (readRequestAmount || writeRequestAmount) {
//        cout << readRequestAmount << " " << writeRequestAmount << endl;
        int action = rand() % 2;
        if (action && !readRequestAmount) {
            action--;
        }

        if (action) { // reader
            readRequestAmount--;
            cleverDBprotector.shared_lock();
            requestImitation();
            cleverDBprotector.shared_unlock();
        }
        else if (writeRequestAmount) { // writer
            writeRequestAmount--;
            cleverDBprotector.lock();
            requestImitation();
            cleverDBprotector.unlock();
        }
    }
}

void thisIsMyStupidDB(int readRequestAmount, int writeRequestAmount) {
    while (readRequestAmount || writeRequestAmount) {
//        cout << readRequestAmount << " " << writeRequestAmount << endl;
        int action = rand() % 2;
        if (action && !readRequestAmount) {
            action--;
        }

        if (action) { // reader
            readRequestAmount--;
            stupidDBprotector.lock();
            requestImitation();
            stupidDBprotector.unlock();
        }
        else if (writeRequestAmount) { // writer
            writeRequestAmount--;
            stupidDBprotector.lock();
            requestImitation();
            stupidDBprotector.unlock();
        }
    }
}

void cleverSolution() {
    vector<threadGuard> vectorThreads;
    for (int i = 1; i < thread::hardware_concurrency(); ++i) {
        vectorThreads.push_back(move(threadGuard{thread(thisIsMyCleverDB, READER_AMOUNT, WRITER_AMOUNT)}));
    }
}

void stupidSolution() {
    vector<threadGuard> vectorThreads;
    for (int i = 1; i < thread::hardware_concurrency(); ++i) {
        vectorThreads.push_back(move(threadGuard{thread(thisIsMyStupidDB, READER_AMOUNT, WRITER_AMOUNT)}));
    }
}

int main(void) {
    srand(time(NULL));

    auto startClever = high_resolution_clock::now();
    cleverSolution();
    auto stopClever = high_resolution_clock::now();
    auto durationClever = duration_cast<milliseconds>(stopClever - startClever);
    cout << "Time clever: " << ((double)durationClever.count() / (double)1000) << " seconds" << endl;

    auto startStupid = high_resolution_clock::now();
    stupidSolution();
    auto stopStupid = high_resolution_clock::now();
    auto durationStupid = duration_cast<milliseconds>(stopStupid - startStupid);
    cout << "Time stupid: " << ((double)durationStupid.count() / (double)1000) << " seconds" << endl;

    return 0;
}

