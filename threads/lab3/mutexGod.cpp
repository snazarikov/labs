#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>

#include <thread>
#include <mutex>
#include <memory>

#include <stack>
#include <fstream>

using namespace std;

const int MAX_PRIORITY = 1e9;

class threadGuard {
    private:
        thread _t;
    public:
        threadGuard(thread obj) : _t(move(obj)) {}
        ~threadGuard() {
            if (_t.joinable()) {
                _t.join();
            }
        }
        threadGuard(threadGuard &obj) = delete;
        threadGuard& operator=(threadGuard& obj) = delete;

        threadGuard(threadGuard&& other) {
            _t = move(other._t);
        }
};

class hierarchicalMutex {
    private:
        mutex _m;
        int _currentPossiblePriority;
        int _previousPriority;

        void isUpdateCorrect() {
            if (_currentPossiblePriority >= _threadPriority) {
                throw logic_error("priority that you want to lock is greater or equal than current thread priority");
            }
            else {
                cout << "new priority been set" << endl;
            }
        }
        void makeUpdate() {
            _previousPriority = _threadPriority;
            _threadPriority = _currentPossiblePriority;
        }
    public:
        static thread_local int _threadPriority;
        hierarchicalMutex(int priority) : _currentPossiblePriority(priority), _previousPriority(0) {} // why 0

        void lock() {
            isUpdateCorrect();
            _m.lock();
            makeUpdate();
        }
        void unlock() {
            _threadPriority = _previousPriority;
            _m.unlock();
        }
        bool try_lock() {
            isUpdateCorrect();
            if (!_m.try_lock()) {
                return false;
            }
            makeUpdate();
            return true;
        }
};

thread_local int hierarchicalMutex::_threadPriority(MAX_PRIORITY);

hierarchicalMutex highLevelMutex(10000);
hierarchicalMutex lowLevelMutex(5000);
hierarchicalMutex otherMutex(100);

int doLowLevelStuff() {
    return 2;
}

int lowLevelFunction() {
    lock_guard<hierarchicalMutex> myMutex(lowLevelMutex);
    return doLowLevelStuff();
}

void highLevelStuff(int param) {
    return;
}

void highLevelFunction() {
    lock_guard<hierarchicalMutex> myMutex(highLevelMutex);
    highLevelStuff(lowLevelFunction());
}

void doOtherStuff() {
    return;
}

void otherStuff() {
    highLevelFunction();
    doOtherStuff();
}

void threadA() {
    highLevelFunction();
}

void threadB() {
    lock_guard<hierarchicalMutex> myMutex(otherMutex);
    otherStuff();
}

int main(void) {
    srand(time(NULL));

    threadGuard A{thread(threadA)};
    threadGuard B{thread(threadB)};

    return 0;
}

