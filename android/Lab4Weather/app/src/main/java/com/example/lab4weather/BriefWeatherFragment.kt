package com.example.lab4weather

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_brief_weather.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*

class BriefWeatherFragment : Fragment() {
    companion object {
        val DETAILED_WEATHER_KEY = "DETAILED_WEATHER_KEY_"
        val CITY_ID = 22;
    }

    private var briefWeatherData: ArrayList<Weather>? = null
    private var detailedWeatherData: ArrayList<Weather>? = null
    private val onItemClickListener = View.OnClickListener { view ->
        // This viewHolder will have all required values.
        val viewHolder = view.tag as RecyclerView.ViewHolder
        val position = viewHolder.adapterPosition
        val thisItem = briefWeatherData!!.get(position)

        Toast.makeText(context, "You Clicked: " + thisItem.temp, Toast.LENGTH_SHORT).show()

        var chosenData = detailedWeatherData!!.filter { currWeather -> (currWeather.date == thisItem.date)} as ArrayList<Weather>?

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            val intent = Intent(context, DetailedWeatherActivity::class.java)
            for (i in 0..chosenData!!.size - 1) {
                intent.putExtra(DETAILED_WEATHER_KEY + chosenData?.get(i)?.timeOfDay,  chosenData?.get(i).toString())
            }
            startActivity(intent)
        }
        else {
            val fragmentTwo = DetailedWeatherFragment()
            val bundle = Bundle()

            //put your ArrayList data in bundle
            for (i in 0..chosenData!!.size - 1) {
                bundle.putString(DETAILED_WEATHER_KEY + chosenData?.get(i)?.timeOfDay,  chosenData?.get(i).toString())
            }
            fragmentTwo.setArguments(bundle)

            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_land_right, fragmentTwo)
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val smth : View = inflater.inflate(R.layout.fragment_brief_weather, container, false)

        // kostili
        smth.setFocusableInTouchMode(true)
        smth.requestFocus()
        smth.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.getAction() === KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        return true
                    }
                }
                return false
            }
        })
        //

        // rofli s retrofitom
        val retrofit = Retrofit.Builder()
            .baseUrl("http://icomms.ru/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create<APIRuiner>(APIRuiner::class.java)
        val call = service.getWeatherList(CITY_ID)

        call.enqueue(object : Callback<List<Weather>> {
            override fun onResponse(call: Call<List<Weather>>, response: Response<List<Weather>>) {
                var weatherData = (response.body() as? ArrayList<Weather>)!!
                detailedWeatherData = weatherData

                var someData = weatherData.filter { currWeather -> (currWeather.timeOfDay == "2")} as ArrayList<Weather>

                briefWeatherData = someData
                var weatherAdapter = WeatherAdapter(briefWeatherData)
                smth.weather_brief_list.setLayoutManager(LinearLayoutManager(activity!!.applicationContext))
                smth.weather_brief_list.adapter = weatherAdapter
//                smth.weather_brief_list.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
                weatherAdapter.setItemClickListener(onItemClickListener)
            }

            override fun onFailure(call: Call<List<Weather>>, t: Throwable) {
                var someData = arrayListOf<Weather>()
                for (i in 1..4) {
                    someData.add(Weather("kek", "2", "kek", i.toString(), "kek", "kek", "kek"))
                }
                briefWeatherData = someData
                detailedWeatherData = someData
                Toast.makeText(activity!!.applicationContext, "PROIZOSHOL DDOS", Toast.LENGTH_LONG).show()
                var weatherAdapter = WeatherAdapter(briefWeatherData)
                smth.weather_brief_list.setLayoutManager(LinearLayoutManager(activity!!.applicationContext))
                smth.weather_brief_list.adapter = weatherAdapter
//        smth.recycler_list_f1.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
                weatherAdapter.setItemClickListener(onItemClickListener)
            }
        })
        //

        return smth
    }
}