package com.example.lab4weather

import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.fragment_detaliled_weather.*
import kotlinx.android.synthetic.main.fragment_detaliled_weather.view.*

class DetailedWeatherFragment : Fragment() {
    companion object {
        val DETAILED_WEATHER_KEY = "DETAILED_WEATHER_KEY_"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val smth : View = inflater.inflate(R.layout.fragment_detaliled_weather, container, false)

        val data0 = arguments!!.getString(DETAILED_WEATHER_KEY + 0.toString())
        val data1 = arguments!!.getString(DETAILED_WEATHER_KEY + 1.toString())
        val data2 = arguments!!.getString(DETAILED_WEATHER_KEY + 2.toString())
        val data3 = arguments!!.getString(DETAILED_WEATHER_KEY + 3.toString())

        smth.detailed_text_0.setText("Ночь\n" + data0)
        smth.detailed_text_1.setText("Утро\n" + data1)
        smth.detailed_text_2.setText("День\n" + data2)
        smth.detailed_text_3.setText("Вечер\n" + data3)

        changeImage(data1, smth.detailed_image_1)
        changeImage(data2, smth.detailed_image_2)
        changeImage(data3, smth.detailed_image_3)
        smth.detailed_image_0.setImageResource(R.drawable.moon_icon_256)


        return smth
    }
}