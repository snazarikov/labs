package com.example.kotlin.click

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class ResultActivity : AppCompatActivity() {

    companion object {
        val INTENT_POINT_KEY = "intent_point_key"
        val INTENT_RECORD_KEY = "intent_record_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        var point = intent.getIntExtra(INTENT_POINT_KEY, 0)
        var maxPoint = intent.getIntExtra(INTENT_RECORD_KEY, 0)
        Log.d("my_log", point.toString())

        var resultText = findViewById(R.id.result_text) as TextView
        var resultMsg = findViewById(R.id.result_message_text) as TextView
        var resultImg = findViewById(R.id.result_image) as ImageView

        when (point / maxPoint.toDouble()) {
            in 1..500 -> {
                resultImg.setImageDrawable(applicationContext.resources.getDrawable(R.drawable.smile))
                resultMsg.text = "Отлично!\nТвой результат " + point.toString()
                resultText.text = "Рекорд " + maxPoint.toString()
            }
            else -> if (point / maxPoint.toDouble() >= 0.7  && point / maxPoint.toDouble() < 1.0) {
                resultImg.setImageDrawable(applicationContext.resources.getDrawable(R.drawable.sad))
                resultMsg.text = "Ты можешь лучше\nТвой результат " + point.toString()
                resultText.text = "Рекорд " + maxPoint.toString()
            }
            else {
                resultImg.setImageDrawable(applicationContext.resources.getDrawable(R.drawable.frightened))
                resultMsg.text = "Плохо\nТвой результат " + point.toString()
                resultText.text = "Рекорд " + maxPoint.toString()
            }
        }

        var restartBtn = findViewById(R.id.restart_btn) as Button
        restartBtn.setOnClickListener {
            var intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

    }
}