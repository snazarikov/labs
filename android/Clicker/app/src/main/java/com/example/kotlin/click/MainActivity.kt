package com.example.kotlin.click

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.util.*
import kotlin.concurrent.timerTask

class MainActivity : AppCompatActivity() {
    companion object {
        private val POINT_KEY = "point_key"
        private val TIME_KEY = "time_key"
        private val SHARED_KEY = "clicker_shared"
        private val SHARED_KEY_POINTS = "clicker_shared_points"
        private var mpBackground = MediaPlayer()
        private var mp = MediaPlayer()
    }

    private var point = 0
    private var time = 11

    private var pointText: TextView? = null
    private var timeText: TextView? = null
    private var recordText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var clickBtn = findViewById(R.id.click_btn) as Button
        pointText = findViewById(R.id.point_text) as TextView
        recordText = findViewById(R.id.record_text) as TextView
        timeText = findViewById(R.id.time_text) as TextView

        recordText?.text = "Твой рекорд " + getIntPreferences(SHARED_KEY_POINTS).toString()

        if (savedInstanceState == null)
            mpBackground = MediaPlayer.create(applicationContext, R.raw.background_trak)
        mpBackground.start()

        mpBackground.setOnCompletionListener {
            mpBackground.start()
        }

        clickBtn.setOnClickListener {
            point++
            pointText?.text = point.toString()
        }


        var timer = Timer()
        timer.schedule(timerTask {
            time--
            if (time <= 0) timer.cancel()
            timeText?.post {
                timeText?.text = (time / 60).toString() + ":" + (time % 60).toString()
            }
            var random = (Math.random() * 10).toInt()
            if (random == 1 && !mp.isPlaying) {
                mp = MediaPlayer.create(applicationContext, R.raw.trak_2)
                mp.start()
            }
            if (time <= 0) {
                clickBtn.post {
                    clickBtn.isEnabled = false
                }
                if (getIntPreferences(SHARED_KEY_POINTS) < point)
                    setIntPreferences(SHARED_KEY_POINTS, point)
                var intent = Intent(applicationContext, ResultActivity::class.java)
                intent.putExtra(ResultActivity.INTENT_POINT_KEY, point)
                intent.putExtra(ResultActivity.INTENT_RECORD_KEY, getIntPreferences(SHARED_KEY_POINTS))
                startActivity(intent)
                timer.cancel()
                finish()
            }
        }, 500, 1000)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (outState != null) {
            outState.putInt(POINT_KEY, point)
            outState.putInt(TIME_KEY, time)
        }


    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState != null) {
            point = savedInstanceState.getInt(POINT_KEY)
            time = savedInstanceState.getInt(TIME_KEY)
            pointText?.text = point.toString()
            timeText?.text = (time / 60).toString() + ":" + (time % 60).toString()

        }
    }

    private fun setIntPreferences(key: String, point: Int) {
        val sh = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE)
        val editor = sh.edit()
        editor.putInt(key, point)
        editor.apply()
    }

    private fun getIntPreferences(key: String): Int {
        val sh = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE)
        return sh.getInt(key, 0)
    }

    override fun onStop() {
        super.onStop()
        mpBackground.pause()
    }
}


