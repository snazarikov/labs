package com.example.a1.noticeit.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a1.noticeit.R;
import com.example.a1.noticeit.model.NoticeModel;
import com.example.a1.noticeit.utility.DBHelper;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class CreateActivity extends AppCompatActivity {

    private EditText titleOfNotice;
    private EditText descriptionOfNotice;

//    private Button doneButton;
//    private Button cancelButton;

    private ImageView doneImage;
    private ImageView cancelImage;

    private Intent intent;

    private int id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        titleOfNotice = (EditText) findViewById(R.id.title_of_notice);
        descriptionOfNotice = (EditText) findViewById(R.id.description_of_notice);

//        doneButton = (Button) findViewById(R.id.done_button);
//        cancelButton = (Button) findViewById(R.id.cancel_button);

        doneImage = (ImageView) findViewById(R.id.done_image);
        cancelImage = (ImageView) findViewById(R.id.cancel_image);

        doneImage.setClickable(true);
        cancelImage.setClickable(true);

        intent = getIntent();

        if (intent.getStringExtra("action").equals("edit")) {
//            Toast.makeText(CreateActivity.this, "IT WORKS LUL", Toast.LENGTH_SHORT).show();
            ((TextView) findViewById(R.id.title_create)).setText("Edit notice");
            titleOfNotice.setText(intent.getStringExtra("title"));
            descriptionOfNotice.setText(intent.getStringExtra("description"));
            id = intent.getIntExtra("id", -1);
        }

        doneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper dbHelper = new DBHelper(CreateActivity.this);

                dbHelper.initDB(CreateActivity.this);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy");
                String dateTime = simpleDateFormat.format(new java.util.Date(System.currentTimeMillis()));

                NoticeModel noticeModel = new NoticeModel(titleOfNotice.getText().toString(), descriptionOfNotice.getText().toString(), dateTime, id);

                if (intent.getStringExtra("action").equals("edit")) {
//                    Toast.makeText(CreateActivity.this, "IT WORKS LUL #2", Toast.LENGTH_SHORT).show();
                    dbHelper.updateNotice(noticeModel);
                }
                else {
                    dbHelper.addNotice(noticeModel);
                }

                dbHelper.close();

                Intent startIntent = new Intent(CreateActivity.this, MainActivity.class);
                startActivity(startIntent);
                finish();
            }
        });

        cancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

}
