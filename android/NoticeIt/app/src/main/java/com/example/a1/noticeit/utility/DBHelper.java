package com.example.a1.noticeit.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.a1.noticeit.model.NoticeModel;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private DBHelper dbHelper;
    private SQLiteDatabase database;

    public static String DB_NAME = "noticeDB";
    public static int DB_VERSION = 1;
    public static String DB_COLUMN_NAME = "name";
    public static String DB_COLUMN_DESCRIPTION = "description";
    public static String DB_COLUMN_DATE = "date";
    public static String DB_TABLE_NOTICE = "noticeDB";
    public static String DB_CREATE_QUERY = "CREATE TABLE " + DB_TABLE_NOTICE + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DB_COLUMN_NAME + " TEXT NOT NULL, " + DB_COLUMN_DESCRIPTION + " TEXT, " + DB_COLUMN_DATE + " TEXT )";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void initDB(Context context) {
        if (dbHelper == null) {
            dbHelper = new DBHelper(context);
            database = dbHelper.getWritableDatabase();
        }
    }

    public ArrayList<NoticeModel> getData () {
        ArrayList<NoticeModel> arrayList = new ArrayList<>();
        Cursor cursor = database.query(DB_TABLE_NOTICE, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                NoticeModel newNotice = new NoticeModel(
                        cursor.getString(cursor.getColumnIndex(DB_COLUMN_NAME)),
                        cursor.getString(cursor.getColumnIndex(DB_COLUMN_DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndex(DB_COLUMN_DATE)),
                        cursor.getInt(cursor.getColumnIndex("_id"))
                );
                arrayList.add(newNotice);
            }
            while (cursor.moveToNext());
        }

        cursor.close();

        return arrayList;
    }

    public void addNotice(NoticeModel noticeModel) {
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(DB_COLUMN_NAME, noticeModel.getTitle());
//        contentValues.put(DB_COLUMN_DESCRIPTION, noticeModel.getDescription());
//        database.insert(DB_TABLE_NOTICE, null, contentValues);


        try {
            database.beginTransaction();
            ContentValues contentValues = new ContentValues();
            contentValues.put(DB_COLUMN_NAME, noticeModel.getTitle());
            contentValues.put(DB_COLUMN_DESCRIPTION, noticeModel.getDescription());
            contentValues.put(DB_COLUMN_DATE, noticeModel.getDate());
            database.insert(DB_TABLE_NOTICE, null, contentValues);
            contentValues.clear();
            database.setTransactionSuccessful();
        }
        finally {
            database.endTransaction();
        }
    }

    public void updateNotice(NoticeModel noticeModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_NAME, noticeModel.getTitle());
        contentValues.put(DB_COLUMN_DESCRIPTION, noticeModel.getDescription());
        contentValues.put(DB_COLUMN_DATE, noticeModel.getDate());
        database.update(DB_TABLE_NOTICE, contentValues, "_id = ?", new String[] {String.valueOf(noticeModel.getId())});
    }

    public void deleteNotice(NoticeModel noticeModel) {
        int id = noticeModel.getId();
        database.delete(DB_TABLE_NOTICE, "_id = ?", new String[] {String.valueOf(id)});
    }
}
