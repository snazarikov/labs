package com.example.a1.noticeit.utility;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.a1.noticeit.R;
import com.example.a1.noticeit.activity.CreateActivity;
import com.example.a1.noticeit.model.NoticeModel;

import java.util.ArrayList;
import java.util.Collections;


public class NoticeModelAdapter extends RecyclerView.Adapter<NoticeModelAdapter.ViewHolder> {

    private ArrayList<NoticeModel> arrayList;
    private Context context;
    public static NoticeModelAdapter myAdapter;

    public NoticeModelAdapter(ArrayList<NoticeModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    public static NoticeModelAdapter initNoticeModelAdapter(ArrayList<NoticeModel> arrayList, Context context) {
//        Collections.reverse(arrayList);
        myAdapter = new NoticeModelAdapter(arrayList, context);
        return myAdapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final NoticeModelAdapter.ViewHolder holder, int position) {
        final NoticeModel noticeModel = arrayList.get(position);

        holder.titleText.setText(noticeModel.getTitle());
        holder.descriptionText.setText(noticeModel.getDescription());
        holder.dateText.setText(noticeModel.getDate());

//        holder.editButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, CreateActivity.class);
//
//                intent.putExtra("action", "edit");
//                intent.putExtra("title", noticeModel.getTitle());
//                intent.putExtra("description", noticeModel.getDescription());
//                intent.putExtra("id", noticeModel.getId());
//
//                context.startActivity(intent);
////                ((Activity) context).finish();
//            }
//        });

        holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(final ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle("Select The Action");

                menu.add("Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        DBHelper dbHelper = new DBHelper(context);
                        dbHelper.initDB(context);
                        dbHelper.deleteNotice(noticeModel);
                        myAdapter.arrayList = dbHelper.getData();
                        myAdapter.notifyDataSetChanged();
                        dbHelper.close();
                        Collections.reverse(arrayList);
                        return true;
                    }
                });

                menu.add("Edit").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(context, CreateActivity.class);

                        intent.putExtra("action", "edit");
                        intent.putExtra("title", noticeModel.getTitle());
                        intent.putExtra("description", noticeModel.getDescription());
                        intent.putExtra("id", noticeModel.getId());

                        context.startActivity(intent);
                        Collections.reverse(arrayList);
//                        ((Activity) context).finish();
                        return true;
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titleText;
        public TextView descriptionText;
        public TextView dateText;
//        public Button editButton;

        public ViewHolder(View itemView) {
            super(itemView);

            titleText = (TextView) itemView.findViewById(R.id.title_text);
            descriptionText = (TextView) itemView.findViewById(R.id.description_text);
            dateText = (TextView) itemView.findViewById(R.id.current_date_text);
//            editButton = (Button) itemView.findViewById(R.id.edit_button);

        }
    }
}
