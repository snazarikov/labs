package com.example.a1.noticeit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.a1.noticeit.R;
import com.example.a1.noticeit.model.NoticeModel;
import com.example.a1.noticeit.utility.DBHelper;
import com.example.a1.noticeit.utility.NoticeModelAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private DBHelper dbHelper;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    private ArrayList<NoticeModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, CreateActivity.class);
                intent.putExtra("action", "create");
                startActivity(intent);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        layoutManager = new LinearLayoutManager(this);

        dbHelper = new DBHelper(this);
        dbHelper.initDB(this);

        list = dbHelper.getData();
        Collections.reverse(list);
        adapter = NoticeModelAdapter.initNoticeModelAdapter(list, getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        dbHelper.close();

        registerForContextMenu(recyclerView);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
