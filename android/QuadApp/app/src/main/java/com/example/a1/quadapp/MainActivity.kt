package com.example.a1.quadapp

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.EditText
import android.view.View
import android.widget.TextView
import android.widget.Toast
import java.lang.Double.NaN
import java.lang.Math.sqrt

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var point = 0
        var flag = 0
        var solveBtn = findViewById(R.id.solve_btn) as Button
        var scan_A = findViewById(R.id.scanA) as EditText
        var scan_B = findViewById(R.id.scanB) as EditText
        var scan_C = findViewById(R.id.scanC) as EditText
        var discrim = findViewById(R.id.disc) as TextView
        var answerF = findViewById(R.id.first) as TextView
        var answerS = findViewById(R.id.second) as TextView
        //var check = findViewById(R.id.checker) as TextView
        var a = "CheckF"
        var b = "CheckS"
        var c = "CheckT"
        var D: Double
        var firstAns: Double
        var secondAns: Double

        //solveBtn.isEnabled = false

        // a = scan_A.text.toString() + ".0"
        // b = scan_B.text.toString() + ".0"
        // c = scan_C.text.toString() + ".0"


        scan_A.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event != null) {
                if (event.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    a = scan_A.text.toString()
                    if (a == "") {
                        a = "checkF"
                    }
                    return@OnKeyListener true
                }
            }
            return@OnKeyListener false
        })

        scan_B.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event != null) {
                if (event.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    b = scan_B.text.toString()
                    if (b == "") {
                        b = "checkS"
                    }
                    return@OnKeyListener true
                }
            }
            return@OnKeyListener false
        })



        scan_C.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event != null) {
                if (event.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    c = scan_C.text.toString()
                    if (c == "") {
                        c = "checkT"
                    }
                    return@OnKeyListener true
                }
            }
            return@OnKeyListener false
        })

        /*
        if (a!="checkF" && b!="checkS" && c!="checkT") {
            solveBtn.post {
                solveBtn.isEnabled = true
            }
        }
        */
        solveBtn.setOnClickListener {
            if (scan_A.text.isNotEmpty() && scan_B.text.isNotEmpty() && scan_C.text.isNotEmpty()) {

                val A = a.toDouble()
                val B = b.toDouble()
                val C = c.toDouble()
                //D=B*B-4*A*C
                if (A == 0.0 && B == 0.0 && C == 0.0) {
                    discrim.text = "D=0"
                    answerF.text = "Корней"
                    answerS.text = "бесконечно много"
                }
                if (A == 0.0 && B != 0.0 && C != 0.0) {
                    secondAns = -C / B
                    discrim.text = "Это линейное уравнение"
                    answerF.text = "Единственный корень:"
                    answerS.text = "X=" + secondAns.toString()
                }
                if (A == 0.0 && B == 0.0 && C != 0.0) {
                    discrim.text = "Неверное равенство"
                    answerF.text = "Корней"
                    answerS.text = "нет"
                }
                if (A == 0.0 && B != 0.0 && C == 0.0) {
                    discrim.text = "Это линейное уравнение"
                    answerF.text = "Единственный корень:"
                    answerS.text = "X=0"
                }
                if (A !== 0.0 && B == 0.0 && C == 0.0) {
                    discrim.text = "Это квадратное уравнение"
                    answerF.text = "Единственный корень:"
                    answerS.text = "X=0"
                }
                if (A != 0.0 && B != 0.0 && C != 0.0) {
                    D = B * B - 4 * A * C
                    if (D > 0) {
                        firstAns = (-B - sqrt(D)) / 2 * A
                        secondAns = (-B + sqrt(D)) / 2 * A
                        discrim.text = "D=" + D.toString()
                        answerF.text = "X1=" + firstAns.toString()
                        answerS.text = "X2=" + secondAns.toString()
                    }
                    if (D == 0.0) {
                        secondAns = -B / 2 * A
                        discrim.text = "D=" + D.toString()
                        answerF.text = "Единственный корень:"
                        answerS.text = "X=" + secondAns.toString()
                    }
                    if (D < 0) {
                        discrim.text = "D<0"
                        answerF.text = "Корней"
                        answerS.text = "нет"
                    }

                }

            }
        }

        /*if (solveBtn.isPressed==true) {
            val A = a.toDouble()
            val B = b.toDouble()
            val C = c.toDouble()
            D=B*B-4*A*C
            if (D>=0) {
                firstAns=(-B-sqrt(D))/2*A
                secondAns=(-B+sqrt(D))/2*A
                discrim.text="D=" + D.toString()
                answerF.text="X1="+firstAns.toString()
                answerS.text="X2="+secondAns.toString()
            }
        }*/
    }
}


