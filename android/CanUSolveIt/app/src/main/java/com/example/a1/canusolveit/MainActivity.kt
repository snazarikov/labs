package com.example.a1.canusolveit

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.util.*
import kotlin.concurrent.timerTask

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var generateBtn = findViewById(R.id.generate_btn) as Button
        var answerBtn = findViewById(R.id.answer_btn) as Button
        var answerTextOne = findViewById(R.id.answer_text_one) as EditText
        var answerTextTwo = findViewById(R.id.answer_text_two) as EditText
        var answerTextThree = findViewById(R.id.answer_text_three) as EditText
        var taskTextOne = findViewById(R.id.task_text_one) as TextView
        var taskTextTwo = findViewById(R.id.task_text_two) as TextView
        var taskTextThree = findViewById(R.id.task_text_three) as TextView
        //var resultText = findViewById(R.id.result_text) as TextView
        var modText = findViewById(R.id.mod_text) as TextView
        var firstOne: Int
        var secondOne: Int
        var firstTwo: Int
        var secondTwo: Int
        var firstThree: Int
        var secondThree: Int
        var correctAnswerOne: Int
        var userAnsOne: Int
        var correctAnswerTwo: Int
        var userAnsTwo: Int
        var correctAnswerThree: Int
        var userAnsThree: Int
        //answerBtn.isEnabled = false
        generateBtn.setOnClickListener {
            firstOne = (Math.round(Math.random() * 100).toInt())
            secondOne = (Math.round(Math.random() * 100).toInt())
            firstTwo = (Math.round(Math.random() * 10).toInt())
            secondTwo = (Math.round(Math.random() * 10).toInt())
            firstThree = (Math.round(Math.random() * 100).toInt())
            secondThree = (Math.round(Math.random() * 100).toInt())
            taskTextOne.text = firstOne.toString() + "        +        " + secondOne.toString() + "              ="
            taskTextTwo.text = firstTwo.toString() + "        *        " + secondTwo.toString() + "              ="
            taskTextThree.text = firstThree.toString() + "        -        " + secondThree.toString() + "              ="
            correctAnswerOne = firstOne + secondOne
            correctAnswerTwo = firstTwo * secondTwo
            correctAnswerThree = firstThree - secondThree
            //userAnsOne = (answerTextOne.text).toString().toInt()
            //userAnsTwo = (answerTextTwo.text).toString().toInt()
            //userAnsThree = (answerTextThree.text).toString().toInt()


            //if (answerTextOne.text.isNotEmpty() && answerTextTwo.text.isNotEmpty() && answerTextThree.text.isNotEmpty()) {
            //generateBtn.isEnabled = true
            answerBtn.setOnClickListener {
                if (answerTextOne.text.isNotEmpty() && answerTextTwo.text.isNotEmpty() && answerTextThree.text.isNotEmpty()) {
                    var counterOfCorrectAns = 0
                    userAnsOne = (answerTextOne.text).toString().toInt()
                    userAnsTwo = (answerTextTwo.text).toString().toInt()
                    userAnsThree = (answerTextThree.text).toString().toInt()
                    if (correctAnswerOne == userAnsOne) counterOfCorrectAns++
                    if (correctAnswerTwo == userAnsTwo) counterOfCorrectAns++
                    if (correctAnswerThree == userAnsThree) counterOfCorrectAns++
                    //resultText.text = "Ты решил верно\n" + counterOfCorrectAns.toString() + " из 3"
                    var intent = Intent(applicationContext, ResultActivity::class.java)
                    intent.putExtra("counterOfCorrectAns", counterOfCorrectAns.toString())
                    startActivity(intent)
                    finish()
                    //modText.text = counterOfCorrectAns.toString()
                }
            }
            //}
        }
    }
}


