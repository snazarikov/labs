package com.example.a1.canusolveit

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView


class ResultActivity : AppCompatActivity() {

    public val INTENT_KEY = "intetnt_key"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        var resultText = findViewById(R.id.result_text) as TextView
        var congratulationsText = findViewById(R.id.congratulations_text) as TextView
        var resultImage = findViewById(R.id.result_image) as ImageView
        var countOfCorrectAns = (intent.getStringExtra("counterOfCorrectAns")).toInt()
        //resultText.text = "Ты решил верно\n" + countOfCorrectAns.toString() + " из 3"

        if (countOfCorrectAns == 3) {
            resultText.text = "Молодец, ты решил верно\n" + countOfCorrectAns.toString() + " из 3"
            congratulationsText.text = "Владимир Николаевич\nдоволен тобой"
        }

        if (countOfCorrectAns == 2) {
            resultText.text = "Хорошо, ты решил верно\n" + countOfCorrectAns.toString() + " из 3"
            congratulationsText.text = "Но Владимир Николаевич\nвсе ещё недоволен тобой"
            resultImage.setImageResource(R.drawable.sooo)
        }

        if (countOfCorrectAns == 1) {
            resultText.text = "Ты можешь лучше, ты решил верно\n" + countOfCorrectAns.toString() + " из 3"
            resultImage.setImageResource(R.drawable.bad)
            congratulationsText.text = "Владимир Николаевич\nвсе ещё недоволен тобой"
        }

        if (countOfCorrectAns == 0) {
            resultText.text = "Плохо, ты решил верно\n" + countOfCorrectAns.toString() + " из 3"
            congratulationsText.text = "Владимир Николаевич\nнедоволен тобой"
            resultImage.setImageResource(R.drawable.bad)
        }


        var restartBtn = findViewById(R.id.restart_btn) as Button
        restartBtn.setOnClickListener {
            var intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

    }
}