package com.example.testmoxyfragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.weather_list_item.view.*

class WeatherAdapter(var items: ArrayList<Weather>?) : RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.weather_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    override fun onBindViewHolder(holder: WeatherAdapter.ViewHolder, position: Int) {
        holder.weatherText.text = items!!.get(position).date + "\n" + items!!.get(position).temp + "\n" + items!!.get(position).cloud
        changeImage(items!!.get(position).cloud, holder.weatherImage)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val weatherText = view.weather_item_text
        val weatherImage = view.weather_item_image
    }
}