package com.example.testmoxyfragment

import android.widget.ImageView

fun changeImage(data : String?, image : ImageView) {
    if (data!!.toLowerCase().contains("гроза")) {
        image.setImageResource(R.drawable.storm_icon_256)
    }
    else if (data!!.toLowerCase().contains("дождь")) {
        image.setImageResource(R.drawable.rain_icon_256)
    }
    else if (data!!.toLowerCase().contains("пасмурно")) {
        image.setImageResource(R.drawable.little_rain_icon_256)
    }
    else if (data!!.toLowerCase().contains("снег")) {
        image.setImageResource(R.drawable.snow_icon_256)
    }
    else if (data!!.toLowerCase().contains("облачно")) {
        image.setImageResource(R.drawable.partly_cloudy_day_icon_256)
    }
    else if (data!!.toLowerCase().contains("ясно")) {
        image.setImageResource(R.drawable.sun_icon_256)
    }
    else {
        image.setImageResource(R.drawable.mushroom_cloud_icon_256)
    }
}