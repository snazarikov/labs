package com.example.testmoxyfragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.fragment_brief_weather.view.*

class BriefWeatherFragment : MvpAppCompatFragment(), IBriefWeatherView {
    @InjectPresenter
    lateinit var mPresenter: BriefWeatherPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val smth: View = inflater.inflate(R.layout.fragment_brief_weather, container, false)

        // magiya chelika s habra
        with(ItemClickSupport.addTo(smth.weather_brief_list)) {
            setOnItemClickListener { recyclerView, position, v -> mPresenter.handleItemClick(activity!!, position) }
//            setOnItemLongClickListener { recyclerView, position, v -> mPresenter.showNoteContextDialog(position); true }
        }
        //

        mPresenter.loadWeather(activity!!, smth)

        return smth
    }

    override fun displayBriefWeather(smth : View, weatherData: ArrayList<Weather>?) {
        var weatherAdapter = WeatherAdapter(weatherData)
        smth.weather_brief_list.setLayoutManager(LinearLayoutManager(activity!!.applicationContext))
        smth.weather_brief_list.adapter = weatherAdapter
    }
}