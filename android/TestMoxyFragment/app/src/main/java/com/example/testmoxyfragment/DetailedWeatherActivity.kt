package com.example.testmoxyfragment

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class DetailedWeatherActivity : AppCompatActivity() {

    companion object {
        val DETAILED_WEATHER_KEY = "DETAILED_WEATHER_KEY_"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed_weather)

        val fragment = DetailedWeatherFragment()
        val bundle = Bundle()

        for (i in 0..3) {
            bundle.putString(DETAILED_WEATHER_KEY + i.toString(), intent.getStringExtra(DETAILED_WEATHER_KEY + i.toString()))
        }

        fragment.arguments = bundle

        supportFragmentManager.beginTransaction()
            .replace(R.id.frame_detailed_activity, fragment)
            .addToBackStack(null)
            .commit()
    }
}
