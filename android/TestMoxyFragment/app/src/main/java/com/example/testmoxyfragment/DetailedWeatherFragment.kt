package com.example.testmoxyfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import kotlinx.android.synthetic.main.fragment_detailed_weather.view.*

class DetailedWeatherFragment : MvpAppCompatFragment() {
    companion object {
        val DETAILED_WEATHER_KEY = "DETAILED_WEATHER_KEY_"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val smth : View = inflater.inflate(R.layout.fragment_detailed_weather, container, false)

        var data = arrayListOf("", "", "", "")

        for (i in 0..3) {
            data[i] = arguments!!.getString(DETAILED_WEATHER_KEY + i.toString())
        }

        smth.detailed_text_0.setText("Ночь\n" + data[0])
        smth.detailed_text_1.setText("Утро\n" + data[1])
        smth.detailed_text_2.setText("День\n" + data[2])
        smth.detailed_text_3.setText("Вечер\n" + data[3])

        changeImage(data[1], smth.detailed_image_1)
        changeImage(data[2], smth.detailed_image_2)
        changeImage(data[3], smth.detailed_image_3)
        smth.detailed_image_0.setImageResource(R.drawable.moon_icon_256)

        return smth
    }
}