﻿using System;
using MyContainer;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            MyBitContainer test_container = new MyBitContainer();
            for (int i = 0; i < 4; ++i) {
                test_container.pushBit(false);
                test_container.pushBit(true);
            }
            test_container.pushBit(false);
            //test_container.pushBit(true);
            //test_container.pushBit(true);
            //test_container.pushBit(true);
            //test_container.pushBit(true);
            //test_container.pushBit(true);
            //test_container.print_kostil();
            Console.WriteLine(test_container.ToString());

            byte a = 255;
            byte d = 1;

            //byte b = MyBitContainer.turn_off_ith_bit(a, 7);
            //byte c = MyBitContainer.turn_on_ith_bit(d, 0);

            //bool bit = test_container.getBit(43);
            //Console.WriteLine(bit);
            //Console.WriteLine(test_container.ToString());
            //Console.WriteLine(test_container[0]);
            //Console.WriteLine(test_container[1]);

            //test_container.setBit(4, false);
            //test_container.print_kostil();
            test_container.remove(0);
            //test_container.print_kostil();
            Console.WriteLine(test_container.ToString());
            Console.WriteLine(test_container.length());
            //test_container.insert(2, false);
            //test_container.print_kostil();
            //test_container.insert(39, true);
            //test_container.print_kostil();
            //test_container.remove(0);
            //test_container.print_kostil();

            //test_container.insert(16, true);
            //test_container.print_kostil();
            //test_container.insert(45, true);
            //test_container.print_kostil();
            //test_container.insert(46, true);
            //test_container.print_kostil();
            //test_container.insert(47, false);
            //test_container.print_kostil();

            foreach (bool elem in test_container) {
                Console.Write(elem);
            }
        }
    }
}
