﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace MyContainer {
    public class MyBitContainer {
        private List<byte> container_;
        private int size_;

        public MyBitContainer() {
            container_ = new List<byte>();
            size_ = 0;
        }

        public int length() {
            return size_;
        }

        private static byte set_ith_bit(byte number, int i, bool value) {
            i = 7 - i;
            if (i < 0 || i > 7) {
                throw new ArgumentOutOfRangeException("Bit position must be between 0 and 7");
            }

            if (value) {
                return (byte)(number | (1 << i));
            }
            else {
                return (byte)(number & ~(1 << i));
            }
        }

        public void print_kostil() {
            for (int i = 0; i < (size_ + 7) / 8; ++i) {
                Console.Write("{0} ", container_[i]);
            }
            Console.WriteLine();
            for (int i = 0; i < container_.Count; ++i) {
                Console.Write("{0} ", Convert.ToString(container_[i], 2).PadLeft(8, '0'));
            }
            Console.WriteLine();
        }

        public void pushBit(bool bit)
        {
            if (size_ % 8 == 0) {
                container_.Add(0);
            }
            container_[size_ / 8] <<= 1;
            container_[size_ / 8] += (byte)((bit == true) ? (1) : (0));
            size_++;
        }

        public void pushBit(int bit) {
            if (size_ % 8 == 0) {
                container_.Add(0);
            }
            container_[size_ / 8] <<= 1;
            container_[size_ / 8] += (byte)bit;
            size_++;
        }

        public bool getBit(int index) {
            if (index < 0 || index >= size_) {
                Console.WriteLine("index = {0} size_ = {1}", index, size_);
                throw new ArgumentOutOfRangeException("getBit, Index out of range");
            }

            int res;
            if (index >= (size_ / 8) * 8 && index < size_) {
                res = ((byte)(container_[container_.Count - 1] >> (8 - (8 - size_ % 8) - index % 8 - 1))) % 2;
            }
            else {
                res = ((byte)(container_[index / 8] >> (8 - index % 8 - 1))) % 2;
            }
            return ((res == 1) ? (true) : (false)); ;
        }

        public void setBit(int index, bool bit) {
            if (index < 0 || index >= size_) {
                throw new ArgumentOutOfRangeException("setBit, Index out of range");
            }

            if (index >= (size_ / 8) * 8 && index < size_) {
                container_[container_.Count - 1] = set_ith_bit(container_[container_.Count - 1], 8 - size_ % 8 + index % 8, bit);
            }
            else {
                container_[index / 8] = set_ith_bit(container_[index / 8], index % 8, bit);
            }
        }

        public void clear() {
            while (container_.Count > 0) {
                container_.RemoveAt(container_.Count - 1);
            }
            size_ = 0;
        }

        public void insert(int index, bool bit) {
            if (index < 0 || (index >= size_ && size_ != 0)) {
                throw new ArgumentOutOfRangeException("insert, Index out of range");
            }

            byte right_part;
            byte left_part;

            if (size_ == 0) {
                pushBit(bit);
                return;
            }

            if (index >= (size_ / 8) * 8 && index < size_) {
                right_part = (byte)((byte)(container_[size_ / 8] << (8 - ((size_ % 8) - (index % 8)))) >> (8 - ((size_ % 8) - (index % 8))));
                left_part = (byte)((byte)(container_[size_ / 8] >> ((size_ % 8) - (index % 8))) << (((size_ % 8) - (index % 8)) + 1));
                byte tmp = (byte)(right_part | left_part);
                container_[size_ / 8] = set_ith_bit(tmp, (index % 8) + (8 - size_ % 8 - 1), bit);
                size_++;
                return;
            }

            if (size_ % 8 == 0) {
                container_.Add(0);
            }

            byte last_bit = (byte)(container_[index / 8] % 2);
            right_part = (byte)((byte)(container_[index / 8] << (index % 8)) >> ((index % 8) + 1));
            left_part = (byte)((byte)(container_[index / 8] >> (8 - index % 8)) << (8 - index % 8));
            left_part = set_ith_bit(left_part, index % 8, bit);

            container_[index / 8] = (byte)(left_part | right_part);
            
            for (int i = (index / 8) + 1; i < size_ / 8; ++i) {
                right_part = (byte)(container_[i] % 2);
                container_[i] >>= 1;
                container_[i] = set_ith_bit(container_[i], 0, ((last_bit == 1) ? (true) : (false)));
                last_bit = right_part;
            }

            container_[size_ / 8] = set_ith_bit(container_[size_ / 8], (8 - size_ % 8) - 1, ((last_bit == 1) ? (true) : (false)));
            size_++;
        }

        public void remove(int index) {
            if (index < 0 || index >= size_) {
                throw new ArgumentOutOfRangeException("remove, Index out of range");
            }

            byte right_part;
            byte left_part;
            bool delete_last_byte = false;

            if (size_ % 8 == 1) {
                delete_last_byte = true;
            }

            int kostil = ((size_ % 8 == 0) ? (8) : (size_ % 8));
            if (index >= ((size_ - 1) / 8) * 8 && index < size_) {
                if (delete_last_byte == true) {
                    container_.RemoveAt(container_.Count - 1);
                    size_--;
                    return;
                }
                right_part = (byte)((byte)(container_[container_.Count - 1] << (8 - kostil + index % 8 + 1)) >> (8 - kostil + index % 8 + 1));
                left_part = (byte)((byte)(container_[container_.Count - 1] >> (kostil - index % 8)) << (kostil - index % 8 - 1));

                container_[container_.Count - 1] = (byte)(left_part | right_part);

                size_--;
                return;
            }
            
            right_part = (byte)(((byte)(container_[index / 8] << (index % 8 + 1))) >> (index % 8));
            left_part = (byte)(((byte)(container_[index / 8] >> (8 - index % 8))) << (8 - index % 8));
            container_[index / 8] = (byte)((byte)(right_part | left_part) + (byte)(container_[(index / 8) + 1] >> 7));

            for (int i = (index / 8) + 1; i < container_.Count - 2; ++i) {
                container_[i] <<= 1;
                container_[i] += (byte)(container_[i + 1] >> 7);
            }

            if (container_.Count > 2) {
                container_[container_.Count - 2] <<= 1;
                container_[container_.Count - 2] += (byte)(container_[container_.Count - 1] >> (kostil - 1));
            }
            container_[container_.Count - 1] <<= (8 - kostil + 1);
            container_[container_.Count - 1] >>= (8 - kostil + 1);

            if (delete_last_byte == true) {
                container_.RemoveAt(container_.Count - 1);
            }

            size_--;
        }

        public override string ToString()
        {
            string res = "";
            for (int i = 0; i < container_.Count - 1; ++i) {
                res += Convert.ToString(container_[i], 2).PadLeft(8, '0');
            }
            string last = "";
            byte tmp = container_[container_.Count - 1];
            for (int i = 0; i < ((size_ % 8 == 0) ? (8) : (size_ % 8)); ++i) {
                if (tmp % 2 == 1) {
                    last = last.Insert(0, "1");
                }
                else {
                    last = last.Insert(0, "0");
                }
                tmp >>= 1;
            }
            res += last;
            return res;
        }

        public bool this[int index]
        {
            get {
                return getBit(index);
            }
            set {
                setBit(index, value);
            }
        }

        public IEnumerator GetEnumerator() {
            for (int i = 0; i < size_; i++) {
                yield return getBit(i);
            }
        }
    }
}
