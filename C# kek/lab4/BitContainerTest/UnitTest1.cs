using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyContainer;

namespace BitContainerTest
{
    [TestClass]
    public class UnitTest1
    {
        private void fill_container(ref MyBitContainer container, int size) {
            for (int i = 0; i < size; ++i) {
                container.pushBit(((i % 2 == 1) ? (1) : (0)));
            }
        }

        [TestMethod]
        public void push_int_test()
        {
            MyBitContainer container = new MyBitContainer();
            container.pushBit(1);
            Assert.AreEqual(true, container[0] == true && container.length() == 1);
        }

        [TestMethod]
        public void push_bool_test() {
            MyBitContainer container = new MyBitContainer();
            container.pushBit(true);
            Assert.AreEqual(true, container[0] == true && container.length() == 1);
        }

        [TestMethod]
        public void get_bit_test() {
            MyBitContainer container = new MyBitContainer();
            fill_container(ref container, 10);
            Assert.AreEqual(true, (container.getBit(5) == true && container.getBit(6) == false));
        }

        [TestMethod]
        public void set_bit_test() {
            MyBitContainer container = new MyBitContainer();
            fill_container(ref container, 10);
            container.setBit(5, false);
            container.setBit(6, true);
            Assert.AreEqual(true, (container.getBit(5) == false && container.getBit(6) == true));
        }

        [TestMethod]
        public void clear_test() {
            MyBitContainer container = new MyBitContainer();
            fill_container(ref container, 10);
            container.clear();
            Assert.AreEqual(true, container.length() == 0);
        }

        [TestMethod]
        public void to_string_test() {
            MyBitContainer container = new MyBitContainer();
            fill_container(ref container, 8);
            string res = "01010101";
            Assert.AreEqual(true, container.ToString() == res);
        }

        [TestMethod]
        [DataRow(0, 0)]
        [DataRow(1, 0)]
        [DataRow(6, 0)]
        [DataRow(8, 0)]
        [DataRow(40, 0)]
        [DataRow(44, 0)]
        [DataRow(1, 1 - 1)]
        [DataRow(6, 6 - 1)]
        [DataRow(8, 8 - 1)]
        [DataRow(40, 40 - 1)]
        [DataRow(44, 44 - 1)]
        [DataRow(6, 6 / 2)]
        [DataRow(8, 8 / 2)]
        [DataRow(40, 40 / 2)]
        [DataRow(44, 44 / 2)]
        [DataRow(9, 0)]
        [DataRow(9, 9 - 1)]
        [DataRow(9, 9 / 2)]
        public void insert_test(int size, int index) {
            MyBitContainer container = new MyBitContainer();
            fill_container(ref container, size);
            container.insert(index, true);

            bool is_ok = true;
            for (int i = 0; i < index; ++i) {
                if (!(container[i] == ((i % 2 == 1) ? (true) : (false)))) {
                    is_ok = false;
                }
            }

            if (container[index] != true) {
                is_ok = false;
            }
            for (int i = index + 1; i < container.length(); ++i) {
                if (!(container[i] == ((i % 2 == 1) ? (false) : (true)))) {
                    is_ok = false;
                }
            }

            Assert.IsTrue(is_ok == true);
        }

        [TestMethod]
        [DataRow(1, 0)]
        [DataRow(6, 0)]
        [DataRow(8, 0)]
        [DataRow(40, 0)]
        [DataRow(44, 0)]
        [DataRow(1, 1 - 1)]
        [DataRow(6, 6 - 1)]
        [DataRow(8, 8 - 1)]
        [DataRow(40, 40 - 1)]
        [DataRow(44, 44 - 1)]
        [DataRow(6, 6 / 2)]
        [DataRow(8, 8 / 2)]
        [DataRow(40, 40 / 2)]
        [DataRow(44, 44 / 2)]
        [DataRow(9, 0)]
        [DataRow(9, 9 - 1)]
        [DataRow(9, 9 / 2)]
        public void remove_test(int size, int index) {
            MyBitContainer container = new MyBitContainer();
            fill_container(ref container, size);
            container.remove(index);

            bool is_ok = true;
            for (int i = 0; i < index; ++i) {
                if (!(container[i] == ((i % 2 == 1) ? (true) : (false)))) {
                    is_ok = false;
                }
            }
            for (int i = index; i < container.length(); ++i) {
                if (!(container[i] == ((i % 2 == 1) ? (false) : (true)))) {
                    is_ok = false;
                }
            }

            Assert.IsTrue(is_ok == true);
        }
    }
}
