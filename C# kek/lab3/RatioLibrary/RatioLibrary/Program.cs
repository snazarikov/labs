﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RatioLibrary
{
    public class RatioException : ArgumentException
    {
        public RatioException(string message) : base(message) {
            Console.WriteLine("Custom ArgumentException");
        }
    }

    public class Ratio
    {
        private int _numerator;
        private int _denominator;

        public Ratio(int numerator, int denominator) {
            if (denominator == 0) {
               throw new RatioException("Denominator is 0 in ratio: " + String.Format("{0}/{1}", numerator, denominator));
           }

           int divisor = gcd(denominator, numerator);
           _numerator = numerator / divisor;
           _denominator = denominator / divisor;
        }

        public int Numerator
        {
            get {
                return _numerator;
            }
        }

        public int Denominator
        {
            get {
                return _denominator;
            }
        }

        public static int gcd(int a, int b) {
            return (b == 0) ? (a) : (gcd(b, a % b));
        }

        public override string ToString() {
            // to construct
            if (_numerator > 0 && _denominator < 0) {
                _numerator *= -1;
                _denominator *= -1;
            }
            //
            return String.Format("{0}/{1}", _numerator, _denominator);
        }

        public double ToDouble() {
            return (double)_numerator / _denominator;
        }

        public static Ratio operator +(Ratio r) {
            return new Ratio(r._numerator, r._denominator);
        }

        public static Ratio operator -(Ratio r) {
            return new Ratio(-r._numerator, r._denominator);
        }

        public static Ratio operator +(Ratio r1, Ratio r2) {
            //
            int tmp = gcd(r1._denominator, r2._denominator);
            int tmp1 = r2._denominator / tmp;
            int tmp2 = r1._denominator / tmp;
			int tmp3 = (r1._denominator / tmp) * r2._denominator;
			
			int numerator = r1._numerator * tmp1 + r2._numerator * tmp2;
            int denominator = tmp3;
            // dodelat

			
            //int numerator = r1._numerator * r2._denominator + r2._numerator * r1._denominator;
            //int denominator = r1._denominator * r2._denominator;
            return new Ratio(numerator, denominator);
        }

        public static Ratio operator -(Ratio r1, Ratio r2) {
			int tmp = gcd(r1._denominator, r2._denominator);
            int tmp1 = r2._denominator / tmp;
            int tmp2 = r1._denominator / tmp;
			int tmp3 = (r1._denominator / tmp) * r2._denominator;
			
			int numerator = r1._numerator * tmp1 - r2._numerator * tmp2;
            int denominator = tmp3;
			
            //int numerator = r1._numerator * r2._denominator - r2._numerator * r1._denominator;
            //int denominator = r1._denominator * r2._denominator;
            return new Ratio(numerator, denominator);
        }

        public static Ratio operator *(Ratio r1, Ratio r2) {
            int divisor1 = gcd(r1._numerator, r2._denominator);
            int divisor2 = gcd(r2._numerator, r1._denominator);
            int numerator = (r1._numerator / divisor1) * (r2._numerator / divisor2);
            int denominator = (r1._denominator / divisor2) * (r2._denominator / divisor1);
            return new Ratio(numerator, denominator);
        }

        public static Ratio operator /(Ratio r1, Ratio r2) {
            int divisor1 = gcd(r1._numerator, r2._numerator);
            int divisor2 = gcd(r1._denominator, r2._denominator);
            int numerator = (r1._numerator / divisor1) * (r2._denominator / divisor2);
            int denominator = (r1._denominator / divisor2) * (r2._numerator / divisor1);
            return new Ratio(numerator, denominator);
        }
        
        //public static bool operator >(Ratio r1, Ratio r2) {
        //    return r1._numerator * r2._denominator > r2._numerator * r2._denominator;
        //}

        //public static bool operator <(Ratio r1, Ratio r2) {
        //    return r1._numerator * r2._denominator < r2._numerator * r2._denominator;
        //}

        //public static bool operator >=(Ratio r1, Ratio r2) {
        //    return r1._numerator * r2._denominator >= r2._numerator * r2._denominator;
        //}

        //public static bool operator <=(Ratio r1, Ratio r2) {
        //    return r1._numerator * r2._denominator <= r2._numerator * r2._denominator;
        //}
    }

}
