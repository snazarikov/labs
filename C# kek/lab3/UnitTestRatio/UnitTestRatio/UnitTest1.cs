using Microsoft.VisualStudio.TestTools.UnitTesting;
using RatioLibrary;

namespace UnitTestRatio
{
    [TestClass]
    public class RatioTest
    {
        [TestMethod]
        public void check_unary_plus() {
            Ratio r1 = new Ratio(3, 15);
            Ratio r2 = +r1;

            Assert.IsTrue((r1.Numerator == r2.Numerator && r1.Denominator == r2.Denominator), "Unary plus isn't okay");
        }

        [TestMethod]
        public void check_unary_minus() {
            Ratio r1 = new Ratio(1, 3);
            Ratio r2 = -r1;

            Assert.IsTrue(((r1.Numerator == -r2.Numerator && r1.Denominator == r2.Denominator) || (r1.Numerator == r2.Numerator && r1.Denominator == -r2.Denominator)), "Unary minus isn't okay");
        }

        [TestMethod]
        public void check_plus() {
            Ratio r1 = new Ratio(1, 2);
            Ratio r2 = new Ratio(1, 3);
            Ratio r3 = r1 + r2;
            Assert.IsTrue((r3.Numerator == 5 && r3.Denominator == 6), "Binary plus isn't okay");
        }

        [TestMethod]
        public void check_minus() {
            Ratio r1 = new Ratio(1, 2);
            Ratio r2 = new Ratio(1, 3);
            Ratio r3 = r1 - r2;
            Assert.IsTrue((r3.Numerator == 1 && r3.Denominator == 6), "Binary minus isn't okay");
        }

        [TestMethod]
        public void check_multiplication() {
            Ratio r1 = new Ratio(1, 2);
            Ratio r2 = new Ratio(1, 3);
            Ratio r3 = r1 * r2;
            Assert.IsTrue((r3.Numerator == 1 && r3.Denominator == 6), "Multiplication isn't okay");
        }

        [TestMethod]
        public void check_division() {
            Ratio r1 = new Ratio(1, 2);
            Ratio r2 = new Ratio(1, 3);
            Ratio r3 = r1 / r2;
            Assert.IsTrue((r3.Numerator == 3 && r3.Denominator == 2), "Division isn't okay");
        }

        [TestMethod]
        public void check_frac_reduce_3_over_15()
        {
            Ratio r = new Ratio(3, 15);
            int divisor = Ratio.gcd(3, 15);

            Assert.IsTrue((r.Numerator * divisor == 3 && r.Denominator * divisor == 15), "It should be like that: 3 / 15 --> 1 / 5");
        }

        [TestMethod]
        [ExpectedException(typeof(RatioException), "Ratio with 0 denominator isn't allowed.")]
        public void check_zero_denominator_constructor()
        {
            Ratio r = new Ratio(10, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(RatioException), "Ratio with 0 denominator isn't allowed.")]
        public void check_zero_division()
        {
            Ratio r1 = new Ratio(10, 5);
            Ratio r2 = new Ratio(0, 1234);
            Ratio r3 = r1 / r2;
        }
    }
}
