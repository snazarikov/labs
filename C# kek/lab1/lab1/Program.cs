﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NDesk.Options;

namespace check {
    class Program {
        static void Main(string[] args) {
            // keys
            int max_len = 0;
            bool text_instead_code = false;
            bool side_by_side = false;
            bool brief = false;
            //

            // key parser declaration
            OptionSet options = new OptionSet()
                .Add("l|len=", l => max_len = int.Parse(l))
                .Add("a|text", a => text_instead_code = a != null)
                .Add("y|side-by-side", y => side_by_side = y != null)
                .Add("q|brief", q => brief = q != null);
            //

            // parsing keys & args
            List<string> extra;
            try {
                extra = options.Parse(args);
            }
            catch (OptionException e) {
                Console.WriteLine(e.Message);
                Console.WriteLine("Smth crashed. Stupid user c:");
                return;
            }
            //

            // args
            string file1_name = extra[0];
            string file2_name = extra[1];
            //

            // files opening
            System.IO.FileStream file1 = null;
            System.IO.FileStream file2 = null;
            string path_to_files = @"D:\C# kek\lab1_fix\lab1\";

            try {
                file1 = new System.IO.FileStream(path_to_files + file1_name, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                file2 = new System.IO.FileStream(path_to_files + file2_name, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            }
            catch (System.IO.FileNotFoundException ex) {
                Console.WriteLine(ex.Message);
                Environment.Exit(0);
            }
            //
            
            bool new_difference = true; // signal of beginning of the new pack diff elems
            bool is_identical = true;
            bool is_EOF_printed = false; 

            int byte1 = 0;
            int byte2 = 0;

            int counter = 0; // offset
            int curr_len = 0; // curr len of difference

            // buffs if side by side is needed
            List<int> file1_data = new List<int>(); 
            List<int> file2_data = new List<int>();
            //

            // some str format consts
            string format_byte = "(0x{0:X})";
            string format_byte_symb = "({0})";
            string format_EOF1 = "<EOF>";
            string format_EOF2 = "(<EOF>)";
            //

            while (byte1 != -1 || byte2 != -1) {       
                if (byte1 != -1) byte1 = file1.ReadByte();
                if (byte2 != -1) byte2 = file2.ReadByte();

                if (byte1 != byte2) {
                    curr_len++;

                    if (is_identical) {
                        is_identical = false;
                        Console.WriteLine("Files differ");
                        if (brief) break;
                    }

                    if (side_by_side) { // saving elems
                        file1_data.Add(byte1);
                        file2_data.Add(byte2);
                        if (max_len > 0 && curr_len >= max_len) {
                            print_side_by_side(ref file1_data, ref file2_data, text_instead_code);
                            break;
                        }
                    }
                    
                    if (new_difference) {
                        new_difference = false;
                        Console.Write("0x{0:X8}: ", counter);
                    }

                    if (byte1 == -1) {
                        how_to_print(byte2, side_by_side, text_instead_code, format_byte, format_byte_symb, format_EOF1, ref is_EOF_printed);
                    }
                    else if (byte2 == -1) {
                        how_to_print(byte1, side_by_side, text_instead_code, format_byte, format_byte_symb, format_EOF2, ref is_EOF_printed);
                    }
                    else {
                        if (!side_by_side) {
                            if (!text_instead_code) Console.Write("0x{0:X}(0x{1:X}) ", byte1, byte2);
                            else Console.Write("{0}({1}) ", (char)byte1, (char)byte2);
                        }                        
                    }

                    if (max_len > 0 && curr_len >= max_len) break;
                }
                else if (!new_difference) {
                    if (side_by_side) {
                        print_side_by_side(ref file1_data, ref file2_data, text_instead_code);
                        file1_data.Clear();
                        file2_data.Clear();
                    }

                    curr_len = 0;
                    new_difference = true;
                    is_EOF_printed = false;

                    Console.WriteLine("");
                }

                counter++;
            }

            if (is_identical) Console.WriteLine("Files identical");
        }

        private static void how_to_print(int byte_, bool side_by_side, bool text_instead_code, string format_byte, string format_byte_symb, string format_EOF, ref bool is_EOF_printed) {
            if (!is_EOF_printed && !side_by_side) Console.Write(format_EOF);
            is_EOF_printed = true;
            if (!side_by_side) {
                if (!text_instead_code) Console.Write(format_byte + " ", byte_);
                else Console.Write(format_byte_symb + " ", (char)byte_);
            }
        }

        private static void print_side_by_side(ref List<int> l1, ref List<int> l2, bool is_text) {
            print_list(ref l1, is_text);
            Console.Write(" | ");
            print_list(ref l2, is_text);
        }

        private static void print_list(ref List<int> list, bool is_text) {
            for (int i = 0; i < list.Count; ++i) {
                if (list[i] == -1) {
                    Console.Write("<EOF>");
                    break;
                } 
                if (is_text) Console.Write("{0}", (char)list[i]);
                else Console.Write("0x{0:X} ", list[i]);
            }
        }
    }
}
