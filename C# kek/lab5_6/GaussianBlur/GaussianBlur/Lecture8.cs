﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Lecture8
{
	class MainClass {
		/*** Не освещённым осталось:
		 * Многопоточность (2 лекции)
		 * Рефлексия
		 * unsafe (небезопасный контекст) и указатели
		 * LINQ
		 * Сборка мусора в C#
		 */
		//Project Options -> Build/General -> Allow Unsafe code (для Monodevelop)
		//Project Properties -> build property -> Allow unsafe code (для Visual Studio)
		//включает параметр компилятора /unsafe
		public struct Point{
			public float x, y;
		};
		public struct StructWithRef {
			Object data;
		};
		//метод с unsafe-кодом
		public static unsafe float SumOfNumbers (float *ptr, int count) {
			float result = 0;
			for (int i = 0; i < count; i++)
				result += *(ptr + i); //здесь работает такая же адресная арифметика, как в C
			return result;
		}

		/* Пример unsafe-копирования элементов (на преобразование типов) */
		//вспомогтаельная функция, копирующая байты по указателю
		unsafe static void CopyByPtr(byte * ps , byte * pd , int count ) {
			//копируем по 8 байта
			//long* _pd = (long *)pd;
			//long* _ps = (long *)ps;
			for (int i = 0; i < count / 8; i++) {
				*((long*)pd + i) = *((long*)ps + i); //здесь выполняется преобразование типов указателей
				//_pd[i] = _ps[i];
			}
			//копируем остаток
			for (int i = 0; i < count % 8; i++) {
				*pd = *ps;
				ps++; pd++;
			}
		}
		unsafe static void FastCopy(byte[] src, byte[] dst, int count) {
			fixed (byte* source = src, dest = dst) {
				CopyByPtr(source, dest, count);
			}
		}

		//это будет работать для последних версий C#
		#if false
		unsafe static void GeneralizedCopy<T>(T[] src, T[] dst, int count) 
		where T: unmanaged {
			fixed (T * s = src) {
			}
		}
		#endif

		//Структура, содержащая закреплённый в памяти буфер фиксированного размера
		public unsafe struct FixedBuffer{
			//фиксированные буферы могут использоваться только в unsafe-контексте
			public fixed char buffer[128];
			int _from, _till;
		}

		//Функция прохода по пикселям изображения, используя unsafe
		public static void ProcessBitmap(Bitmap processedBitmap) {
			unsafe {
				BitmapData bitmapData = processedBitmap.LockBits(
					new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), 
					ImageLockMode.ReadWrite, processedBitmap.PixelFormat
				);
				int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
				int heightInPixels = bitmapData.Height;
				int widthInBytes = bitmapData.Width * bytesPerPixel;
				byte* ptrFirstPixel = (byte*)bitmapData.Scan0; //адрес данных первого пикселя

				for (int y = 0; y < heightInPixels; y++) {
					//stride -- это ширина шага, необходимого для перехода на новую строку изображения
					byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);
					for (int x = 0; x < widthInBytes; x = x + bytesPerPixel) {
						// calculate new pixel value
						currentLine[x] = 127;//(byte)oldBlue;
						currentLine[x + 1] = 127;//(byte)oldGreen;
						currentLine[x + 2] = 127;//(byte)oldRed;
					}
				}
				processedBitmap.UnlockBits(bitmapData);
			}
		}
		//заполнение изображения с помощью штатной SetPixel
		public static void ProcessBitmapSetPixel(Bitmap processedBitmap) {
			Color fillColor = Color.FromArgb(0x7F7F7F);
			for (int y = 0; y < processedBitmap.Height; y++)
				for (int x = 0; x < processedBitmap.Width; x++)
					processedBitmap.SetPixel(x, y, fillColor);
		}

		public static void Main(string[] args) {
			float[] a = new float[10] {0, 10, 20, 30, 40, 50, 60, 70, 80, 90};
			unsafe {
				#if false
				//объявление указателя на неизвестный тип
				void* unTypedPtr;
				//объявление указателя на заданный тип
				int *intPtr;
				//несколько указателей
				int* ptr1, ptr2, ptr3;
				/* Типом может быть: 
				 * Любой целочисленный, с плавающей точкой, char, bool, enum;
				 * Любой тип пользовательской струкуры, не содержащей управляемых типов (т.е. фактически, ссылочных)
				 * Любой другой тип указателя */
				void** voidPtr;
				//Object* objPtr; //ошибка компиляции
				//StructWithRef* sPtr; //аналогично (содержит ManagedType внутри)
				Point *pPtr = null; //а здесь нормально (инициализация указателя null)
				FixedBuffer *fb = null;
				FixedBuffer f = new FixedBuffer();
				fb = &f; //создание структуры с присвоением указателя на неё
				fb->buffer[0] = '\n'; //доступ к внутренним полям по указателю (как в C)
				//(*fb).buffer[0] = '\n';
				#endif

				#if false
				float res;
				//fixed закрепляет переменную, на которую указывает ссылка 
				//в памяти, не позволяя переноситься при сборке мусора
				fixed ( float* first = &a[0] ) { //первый вариант инициализации
					res = SumOfNumbers(first, a.Length);
				}
				Console.WriteLine(res);
				fixed (float* first = a) { //инициализация массивом (эквивалентно)
					res = SumOfNumbers(first, a.Length); //a.Length + 100
				}
				Console.WriteLine(res);
				//Что будет, если в качестве длины передать чуть больше, чем фактически выделено в массиве?
				//Код не упал, но прошёлся по какой-то памяти и даже посчитал (благо доступ по указателю только на чтение)
				#endif
			}
			#if false
			/* Код, копирующий элементы из одного массива в другой */
			byte[] first = new byte [10000000];
			byte[] second = new byte[10000000];
			Random r = new Random();
			for (int i=0; i<first.Length; i++)
				first[i] = (byte) (r.Next()%256);
			DateTime start = DateTime.Now;
			//FastCopy(first, second, first.Length); //Custom copy algorithm
			Array.Copy(first, second, first.Length); //Standart Array.Copy
			/*for (int i = 0; i < first.Length; i++)
				second[i] = first[i]; //Naive copying */
			TimeSpan elapsed = DateTime.Now - start;
			//сравним элементы после копирования
			Console.WriteLine("Elapsed = {0}\r\n{1}\r\n{2}", 
				elapsed.TotalMilliseconds, 
				first[first.Length/2], 
				second[first.Length/2]
			);
			#endif

			unsafe {
				//Ключевое слово Stackalloc используется для выделения памяти на стеке в небезопасном контексте
				//int[] ArrayOnStack = stackalloc int[]{10, 20, 30}; //так можно в более новом C#
				int* ArrayOnStack = stackalloc int[3];
				//массив будет освобождён, когда мы выйдем из тела метода
			}
			#if true
			Bitmap bt = new Bitmap(4096, 4*768, PixelFormat.Format24bppRgb);
			DateTime start = DateTime.Now;
			//ProcessBitmapSetPixel(bt);
			ProcessBitmap(bt);
			TimeSpan elapsed = DateTime.Now - start;
			Console.WriteLine("time = {0}", elapsed.TotalMilliseconds);
			bt.Save("out.png");
			#endif

			//ссылка про unmanaged
			//https://blogs.msdn.microsoft.com/seteplia/2018/06/12/dissecting-new-generics-constraints-in-c-7-3/
		}
	}
}
