﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;

namespace GaussianBlur {
    class Program {
        const string PATH_TO_FILE = "..\\..\\images\\";
        const string FILE_NAME = "temchik";
        const string FILE_FORMAT = ".jpg";
        const int r = 8;

        static void Main(string[] args) {
            Bitmap image = new Bitmap(PATH_TO_FILE + FILE_NAME + FILE_FORMAT);
            Console.WriteLine("Image size = {0}", image.Size);

            GaussianBlur test_filter = new GaussianBlur(r);
            Console.WriteLine("Filter size = {0}", test_filter.side_length);
            //test_filter.print_filter();

            var stopwatch = new Stopwatch();

            //stopwatch.Start();
            //Bitmap blured_naive = test_filter.apply_filter_naive(image);
            //stopwatch.Stop();
            //long time_naive = stopwatch.ElapsedMilliseconds;
            //stopwatch.Reset();
            //Console.WriteLine("time_naive = {0}", time_naive);
            //blured_naive.Save(PATH_TO_FILE + FILE_NAME + "_blured_naive_" + test_filter.side_length.ToString() + FILE_FORMAT);

            //stopwatch.Start();
            //Bitmap blured_unsafe = test_filter.apply_filter_unsafe(image);
            //stopwatch.Stop();
            //long time_unsafe = stopwatch.ElapsedMilliseconds;
            //stopwatch.Reset();
            //Console.WriteLine("time_unsafe = {0}", time_unsafe);
            //blured_unsafe.Save(PATH_TO_FILE + FILE_NAME + "_blured_unsafe_" + test_filter.side_length.ToString() + FILE_FORMAT);

            stopwatch.Start();
            Bitmap blured_unsafe_parallel = test_filter.apply_filter_unsafe_parallel(image);
            stopwatch.Stop();
            long time_unsafe_parallel = stopwatch.ElapsedMilliseconds;
            stopwatch.Reset();
            Console.WriteLine("time_unsafe_parallel = {0}", time_unsafe_parallel);
            blured_unsafe_parallel.Save(PATH_TO_FILE + FILE_NAME + "_blured_unsafe_parallel_" + test_filter.side_length.ToString() + FILE_FORMAT);

            //for (int y = 0; y < blured_naive.Height; y++) {
            //    for (int x = 0; x < blured_naive.Width; x++) {
            //        if (blured_naive.GetPixel(x, y) != blured_unsafe.GetPixel(x, y) || blured_naive.GetPixel(x, y) != blured_unsafe_parallel.GetPixel(x, y)) {
            //            Console.WriteLine("blured_naive, blured_unsafe, blured_unsafe_parallel are not equal");
            //            Environment.Exit(0);
            //        }
            //    }
            //}
            //Console.WriteLine("blured_naive, blured_unsafe, blured_unsafe_parallel are equal");

            //for (int y = 0; y < blured_unsafe.Height; y++) {
            //    for (int x = 0; x < blured_unsafe.Width; x++) {
            //        if (blured_unsafe_parallel.GetPixel(x, y) != blured_unsafe.GetPixel(x, y)) {
            //            Console.WriteLine("blured_naive, blured_unsafe, blured_unsafe_parallel are not equal");
            //            Environment.Exit(0);
            //        }
            //    }
            //}
            //Console.WriteLine("blured_unsafe, blured_unsafe_parallel are equal");
        }
    }

    abstract class SpatialFilter {
        protected List<List<double>> filter_;
        protected int side_len_;
        protected int r_;
        protected abstract void fill_filter();
        public abstract Bitmap apply_filter(Bitmap original);

        public int side_length {
            get {
                return side_len_;
            }
        }

        public void print_filter() {
            for (int i = 0; i < side_len_; ++i) {
                for (int j = 0; j < side_len_; ++j) {
                    Console.Write("{0} ", filter_[i][j]);
                }
                Console.WriteLine();
            }
        }

        public static void check_index(ref int index, int max_val, int min_val = 0) {
            if (index < min_val) {
                index = min_val;
            }
            else if (index > max_val) {
                index = max_val;
            }
        }
    }

    class GaussianBlur : SpatialFilter {
        private double sigma_;
        public GaussianBlur(int r) {
            r_ = r;
            side_len_ = 2 * r_ + 1;
            sigma_ = (double)r_ / 3;

            fill_filter();
        }

        protected override void fill_filter() {
            filter_ = new List<List<double>>();
            double tmp_sum = 0.0;
            for (int i = -r_; i <= r_; ++i) {
                filter_.Add(new List<double>());
                for (int j = -r_; j <= r_; ++j) {
                    filter_[i + r_].Add((1 / (2 * Math.PI * sigma_)) * Math.Exp(-(Math.Pow(i, 2) + Math.Pow(j, 2)) / (2 * sigma_)));
                    tmp_sum += filter_[i + r_][j + r_];
                }
            }

            for (int i = 0; i < side_len_; ++i) {
                for (int j = 0; j < side_len_; ++j) {
                    filter_[i][j] /= tmp_sum;
                }
            }
        }

        public override Bitmap apply_filter(Bitmap original) {
            return apply_filter_unsafe_parallel(original);
        }

        public Bitmap apply_filter_naive(Bitmap original) {
            Bitmap blured = new Bitmap(original);
            int original_height = original.Height;
            int original_width = original.Width;
            Color fillColor;
            for (int y = 0; y < original_height; y++) {
                for (int x = 0; x < original_width; x++) {
                    double new_R = 0.0;
                    double new_G = 0.0;
                    double new_B = 0.0;
                    for (int i = 0; i < side_len_; ++i) {
                        int index_y = y - r_ + i;
                        check_index(ref index_y, original_height - 1);
                        for (int j = 0; j < side_len_; ++j) {
                            int index_x = x - r_ + j;
                            check_index(ref index_x, original_width - 1);

                            new_R += original.GetPixel(index_x, index_y).R * filter_[i][j];
                            new_G += original.GetPixel(index_x, index_y).G * filter_[i][j];
                            new_B += original.GetPixel(index_x, index_y).B * filter_[i][j];
                        }
                    }
                    new_R = (new_R > 255) ? (255) : (new_R);
                    new_G = (new_G > 255) ? (255) : (new_G);
                    new_B = (new_B > 255) ? (255) : (new_B);
                    fillColor = Color.FromArgb((int)new_R, (int)new_G, (int)new_B);
                    blured.SetPixel(x, y, fillColor);
                }
            }
            return blured;
        }

        unsafe public Bitmap apply_filter_unsafe(Bitmap original) {
            Bitmap blured = new Bitmap(original);
            unsafe {
                //BitmapData bitmap_data_original = original.LockBits(new Rectangle(0, 0, original.Width, original.Height), ImageLockMode.ReadOnly, original.PixelFormat);
                //BitmapData bitmap_data_blured = blured.LockBits(new Rectangle(0, 0, blured.Width, blured.Height), ImageLockMode.ReadWrite, blured.PixelFormat);
                BitmapData bitmap_data_original = original.LockBits(new Rectangle(0, 0, original.Width, original.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                BitmapData bitmap_data_blured = blured.LockBits(new Rectangle(0, 0, blured.Width, blured.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                int bytes_per_pixel = System.Drawing.Bitmap.GetPixelFormatSize(original.PixelFormat) / 8;
                int height_in_pixels = bitmap_data_original.Height;
                int width_in_bytes = bitmap_data_original.Width * bytes_per_pixel;

                byte* ptr_first_pixel_original = (byte*)bitmap_data_original.Scan0; //адрес данных первого пикселя bitmap_data_original
                byte* ptr_first_pixel_blured = (byte*)bitmap_data_blured.Scan0; //адрес данных первого пикселя bitmap_data_blured
                
                uint addr_original = (uint)ptr_first_pixel_original;
                uint addr_blured = (uint)ptr_first_pixel_blured;
                //Console.WriteLine("Address bitmap_data_original = {0}", addr_original);
                //Console.WriteLine("Address bitmap_data_original = {0}", addr_blured);

                int blured_stride = bitmap_data_blured.Stride;
                int original_stride = bitmap_data_original.Stride;

                int original_height = original.Height;
                int original_width = original.Width;

                for (int y = 0; y < height_in_pixels; y++) {
                    byte* current_line_blured = ptr_first_pixel_blured + (y * blured_stride);
                    for (int x = 0; x < width_in_bytes; x += bytes_per_pixel) {
                        double new_R = 0.0;
                        double new_G = 0.0;
                        double new_B = 0.0;
                        for (int i = 0; i < side_len_; ++i) {
                            int index_y = y - r_ + i;
                            check_index(ref index_y, original_height - 1);
                            byte* current_line_original = ptr_first_pixel_original + (index_y * original_stride);
                            for (int j = 0; j < side_len_; ++j) {
                                int index_x = x - r_ * bytes_per_pixel + j * bytes_per_pixel;

                                check_index(ref index_x, original_width * bytes_per_pixel - 3);

                                new_R += current_line_original[index_x + 2] * filter_[i][j];
                                new_G += current_line_original[index_x + 1] * filter_[i][j];
                                new_B += current_line_original[index_x] * filter_[i][j];
                            }
                        }
                        new_R = (new_R > 255) ? (255) : (new_R);
                        new_G = (new_G > 255) ? (255) : (new_G);
                        new_B = (new_B > 255) ? (255) : (new_B);

                        //calculate new pixel value
                        current_line_blured[x] = (byte)new_B; //(byte)oldBlue;
                        current_line_blured[x + 1] = (byte)new_G; //(byte)oldGreen;
                        current_line_blured[x + 2] = (byte)new_R; //(byte)oldRed;
                    }
                }
                original.UnlockBits(bitmap_data_original);
                blured.UnlockBits(bitmap_data_blured);
            }
            return blured;
        }

        unsafe public Bitmap apply_filter_unsafe_parallel(Bitmap original) {
            Bitmap blured = new Bitmap(original);
            unsafe {
                //BitmapData bitmap_data_original = original.LockBits(new Rectangle(0, 0, original.Width, original.Height), ImageLockMode.ReadOnly, original.PixelFormat);
                //BitmapData bitmap_data_blured = blured.LockBits(new Rectangle(0, 0, blured.Width, blured.Height), ImageLockMode.ReadWrite, blured.PixelFormat);
                BitmapData bitmap_data_original = original.LockBits(new Rectangle(0, 0, original.Width, original.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                BitmapData bitmap_data_blured = blured.LockBits(new Rectangle(0, 0, blured.Width, blured.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                int bytes_per_pixel = System.Drawing.Bitmap.GetPixelFormatSize(original.PixelFormat) / 8;
                int height_in_pixels = bitmap_data_original.Height;
                int width_in_bytes = bitmap_data_original.Width * bytes_per_pixel;

                byte* ptr_first_pixel_original = (byte*)bitmap_data_original.Scan0; //адрес данных первого пикселя bitmap_data_original
                byte* ptr_first_pixel_blured = (byte*)bitmap_data_blured.Scan0; //адрес данных первого пикселя bitmap_data_blured

                uint addr_original = (uint)ptr_first_pixel_original;
                uint addr_blured = (uint)ptr_first_pixel_blured;
                //Console.WriteLine("Address bitmap_data_original = {0}", addr_original);
                //Console.WriteLine("Address bitmap_data_original = {0}", addr_blured);

                int blured_stride = bitmap_data_blured.Stride;
                int original_stride = bitmap_data_original.Stride;

                int original_height = original.Height;
                int original_width = original.Width;

                Parallel.For(0, height_in_pixels,
                    (int y) => {
                        //Console.WriteLine("Выполняется задача {0}", Task.CurrentId);
                        byte* current_line_blured = ptr_first_pixel_blured + (y * blured_stride);
                        for (int x = 0; x < width_in_bytes; x += bytes_per_pixel) {
                            double new_R = 0.0;
                            double new_G = 0.0;
                            double new_B = 0.0;
                            for (int i = 0; i < side_len_; ++i) {
                                int index_y = y - r_ + i;
                                check_index(ref index_y, original_height - 1);
                                byte* current_line_original = ptr_first_pixel_original + (index_y * original_stride);
                                for (int j = 0; j < side_len_; ++j) {
                                    int index_x = x - r_ * bytes_per_pixel + j * bytes_per_pixel;

                                    check_index(ref index_x, original_width * bytes_per_pixel - 3);

                                    new_R += current_line_original[index_x + 2] * filter_[i][j];
                                    new_G += current_line_original[index_x + 1] * filter_[i][j];
                                    new_B += current_line_original[index_x] * filter_[i][j];
                                }
                            }
                            new_R = (new_R > 255) ? (255) : (new_R);
                            new_G = (new_G > 255) ? (255) : (new_G);
                            new_B = (new_B > 255) ? (255) : (new_B);

                            //calculate new pixel value
                            current_line_blured[x] = (byte)new_B; //(byte)oldBlue;
                            current_line_blured[x + 1] = (byte)new_G; //(byte)oldGreen;
                            current_line_blured[x + 2] = (byte)new_R; //(byte)oldRed;
                        }
                    });
                original.UnlockBits(bitmap_data_original);
                blured.UnlockBits(bitmap_data_blured);
            }
            return blured;
        }
    }
}
