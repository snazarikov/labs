﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeClass {
	abstract class Shape {
		public abstract void print_parameters();

		public abstract double calc_area();
		public abstract double calc_perimeter();
		public abstract Tuple<double, double> calc_center_mass();

		public virtual void print_info() {
			Tuple<double, double> center_mass = calc_center_mass();

			print_parameters();
			Console.WriteLine("Area: {0}", calc_area());
			Console.WriteLine("Perimiter: {0}", calc_perimeter());
			Console.WriteLine("Center mass: x = {0} y = {0}", center_mass.Item1, center_mass.Item1);
			Console.WriteLine();
		}
	}
}