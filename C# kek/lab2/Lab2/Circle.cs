﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeClass {
	class Circle : Shape {
		private Tuple<double, double> _center;
		private double _radius;

		public Circle(Tuple<double, double> center, double radius) {
			_center = center;
			_radius = radius;
		}

		public override double calc_area() {
			return Math.PI * _radius * _radius;
		}

		public override double calc_perimeter() {
			return 2.0 * Math.PI * _radius;
		}

		public override Tuple<double, double> calc_center_mass() {
			return _center;
		}
        // to string
		public override void print_parameters() {
			Console.WriteLine("Circle");
			Console.WriteLine(_center.Item1 + " " + _center.Item2 + " " + _radius);
		}
	}
}