﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeClass {
	class Ellipse : Shape {
		private Tuple<double, double> _focus1;
		private Tuple<double, double> _focus2;
		private double _a;
		private double _b;

		public Ellipse(Tuple<double, double> focus1, Tuple<double, double> focus2, double a) {
			_focus1 = focus1;
			_focus2 = focus2;
			_a = a;
			double eccentricity = dist_btw_points(_focus1, _focus2) / (2 * _a);
			_b = _a * Math.Sqrt(1 - Math.Pow(eccentricity, 2));
		}

		private double dist_btw_points(Tuple<double, double> p1, Tuple<double, double> p2) {
			return Math.Sqrt(Math.Pow(p1.Item1 - p2.Item1, 2) + Math.Pow(p1.Item2 - p2.Item2, 2));
		}

		public override double calc_area() {
			return Math.PI * _a * _b;
		}

		public override double calc_perimeter() {
			return 4 * (Math.PI * _a * _b + Math.Pow(_a - _b, 2)) / (_a + _b);
		}

		public override Tuple<double, double> calc_center_mass() {
			double x1 = _focus1.Item1;
			double x2 = _focus2.Item1;
			double y1 = _focus1.Item2;
			double y2 = _focus2.Item2;

			double centerX = (x1 + x2) / 2;
			double centerY = ((y2 - y1) / (x2 - x1)) * centerX + ((x2 * y1 - x1 * y2) / (x2 - x1));

			return Tuple.Create(centerX, centerY);
		}

		public override void print_parameters() {
			Console.WriteLine("Ellipse");
			Console.WriteLine(_focus1.Item1 + " " + _focus1.Item2 + " " + _focus2.Item1 + " " + _focus2.Item2 + " " + _a);
		}
	}
}