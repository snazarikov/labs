﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ShapeClass;

namespace Lab2 {

    class Program {
        const double BAD_PARSE = -1e9;

        public static void Main(string[] args) {
            // Test
            var shapes = new List<Shape> {
                new Polygon(new List<Tuple<double, double>> {Tuple.Create(-1.0, -1.0), Tuple.Create(-1.0, 1.0), Tuple.Create(1.0, 1.0), Tuple.Create(1.0, -1.0)}, 4),
                new Polygon(new List<Tuple<double, double>> {Tuple.Create(-2.0, -2.0), Tuple.Create(-2.0, 2.0), Tuple.Create(2.0, 2.0), Tuple.Create(2.0, -2.0)}, 4),
                new Ellipse(Tuple.Create(-0.5, 0.0), Tuple.Create(0.5, 0.0), 1.0),
                new Circle(Tuple.Create(0.0, 0.0), 1.0)
            };
            //

            List<CommandHandler> main_menu_handlers = new List<CommandHandler> {info_handler, help_handler, creation_handler};
            List<string> main_menu_commands = new List<string> { "L", "H", "C" };
            SortedDictionary<string, CommandHandler> main_menu_map = new SortedDictionary<string, CommandHandler>();
            create_map(ref main_menu_map, main_menu_commands, main_menu_handlers);

            List<CommandHandler> creation_menu_handlers = new List<CommandHandler> {info_handler, create_circle_handler, create_ellipse_handler, create_polygon_handler};
            List<string> creation_menu_commands = new List<string> {"L", "C", "E", "P"};
            SortedDictionary<string, CommandHandler> creation_menu_map = new SortedDictionary<string, CommandHandler>();
            create_map(ref creation_menu_map, creation_menu_commands, creation_menu_handlers);
            
            int flag = 1;
            help_handler(shapes);
            while (flag > 0) {
                if (flag == 1) {
                    menu_logic(main_menu_map, shapes, ref flag);
                }
                else
                {
                    menu_logic(creation_menu_map, shapes, ref flag);
                }
            }
        }

        private static void menu_logic(SortedDictionary<string, CommandHandler> map, List<Shape> figures, ref int flag) {
            string command;
            while ((command = Console.ReadLine()) != "Q") {
                if (map.ContainsKey(command)) {
                    if (flag == 1 && command == "C") {
                        flag++;
                        return;
                    }
                    map[command](figures);
                }
                else {
                    Console.WriteLine("You typed wrong command");
                    Console.WriteLine("Try again pls");
                }
            }
            flag--;
        }

        private static void create_map(ref SortedDictionary<string, CommandHandler> map, List<string> keys, List<CommandHandler> handlers) {
            for (int i = 0; i < handlers.Count; ++i) {
                map.Add(keys[i], handlers[i]);
            }
        }

        delegate void CommandHandler(List<Shape> figures);

        private static void create_circle_handler(List<Shape> figures) {
            Console.WriteLine("Create circle");
            string[] nums_strings = Console.ReadLine().Split();
            double[] nums = new double[nums_strings.Length];

            for (int i = 0; i < nums_strings.Length; i++) {
                nums[i] = magic_parse_double(nums_strings[i]);
            }

            figures.Add(new Circle(Tuple.Create(nums[0], nums[1]), nums[2]));
        }

        private static void create_ellipse_handler(List<Shape> figures) {
            Console.WriteLine("Create ellipse");
            string[] nums_strings = Console.ReadLine().Split();
            double[] nums = new double[nums_strings.Length];

            for (int i = 0; i < nums_strings.Length; i++) {
                //nums_strings[i].Replace("-", "-");
                //Console.WriteLine(nums_strings[i]);
                //nums[i] = Convert.ToDouble(nums_strings[i]);
                //nums[i] = double.Parse(nums_strings[i]);
                nums[i] = magic_parse_double(nums_strings[i]);
            }

            figures.Add(new Ellipse(Tuple.Create(nums[0], nums[1]), Tuple.Create(nums[2], nums[3]), nums[4]));
        }

        private static void create_polygon_handler(List<Shape> figures) {
            Console.WriteLine("Create polygon");
            string[] nums_strings = Console.ReadLine().Split();
            double[] nums = new double[nums_strings.Length];

            for (int i = 0; i < nums_strings.Length; i++) {
                nums[i] = magic_parse_double(nums_strings[i]);

            }

            List<Tuple<double, double>> polygon = new List<Tuple<double, double>>();
            for (int i = 1; i < nums.Length; i += 2) {
                polygon.Add(Tuple.Create(nums[i], nums[i + 1]));
            }

            figures.Add(new Polygon(polygon, (int)nums[0]));
        }

        private static void info_handler(List<Shape> figures) {
            foreach (var shape in figures) {
                shape.print_info();
            }
        }

        private static CommandHandler help_handler = delegate {
            Console.WriteLine("Basically you have 4 commands:");

            Console.WriteLine();
            Console.WriteLine("\tC - create new shape:");
            Console.WriteLine("\tYou're able to create 3 types of object:");

            Console.WriteLine();
            Console.WriteLine("\t\tE - create new Ellipse");
            Console.WriteLine("\t\tExample:");
            Console.WriteLine("\t\tE");
            Console.WriteLine("\t\tE focus1.x focus1.y focus2.x focus2.y a");
            Console.WriteLine("\t\tE");
            Console.WriteLine("\t\tE -0.5 0.0 0.5 0.0 1.0");

            Console.WriteLine();
            Console.WriteLine("\t\tC - create new Circle");
            Console.WriteLine("\t\tExample:");
            Console.WriteLine("\t\tC");
            Console.WriteLine("\t\tC center.x center.y radius");
            Console.WriteLine("\t\tC");
            Console.WriteLine("\t\tC 0.0 0.0 1.0");

            Console.WriteLine();
            Console.WriteLine("\t\tP - create new Polygon");
            Console.WriteLine("\t\tExample:");
            Console.WriteLine("\t\tP");
            Console.WriteLine("\t\tP points_amount point1.x point1.y ... pointN.x pointN.y");
            Console.WriteLine("\t\tP");
            Console.WriteLine("\t\tP 4 -1.0 -1.0 -1.0, 1.0 1.0, 1.0 1.0, -1.0");

            Console.WriteLine();
            Console.WriteLine("\t\tQ - quit from main menu or porgramm");


            Console.WriteLine();
            Console.WriteLine("\tL - print all characteristics of shapes");

            Console.WriteLine();
            Console.WriteLine("\tH - print help");

            Console.WriteLine();
            Console.WriteLine("\tQ - quit");
        };

        private static CommandHandler creation_handler = delegate {
            return;
        };

        public static double magic_parse_double(string value) {
            double result;

            //Try parsing in the current culture
            if (!double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.CurrentCulture, out result) &&
                //Then try in US english
                !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out result) &&
                //Then in neutral language
                !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                result = BAD_PARSE;
            }

            return result;
        }
    }
}
