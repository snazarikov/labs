﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeClass {
	class Polygon : Shape {
		private List<Tuple<double, double>> _point;
		private int _point_amount;

		public Polygon(List<Tuple<double, double>> point, int point_amount) {
			_point = new List<Tuple<double, double>> (point);
			_point_amount = point_amount;
		}

		private double dist_btw_points(Tuple<double, double> p1, Tuple<double, double> p2) {
			return Math.Sqrt(Math.Pow(p1.Item1 - p2.Item1, 2) + Math.Pow(p1.Item2 - p2.Item2, 2));
		}

		public override double calc_area() {
			double area = 0.0;
			for (int i = 1; i < _point.Count + 1; ++i) {
				area += (_point[i - 1].Item2 + _point[i % _point.Count].Item2) * (_point[i - 1].Item1 - _point[i % _point.Count].Item1);
			}

			area = Math.Abs(area) / 2;

			return area;
		}

		public override double calc_perimeter() {
			double perimeter = 0.0;
			for (int i = 1; i < _point.Count + 1; ++i) {
				perimeter += dist_btw_points(_point[i - 1], _point[i % _point.Count]);
			}
			return perimeter;
		}

		public override Tuple<double, double> calc_center_mass() {
			double centerX = 0.0;
			double centerY = 0.0;
			double perimeter = 0.0;

			double dist;
			for (int i = 1; i < _point.Count + 1; ++i) {
				dist = dist_btw_points(_point[i - 1], _point[i % _point.Count]);
				perimeter += dist;
				centerX += dist * (_point[i - 1].Item1 + _point[i % _point.Count].Item1) / 2;
				centerY += dist * (_point[i - 1].Item2 + _point[i % _point.Count].Item2) / 2;
			}

			centerX /= perimeter;
			centerY /= perimeter;

			return Tuple.Create(centerX, centerY);
		}

		public override void print_parameters() {
			Console.WriteLine("Polygon");
			Console.Write(_point_amount + " ");
			for (int i = 0; i < _point.Count; ++i) {
				Console.Write(_point[i].Item1 + " " + _point[i].Item2 + " ");
			}
			Console.WriteLine();
		}
	}
}