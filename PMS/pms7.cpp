﻿// lab7.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <chrono>

using namespace std;

int n, threads;

void ProcessInitialization(int *&pMatrixA, int *&pMatrixB, int Size) {
	pMatrixA = new int[Size * Size];
	pMatrixB = new int[Size * Size];

	for (int i = 0; i < Size; i++) {    /// заолняем рандомные веса матрицы
		for (int j = 0; j < Size; j++) {
			pMatrixA[i, j] = rand() % 200;
			pMatrixB[i, j] = pMatrixA[i, j];
		}
	}
	
	for (int i = 0; i < Size*Size / 5; i++) {	/// делаем граф чуть менее плотным
		int tmp1 = rand() % Size;
		int tmp2 = rand() % Size;

		pMatrixA[tmp1, tmp2] = -1;
		pMatrixB[tmp1, tmp2] = -1;
	}

	for (int i = 0; i < Size; i++) {	/// путь из вершины в саму себя равен 0
		for (int j = 0; j < Size; j++) {
			if (i == j) {
				pMatrixA[i, j] = 0;
				pMatrixB[i, j] = 0;
			}
		}
	}
	
	
}

int min(int a, int b) {    /// ф-я поиска минимума
	return (a < b) ? a : b;
}

void ParallelFloyd(int *pMatrix, int Size, int _threads) {

	for (int k = 0; k < Size; k++) {

		int block = Size / _threads;	/// определяем размер блока

		omp_set_num_threads(_threads);	/// задаем количество потоков

#pragma omp parallel for schedule(static, block)	/// выполняем параллельно рассылая потокам блоки шириной block
		for (int i = 0; i < Size; i++)
		{
			for (int j = 0; j < Size; j++)
			{
				if ((pMatrix[i, k] != -1) && (pMatrix[k, j] != -1))
				{
					pMatrix[i, j] = min(pMatrix[i, j], pMatrix[i, k] + pMatrix[k, j]);
				}
			}
		}
	}
}

void SequentalFloyd(int *pMatrix, int n) {
	for (int k = 0; k < n; k++)
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				if ((pMatrix[i, k] != -1) && (pMatrix[k, j] != -1))
				{
					pMatrix[i, j] = min(pMatrix[i, j], pMatrix[i, k] + pMatrix[k, j]);
				}
			}
		}
	}
}


int main()
{
	srand(time(NULL));

	int *pMatrixSequental; 
	int *pMatrixParallel;

	std::chrono::time_point<std::chrono::system_clock> start, end;


	int thr[3] = { 2,4 };
	int nnn[4] = { 100,500,700, 1000 };

	for (int i = 0; i < 4; i++) {

		ProcessInitialization(pMatrixSequental, pMatrixParallel, nnn[i]);

		cout <<endl<< nnn[i] << endl;

		start = std::chrono::system_clock::now();
		SequentalFloyd(pMatrixSequental, nnn[i] );
		end = std::chrono::system_clock::now();
		int elapsed_seconds1 = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
		

		for (int j = 0; j < 2; j++) {

			start = std::chrono::system_clock::now();
			ParallelFloyd(pMatrixParallel, nnn[i], thr[j]);
			end = std::chrono::system_clock::now();
			int elapsed_seconds2 = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
			cout << endl << "Threads: " << thr[j] ;
			cout << endl << "Time spend sequental: " << elapsed_seconds1 << " ms";
			cout << endl << "Time spend parallel: " << elapsed_seconds2 << " ms";
			cout << endl << "Time coefficient: " << (double)elapsed_seconds1 / (double)elapsed_seconds2 << "\n";
		}
	}

	

	return 0;
}
