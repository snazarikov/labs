#include <cstdio>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <chrono>
#include <cmath>
#include <string>

using namespace std;

const int size_ = 10;

void alloc_matrix(vector<vector<int> >& matrix, int n) {
	matrix.resize(n);
	for (int i = 0; i < n; ++i) {
		matrix[i].resize(n);
	}
}

void generate_matrix(vector<vector<int> >& matrix, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			matrix[i][j] = rand() % 5;
		}
	}
}

void memset_zero(vector<vector<int> >& matrix, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			matrix[i][j] = 0;
		}
	}
}

void print_matrix(vector<vector<int> >& matrix, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void matmul(vector<vector<int> > matrix1, vector<vector<int> > matrix2, vector<vector<int> >& res, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			int tmp = 0;
			for (int k = 0; k < n; ++k) {
				tmp += matrix1[i][k] * matrix2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

void matmul_opt1(vector<vector<int> > matrix1, vector<vector<int> > matrix2, vector<vector<int> >& res, int n) {
	for (int j = 0; j < n; ++j) {
		for (int i = 0; i < n; ++i) {
			int tmp = 0;
			for (int k = 0; k < n; ++k) {
				tmp += matrix1[i][k] * matrix2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

void matmul_opt_habr1(vector<vector<int> > matrix1, vector<vector<int> > matrix2, vector<vector<int> >& res, int n) {
	for (int i = 0; i < n; ++i) {
		for (int k = 0; k < n; ++k) {
			for (int j = 0; j < n; ++j) {
				res[i][j] += matrix1[i][k] * matrix2[k][j];
			}
		}
	}
}

void matmul_opt2(vector<vector<int> > matrix1, vector<vector<int> > matrix2, vector<vector<int> >& res, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			res[i][j] = 0;
			for (int k = 0; k < n; ++k) {
				res[i][j] += matrix1[i][k] * matrix2[k][j];
			}
		}
	}
}

void matmul_opt5(vector<vector<int> > matrix1, vector<vector<int> > matrix2, vector<vector<int> >& res, int n) {
	for (int i = 0; i < n; i += 2) {
		for (int j = 0; j < n; ++j) {
			int tmp1 = 0;
			int tmp2 = 0;
			for (int k = 0; k < n; ++k) {
				tmp1 += matrix1[i][k] * matrix2[k][j];
				if (i + 1 < n) {
					tmp2 += matrix1[i + 1][k] * matrix2[k][j];
				}
			}
			res[i][j] = tmp1;
			if (i + 1 < n) {
				res[i + 1][j] = tmp2;
			}
		}
	}
}

void matmul_opt6(vector<vector<int> > matrix1, vector<vector<int> > matrix2, vector<vector<int> >& res, int n) {
	for (int i = 0; i < n; i += 2) {
		for (int j = 0; j < n; ++j) {
			for (int k = 0; k < n; ++k) {
				for (int ii = i; ii <= i + 1 && ii < n; ++ii) {
					res[ii][j] += matrix1[ii][k] * matrix2[k][j];
				}
			}
		}
	}
}

double calc_matrix_norm(vector<vector<int> >& matrix, int n) {
	long long norm = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			norm += (long long)matrix[i][j] * (long long)matrix[i][j];
		}
	}
	return sqrt(norm);
}

void elapse_method(vector<vector<int> >& matrix, vector<vector<int> >& res, int n, string method_type, void (*matmul_method)(vector<vector<int> >, vector<vector<int> >, vector<vector<int> >&, int)) {
    memset_zero(res, n);

	std::chrono::time_point<std::chrono::system_clock> start, finish;
	start = std::chrono::system_clock::now();

	matmul_method(matrix, matrix, res, n);
	matmul_method(res, matrix, res, n);
	long long norm = calc_matrix_norm(res, n);

	finish = std::chrono::system_clock::now();
	int elapsed_secs = std::chrono::duration_cast<std::chrono::milliseconds> (finish - start).count();
	cout << method_type << endl;
	cout << elapsed_secs << "ms" << endl;
	cout << "norm: " << norm << endl;
	cout << endl;
}

int main() {
	srand(time(0));

	vector<vector<int> > matrix;
	vector<vector<int> > res;

	alloc_matrix(matrix, size_);
	alloc_matrix(res, size_);

	generate_matrix(matrix, size_);
//	print_matrix(matrix, size_);

	elapse_method(matrix, res, size_, "default", matmul);
	elapse_method(matrix, res, size_, "opt1", matmul_opt1);
	elapse_method(matrix, res, size_, "opt2", matmul_opt2);
	elapse_method(matrix, res, size_, "opt5", matmul_opt5);
	elapse_method(matrix, res, size_, "opt6", matmul_opt6);
	elapse_method(matrix, res, size_, "opt_habr1", matmul_opt_habr1);

//	print_matrix(res, size_);

	return 0; 
}
