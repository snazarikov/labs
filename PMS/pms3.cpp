// lab3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <omp.h>
#include <chrono>
#include <vector>

using namespace std;

const int NUM_THREAD = 4;
const int NMAX = 5e3 + 5;

int A[NMAX][NMAX];
int B[NMAX][NMAX];
int x[NMAX];

int y[NMAX];
int z[NMAX];
double norma = 0;

vector<double> times_seq;
vector<double> times_par;
vector<double> speed_up;

void generate_matrix(int(&matrix)[NMAX][NMAX], int max_value = 1) {
	for (int i = 0; i < NMAX; ++i) {
		for (int j = 0; j < NMAX; ++j) {
			matrix[i][j] = rand() % (max_value + 1);
		}
	}
}

void generate_vector(int* vec, int max_value = 3) {
	for (int i = 0; i < NMAX; ++i) {
		vec[i] = rand() % (max_value + 1);
	}
}

void print_matrix(int(&matrix)[NMAX][NMAX], int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void print_vector(int* vec, int n) {
	for (int i = 0; i < n; ++i) {
		cout << vec[i] << " ";
	}
	cout << endl << endl;
}

void matrix_vec_mult_parallel(int(&matrix)[NMAX][NMAX], int* vec, int* res, int n) {
	omp_set_num_threads(NUM_THREAD);
	#pragma omp parallel for /*num_threads(8)*/
	for (int i = 0; i < n; ++i) {
		res[i] = 0;
		for (int j = 0; j < n; ++j) {
			res[i] += matrix[i][j] * vec[j];
		}
	}
}

void matrix_vec_mult_for(int(&matrix)[NMAX][NMAX], int* vec, int* res, int n) {
	omp_set_num_threads(NUM_THREAD);
#pragma omp parallel for /*num_threads(8)*/
	for (int i = 0; i < n; ++i) {
		res[i] = 0;
#pragma omp parallel for
		for (int j = 0; j < n; ++j) {
			res[i] += matrix[i][j] * vec[j];
		}
	}
}

void matrix_vec_mult(int(&matrix)[NMAX][NMAX], int* vec, int* res, int n) {
	for (int i = 0; i < n; ++i) {
		res[i] = 0;
		for (int j = 0; j < n; ++j) {
			res[i] += matrix[i][j] * vec[j];
		}
	}
}

double calc_vec_norm_for(int* vec, int n) {
	double res = 0;
	omp_set_num_threads(NUM_THREAD);
#pragma omp parallel for
	for (int i = 0; i < n; ++i) {
#pragma omp critical
		res += vec[i] * vec[i];
	}
	return sqrt(res);
}

double calc_vec_norm(int* vec, int n) {
	double res = 0;
	for (int i = 0; i < n; ++i) {
		res += vec[i] * vec[i];
	}
	return sqrt(res);
}

int main(int argc, char **argv)
{
	//int test(999);

	//omp_set_num_threads(4);
	//#pragma omp parallel reduction(+:test)
	//{
	//	#pragma omp critical
	//	cout << "Test = " << test << endl;
	//}

	srand(time(0));

	int n = 4 * 1e3;
	cout << "n = " << n << endl;

	int repeat_amount = 5;
	for (int i = 0; i < repeat_amount; ++i) {
		cout << "i = " << i << endl;

		generate_matrix(A);
		generate_matrix(B);
		generate_vector(x);

		//print_matrix(A, n);
		//print_matrix(B, n);
		//print_vector(x, n);

		auto start = chrono::steady_clock::now();
		matrix_vec_mult(B, x, y, n);
		matrix_vec_mult(A, y, z, n);
		norma = calc_vec_norm(z, n);
		auto end = chrono::steady_clock::now();
		auto seq_time = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
		//cout << "Elapsed time in milliseconds : " << seq_time << " ms" << endl;

		//print_vector(y, n);
		//print_vector(z, n);
		cout << "Norma: " << norma << endl;

		start = chrono::steady_clock::now();
		matrix_vec_mult_for(B, x, y, n);
		matrix_vec_mult_for(A, y, z, n);
		norma = calc_vec_norm_for(z, n);
		end = chrono::steady_clock::now();
		auto par_time = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
		//cout << "Elapsed time in milliseconds : " << par_time << " ms" << endl;

		//print_vector(y, n);
		//print_vector(z, n);
		cout << "Norma: " << norma << endl;

		double speed = (double)seq_time / (double)par_time;

		cout << "Efficiency: " << speed << endl;

		times_seq.push_back(seq_time);
		times_par.push_back(par_time);
		speed_up.push_back(speed);
	}

	double tmp = 0.0;
	for (int i = 0; i < repeat_amount; ++i) {
		tmp += speed_up[i];
	}
	cout << " Avg efficiency: " << tmp / speed_up.size() << endl;

	return 0;
}

