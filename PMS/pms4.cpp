// lab4fix.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <cmath>
#include <algorithm>
#include <chrono>
#include <omp.h>
#include <mutex>
#include <condition_variable>
#include <iomanip>
#include <random>

using namespace std;

#define DEBUG 0

// Consts
double TAU = 0.25; // 0.25
const double EPS = 1e-3;
const int PRINT_EVERY = 5000;
const int IT_AMOUNT = 20000;
//

// Data
int n;
int size_;
double* solutionThread;
double* solutionOMP;
double* solutionSequential;
double* buffer;
double* rightPart;
//

double F(double x, double y) {
	return -6 * (x * x * (x - 3) * (y - 1) + y * y * (y - 3) * (x - 1));
}

// Calculations.
int p;

int waitingCount = 0;
int periodsCount = 0;
mutex waitingCountMutex;
condition_variable conditionVariable;

mutex finalMutex;

bool stopCalculations = false;
vector<double> errors;

int iterations;
double finalError;

void Barrier() {
	unique_lock<mutex> lock(waitingCountMutex);
	++waitingCount;
	if (waitingCount == p) {
		waitingCount = 0;
		++periodsCount;
		conditionVariable.notify_all();
	}
	else {
		int waitFor = periodsCount + 1;
		conditionVariable.wait(lock, [waitFor] { return periodsCount == waitFor; });
	}
}

inline double CalculateLeftPart(double* source, int i, int j) {
	return source[(i - 1) * size_ + j] + source[i * size_ + (j - 1)] - 4 * source[i * size_ + j] + source[(i + 1) * size_ + j] + source[i * size_ + (j + 1)];
}

inline double CalculateSolutionValue(double* source, int i, int j) {
	return source[i * size_ + j] + TAU * (CalculateLeftPart(source, i, j) - rightPart[i * size_ + j]);
}

void Run(int id, double* solution) {
	int iStart = 1 + id * (n / p);
	int iFinish = ((id != p - 1) ? ((id + 1) * (n / p)) : n);
	for (int it = 0; it < IT_AMOUNT; it += 2) {
	//while (!stopCalculations) {
		// First iteration.
		for (int i = iStart; i <= iFinish; ++i) {
			for (int j = 1; j <= n; ++j) {
				buffer[i * size_ + j] = CalculateSolutionValue(solution, i, j);
			}
		}
		Barrier();
		//

		// Second iteration.
		for (int i = iStart; i <= iFinish; ++i) {
			for (int j = 1; j <= n; ++j) {
				solution[i * size_ + j] = CalculateSolutionValue(buffer, i, j);
			}
		}
		Barrier();
		//
	}

	// Error calculating.
	double maxError = 0;
	for (int i = iStart; i <= iFinish; ++i) {
		for (int j = 1; j <= n; ++j) {
			maxError = max(maxError, std::abs(CalculateLeftPart(solution, i, j) - rightPart[i * size_ + j]));
		}
	}
	errors[id] = maxError;
	if (id == 0) {
		finalMutex.lock();
	}
	Barrier();
	//

	// Error checking.
	if (id == 0) {
		finalError = 0;
		for (int i = 0; i < p; ++i) {
			finalError = max(finalError, errors[i]);
		}
		cout << "Error" << ": " << finalError << endl;
		iterations += 2;
	}
	else {
		finalMutex.lock();
	}
	finalMutex.unlock();
	//
}

void parallel_omp(int threadAmount, double* solution, double& finalError) {
	omp_set_num_threads(threadAmount);
	for (int it = 0; it < IT_AMOUNT; it += 2) {
	//while (!stopCalculations) {
		// First iteration
#pragma omp parallel for if (threadAmount > 1)
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= n; ++j) {
				buffer[i * size_ + j] = CalculateSolutionValue(solution, i, j);
			}
		}
		//

		// Second iteration
#pragma omp parallel for if (threadAmount > 1)
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= n; ++j) {
				solution[i * size_ + j] = CalculateSolutionValue(buffer, i, j);
			}
		}
		//

		iterations += 2;
	}

	// Error calculating
	double maxError = 0;
#pragma omp parallel for if (threadAmount > 1)
	for (int i = 1; i <= n; ++i) {
		double currError = 0;
		for (int j = 1; j <= n; ++j) {
			currError = max(currError, abs(CalculateLeftPart(solution, i, j) - rightPart[i * size_ + j]));
		}
#pragma omp critical
		maxError = max(maxError, currError);
	}
	//

	cout << "Error" << ": " << maxError << endl;
}

int main() {
	//random_device rd;
	//mt19937 e2(rd());
	//uniform_real_distribution<> dist(0, 1);
	//TAU = dist(e2);
	//TAU = 0.15404;
	//cout << "TAU = " << fixed << setprecision(40) << TAU << endl;

	// Parameters
	n = 200;
	p = 4;
	//cout << "n = ";
	//cin >> n;

	//cout << "p = ";
	//cin >> p;

	cout << endl;

	//

	// Initialize data
	size_ = n + 2;
	solutionThread = new double[size_ * size_]();
	buffer = new double[size_ * size_]();
	rightPart = new double[size_ * size_]();

	double h = 3. / ((double)n + 1);
	for (int i = 0; i <= n + 1; ++i) {
		for (int j = 0; j <= n + 1; ++j) {
			double x = i * h;
			double y = j * h;
			rightPart[i * size_ + j] = h * h * F(x, y);
		}
	}
	//

	// Thread
	cout << "Thread:" << endl;
	errors = vector<double>(p);
	vector<thread> threads;
	std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();
	for (int i = 1; i < p; ++i) {
		threads.push_back(std::thread(*Run, i, solutionThread));
	}
	Run(0, solutionThread);
	std::chrono::steady_clock::time_point finishTime = std::chrono::steady_clock::now();
	cout << "Final error: " << finalError << endl;
	//cout << "Iterations: " << iterations << endl;
	int timeThread = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime).count();
	cout << "Wasted time: " << timeThread << " ms" << endl;

	for (std::thread& thread : threads) {
		thread.join();
	}

	//std::cout << "Solution:" << std::endl;
	//for (int i = 0; i < size * size; ++i) {
	//	std::cout << solution[i] << " ";
	//}
	//std::cout << std::endl;

	std::cout << std::endl;
	//

	// OMP
	cout << "OMP:" << endl;
	stopCalculations = false;
	iterations = 0;
	solutionOMP = new double[size_ * size_]();

	for (int i = 0; i < size_ * size_; ++i) {
		//solution[i] = 0;
		buffer[i] = 0;
	}

	startTime = std::chrono::steady_clock::now();
	parallel_omp(p, solutionOMP, finalError);
	finishTime = std::chrono::steady_clock::now();
	cout << "Final error: " << finalError << endl;
	//cout << "Iterations: " << iterations << endl;
	int timeOMP = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime).count();
	cout << "Wasted time: " << timeOMP << " ms" << endl;
	cout << endl;
	//

	// Sequential
	cout << "Sequential:" << endl;
	stopCalculations = false;
	iterations = 0;
	solutionSequential = new double[size_ * size_]();

	for (int i = 0; i < size_ * size_; ++i) {
		//solution[i] = 0;
		buffer[i] = 0;
	}

	startTime = std::chrono::steady_clock::now();
	parallel_omp(1, solutionSequential, finalError);
	finishTime = std::chrono::steady_clock::now();
	cout << "Final error: " << finalError << endl;
	//cout << "Iterations: " << iterations << endl;
	int timeSequential = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime).count();
	cout << "Wasted time: " << timeSequential << " ms" << endl;
	cout << std::endl;
	//

	// Check solutions
	bool isEqual = true;
	for (int i = 0; i < size_ * size_; ++i) {
		if (solutionThread[i] != solutionOMP[i] || solutionThread[i] != solutionSequential[i]) {
			cout << "i = " << i << " solution[i] = " << solutionThread[i] << " solutionOMP[i] = " << solutionOMP[i] << " solutionSequential[i] = " << solutionSequential[i] << endl;
			isEqual = false;
			break;
		}
	}

	if (isEqual) {
		cout << "All is ok";
	}
	else {
		cout << "Smth happened";
	}

	cout << endl;
	//

	// E(p)
	cout << "timeSequential / timeOMP = " << (double)timeSequential / (double)timeOMP << endl;
	cout << "timeSequential / timeThreads = " << (double)timeSequential / (double)timeThread << endl;
	//

	// Free
	delete[] solutionThread;
	delete[] solutionOMP;
	delete[] solutionSequential;
	delete[] buffer;
	delete[] rightPart;
	//

	return 0;
}
