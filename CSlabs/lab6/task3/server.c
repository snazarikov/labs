#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_BUF 1024

int main() {
	int fd;
	char* myfifo = "/gpfs/home/snazarikov/lab6/task3/myfifo";
	char buf[MAX_BUF];

	fd = open(myfifo, O_RDONLY);
	if (fd == -1) {
		printf("ERROR 2\n");
		perror("open");
		return EXIT_FAILURE;
	}

	read(fd, buf, MAX_BUF);

	int i;
	for (i = 0; i < 10; ++i) {
		printf("%s\n", buf);
	}

	close(fd);
	unlink(myfifo);

	return 0;
}
