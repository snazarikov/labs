#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

int main() {
	int fd;
	char* myfifo = "/gpfs/home/snazarikov/lab6/task3/myfifo";

	mkfifo(myfifo, 0666);

	fd = open(myfifo, O_WRONLY);
	write(fd, "PAIPOS", sizeof("PAIPOS"));
	close(fd);

	unlink(myfifo);

	return 0;
}
