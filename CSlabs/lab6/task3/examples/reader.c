#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_BUF 1024
#define EXIT_FAILURE 5051

int main() {
	int fd;
	char* myfifo = "/gpfs/home/snazarikov/lab6/task3/myfifo";
	char buf[MAX_BUF];

	fd = open(myfifo, O_RDONLY);
	if (fd == -1) {
		printf("ERROR BITCH\n");
		perror("open");
		return EXIT_FAILURE;
	}

	read(fd, buf, MAX_BUF);
	printf("Recieved: %s\n", buf);
	close(fd);

	return 0;
}
