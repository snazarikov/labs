#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_BUF 1024

int main(int argc, char *argv[]) {
	char buf[MAX_BUF];

	int count = 1;
	int len = 0;

	while (count < argc) {
		int i;
		for (i = 0; i < strlen(argv[count]); ++i) {
			buf[len] = argv[count][i];
			len++;
		}
		buf[len] = 32;
		len++;
		count++;
	}

	printf("%s\n", buf);

	return 0;
}
