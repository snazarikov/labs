#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>

#define MAX_BUF 1024

char buf[MAX_BUF];

void mySignalHandler(int signalno) {
	printf("Signal come\n");
	char* myfifo = "/gpfs/home/snazarikov/lab6/task3/myfifo";
	int fd;

	mkfifo(myfifo, 0666);

	fd = open(myfifo, O_WRONLY);
	if (fd == -1) {
		perror("open");
	}
	
	write(fd, buf, sizeof(buf));

	close(fd);

	printf("\nTask has been done\n");

	exit(0);
}

int main(int argc, char* argv[]) {
	int c = 1;

	int count = 1;
	int len = 0;

	while (count < argc) {
		int i;
		for (i = 0; i < strlen(argv[count]); ++i) {
			buf[len] = argv[count][i];
			len++;
		}
		buf[len] = 32;
		len++;
		count++;
	}
						
	signal(SIGUSR1, mySignalHandler);
	
	while (1) {
		printf("Waiting ur signal already %dsec\n", c++);
		usleep(1000000);
	}

	return 0;
}
