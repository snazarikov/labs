#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAX_SIZE_OF_STR 105
#define MAX_COUNT_OF_STR 1005
#define MAX_COUNT_OF_JOKES 10

void freeMemory(char** text, int amount) {
	int i;
	for (i = 0; i < amount; ++i) {
		free(text[i]);
	}
	free(text);
}

int getData(char** text) {
	char* S = (char*) malloc(MAX_SIZE_OF_STR * sizeof(char));

	int counter = 0;

	FILE* input = fopen("input.txt", "r");

	while (fgets(S, MAX_SIZE_OF_STR, input)) {
		text[counter] = (char*) malloc(MAX_SIZE_OF_STR * sizeof(char));
		strcpy(text[counter], S);

		counter++;
	}

	free(S);
	fclose(input);

	return counter;
}

void sortData(char** text, int amount) {
	int i, j;
	char* tmp;
	for (i = 0; i < amount - 1; ++i) {
		for (j = i + 1; j < amount; ++j) {
			if (strcmp(text[i], text[j]) > 0) {
				tmp = text[i];
				text[i] = text[j];
				text[j] = tmp;
			}
		}
	}	
}

void printData(char** text, int amount) {
	FILE* output = fopen("output.txt", "w");

	int i;
	for (i = 0; i < amount; ++i) {
		fputs(text[i], output);
	//	fputs(text[i], stdout);
	}

	fclose(output);
}

void makeJokes(char** joke) {
	int i;
	for (i = 0; i < 10; ++i) {
		joke[i] = (char*) malloc(MAX_SIZE_OF_STR * sizeof(char));
		if (i < 4 || i > 7) {
			strcpy(joke[i], "some funny joke\n");
		}
	}
	strcpy(joke[4], "u will pass all ur exams\n");
	strcpy(joke[5], "ahaha\n");
	strcpy(joke[6], "ahahhahahaa\n");
	strcpy(joke[7], "AHAHAHAHHAAHAAHHAHAHAAH\n");
}

int main(int argc, char* argv) {
	pid_t pid = fork();
	int status;

	if (pid) {
		char** joke = (char**) malloc(MAX_COUNT_OF_JOKES * sizeof(char*));
		makeJokes(joke);

		int i;
		for (i = 0; i < MAX_COUNT_OF_JOKES; ++i) {
			fputs(joke[i], stdout);
			usleep(1000000);
		}

		freeMemory(joke, MAX_COUNT_OF_JOKES);

		wait(&status);
	}	
	else if (!pid) {
		int i;
		char** text = (char**) malloc(MAX_COUNT_OF_STR * sizeof(char*));
		int amount = getData(text);
		sortData(text, amount);
		printData(text, amount);
		freeMemory(text, amount);
	}

	return 0;
}
