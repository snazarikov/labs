#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char* argv) {
	printf("It works in parent process\n");

	pid_t pid = fork();
	int status;

	printf("It works in parent and child processes\n");

	if (pid) {
		printf("I hope that it works in parent process\n");
		wait(&status);
		int i;
		for (i = 0; i < 10; ++i) {
			printf("Cycle in parent process %d\n", i);
		}

		printf("\nThe end of the expirement\n");
	}
	else if (!pid) {
		printf("I hope that it works in child process\n");
		int i;
		for (i = 0; i < 20; ++i ) {
			printf("Cycle in child process %d\n", i);
		}
	}

	//printf("The end of the expirement\n");

	return 0;
}
