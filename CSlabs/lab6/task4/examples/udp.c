#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>

int main(void) {
	struct sockaddr_in local;
	inet_aton("127.0.0.1", &local.sin_addr);
	local.sin_port = htons(3000);
	local.sin_family = AF_INET;

	int s = socket(AF_INET, SOCK_DGRAM, 0);
	printf("socked id = %d\n", s);

	int result = bind(s, (struct sockaddr*) &local, sizeof(local));
	printf("result = %d\n", result);

	char buf[1024];
	if (read(s, buf, 1024) == -1) {
		perror("read");
	}
	
	printf("%s", buf);

	return 0;
}
