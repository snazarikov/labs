#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>

struct sockaddr_in local;

void server();
void client();

int main(int argc, char* argv[]) {
	if (!strcmp(argv[1], "c")) {
		printf("Client started\n");
		client();
	}
	else if (!strcmp(argv[1], "s")) {
		printf("Server started\n");
		server();
	}
	return 0;
}

void server() {
	inet_aton("127.0.0.1", &local.sin_addr);
	local.sin_port = htons(3000);
	local.sin_family = AF_INET;

	int s = socket(AF_INET, SOCK_STREAM, 0);
	bind(s, (struct sockaddr*) &local, sizeof(local));
	listen(s,5);

	int cs = accept(s, NULL, NULL);
	char buf[1024];
	read(cs, buf, 1024);
	printf("%s\n", buf);
	close(cs);
}

void client() {
	inet_aton("127.0.0.1", &local.sin_addr);
	local.sin_port = htons(3000);
	local.sin_family = AF_INET;
	
	int s = socket(AF_INET, SOCK_STREAM, 0);
	connect(s, (struct sockaddr*) &local, sizeof(local));

	char buf[1024] = "Hello TCP/IP";
	write(s, buf, strlen(buf) + 1);
	close(s);
	printf("%s\n", buf);
}
