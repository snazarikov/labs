#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

#define MAX_BUF 1024

int main(int argc, char* argv[]) {
	if (argc != 2) {
		return -1;
	}

	struct hostent *h;
	h = gethostbyname(argv[1]);

	if (NULL == h) {
		printf("ERROR\n");
		return -1;
	}

	printf("Canonical name %s\n", h->h_name);
	printf("Type = %s len = %d\n", (h->h_addrtype == AF_INET) ? "IPv4" : "IPv6", h->h_length);

	if (h->h_addrtype != AF_INET) {
		return 0;
	}

	int i = 0;
	while (NULL != h->h_addr_list[i]) {
		struct in_addr* tmp = (struct in_addr*) h->h_addr_list[i];
		printf("%s\n", inet_ntoa(*tmp));
		i++;
	}

	return 0;
}
