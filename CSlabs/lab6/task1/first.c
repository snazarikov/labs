#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAGIC_CONST 1e8

int main() {
	FILE* file = fopen("input.txt", "w");

	srand((unsigned) time(NULL));

	int i;
	for (i = 0; i < MAGIC_CONST; ++i) {
		fprintf(file, "%c", 'a' + (0 + rand() % 26));
		if (!(i % 20) && i) {
			fprintf(file, "\n");
		}
	}

	fclose(file);

	return 0;
}
