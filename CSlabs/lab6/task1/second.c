#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BUF_SIZE ((int)1e9)
#define SIZE_MB 20

char buf[BUF_SIZE];
int main() {
	int file;
	char* fileName = "input.txt";

	file = open(fileName, O_WRONLY);	
	FILE* out=fopen("out.txt","w");

	srand((unsigned) time(NULL));

	int i, j;
	//for (i = 0; i < SIZE_MB * 1024 / BUF_SIZE * 1024; ++i) {
	for (j = 0; j < BUF_SIZE; ++j) {
		buf[j] = 'a' + (0 + rand() % 26);
	}
	clock_t begin = clock();
	write(file, buf, sizeof(buf));
	/* for (j = 0; j < BUF_SIZE; ++j) { */
	/* 	fprintf(out,"%c",buf[j]); */
	/* } */
	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("%lf\n",time_spent);
	close(file);
	fclose(out);
	//}

	return 0;
}
