#include <iostream>
#include "Arena.h"

using namespace std;

int main() {
    srand((unsigned) time(NULL));
    Arena gameArena;
    gameArena.chooseHero();
    gameArena.battle();
    gameArena.getResults();
    return 0;
}
