#ifndef KNIGHT_H
#define KNIGHT_H

#include <Hero.h>


class Knight : public Hero {
    public:
        Knight();
        virtual ~Knight();

        vector<OptionModel> optionHit(const vector<Hero*> &heroes);
        vector<OptionModel> optionSkill();

        int getAttack(const int damage);
        void activeSkill(vector<Hero*> &heroes, const int skillType);
    private:
        void skillNakedKnight();
        void skillDinnerWithArmour();
};

#endif // KNIGHT_H
