#ifndef ARENA_H
#define ARENA_H

#include "Hero.h"
#include "HeroFactory.h"
#include "support.h"
#include "supportFunc.h"

class Arena {
    public:
        Arena();
        virtual ~Arena();

        void chooseHero();
        void battle();
        void getResults();

    private:
        vector<Hero*> heroes;
        vector<pair<int, string>> gameField;

        HeroFactory _factory;

        static int _idCounter;
        int _redHeroAmount;
        int _blueHeroAmount;
        int _moveCounter;

        int addHero(int type, int team);
        void moveHero(OptionModel option, int heroNumber);
        void deleteHero(int position);
};

#endif // ARENA_H
