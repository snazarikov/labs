#ifndef WIZARD_H
#define WIZARD_H

#include <Hero.h>


class Wizard : public Hero
{
    public:
        Wizard();
        virtual ~Wizard();

        vector<OptionModel> optionHit(const vector<Hero*> &heroes);
        vector<OptionModel> optionSkill();

        int getAttack(const int damage);
        void activeSkill(vector<Hero*> &heroes, const int skillType);
    private:
        int _manaPerHit;

        int skillMagicShield(const int damage);
        void skillRandom();

};

#endif // WIZARD_H
