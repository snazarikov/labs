#ifndef HERO_H
#define HERO_H

#include "support.h"


class Hero {
    public:
        Hero();
        virtual ~Hero();

        void setId(const int id);
        void setType(const int type);
        void setPosition(const int position);
        void setTeam(const int team);

        int getId();
        int getType();
        int getPosition();
        int getTeam();
        int getRange();
        int getHp();
        int getArmour();
        int getMana();
        int getDamage();

        void substractHp(const int damage);
        void substractMana(const int mana);
        void substractArmour(const int armour);
        void addHp(const int addition);
        void addArmour(const int addition);
        void addDamage(const int addition);

        int isDead();

        vector<OptionModel> optionMove(const vector<Hero*> &heroes);
        virtual vector<OptionModel> optionHit(const vector<Hero*> &heroes) = 0;
        virtual vector<OptionModel> optionSkill() = 0;

        virtual int getAttack(const int damage) = 0;
        virtual void activeSkill(vector<Hero*> &heroes, const int skillType) = 0;

    protected:
        int _team;
        int _hp;
        int _armour;
        int _damage;
        int _position;
        int _mana;
        int _range;
        int _type;
        int _id;
};

#endif // HERO_H
