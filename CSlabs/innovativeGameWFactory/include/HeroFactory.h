#ifndef HEROFACTORY_H
#define HEROFACTORY_H

#include "support.h"
#include "Hero.h"
#include "Wizard.h"
#include "Knight.h"
#include "Goblin.h"

class HeroFactory {
    public:
        HeroFactory();
        virtual ~HeroFactory();

        Hero* createHero(int type);
};

#endif // HEROFACTORY_H
