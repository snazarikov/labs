#include "Hero.h"
#include "support.h"

int findEmptyCell(vector<pair<int, string>> gameField, int team);
void getOptions(vector<Hero*> heroes, vector<OptionModel>& allOptions, const int team);
void printOptions(const vector<OptionModel>& options, const int team);
void printInfo(vector<Hero*> heroes);
int findHero(vector<Hero*> heroes, int id);
void printGameField(vector<pair<int, string>> gameField);
void printResult(int redAmount, int blueAmount, int moveAmount);
template <typename type> void printColorfulText(const int color, const type text);

enum ConsoleColor {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    LightMagenta = 13,
    Yellow = 14,
    White = 15
};
