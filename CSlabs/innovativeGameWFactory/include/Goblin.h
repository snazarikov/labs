#ifndef GOBLIN_H
#define GOBLIN_H

#include <Hero.h>


class Goblin : public Hero {
    public:
        Goblin();
        virtual ~Goblin();

        int getBomb() const;
        void setBomb(const int bomb);

        vector<OptionModel> optionHit(const vector<Hero*> &heroes);
        vector<OptionModel> optionSkill();

        int getAttack(const int damage);
        void activeSkill(vector<Hero*> &heroes, const int skillType);
    private:
        int _bomb;
        int _cloneAmount;

        int skillCowardice();
        void skillBomb(vector<Hero*> &heroes);
        void skillClone();
};

#endif // GOBLIN_H
