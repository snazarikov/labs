#include <string>
#include <cmath>
#include <ctime>
#include <vector>
#include <iostream>
#include <Windows.h>

using namespace std;

#ifndef USEFULFILE_H_INCLUDED
#define USEFULFILE_H_INCLUDED

// ints
const int RED_PLAYER = 1;
const int BLUE_PLAYER = 2;

const int EMPTY = -1;
const int FIELD_SIZE = 7;

const int MANA_FOR_SKILL = 25;
const int NOT_ENOUGH = -505;

const int SUCCESS_ADD_HERO = 0;

const int ERROR_FULL_FIELD_INT = -2;
const int ERROR_CHOOSE_INT = -1;

const int ARMOUR_FOR_DINNER = 5;

enum HeroType {
    WIZARD = 1,
    KNIGHT = 2,
    GOBLIN = 3
};

// srings
const std::string CHOOSE_FIRST_PLAYER = "Red player choose your hero pls";
const std::string CHOOSE_SECOND_PLAYER = "Blue player choose your hero pls";
const std::string CHOOSE_VARIANTS = "Type:\n1. To choose Wizard\n2. To choose Knight\n3. To choose Goblin";

const std::string RED_WIZARD = "RW  ";
const std::string RED_KNIGHT = "RK  ";
const std::string RED_GOBLIN = "RG  ";
const std::string BLUE_WIZARD = "BW  ";
const std::string BLUE_KNIGHT = "BK  ";
const std::string BLUE_GOBLIN = "BG  ";
const std::string EMPTY_HERO = "E  ";

const std::string WIZARD_CHOSEN = "You've chosen wizard successfully!";
const std::string KNIGHT_CHOSEN = "You've chosen knight successfully!";
const std::string GOBLIN_CHOSEN = "You've chosen goblin successfully!";

const std::string ERROR_CHOOSE_STR = "You've typed wrong numeral\nTry it again pls";
const std::string ERROR_FULL_FIELD_STR = "Seems that game-field has no empty cells";

const std::string CHOOSE_OPTION = "Choose one option that you want to use now";

// structs
struct OptionModel {
    int idAttacker;
    int idDefender;
    std::string description;
    int damage;
    int positionChange;
};

#endif // USEFULFILE_H_INCLUDED
