#include "Hero.h"

Hero::Hero() {
    //ctor
}

Hero::~Hero() {
    //dtor
}

int Hero::getId() {
    return _id;
}

void Hero::setId(const int id) {
    _id = id;
}

int Hero::getType() {
    return _type;
}

void Hero::setType(const int type) {
    _type = type;
}

int Hero::getPosition() {
    return _position;
}

void Hero::setPosition(const int position) {
    _position = position;
}

int Hero::getTeam() {
    return _team;
}

void Hero::setTeam(const int team) {
    _team = team;
}

int Hero::getRange() {
    return _range;
}

int Hero::getHp() {
    return _hp;
}

int Hero::getArmour() {
    return _armour;
}

int Hero::getMana() {
    return _mana;
}

int Hero::getDamage() {
    return _damage;
}

void Hero::substractHp(const int damage) {
    _hp = (_hp - damage > 0) ? _hp - damage : 0;
}

void Hero::substractMana(const int mana) {
    _mana = (_mana - mana > 0) ? _mana - mana : 0;
}

void Hero::substractArmour(const int armour) {
    _armour = (_armour - armour > 0) ? _armour - armour : 0;
}

void Hero::addArmour(const int additon) {
    _armour += additon;
}

void Hero::addHp(const int additon) {
    _hp += additon;
}

void Hero::addDamage(const int additon) {
    _damage += additon;
}

int Hero::isDead() {
    if (_hp > 0) {
        return 0;
    }
    return 1;
}

vector<OptionModel> Hero::optionMove(const vector<Hero*> &heroes) {
    vector<int> gameField(FIELD_SIZE, EMPTY);

    for (auto& hero : heroes) {
        gameField[hero->getPosition()] = hero->getId();
    }

    vector<OptionModel> optMove;

    for (int i = getPosition() + 1; i <= 7 && i <= getPosition() + getRange(); ++i) {       // ability to move left
        if (gameField[i] == EMPTY) {
            string description = "You can move your hero in " + std::to_string(getPosition()) + " cell to " + std::to_string(i) + " cell";
            OptionModel newOption = {getId(), getId(), description, 0, i};
            optMove.push_back(newOption);
        }
    }

    for (int i = getPosition() - 1; i >= 0 && i >= getPosition() - getRange(); --i) {       // ability to move right
        if (gameField[i] == EMPTY) {
            string description = "You can move your hero in " + std::to_string(getPosition()) + " cell to " + std::to_string(i) + " cell";
            OptionModel newOption = {getId(), getId(), description, 0, i};
            optMove.push_back(newOption);
        }
    }

    return optMove;
}
