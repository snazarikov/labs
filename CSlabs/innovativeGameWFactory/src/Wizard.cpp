#include "Wizard.h"

Wizard::Wizard() {
    _hp = 100;
    _armour = 0;
    _damage = 15;
    _range = 3;
    _mana = 100;
    _manaPerHit = 10;
}

Wizard::~Wizard() {
    //dtor
}

int Wizard::skillMagicShield(const int damage) {
//    cout << "skillMagicShield() of wizard" << endl;

    int chance = 1 + rand() % 10;

    if (chance <= 3 && getMana() - damage * 2 >= 0) {
        return 1;
    }
    return 0;
}

void Wizard::skillRandom() {
//    cout << "skillOneOfEight() of wizard" << endl;
    substractMana(_mana);

    int type = 1 + rand() % 4;
    switch (type) {
        case 1: {
            // useful double mana
            cout << "Wizard got double mana!" << endl;
            break;
        }
        case 2: {
            cout << "Wizard got double hp!" << endl;
            addHp(_hp);
            break;
        }
        case 3: {
            cout << "Wizard got death! HAHAHAHAAHHAHAHAHAHAHA" << endl;
            substractHp(_hp);
            break;
        }
        case 4: {
            cout << "Wizard got +50 armour!" << endl;
            addArmour(50);
            break;
        }
    }
}

int Wizard::getAttack(const int damage) {
//    cout << "getAttack() implementation in Wizard" << endl;

    if (skillMagicShield(damage)) {
        cout << "Wizard blocked damage by magic shield!" << endl;
        substractMana(damage * 2);
        return 1;
    }
    else {
        substractHp(damage);
        if (getHp() > 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

void Wizard::activeSkill(vector<Hero*> &heroes, const int skillType) {
//    cout << "skill choosing for wizard" << endl;
    switch (skillType) {
        case 1: {
            skillRandom();
            break;
        }
    }
}

vector<OptionModel> Wizard::optionHit(const vector<Hero*> &heroes) {
    vector<OptionModel> optHit;

    for (auto& hero : heroes) {           // abilities to hit
        if (hero->getTeam() != getTeam() && !hero->isDead() && abs(getPosition() - hero->getPosition()) <= getRange()) {
            string description = "You can hit opposite hero in " + std::to_string(hero->getPosition()) + " cell by your hero in " + std::to_string(getPosition()) + " cell";
            OptionModel newOption = {getId(), hero->getId(), description, getDamage(), getPosition()};
            optHit.push_back(newOption);
        }
    }

    return optHit;
}

vector<OptionModel> Wizard::optionSkill() {
    vector<OptionModel> optSkill;
    if (getMana() - MANA_FOR_SKILL >= 0) {
        string description = "You can randomly get\n    Double mana\n    Double hp\n    Armour\n    Death";
        OptionModel newOption = {getId(), getId(), description, -1, -1};
        optSkill.push_back(newOption);
    }
    return optSkill;
}
