#include "supportFunc.h"

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

int findEmptyCell(vector<pair<int, string>> gameField, int team) {
    if (team == RED_PLAYER) {
        for (int i = 0; i < FIELD_SIZE; ++i) {
            if (gameField[i].first == EMPTY) {
                return i;
            }
        }
    }
    else if (team == BLUE_PLAYER) {
        for (int i = FIELD_SIZE - 1; i >= 0; --i) {
            if (gameField[i].first == EMPTY) {
                return i;
            }
        }
    }
    return ERROR_FULL_FIELD_INT;
}

void getOptions(vector<Hero*> heroes, vector<OptionModel>& allOptions, const int team) {
    for (auto& hero : heroes) {
        if (hero->getTeam() == team && !hero->isDead()) {
            vector<OptionModel> optionMove = hero->optionMove(heroes);
            allOptions.insert(allOptions.end(), optionMove.begin(), optionMove.end());      // concatenation 2 vectors

            vector<OptionModel> optionHit = hero->optionHit(heroes);
            allOptions.insert(allOptions.end(), optionHit.begin(), optionHit.end());

            vector<OptionModel> optionSkill = hero->optionSkill();
            allOptions.insert(allOptions.end(), optionSkill.begin(), optionSkill.end());
        }
    }
}

int findHero(vector<Hero*> heroes, int id) {
    int counter = 0;
    for (auto& hero : heroes) {
        if (hero->getId() == id) {
            return counter;
        }
        ++counter;
    }
    return -1;
}

template <typename type>
void printColorfulText(const int color, const type text) {
    SetConsoleTextAttribute(hConsole, (WORD) ((Black << 4) | color));
    cout << text;
    SetConsoleTextAttribute(hConsole, (WORD) ((Black << 4) | White));
}

void printOptions(const vector<OptionModel>& options, const int team) {
    int counter = 0, color;
    for (auto option : options) {
        (team == RED_PLAYER) ? (color = LightRed) : (color = Magenta);
        printColorfulText<string>(color, to_string(counter++) + ". ");
        cout << option.description << endl;
    }
    cout << endl;
}

void printInfo(vector<Hero*> heroes) {
    typedef int (Hero::*GetterFuncPointer)();
    GetterFuncPointer methodsArray[] = {Hero::getPosition,
                                        Hero::getId,
                                        Hero::getType,
                                        Hero::getTeam,
                                        Hero::getDamage,
                                        Hero::getHp,
                                        Hero::getArmour,
                                        Hero::getMana};

    string titleMethodsArray[] = {"Position: ",
                                  "ID:       ",
                                  "Type:     ",
                                  "Team:     ",
                                  "Damage:   ",
                                  "Hp:       ",
                                  "Armour:   ",
                                  "Mana:     "};

    for (auto& hero : heroes) {
        if (!hero->isDead()) {
            if (hero->getTeam() == RED_PLAYER) {
                cout << "Info about ";
                printColorfulText<string>(LightRed, "red player");
            }
            else {
                cout << "Info about ";
                printColorfulText<string>(Magenta, "blue player");
            }
            cout << endl << endl;

            for (int i = 0; i < 8; ++i) {
                cout << titleMethodsArray[i];
                printColorfulText<int>(LightGreen, (hero->*methodsArray[i])());
                cout << endl;
            }
        }

    }
    cout << endl;
}

void printGameField(vector<pair<int, string>> gameField) {
    system("cls");
    cout << endl << endl;

    int color;
    for (int i = 0; i < FIELD_SIZE; ++i) {
        if (gameField[i].second == RED_GOBLIN || gameField[i].second == RED_KNIGHT || gameField[i].second == RED_WIZARD) {
            color = LightRed;
        }
        else if (gameField[i].second == EMPTY_HERO) {
            color = White;
        }
        else {
            color = Magenta;
        }
        printColorfulText<string>(color, to_string(i) + "-" + gameField[i].second);
    }
    cout << endl << endl;
}

void printResult(int redAmount, int blueAmount, int moveAmount) {
    cout << "Game was finished in ";
    printColorfulText<int>(Yellow, moveAmount);
    cout << " turns!" << endl;
    if (!blueAmount) {
        printColorfulText<string>(Yellow, "Congratulations to ");
        printColorfulText<string>(LightRed, "red player");
        printColorfulText<string>(Yellow, "!");
        cout << endl;
    }
    else if (!redAmount) {
        printColorfulText<string>(Yellow, "Congratulations to ");
        printColorfulText<string>(Magenta, "blue player");
        printColorfulText<string>(Yellow, "!");
        cout << endl;
    }
    else {
        printColorfulText<string>(Yellow, "THE DRAW!\nCongratulations to both players!");
    }
}
