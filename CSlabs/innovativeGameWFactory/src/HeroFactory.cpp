#include "HeroFactory.h"

HeroFactory::HeroFactory() {
    //ctor
}

HeroFactory::~HeroFactory() {
    //dtor
}

Hero* HeroFactory::createHero(int type) {
    switch (type) {
        case WIZARD: {
            return new Wizard();
        }
        case KNIGHT: {
            return new Knight();
        }
        case GOBLIN: {
            return new Goblin();
        }
    }
}
