#include "Knight.h"

Knight::Knight() {
    _hp = 125;
    _armour = 75;
    _damage = 25;
    _range = 4;
    _mana = 0;
}

Knight::~Knight() {

}

void Knight::skillNakedKnight() {
//    cout << "skillNakedKnight() of knight" << endl;
    if (getArmour() <= 0) {
        substractArmour(_armour);
        addDamage(_damage);
    }
}

void Knight::skillDinnerWithArmour() {
//    cout << "skillDinnerWithArmour() of knight" << endl;
    substractArmour(ARMOUR_FOR_DINNER);
    addDamage(ARMOUR_FOR_DINNER);

}

int Knight::getAttack(const int damage) {
//    cout << "getAttack() implementation in Knight" << endl;

    if (getArmour() >= damage) {
//        setArmour((getArmour() - damage) / 2);
        substractArmour(getArmour() / 2 + damage / 2);
        skillNakedKnight();
        return 1;
    }
    else {
        addHp(getArmour() - damage);
        substractArmour(_armour);
        skillNakedKnight();
        if (getHp() > 0) {
            return 1;
        }
        else {
            return 0;
        }
    }

}

void Knight::activeSkill(vector<Hero*> &heroes, const int skillType) {
//    cout << "skill choosing for knight" << endl;
    switch (skillType) {
        case 1: {
            skillDinnerWithArmour();
            break;
        }
    }
}

vector<OptionModel> Knight::optionHit(const vector<Hero*> &heroes) {
    vector<int> gameField(FIELD_SIZE, EMPTY);
    for (auto& hero : heroes) {
        gameField[hero->getPosition()] = hero->getId();
    }

    vector<OptionModel> optHit;

    for (auto& hero : heroes) {           // abilities to hit
        if (hero->getTeam() != getTeam() && !hero->isDead() && hero->getPosition() - 1 >= 0 && (gameField[hero->getPosition() - 1] == EMPTY || gameField[hero->getPosition() - 1] == getId()) && abs(getPosition() - hero->getPosition() + 1) <= getRange()) {
            string description = "You can hit opposite hero in " + std::to_string(hero->getPosition()) + " cell by your hero in " + std::to_string(getPosition()) + " cell who have to move to " + std::to_string(hero->getPosition() - 1) + " cell";
            OptionModel newOption = {getId(), hero->getId(), description, getDamage(), hero->getPosition() - 1};
            optHit.push_back(newOption);
        }
        if (hero->getTeam() != getTeam() && !hero->isDead() && hero->getPosition() + 1 <= 7 && (gameField[hero->getPosition() + 1] == EMPTY || gameField[hero->getPosition() + 1] == getId()) && abs(getPosition() - hero->getPosition() - 1) <= getRange()) {
            string description = "You can hit opposite hero in " + std::to_string(hero->getPosition()) + " cell by your hero in " + std::to_string(getPosition()) + " cell who have to move to " + std::to_string(hero->getPosition() + 1) + " cell";
            OptionModel newOption = {getId(), hero->getId(), description, getDamage(), hero->getPosition() + 1};
            optHit.push_back(newOption);
        }
    }

    return optHit;
}

vector<OptionModel> Knight::optionSkill() {
    vector<OptionModel> optSkill;
    if (getArmour() - ARMOUR_FOR_DINNER > 0) {
        string description = "You can eat some part of your armour and increase your hit-damage";
        OptionModel newOption = {getId(), getId(), description, -1, -1};
        optSkill.push_back(newOption);
    }
    return optSkill;
}
