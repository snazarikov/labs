#include "Arena.h"

Arena::Arena() {
    _moveCounter = 0;
    _redHeroAmount = 0;
    _blueHeroAmount = 0;

    gameField = vector<pair<int, string>>(FIELD_SIZE);
    for (int i = 0; i < FIELD_SIZE; ++i) {
        gameField[i].first = EMPTY;
        gameField[i].second = EMPTY_HERO;
    }
}

Arena::~Arena() {
    for (auto& hero : heroes) {
        delete(hero);
    }
}

int Arena::_idCounter = 0;

int Arena::addHero(int type, int team) {
    int position = findEmptyCell(gameField, team);

    (team == RED_PLAYER) ? ++_redHeroAmount : ++_blueHeroAmount;

    if (position != ERROR_FULL_FIELD_INT) {
        switch (type) {
            case WIZARD: {
                gameField[position].second = (team == RED_PLAYER) ? RED_WIZARD : BLUE_WIZARD;
                cout << WIZARD_CHOSEN << endl;
                break;
            }
            case KNIGHT: {
                gameField[position].second = (team == RED_PLAYER) ? RED_KNIGHT : BLUE_KNIGHT;
                cout << KNIGHT_CHOSEN << endl;
                break;
            }
            case GOBLIN: {
                gameField[position].second = (team == RED_PLAYER) ? RED_GOBLIN : BLUE_GOBLIN;
                cout << GOBLIN_CHOSEN << endl;
                break;
            }
            default: {
                return ERROR_CHOOSE_INT;
                break;
            }
        }
        heroes.push_back(_factory.createHero(type));
        heroes.back()->setType(type);
        heroes.back()->setId(++_idCounter);
        heroes.back()->setPosition(position);
        heroes.back()->setTeam(team);
        gameField[position].first = heroes.back()->getId();
        return SUCCESS_ADD_HERO;
    }
    else {
        return ERROR_FULL_FIELD_INT;
    }
}

void Arena::chooseHero() {
    system("color 0F");

    int value;

    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    printColorfulText<string>(LightRed, "Red player ");
    cout << "choose your hero pls" << endl << CHOOSE_VARIANTS << endl;
    while (cin >> value) {
        if (addHero(value, RED_PLAYER) == SUCCESS_ADD_HERO) {
            break;
        }
    }
    cout << endl;

    printColorfulText<string>(Magenta, "Blue player ");
    cout << "choose your hero pls" << endl << CHOOSE_VARIANTS << endl;
    while (cin >> value) {
        if (addHero(value, BLUE_PLAYER) == SUCCESS_ADD_HERO) {
            break;
        }
    }
    cout << endl;
}

void Arena::moveHero(OptionModel option, int heroNumber) {
    int prevPosition = heroes[heroNumber]->getPosition();
    if (option.positionChange == prevPosition) {
        return;
    }

    heroes[heroNumber]->setPosition(option.positionChange);

    gameField[option.positionChange] = gameField[prevPosition];
    gameField[prevPosition].first = EMPTY;
    gameField[prevPosition].second = EMPTY_HERO;
}

void Arena::deleteHero(int position) {
    (gameField[position].second == RED_WIZARD || gameField[position].second == RED_KNIGHT || gameField[position].second == RED_GOBLIN) ? --_redHeroAmount : --_blueHeroAmount;
    gameField[position].first = EMPTY;
    gameField[position].second = EMPTY_HERO;
}

void Arena::battle() {
    vector<OptionModel> options;
    do {
        int team = (_moveCounter % 2) ? BLUE_PLAYER : RED_PLAYER;
        options.clear();

        // print info for current turn
        printGameField(gameField);
        printInfo(heroes);

        // get&print options for current player
        getOptions(heroes, options, team);
        printOptions(options, team);

        // choosing option
        cout << CHOOSE_OPTION << endl;
        int optionIndex;
        while (cin >> optionIndex) {     // reading number of option
            if (optionIndex >= 0 && optionIndex < options.size()) {
                break;
            }
            cout << ERROR_CHOOSE_STR << endl;
        }

        // number of hero in <vector heroes> whose option been chosen
        int activeHeroNumber = findHero(heroes, options[optionIndex].idAttacker);

        //  turn
        if (options[optionIndex].positionChange == -1) {                                  // active skill
//            cout << "Hero will use active skill" << endl;
            heroes[activeHeroNumber]->activeSkill(heroes, 1);
            for (auto& hero : heroes) {
                if (hero->isDead()) {
                    deleteHero(hero->getPosition());
                }
            }
        }
        else if (options[optionIndex].positionChange == -2) {                             // clone goblin
            addHero(GOBLIN, options[optionIndex].damage); // second param - team
            heroes.back()->activeSkill(heroes, 2);
            heroes[activeHeroNumber]->activeSkill(heroes, 2);
        }
        else if (options[optionIndex].idAttacker == options[optionIndex].idDefender) {    // moving hero
//            cout << "Hero will be moved" << endl;
            moveHero(options[optionIndex], activeHeroNumber);
        }
        else {                                                                            // attack opponent
//            cout << "Hero will attack other hero" << endl;
            int defenderHeroNumber = findHero(heroes, options[optionIndex].idDefender);
            if (heroes[activeHeroNumber]->getType() == KNIGHT) {
                moveHero(options[optionIndex], activeHeroNumber);
            }
            if (!heroes[defenderHeroNumber]->getAttack(options[optionIndex].damage)) {
                deleteHero(heroes[defenderHeroNumber]->getPosition());
            }
        }

        _moveCounter++;
        cout << endl;
    }
    while (_redHeroAmount && _blueHeroAmount);
}

void Arena::getResults() {
    printResult(_redHeroAmount, _blueHeroAmount, _moveCounter);
}
