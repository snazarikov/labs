#include "Goblin.h"

Goblin::Goblin() : _bomb(0) {
    _hp = 75;
    _armour = 0;
    _damage = 15;
    _range = 4;
    _mana = 0;
    _cloneAmount = 0;
}

Goblin::~Goblin() {
    //dtor
}

int Goblin::getBomb() const {
    return _bomb;
}

void Goblin::setBomb(const int bomb) {
    _bomb = bomb;
}

int Goblin::skillCowardice() {
//    cout << "skillCowardice() of goblin" << endl;
    int chance = 1 + rand() % 100;

    if (chance <= 100 - getHp()) {
        return 1;
    }
    return 0;
}

void Goblin::skillBomb(vector<Hero*> &heroes) {
//    cout << "skillMakeBomb() of goblin" << endl;
    if (!getBomb()) {
        setBomb(1);
        return;
    }

    for (auto& hero : heroes) {
        if (!hero->isDead()) {
            hero->getAttack(hero->getHp() - 1);
        }
    }
    setBomb(-1);
}

void Goblin::skillClone() {
    _cloneAmount = -1;
}

int Goblin::getAttack(const int damage) {
//    cout << "getAttack() implementation in Goblin" << endl;
    if (skillCowardice()) {
        cout << "Goblin dodged the hit!" << endl;
        return 1;
    }
    else {
        cout << "Goblin hit himself!" << endl;
        substractHp(damage * 2);
        if (!isDead()) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

void Goblin::activeSkill(vector<Hero*> &heroes, const int skillType) {
//    cout << "skill choosing for goblin" << endl;
    switch (skillType) {
        case 1: {
            skillBomb(heroes);
            break;
        }
        case 2: {
            skillClone();
        }
    }
}

vector<OptionModel> Goblin::optionHit(const vector<Hero*> &heroes) {
    vector<OptionModel> optHit;

    for (auto& hero : heroes) {           // abilities to hit
        if (hero->getTeam() != getTeam() && !hero->isDead() && abs(getPosition() - hero->getPosition()) <= getRange()) {
            string description = "You can hit opposite hero in " + std::to_string(hero->getPosition()) + " cell by your hero in " + std::to_string(getPosition()) + " cell";
            OptionModel newOption = {getId(), hero->getId(), description, getDamage(), getPosition()};
            optHit.push_back(newOption);
        }
    }

    return optHit;
}

vector<OptionModel> Goblin::optionSkill() {
    vector<OptionModel> optSkill;
    if (!getBomb()) {
        string description = "You can construct the bomb by your goblin in " + std::to_string(getPosition()) + " cell";
        OptionModel newOption = {getId(), getId(), description, -1, -1};
        optSkill.push_back(newOption);
    }
    else if (getBomb() == 1) {
        string description = "You can detonate the bomb by your goblin in " + std::to_string(getPosition()) + " cell";
        OptionModel newOption = {getId(), getId(), description, -1, -1};
        optSkill.push_back(newOption);
    }
    if (!_cloneAmount) {
        string description = "You can make clone by your goblin in " + std::to_string(getPosition()) + " cell";
        OptionModel newOption = {getId(), getId(), description, getTeam(), -2};
        optSkill.push_back(newOption);
    }
    return optSkill;
}
