#!/bin/bash
set -ueo pipefail

if [ $# -eq 1 ]
then
	echo Choosed version for download is $1
else
	echo No version found, so help should be somewhere here
	echo ./RScript.sh \<version\> \- it will Install R on your PC
	exit 0
fi

path=$HOME/taskR/R_$1
archive=$path/R-$1.tar.gz
URL=http://cran.r-project.org/src/base/R-${1:0:1}/R-$1.tar.gz

if [ -e $path ]
then
	echo Directory already exists
else
	mkdir -p $path
fi

if [ -e $archive ]
then
	echo Archive already downloaded
else
	wget -P $path $URL
	tar -xvf $archive --directory=$path
fi

echo Installation started

libsPath=$path/../libs
installedLibsPath=$libsPath/installed

installedBzip2Path=$installedLibsPath/bzip2
installedLzmaPath=$installedLibsPath/lzma
installedPcrePath=$installedLibsPath/pcre
installedCurlPath=$installedLibsPath/curl

if [ -e $installedLibsPath ]
then
	echo All additional libraries already installed
else
	mkdir -p $libsPath
	mkdir -p $installedLibsPath

	bzip2URL=http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
	bzip2Path=$libsPath/bzip2
	bzip2=$bzip2Path/bzip2-1.0.6.tar.gz

	if [ -e $installedBzip2Path ] 
	then
		echo Bzip2 already installed
	else
		if [ -e $bzip2Path/bzip2-1.0.6 ]
		then
			echo Bzip2 already unpacked
		else
			if [ -e $bzip2 ] 
			then
				echo Bzip2 already downloaded
			else
	                        mkdir -p $bzip2Path
	                        wget -P $bzip2Path $bzip2URL
			fi
			tar -xvf $bzip2 --directory=$bzip2Path
		fi
		cd $bzip2Path/bzip2-1.0.6
                make -f Makefile-libbz2_so
                make
                mkdir -p $installedBzip2Path
                make install PREFIX=$installedBzip2Path
                export CPATH=$CPATH:$installedBzip2Path/include
                export LIBRARY_PATH=$LIBRARY_PATH:$installedBzip2Path/lib
                export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedBzip2Path/lib
	fi

	#export CPPFLAGS=-I$installedBzip2Path/include
	#export LDFLAGS=-L$installedBzip2Path/lib

	lzmaURL=http://tukaani.org/xz/xz-5.2.3.tar.gz
	lzmaPath=$libsPath/lzma
	lzma=$lzmaPath/xz-5.2.3.tar.gz

	if [ -e $installedLzmaPath ] 
	then
		echo Lzma already installed
	else
		if [ -e $lzmaPath/xz-5.2.3 ]
		then
			echo Lzma already unpacked
		else
			if [ -e $lzma ] 
			then
				echo Lzma already downloaded
			else
				mkdir -p $lzmaPath
	                        wget --no-check-certificate -P $lzmaPath $lzmaURL
			fi
			tar -xvf $lzma --directory=$lzmaPath
		fi
		cd $lzmaPath/xz-5.2.3
                mkdir -p $installedLzmaPath
                ./configure --prefix=$installedLzmaPath
                make
                make install
                export CPATH=$CPATH:$installedLzmaPath/include
                export LIBRARY_PATH=$LIBRARY_PATH:$installedLzmaPath/lib
                export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedLzmaPath/lib
	fi

	pcreURL=http://ftp.pcre.org/pub/pcre/pcre-8.40.tar.gz
	pcrePath=$libsPath/pcre
	pcre=$pcrePath/pcre-8.40.tar.gz

	if [ -e $installedPcrePath ] 
	then
		echo Pcre already installed
	else
		if [ -e $pcrePath/pcre-8.40 ]
		then
			echo Pcre already unpacked
		else
			if [ -e $pcre ] 
			then
				echo Pcre already downloaded
			else
				mkdir -p $pcrePath
	                        wget --no-check-certificate -P $pcrePath $pcreURL
			fi
			tar -xvf $pcre --directory=$pcrePath
		fi
		cd $pcrePath/pcre-8.40
                mkdir -p $installedPcrePath
                ./configure --prefix=$installedPcrePath --enable-utf8 --enable-pcre16 --enable-pcre32 --enable-pcregrep-libz --enable-pcregrep-libbz2
                make
                make install
                export CPATH=$CPATH:$installedPcrePath/include
                export LIBRARY_PATH=$LIBRARY_PATH:$installedPcrePath/lib
                export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedPcrePath/lib
	fi

	curlURL=https://curl.haxx.se/download/curl-7.55.1.tar.gz
	curlPath=$libsPath/curl
	curl=$curlPath/curl-7.55.1.tar.gz

	if [ -e $installedCurlPath ] 
	then
		echo Curl already installed
	else
		if [ -e $curlPath/curl-7.55.1 ]
		then
			echo Curl already unpacked
		else
			if [ -e $curl ] 
			then
				echo Curl already downloaded
			else
				mkdir -p $curlPath
	                        wget --no-check-certificate -P $curlPath $curlURL
			fi
			tar -xvf $curl --directory=$curlPath
		fi
		cd $curlPath/curl-7.55.1
                mkdir -p $installedCurlPath
                ./configure --prefix=$installedCurlPath
                make
                make install
                export CPATH=$CPATH:$installedCurlPath/include
                export LIBRARY_PATH=$LIBRARY_PATH:$installedCurlPath/lib
                export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedCurlPath/lib
	fi
fi

export CPATH=$CPATH:$installedBzip2Path/include
export LIBRARY_PATH=$LIBRARY_PATH:$installedBzip2Path/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedBzip2Path/lib

export CPATH=$CPATH:$installedLzmaPath/include
export LIBRARY_PATH=$LIBRARY_PATH:$installedLzmaPath/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedLzmaPath/lib

export CPATH=$CPATH:$installedPcrePath/include
export LIBRARY_PATH=$LIBRARY_PATH:$installedPcrePath/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedPcrePath/lib

export CPATH=$CPATH:$installedCurlPath/include
export LIBRARY_PATH=$LIBRARY_PATH:$installedCurlPath/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$installedCurlPath/lib

if grep -q 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/taskR/libs/installed/pcre/lib/' $HOME/.bash_profile  
then
	echo Variables already added to .bash_profile
else
	echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/taskR/libs/installed/pcre/lib/' >> $HOME/.bash_profile
	echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/taskR/libs/installed/lzma/lib/' >> $HOME/.bash_profile
fi

installationPath=$path/installed
mkdir -p $installationPath
cd $path/R-$1
./configure --prefix=$installationPath
make
make install

softPath=$HOME/soft
linkPath=$installationPath/bin/R

mkdir -p $softPath
cd $softPath
ln -s $linkPath scriptR-$1

if  grep -q 'PATH=$PATH:$HOME/soft' $HOME/.bash_profile 
then
        echo Links already added to PATH
else
	echo 'PATH=$PATH:$HOME/soft' >> $HOME/.bash_profile
        echo 'export PATH' >> $HOME/.bash_profile
fi

echo Installation finished
