#!/bin/bash

if [ $# -eq 1 ]
then
	echo Choosed version for download is $1
else
	echo No version found, so help should be somewhere here
	echo ./pythonScript.sh \<version\> \- it will Install Python on your PC
	exit 0
fi

path=$HOME/taskPython/Python_$1
archive=$path/Python-$1.tar.xz
URL=http://www.python.org/ftp/python/$1/Python-$1.tar.xz

if [ -e $path ]
then
	echo Directory already exists
else
	mkdir -p $path
fi

if [ -e $archive ]
then
	echo Archive already downloaded
else
	wget -P $path $URL
fi

echo Installation started

installationPath=$path/installed

tar -xvf $archive --directory=$path
mkdir -p $installationPath
cd $path/Python-$1
./configure --prefix=$installationPath
make install

softPath=$HOME/soft
linkPath=$installationPath/bin/python${1:0:3}

mkdir -p $softPath
cd $softPath
ln -s $linkPath scriptPython$1

echo Installation finished
