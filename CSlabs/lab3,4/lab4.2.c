#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_LENGTH_OF_PLANET 100
#define MAX_SIZE_OF_TMP_INFO 30

FILE* createInput(const char* format, const int number, char* title) {
	char* txt = (char*) malloc(MAX_SIZE_OF_TMP_INFO * sizeof(char));
	strcpy(txt, ".txt");
	
	sprintf(title, format, number);
	title = strcat(title, txt);
	FILE* input = fopen(title, "r");
	
	free(txt);
	
	return input;
}

void getLengthOfTitle(char* format) {
	int i;
	
	char* title = (char*) malloc(MAX_SIZE_OF_TMP_INFO * sizeof(char));
	
	for (i = 1; i < 20; i++) {
		memset(format, 0, MAX_SIZE_OF_TMP_INFO * sizeof(char)); // clearing format
		
		// making format
		if (i < 10) {					
			format[0] = '%';
			format[1] = '0';
			format[2] = i + '0';
			format[3] = 'd';
		}
		else {
			format[0] = '%';
			format[1] = '0';
			format[2] = i / 10 + '0';
			format[3] = i % 10 + '0';
			format[4] = 'd';
		}
		
		FILE* input = createInput(format, 1, title);
		
		if (input != NULL) {
			free(title);
			return;
		}
	}
	
	free(title);
}

void solve(const char* planet, const char* format) {
	char* title = (char*) malloc(MAX_SIZE_OF_TMP_INFO * sizeof(char));
	int number = 1;	
	FILE* input = createInput(format, number, title);
	char* S = (char*) malloc(sizeof(char) * MAX_LENGTH_OF_PLANET);
	
	while (input != NULL) {
		memset(S, 0, sizeof(char) * MAX_LENGTH_OF_PLANET);
		fgets(S, MAX_LENGTH_OF_PLANET, input);
		
		if (strcmp(S, planet) == 0) {
			fputs(strcat(title, "\n"), stdout);
		}
		
		number++;
		
		fclose(input);
		
		input = createInput(format, number, title);
	}
	
	free(S);
	free(title);
}

int main(int argc, char* argv[]) {
	char* format = (char*) malloc(MAX_SIZE_OF_TMP_INFO * sizeof(char));
	getLengthOfTitle(format);
	
	char* planet = (char*) malloc(MAX_LENGTH_OF_PLANET * sizeof(char));
	fgets(planet, MAX_LENGTH_OF_PLANET, stdin);
	planet[strlen(planet) - 1] = 0; // removing \n
	
	solve(planet, format);
	
	free(format);
	free(planet);
	
	return 0;
}
