#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define NMAX 105
#define LIFE_OF_CARLSON 1
#define WEIGHT_OF_CARLSON 60

void lifeCycle(const int* sweets, const int* physEx, const int sizeOfPhysEx, const int sizeOfSweets, int* Carlson) {
	int sweetCount = 0, exCount = 0; // Count of sweets/exercises that were eaten/done
	
	while (LIFE_OF_CARLSON) {
		
		if (*Carlson < 21) {
			printf("Current weight: %d\nCarlson dead from dystrophy :c", *Carlson);
			break;
		}
		
		if (*Carlson > 99) {
			printf("Current weight: %d\nCarlson succsessfully transformed to balloon", *Carlson);
			break;
		}
		
		int event = 0 + rand() % 2;
		// 1 - Carlson goes to the roof
		// 0 - Carlson goes to the kitchen
		
		if (event) {
			if (exCount == sizeOfPhysEx) {
				printf("All burglars were dispersed\nEnough for today c:");
				break;
			}
			
			*Carlson -= physEx[exCount];
			exCount++;
			
		}
		else {
			if (sweetCount == sizeOfSweets) {
				printf("All jam was eaten\nThat was delicious c:");
				break;
			}
						
			*Carlson += sweets[sweetCount];
			sweetCount++;
		}
	}
}

void init(int** const A, const int size) {
	*A = (int*) malloc(sizeof(int) * size);
}

void putValues(int* A, const int size) {
	int i;
	for (i = 0; i < size; i++) {
		A[i] = 1 + rand() % 31;
	}
}

int main(int argc, char* argv[]) {	
	int Carlson = WEIGHT_OF_CARLSON;

	srand((unsigned) time(NULL));
	
	int sizeOfSweets = 1 + rand() % 11;
	int* sweets;
	init(&sweets, sizeOfSweets);
	putValues(sweets, sizeOfSweets);
	
	int sizeOfPhysEx = 1 + rand() % 11;
	int* physEx;
	init(&physEx, sizeOfPhysEx);
	putValues(physEx, sizeOfPhysEx);

	lifeCycle(sweets, physEx, sizeOfPhysEx, sizeOfSweets, &Carlson);
	
	free(sweets);
	free(physEx);
	
	return 0;
}
