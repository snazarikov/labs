#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))

#define EPS 1e-5
#define COUNT_OF_FUNCTIONS 4

double funcWcos(double x) {
	return x * cos(x);
}

double funcWtan(double x) {
	return x * x + x - tan(x);
}

double funcWexp(double x) {
	return exp(x) + 3;
}

// https://ru.wikipedia.org/wiki/�����_���������������
double calcIntegral(const double a, const double b, const int n, double (*func)(double)) {
	int i;
	double result, h;

	result = 0;

	h = (b - a) / n; 

	for(i = 0; i < n; i++) {
		result += func(a + h * (i + 0.5));
	}

	result *= h;

	return result;
}

double findGreatestSquare(const double S, const double a, const double b) {
	int i, n = 1;
	double integral1, integral2, answer = 0;
	
	typedef double (*func)(double);
	func A[] = {sin, funcWcos, funcWtan, funcWexp};
	
	for (i = 0; i < COUNT_OF_FUNCTIONS; i++) {
		do {
			n *= 2;
			
			integral1 = calcIntegral(a, b, n, A[i]);
			integral2 = calcIntegral(a, b, n * 2, A[i]);
		}
		while (fabs(integral1 - integral2) >= EPS);
		
		if (integral2 > answer && integral2 < S) {
			answer = integral2;
		}
	}
	
	return answer;
}

int main(int argc, char* argv[]) {		
	double S, a, b, y;
	
	scanf("%lf %lf %lf %lf", &S, &a, &b, &y);
	
	printf("The greatest square is: %lf\n", findGreatestSquare(S, a, b) - y * (b - a));
	
	return 0;
}
