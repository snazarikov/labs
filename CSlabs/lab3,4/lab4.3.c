#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_LENGTH_OF_DREAM 150
#define MAX_COUNT_OF_DREAMS 100

void freeMem(char** dreams, const int size) {
	int i;
	
	for (i = 0; i < size; i++) {
		free(dreams[i]);
	}	
}

int getData(char** dreams) {
	char* S = (char*) malloc(MAX_LENGTH_OF_DREAM * sizeof(char));
	int counter = 0;
	
	while (fgets(S, MAX_LENGTH_OF_DREAM, stdin)) {
		if (S[0] == 10) {
			continue;
		}
		dreams[counter] = (char*) malloc((strlen(S) + 1) * sizeof(char));
		strcpy(dreams[counter], S);		
		counter++;
	}
	
	free(S);
	
	return counter;
}

int removeCopy(char** dreams, int* isUnique, const int size) {
	int i, j, uniqueCount = size;
	
	for (i = 0; i < size - 1; i++) {
		for (j = i + 1; j < size; j++) {
			if (strcmp(dreams[i], dreams[j]) == 0) {
				isUnique[i] = 0;
				uniqueCount--;
				break;
			}
		}
	}
	
	return uniqueCount;
}

void printUnique(char** dreams, const int* isUnique, const int size, int uniqueCount) {
	int i, lineBreak = uniqueCount / 2;
	
	for (i = 0; i < size; i++) {
		if (isUnique[i]) {
			fputs(dreams[i], stdout);
			uniqueCount--;
			if (uniqueCount == lineBreak) {
				printf("\n");
			}
		}
	}
}

int main(int argc, char* argv[]) {
	freopen("input.txt", "r", stdin);
	
	char** dreams = (char**) malloc(MAX_COUNT_OF_DREAMS * sizeof(char*));

	int countOfDreams = getData(dreams); // getData returns count of dreams from input
	
	int* isUnique = (int*) malloc((countOfDreams + 1) * sizeof(int));
	memset(isUnique, 1, (countOfDreams + 1) * sizeof(int));
	// isUnique:
	// 1 - that dream is unique
	// 0 - that dream is a copy of another one
	
	int uniqueCount = removeCopy(dreams, isUnique, countOfDreams);  // remove copy returns count of dreams that are unique
	
	printUnique(dreams, isUnique, countOfDreams, uniqueCount);
	
	freeMem(dreams, countOfDreams);
	free(isUnique);
	free(dreams);
	
	return 0;
}
