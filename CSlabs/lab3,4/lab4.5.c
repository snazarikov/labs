#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LNGTH 10005
#define MAX_COUNT_OF_TMP_ARRAYS 5

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define clear(a) (memset(a, 0, MAX_LNGTH * sizeof(int)))
#define copy(a, b) (memcpy(a, b, MAX_LNGTH * sizeof(int)))

int** init(const int count) 
{
	int** res = malloc(sizeof(int*) * count);	
	memset(res, 0, sizeof(int*) * count);
	
	int i;
	
	for (i = 0; i < count; i++) 
	{ 
		res[i] = (int*) malloc(MAX_LNGTH * sizeof(int)); 
		clear(res[i]); 
	}
	
	return res;
}

void freeMem(int** A, const int count)
{
	int i;
	
	for (i = 0; i < count; i++) 
	{
		free(A[i]);
	}	
}

void makeOne(int* A)
{
	clear(A);
	A[0] = 1;
	A[1] = 1;
}

int compareLong(const int* A, const int* B) 
{
    if(A[0] > B[0]) 
	{
    	return 1;
	}
	
    if(A[0] < B[0]) 
	{
		return -1;	
	}
	
	int i;

    for(i = A[0]; i > 0; i--) 
	{
        if(A[i] > B[i]) return 1;
        if(A[i] < B[i]) return -1;
    }

    return 0;
}

void scanLong(int* A) 
{
	char* S = (char*) malloc(MAX_LNGTH * sizeof(char));	
	memset(S, 0, MAX_LNGTH * sizeof(char));

	scanf("%s", S);
	A[0] = strlen(S);
	
	int i, j;

	for (i = A[0] - 1, j = 1; i >= 0; i--, j++) 
	{
		A[j] = S[i] - '0';
	}
	
	free(S);
}

void printLong(const int* A) 
{
	int i;

	for (i = A[0]; i; i--) 
	{
		printf("%c", A[i] + '0');
	}
	printf("\n");
}

void additionLong(const int* A, const int* B, int* Z) 
{
	int d, i;
	
    clear(Z);
    
	Z[0] = max(A[0], B[0]);
	d = 0;

	for (i = 1; i <= Z[0]; i++) 
	{
		d += (A[i] + B[i]);
		Z[i] = d % 10;
		d /= 10;
	}

	if (d) 
	{
		Z[++Z[0]] = d;
	}
}

void divisionLong(const int* A, const int shortNum, int* Z) 
{
	int d, i;
	
    clear(Z);
	Z[0] = A[0];
	d = 0;

	for (i = A[0]; i; i--) 
	{
		d = d * 10 + A[i];
		Z[i] = d / shortNum;
		d %= shortNum;
	}

	while (Z[0] > 1 && !Z[Z[0]]) 
	{
		Z[0]--;
	}
}

void multiplicationLong(const int* A, const int* B, int* Z) 
{
	int d, i, j;
	
    clear(Z);

    for (i = 1; i <= A[0]; i++) 
	{
    	for (j = 1; j <= B[0]; j++) 
		{
    		Z[i + j - 1] += A[i] * B[j];
		}
	}
	
    Z[0] = A[0] + B[0] - 1;
    
    for (i = 1; i <= Z[0]; i++) 
	{
    	Z[i + 1] += Z[i] / 10;
    	Z[i] %= 10;
	}

	d = Z[Z[0]+1];
	
	while (d) 
	{
		Z[++Z[0]] = d % 10;
		d /= 10;
	}
}

void makeMultiplication(int* A, const int* B, int* C) 
{
	multiplicationLong(A, B, C);
	copy(A, C);
}

void makeDivision(const int* A, int* B, int* C, const int number) 
{
	divisionLong(A, number, B);
	copy(C, B);
}

void powerLong(const int* A, int pow, int* result) 
{
	int* help1 = (int*) malloc(MAX_LNGTH * sizeof(int));
	int* help2 = (int*) malloc(MAX_LNGTH * sizeof(int));
	
	clear(help1);
	clear(help2);
	
	memcpy(help1, A, MAX_LNGTH * sizeof(int));
	
	makeOne(result);

	while(pow) 
	{
		clear(help2);
		if (pow % 2 == 0) 
		{
			pow /= 2;
			makeMultiplication(help1, help1, help2);
		}
		else 
		{
			pow--;
			makeMultiplication(result, help1, help2);
		}
	}
	
	free(help1);
	free(help2);
}

void rootLong(const int* num, int* A, int* B, const int groupCount, int* result) 
{
	int** arrays = init(MAX_COUNT_OF_TMP_ARRAYS - 1);

	powerLong(A, groupCount, arrays[1]);

	while (1) 
	{
		clear(arrays[3]);		
		additionLong(A, B, arrays[1]);		
		makeDivision(arrays[1], arrays[0], arrays[2], 2);

		if (compareLong(arrays[0], A) == 0 || compareLong(arrays[0], B) == 0) 
		{
			copy(result, A);
			freeMem(arrays, MAX_COUNT_OF_TMP_ARRAYS - 1);
			free(arrays);
			return;
		}

		powerLong(arrays[0], groupCount, arrays[3]);

		if ((compareLong(arrays[1], num) == 1 && compareLong(arrays[3], num) == 1) || ((compareLong(arrays[1], num) == -1 && compareLong(arrays[3], num) == -1)) || compareLong(arrays[1], num) == 0 || compareLong(arrays[3], num) == 0) 
		{
			memcpy(A, arrays[2], MAX_LNGTH * sizeof(int));
		}

		else 
		{
			memcpy(B, arrays[2], MAX_LNGTH * sizeof(int));
		}
	}
}

void makeInterval(int* A, int* B, const int groupCount, const int lengthOfMult) 
{
	B[0] = lengthOfMult / groupCount;
	
	if (lengthOfMult % groupCount != 0) 
	{
		B[0]++;
	}
	
	B[++B[0]] = 1;
	A[0] = B[0] - 1;
	A[A[0]] = 1;
}

int main(int argc, char* argv[]) 
{
	FILE* input = fopen("input.txt", "r");
	if (!input) {	// checking the existence of input file
		printf("Please create input file \"input.txt\" and write down the data into it");
		return 0;
	}
	
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int elemCount, groupCount, i, j;	
	int** arrays = init(MAX_COUNT_OF_TMP_ARRAYS);
	
	makeOne(arrays[4]);

	scanf("%d", &groupCount);

	for (i=0; i<groupCount; i++)
	{
		scanf("%d\n", &elemCount);
		clear(arrays[1]);

		for (j=0; j<elemCount; j++)
		{
			scanLong(arrays[0]);
			additionLong(arrays[0], arrays[1], arrays[3]);
			clear(arrays[0]);
			copy(arrays[1], arrays[3]);
		}

		makeDivision(arrays[1], arrays[3], arrays[1], elemCount);
		makeMultiplication(arrays[4], arrays[1], arrays[2]);
	}
	
	clear(arrays[0]);
	clear(arrays[1]);
	clear(arrays[2]);
	
	makeInterval(arrays[0], arrays[1], groupCount, arrays[4][0]);

	if (groupCount == 1)  // result^1
	{
		printLong(arrays[4]);
	}
	else 				 // result^(1/a)
	{
		rootLong(arrays[4], arrays[0], arrays[1], groupCount, arrays[3]);
		printLong(arrays[3]);
	}
	
	freeMem(arrays, 5);
	free(arrays);
	fclose(input);

	return 0;
}
