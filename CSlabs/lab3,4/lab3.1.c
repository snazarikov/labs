#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define NMAX 105

void task1() {
	int i;

	for (i = 97; i <= 122; i++) {
		printf("%d %c%c ", i, i - 32, i);
		if (!(i % 4)) {
			printf("\n");
		}
	}
	printf("%d \' \' %d \\n", 32, 10);
}

void task2(int n) {
	int i;
	unsigned long long A[NMAX];

	A[0] = 0;
	A[1] = 1;
	printf("%lld %lld ", A[0], A[1]);
	for (i = 2; i <= n; i++) {
		A[i] = A[i - 1] + A[i - 2];
		printf("%lld ", A[i]);
	}
}

void task3() {
	int whole;
	double decimal = 0, help;

	srand((unsigned) time(NULL));

	whole = 1 + rand() % 100;

	help = 0 + rand() % (int)1e4;
	decimal += help * 1e-12;
	help = 0 + rand() % (int)1e4;
	decimal += help * 1e-8;
	help = 0 + rand() % (int)1e4;
	decimal += help * 1e-4;
	help = 0 + rand() % (int)2;
	decimal += help * 1e-12;

	printf("%d\n%.12lf\n", whole, decimal);
}

int main(int argc, char* argv[]) {

	task1();
	printf("\n\n");
	task2(10);
	printf("\n\n");
	task3();

	return 0;
}
