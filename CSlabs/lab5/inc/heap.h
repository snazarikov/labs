typedef struct {
	int priority;
	void* data;
} heapElement;

typedef void* (*copy)(void*);
typedef void (*freeLocal)(void*);

typedef struct {
	int size;
	heapElement* array;
	copy copier;
	freeLocal cleaner;
} stdHeap;

stdHeap* makeHeap(stdHeap* heap, copy copier, freeLocal cleaner);
void makeHeapClear(stdHeap* heap);
void putElement(stdHeap* heap, heapElement newData);
heapElement extractMax(stdHeap* heap);
heapElement* heapSort(heapElement* newArray, int size, copy copier, freeLocal cleaner);
