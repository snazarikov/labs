#include <stdio.h>
#include <stdlib.h>
#include "heap.h"

typedef struct {
	int x;
	int y;
} pointType;

void* copyInt(void* origin) {
	int* res = malloc(sizeof(int));
	*res = *(int*)origin;
	return res;
}

void* copyPoint(void* origin) {
	pointType* res = malloc(sizeof(pointType));
	*res = *(pointType*)origin;
	return res;
}

void cleanInt(void* origin) {
	free(origin);
}

int main(void) {
	stdHeap* myHeap = makeHeap(myHeap, copyInt, cleanInt);

	int num1 = 7;
	int num2 = 2;
	int num3 = 10;
	int num4 = 5;
	int* intPointer1 = &num1;
	int* intPointer2 = &num2;
	int* intPointer3 = &num3;
	int* intPointer4 = &num4;

	heapElement tmp1;
	tmp1.priority = 5;
	tmp1.data = (int*)intPointer1;

	putElement(myHeap, tmp1);
	
	heapElement tmp2;
	tmp2.priority = 10;
	tmp2.data = (int*)intPointer2;

	putElement(myHeap, tmp2);
	
	heapElement tmp3;
	tmp3.priority = 7;
	tmp3.data = (int*)intPointer3;

	putElement(myHeap, tmp3);

	
	heapElement tmp4;
	tmp4.priority = 2;
	tmp4.data = (int*)intPointer4;

	putElement(myHeap, tmp4);

	heapElement maxInt = extractMax(myHeap);

	printf("KEKES %d %d\n", maxInt.priority, *((int*)(maxInt.data)));

	heapElement* testArray = malloc(sizeof(heapElement) * 5);
	testArray[0] = tmp1;
	testArray[1] = tmp2;
	testArray[2] = tmp3;
	testArray[3] = tmp4;

	heapSort(testArray, 4, copyInt, cleanInt);
	
	printf("\n");
	int i;
	for (i = 0; i < 4; ++i) {
		printf("sortedArray %d %d\n", testArray[i].priority, *((int*)(testArray[i].data)));
		cleanInt(testArray[i].data);
	}

	cleanInt(maxInt.data);
	free(testArray);
	makeHeapClear(myHeap);

	
	pointType p1;
	p1.x = 1;
	p1.y = 2;

	pointType p2;
	p2.x = 3;
	p2.y = 4;
	
	pointType p3;
	p3.x = 5;
	p3.y = 6;

	pointType* pPointer1 = &p1;
	pointType* pPointer2 = &p2;
	pointType* pPointer3 = &p3;

	heapElement tmp5;
	tmp5.priority = 1;
	tmp5.data = (pointType*)pPointer1;

	heapElement tmp6;
	tmp6.priority = 2;
	tmp6.data = (pointType*)pPointer2;

	heapElement tmp7;
	tmp7.priority = 3;
	tmp7.data = (pointType*)pPointer3;

	stdHeap* myHeap2 = makeHeap(myHeap2, copyPoint, cleanInt);

	putElement(myHeap2, tmp5);
	putElement(myHeap2, tmp6);
	putElement(myHeap2, tmp7);

	heapElement maxPoint = extractMax(myHeap2);

	printf("PEKES %d %d %d\n", maxPoint.priority, (*(pointType*)(maxPoint.data)).x, (*(pointType*)(maxPoint.data)).y);

	cleanInt(maxPoint.data);
	makeHeapClear(myHeap2);

	return 0;
}

