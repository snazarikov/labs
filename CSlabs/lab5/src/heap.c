#include <stdio.h>
#include <stdlib.h>
#include "heap.h"

#define SOME_START_SIZE 1

stdHeap* makeHeap(stdHeap* heap, copy copier, freeLocal cleaner) {
	heap = malloc(sizeof(stdHeap));

	heap->cleaner = cleaner;
	heap->copier = copier;

	heap->size = 0;
	heap->array = malloc(sizeof(heapElement) * SOME_START_SIZE);

	return heap;
}

int isEmpty(int heapSize) {
	if (heapSize <= 0) {
		return 1;
	}
	return 0;
}

void lift(stdHeap* heap, int childIndex) {
	int parentIndex;

	while (childIndex > 1) {
		parentIndex = childIndex / 2;


		if (heap->array[parentIndex].priority >= heap->array[childIndex].priority) {
			return;
		}

		heapElement tmp;
		tmp = heap->array[childIndex];
		heap->array[childIndex] = heap->array[parentIndex];
		heap->array[parentIndex] = tmp;

		childIndex = parentIndex;
	}
}

void sift(stdHeap* heap, int parentIndex) {
	int childIndex;
	while (2 * parentIndex <= heap->size) {
		childIndex = 2 * parentIndex;
		if (childIndex < heap->size && heap->array[childIndex + 1].priority > heap->array[childIndex].priority) {
			childIndex++;
		}

		if (heap->array[parentIndex].priority >= heap->array[childIndex].priority) {
			return;
		}
		
		heapElement tmp;
		tmp = heap->array[childIndex];
		heap->array[childIndex] = heap->array[parentIndex];
		heap->array[parentIndex] = tmp;

		parentIndex = childIndex;
	}
}

void putElement(stdHeap* heap, heapElement newData) {
	heap->size++;

	int help = heap->size / SOME_START_SIZE;
	if (help * SOME_START_SIZE != heap->size) {
		help++;
	}

	if (heap->size >= SOME_START_SIZE * help) {
		heap->array = realloc(heap->array, sizeof(heapElement) * (help + 1) * SOME_START_SIZE); 
	}

	heap->array[heap->size].data = heap->copier(newData.data);
	heap->array[heap->size].priority = newData.priority;

	lift(heap, heap->size);
}

heapElement* heapSort(heapElement* newArray, int size, copy copier, freeLocal cleaner) {
	int i;

	stdHeap* newHeap = makeHeap(newHeap, copier, cleaner);

	for (i = 0; i < size; ++i) {
		putElement(newHeap, newArray[i]);
	}

	for (i = 0; i < size; ++i) {
		newArray[i] = extractMax(newHeap);
	}

	makeHeapClear(newHeap);

	return newArray;
}

heapElement extractMax(stdHeap* heap) {
	heapElement max;
	if (!isEmpty(heap->size)) {
		max = heap->array[1];
		heap->array[1] = heap->array[heap->size];
		heap->size--;
		sift(heap, 1);
		return max;
	}
	printf("\nHeap is empty\n");
	return max;
}

void makeHeapClear(stdHeap* heap) {
	int i;
	for (i = 1; i <= heap->size; ++i) {
		heap->cleaner(heap->array[i].data);
	}
	free(heap->array);
	free(heap);
}
