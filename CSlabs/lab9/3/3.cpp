#include <iostream>
#include <fstream>
#include <vector>
#include <queue>

using namespace std;

int n, m;

int di[] = {1, -1, 0, 0};
int dj[] = {0, 0, 1, -1};

void getData(ifstream &in, vector<vector<int>> &A) {
    in >> n >> m;
    A.resize(n);
    char symbol;
    for (int i = 0; i < n; ++i) {
        A[i].resize(m);
        for (int j = 0; j < m; ++j) {
            in >> symbol;
            if (symbol == '#') {
                A[i][j] = 1;
            }
            else {
                A[i][j] = 0;
            }
        }
    }
}

void printData(vector<vector<int>> &A) {
    cout << n << " " << m << endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << A[i][j];
        }
        cout << endl;
    }
}

void bfs(pair<int, int> start, vector<vector<int>> &A) {
    queue<pair<int, int>> q;
    q.push(start);
    A[start.first][start.second] = 0;

    while (!q.empty()) {
        for (int d = 0; d < 4; ++d) {
            int ii = q.front().first + di[d];
            int jj = q.front().second + dj[d];

            if (ii >= 0 && ii < n && jj >= 0 && jj < m && A[ii][jj]) {
                pair<int, int> elem(ii, jj);
                A[ii][jj] = 0;
                q.push(elem);
            }
        }
        q.pop();
    }
}

int solve(vector<vector<int>> &A) {
    int amount = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (A[i][j]) {
                amount++;
                pair<int, int> elem(i, j);
                bfs(elem, A);
            }
        }
    }
    return amount;
}

int main() {
    ifstream in("input.txt");

    vector<vector<int>> A;
    getData(in, A);
//    printData(A);

    cout << solve(A);

    return 0;
}
