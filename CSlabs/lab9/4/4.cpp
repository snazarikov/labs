#include <iostream>
#include <fstream>
#include <map>

using namespace std;

void solve(ifstream &in, map<int, int> &A) {
    int n;
    in >> n;

    for (int i = 0; i < n; ++i) {
        int num;
        in >> num;
        A[num] = 1;
    }

    int m;
    in >> m;

    for (int i = 0; i < m; ++i) {
        int counter = 0;

        int k;
        in >> k;

        for (int j = 0; j < k; ++j) {
            int num;
            in >> num;

            if (A[num]) {
                ++counter;
            }
        }
        cout << counter;
    }
}

int main() {
    ifstream in("input.txt");

    map<int, int> myMap;
    solve(in, myMap);

    return 0;
}

