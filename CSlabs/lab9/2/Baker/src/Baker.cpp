#include "Baker.h"

Baker::Baker() {
    cout << "bakerConstructor" << endl;
    _pattyAmount = 0;
    _skill = 50;
    _satiety = 50;
    _exp = 0;
    patties = new Patty*[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        patties[i] = nullptr;
    }
}

Baker::~Baker() {
    cout << "bakerDestructor" << endl;
    for (int i = 0; i < _pattyAmount; ++i) {
        if (patties[i] != nullptr) {
            delete(patties[i]);
        }
    }
    delete[](patties);
}

Baker::Baker(const Baker& obj) {
    cout << "bakerConstructorCopy" << endl;
    _skill = obj._skill;
    _satiety = obj._satiety;
    _exp = obj._exp;
    _pattyAmount = obj._pattyAmount;
    patties = new Patty*[SIZE];
    for (int i = 0; i < obj._pattyAmount; ++i) {
        if (obj.patties[i] == nullptr) {
            patties[i] = nullptr;
        }
        else {
            patties[i] = new Patty(*obj.patties[i]);
        }
    }
}

Baker& Baker::operator= (const Baker& obj) {
    cout << "bakerOperator=" << endl;
    if (this == &obj) {
        return *this;
    }
    _skill = obj._skill;
    _satiety = obj._satiety;
    _exp = obj._exp;
    _pattyAmount = obj._pattyAmount;
    patties = new Patty*[SIZE];
    for (int i = 0; i < obj._pattyAmount; ++i) {
        if (obj.patties[i] == nullptr) {
            patties[i] = nullptr;
        }
        else {
            patties[i] = new Patty(*obj.patties[i]);
        }
    }
    return *this;
}

int Baker::getPattyAmount() const {
    return _pattyAmount;
}

int Baker::getExp() const {
    return _exp;
}

int Baker::getSkill() const {
    return _skill;
}

void Baker::setExp(int exp) {
    _exp = exp;
}

void Baker::setSkill(int skill) {
    _skill = skill;
}

void Baker::createPatty() {
    --_satiety;
    _exp += (_satiety + _skill) / 2;
    patties[_pattyAmount++] = new Patty();
}

void Baker::dropPatty(Baker& baker) {
    --_satiety;
    if (_pattyAmount > 0) {
        _exp += _satiety / 2;
        baker.patties[baker._pattyAmount++] = new Patty(*patties[--_pattyAmount]);
        delete(patties[_pattyAmount]);
        patties[_pattyAmount] = nullptr;
    }

}

void Baker::eatPatty() {
    _satiety += 3;
    for (int i = _pattyAmount - 1; i >= 0; --i) {
        if (patties[i] != nullptr) {
            _exp += patties[i]->getTasteRate();
            delete(patties[i]);
            patties[i] = nullptr;
            --_pattyAmount;
            break;
        }
    }
}
