#include "Patty.h"

int Patty::amount = 0;

Patty::Patty() {
    cout << "pattyConstructor" << endl;
    amount++;

    int random = 1 + rand() % 3;
    int rate = 1 + rand() % 100;
    switch (random) {
        case 1: {
            _name = "potato";
            _tasteRate = rate;
            break;
        }
        case 2: {
            _name = "meat";
            _tasteRate = rate;
            break;
        }
        case 3: {
            _name = "mushroom";
            _tasteRate = rate;
            break;
        }
    }
}

Patty::Patty(string name, int tasteRate): _name(name), _tasteRate(tasteRate) {
    cout << "pattyConstructor" << endl;
    amount++;
}

Patty::~Patty() {
    cout << "pattyDestructor" << endl;
}

Patty::Patty(const Patty& obj) {
    cout << "pattyConstructorCopy" << endl;
    _name = obj._name;
    _tasteRate = obj._tasteRate;
}

Patty& Patty::operator= (const Patty& obj) {
    cout << "pattyOperator=" << endl;
    if (this == &obj) {
        return *this;
    }
    _name = obj._name;
    _name = obj._tasteRate;
    return *this;
}

string Patty::getName() const {
    return _name;
}

int Patty::getTasteRate() const {
    return _tasteRate;
}
