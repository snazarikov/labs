#include <iostream>
#include <ctime>
#include "Patty.h"
#include "Baker.h"

using namespace std;

const int TASTE_RATE_SUP = 750;
const int EXP_WIN = 1000;

int bake(Baker bakerFirst, Baker bakerSecond) {
    int totalExp = 0;
    for (int i = 0; i < 25; ++i) {
        int totalTasteRate = 0;
        for (int j = 0; j < bakerFirst.getPattyAmount(); ++j) {
            if (bakerFirst.patties[i] != nullptr) {
                totalTasteRate += bakerFirst.patties[i]->getTasteRate();
            }
        }
        for (int j = 0; j < bakerSecond.getPattyAmount(); ++j) {
            if (bakerSecond.patties[i] != nullptr) {
                totalTasteRate += bakerSecond.patties[i]->getTasteRate();
            }
        }

        if (totalTasteRate >= TASTE_RATE_SUP) {
            return EXP_WIN;
        }

        int eventFirstBaker = 1 + rand() + 3;
        int eventSecondBaker = 1 + rand() + 3;

        switch (eventFirstBaker) {
            case 1: {
                bakerFirst.eatPatty();
                break;
            }
            case 2: {
                bakerFirst.createPatty();
                break;
            }
            case 3: {
                bakerFirst.dropPatty(bakerSecond);
                break;
            }
        }

        switch (eventSecondBaker) {
            case 1: {
                bakerSecond.eatPatty();
                break;
            }
            case 2: {
                bakerSecond.createPatty();
                break;
            }
            case 3: {
                bakerSecond.dropPatty(bakerFirst);
                break;
            }
        }
    }
    return bakerFirst.getExp() + bakerSecond.getExp();
}

int main() {
    srand((unsigned)time(NULL));

    Baker bakerFirst, bakerSecond;

    int winsAmount = 0;

    while (winsAmount < 15) {
        for (int i = 0; i < 10; ++i) {
            bakerFirst.createPatty();
            bakerSecond.createPatty();
        }

        int expTraining = bake(bakerFirst, bakerSecond);

        bakerFirst.setSkill(bakerFirst.getSkill() + 1);
        bakerSecond.setSkill(bakerSecond.getSkill() + 1);
        bakerFirst.setExp(bakerFirst.getExp() + expTraining);
        bakerSecond.setExp(bakerSecond.getExp() + expTraining);

        if (expTraining == EXP_WIN) {
                winsAmount++;
        }
    }

    return 0;
}
