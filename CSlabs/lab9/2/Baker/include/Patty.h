#ifndef PATTY_H
#define PATTY_H

#include <iostream>
#include <string>

using namespace std;

class Patty
{
    public:
        Patty();
        Patty(string name, int tasteRate);
        ~Patty();
        Patty(const Patty& other);
        Patty& operator= (const Patty& obj);

        static int amount;

        static Patty moreDelecious(Patty first, Patty second);

        string getName() const;
        int getTasteRate() const;
    private:
        string _name;
        int _tasteRate;

};

#endif // PATTY_H
