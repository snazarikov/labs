#ifndef BAKER_H
#define BAKER_H

#include "Patty.h"

#define SIZE 1000

class Baker
{
    public:
        Baker();
        ~Baker();
        Baker(const Baker& other);
        Baker& operator= (const Baker& obj);

        Patty** patties;

        void createPatty();
        void dropPatty(Baker& baker);
        void eatPatty();

        int getPattyAmount() const;
        int getExp() const;
        int getSkill() const;
        void setExp(int exp);
        void setSkill(int skill);
    private:
        int _skill;
        int _satiety;
        int _exp;
        int _pattyAmount;
};

#endif // BAKER_H
