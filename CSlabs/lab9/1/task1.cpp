#include <cstdio>
#include <iostream>
#include <fstream>

using namespace std;

const int NMAX = 100;

void clear(char* S, int size) {
    for (int i = 0; i < size; ++i) {
        S[i] = 0;
    }
}

void clear(int* A, int size) {
    for (int i = 0; i < size; ++i) {
        A[i] = 0;
    }
}

int getLength(char* S) {
    int i = -1;
    while (S[++i]);
    return i;
}

int convertToNumeral(char* S, int* A) {
    clear(A, NMAX);

    int length = getLength(S);

    for (int i = 0; i < length; i += 2) {
        A[i / 2] = (S[i] - '0') * 10 + (S[i + 1] - '0');
    }

    return length / 2;
}

int palindromity(int* A, int size) {
    for (int i = 0; i < size / 2; ++i) {
        if (A[i] != A[size - 1 - i]) {
            return 0;
        }
    }
    return 1;
}

int palindromity(char* S, int size) {
    for (int i = 0; i < size / 2; ++i) {
        if (S[i] != S[size - 1 - i]) {
            return 0;
        }
    }
    return 1;
}

int increasingSequence(int* A, int size) {
    int difference = A[1] - A[0];
    for (int i = 1; i < size - 1; ++i) {
        if (A[i + 1] - A[i] != difference) {
            return 0;
        }
    }
    if (difference) {
        return 1;
    }
    return 0;
}

int increasingSequence(char* S, int size) {
    int difference = S[1] - S[0];
    for (int i = 1; i < size - 1; ++i) {
        if (S[i + 1] - S[i] != difference) {
            return 0;
        }
    }
    if (difference) {
        return 1;
    }
    return 0;
}

int maxElement(int* A, int size) {
    int max = 0;
    for (int i = 0; i < size; ++i) {
        if (A[i] > max) {
            max = A[i];
        }
    }
    return max;
}

char maxElement(char* S, int size) {
    char max = 0;
    for (int i = 0; i < size; ++i) {
        if (S[i] > max) {
            max = S[i];
        }
    }
    return max;
}

int middleElement(int* A, int size) {
    int sum = 0;
    for (int i = 0; i < size; ++i) {
        sum += A[i];
    }
    return sum / size;
}

char middleElement(char* S, int size) {
    return S[size / 2];
}

solve(ifstream &in) {
    char symbol;

    char* S = new char[NMAX];
    int* A = new int[NMAX];

    clear(S, NMAX);

    while(in >> S) {
        if (S[0] >= '0' && S[0] <= '9') {
            int sizeDigitSequence = convertToNumeral(S, A);
            if (palindromity(A, sizeDigitSequence)) {
                cout << maxElement(A, sizeDigitSequence) << " ";
            }
            else if (increasingSequence(A, sizeDigitSequence)) {
                cout << middleElement(A, sizeDigitSequence) << " ";
            }
            else {
                cout << S << " ";
            }
        }
        else {
            int sizeLetterSequence = getLength(S);
            if (palindromity(S, sizeLetterSequence)) {
                cout << maxElement(S, sizeLetterSequence) << " ";
            }
            else if (increasingSequence(S, sizeLetterSequence)) {
                cout << middleElement(S, sizeLetterSequence) << " ";
            }
            else {
                cout << S << " ";
            }
        }
        clear(S, NMAX);
    }

    delete[] S;
    delete[] A;
}

int main() {
    ifstream in("input.txt");
    solve(in);
    in.close();
    return 0;
}
