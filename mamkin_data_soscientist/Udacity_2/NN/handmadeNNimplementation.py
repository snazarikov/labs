import numpy as np

class NeuralNetwork(object):
    def __init__(self, input_nodes, hidden_nodes, output_nodes, learning_rate):
        # Set number of nodes in input, hidden and output layers.
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = output_nodes

        # Initialize weights
        self.weights_input_to_hidden = np.random.normal(0.0, self.input_nodes**-0.5, (self.input_nodes, self.hidden_nodes))
        self.weights_hidden_to_output = np.random.normal(0.0, self.hidden_nodes**-0.5, (self.hidden_nodes, self.output_nodes))
        self.lr = learning_rate

        def sigmoid(x):
            return 1 / (1 + np.exp(-x))
        self.activation_function = sigmoid

        def sigmoid_derivative(x):
            return sigmoid(x) * (1 - sigmoid(x))
        self.activation_function_derivative = sigmoid_derivative

    def train(self, features, targets):
        # print('CYKA', targets)
        records_amount = features.shape[0]
        delta_weights_i_h = np.zeros(self.weights_input_to_hidden.shape)
        delta_weights_h_o = np.zeros(self.weights_hidden_to_output.shape)

        for X, y in zip(features, targets):
            final_outputs, hidden_outputs = self.forward_pass_train(X)  # Implement the forward pass function below
            # Implement the backproagation function below
            delta_weights_i_h, delta_weights_h_o = self.backpropagation(final_outputs, hidden_outputs,
                                                                        X, y,
                                                                        delta_weights_i_h, delta_weights_h_o)

        self.update_weights(delta_weights_i_h, delta_weights_h_o, records_amount)

    def forward_pass_train(self, X):
        hidden_inputs = X  # signals into hidden layer
        hidden_outputs = np.matmul(hidden_inputs, self.weights_input_to_hidden)  # signals from hidden layer

        final_inputs = self.activation_function(hidden_outputs)  # signals into final output layer
        final_outputs = np.matmul(final_inputs, self.weights_hidden_to_output)  # signals from final output layer

        return final_outputs, hidden_outputs

    def backpropagation(self, final_outputs, hidden_outputs, X, y, delta_weights_i_h, delta_weights_h_o):
        # To make math with dim-s OK
        final_outputs_copy = np.reshape(final_outputs, (1, 1))
        hidden_outputs_copy = np.reshape(hidden_outputs, (1, self.hidden_nodes))
        X_copy = np.reshape(X, (1, self.input_nodes))
        y_copy = np.reshape(y, (1, 1))

        # Backprop through out to hidden layer
        error = (y_copy - final_outputs_copy)
        output_error_term = error * 1

        # Backprop through hidden to input lyer
        hidden_error = np.matmul(output_error_term, self.weights_hidden_to_output.T)
        deriv = self.activation_function_derivative(hidden_outputs_copy)
        hidden_error_term = hidden_error * deriv

        # Weight step (input to hidden)
        delta_weights_i_h += np.matmul(X_copy.T, hidden_error_term)
        # Weight step (hidden to output)
        delta_weights_h_o += np.matmul(self.activation_function(hidden_outputs_copy.T), output_error_term)

        return delta_weights_i_h, delta_weights_h_o

    def update_weights(self, delta_weights_i_h, delta_weights_h_o, records_amount):
        self.weights_hidden_to_output += self.lr * delta_weights_h_o  # update hidden-to-output weights with gradient descent step
        self.weights_input_to_hidden += self.lr * delta_weights_i_h # update input-to-hidden weights with gradient descent step

    def run(self, features):
        # print(features)
        hidden_inputs = features  # signals into hidden layer
        hidden_outputs = np.matmul(hidden_inputs, self.weights_input_to_hidden)  # signals from hidden layer

        final_inputs = self.activation_function(hidden_outputs)  # signals into final output layer
        final_outputs = np.matmul(final_inputs, self.weights_hidden_to_output)  # signals from final output layer

        return final_outputs