
# coding: utf-8

# In[1]:


# These are all the modules we'll be using later. Make sure you can import them# These 
# before proceeding further.
from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle


# In[2]:


# Reload the data we generated in 1_notmnist.ipynb.

pickle_file = 'notMNIST.pickle'

with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']
    del save  # hint to help gc free up memory
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)


# In[3]:


# Reformat into a shape that's more adapted to the models we're going to train:

# data as a flat matrix,
# labels as float 1-hot encodings.

image_size = 28
num_labels = 10

def reformat(dataset, labels):
    dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
#     Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
    labels = (np.arange(num_labels) == labels[:,None]).astype(np.float32)
    return dataset, labels

train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)

print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)


# In[4]:


def print_config(batch_size, step_amount, neuron_amount = 0, lr = 0, reg_param = 0, drop = 0) :
    print("Config:", batch_size, step_amount, neuron_amount, lr, reg_param, drop)


# In[5]:


def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1)) / predictions.shape[0])


# In[6]:


# LOGISTIC START

batch_size = 128
learning_rate = 0.5
regularization_param = 0.002

graph = tf.Graph()
with graph.as_default():

#     Input data. For the training data, we use a placeholder that will be fed
#     at run time with a training minibatch.
    tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size * image_size))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)
    
#     Variables.
    weights = tf.Variable(tf.truncated_normal([image_size * image_size, num_labels]))
    biases = tf.Variable(tf.zeros([num_labels]))
    
#     Training computation.
    logits = tf.matmul(tf_train_dataset, weights) + biases
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))
    loss += regularization_param * (tf.nn.l2_loss(weights) + tf.nn.l2_loss(biases))
    
#     Optimizer.
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
    
#     Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = tf.nn.softmax(tf.matmul(tf_valid_dataset, weights) + biases)
    test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)


# In[7]:


num_steps = 3001

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print("Initialized")
    print("LogClass w/o everything")
    
    for step in range(num_steps):
#         Pick an offset within the training data, which has been randomized.
#         Note: we could use better randomization across epochs.
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        
#         Generate a minibatch.
        batch_data = train_dataset[offset:(offset + batch_size), :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        
#         Prepare a dictionary telling the session where to feed the minibatch.
#         The key of the dictionary is the placeholder node of the graph to be fed,
#         and the value is the numpy array to feed to it.
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        
        if (step % 500 == 0):
            print("Minibatch loss at step %d: %f" % (step, l))
            print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
            print("Validation accuracy: %.1f%%" % accuracy(valid_prediction.eval(), valid_labels))
        
    print("Test accuracy: %.4f%%" % accuracy(test_prediction.eval(), test_labels))
    
# LOGISTIC FINISH


# In[8]:


def get_logits(W_hidden, W_out, b_hidden, b_out, dataset):
    input_hidden = tf.nn.relu(tf.matmul(dataset, W_hidden) + b_hidden)
    logits = tf.matmul(input_hidden, W_out) + b_out
    return logits

def get_l2_loss(W_hidden, W_out, b_hidden, b_out):
    return regularization_param * (tf.nn.l2_loss(W_hidden) + tf.nn.l2_loss(W_out) + tf.nn.l2_loss(b_hidden) + tf.nn.l2_loss(b_out))


# In[1]:


# 1LNN START

batch_size = 128 #128
hidden_neurons_amount = 1024
learning_rate = 0.05
regularization_param = 0.00165

graph = tf.Graph()
with graph.as_default():
      
#     Input data. For the training data, we use a placeholder that will be fed
#     at run time with a training minibatch.
    tf_train_dataset = tf.placeholder(tf.float32,shape=(batch_size, image_size * image_size))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)
    
#     Variables.
    weights_hidden = tf.Variable(tf.truncated_normal([image_size * image_size, hidden_neurons_amount]))
    weights_out = tf.Variable(tf.truncated_normal([hidden_neurons_amount, num_labels]))

    biases_hidden = tf.Variable(tf.zeros([hidden_neurons_amount]))
    biases_out = tf.Variable(tf.zeros([num_labels]))

    
#     Training computation
    logits = get_logits(weights_hidden, weights_out, biases_hidden, biases_out, tf_train_dataset)

    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))
    loss += get_l2_loss(weights_hidden, weights_out, biases_hidden, biases_out)
    
#     Optimizer.
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
#     optimizer = tf.train.AdamOptimizer().minimize(loss)
    
#     Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    
    valid_prediction = get_logits(weights_hidden, weights_out, biases_hidden, biases_out, tf_valid_dataset)
    test_prediction = get_logits(weights_hidden, weights_out, biases_hidden, biases_out, tf_test_dataset)


# In[2]:


num_steps = 40001 #3801

import timeit
start_time = timeit.default_timer()

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print("Initialized")
    print("1LNN w/o dropout")
    
    for step in range(num_steps):
#         Pick an offset within the training data, which has been randomized.
#         Note: we could use better randomization across epochs.
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        
#         Generate a minibatch.
        batch_data = train_dataset[offset:(offset + batch_size), :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        
#         Prepare a dictionary telling the session where to feed the minibatch.
#         The key of the dictionary is the placeholder node of the graph to be fed,
#         and the value is the numpy array to feed to it.
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        
        if (step % 4000 == 0):
            print("Minibatch loss at step %d: %f" % (step, l))
            print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
            print("Validation accuracy: %.1f%%" % accuracy(valid_prediction.eval(), valid_labels))
    
    print("Final loss: %f" % l)
    
    test_pred = test_prediction.eval()
    print("Test accuracy: %.1f%%" % accuracy(test_pred, test_labels))
    
    elapsed = timeit.default_timer() - start_time
    print("Time:", elapsed)
    
# 1LNN FINISH


# In[11]:


# KARTINOCHKI

import matplotlib.pyplot as plt
import random

# print(type(test_dataset))
# print(type(test_dataset[0]))
# print(test_dataset.shape)
# print(test_dataset[0].shape)

# print(type(test_labels))
# print(test_labels.shape)
# print(type(test_pred))
# print(test_pred.shape)

# print(test_pred[0])

colors = ['PuRd', 'Greys', 'flag', 'prism', 'cubehelix', 'gist_stern', 'tab20b', 'seismic', 'PRGn', 'cool', 'BuPu']

def disp_sample_dataset(dataset, labels, labels_pred, title=None):
    fig = plt.figure()
    if title: fig.suptitle(title, fontsize=16, fontweight='bold')
    items = random.sample(range(len(labels)), 20)
    for i, item in enumerate(items):
        plt.subplot(2, 10, i + 1)
        plt.axis('off')
        
        true_label = -1
        pred_label = -1
        some_max = -1
        for j in range(10):
            if labels[item][j] == 1.:
                true_label = j
            if labels_pred[item][j] > some_max:
                some_max = labels_pred[item][j]
                pred_label = j
#         color = random.randint(0, len(colors) - 1)    
        plt.title(chr(ord('A') + true_label) + " " + chr(ord('A') + pred_label))
#         plt.imshow(np.reshape(dataset[item], (28, 28)), cmap=colors[color])
        plt.imshow(np.reshape(dataset[item], (28, 28)), cmap='BuPu')
    plt.show()
                  
disp_sample_dataset(test_dataset, test_labels, test_pred, "SMARI SHO YMEIU")


# In[12]:


# Problem 2
# Let's demonstrate an extreme case of overfitting. Restrict your training data to just a few batches. What happens?

few_batch_size = batch_size * 8
small_train_dataset = train_dataset[:few_batch_size, :]
small_train_labels = train_labels[:few_batch_size, :]

num_steps = 10001 #3801

import timeit
start_time = timeit.default_timer()

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print("Initialized")
    print("1LNN w/o dropout, overfit")
    
    for step in range(num_steps):
#         Pick an offset within the training data, which has been randomized.
#         Note: we could use better randomization across epochs.
        offset = (step * batch_size) % (small_train_labels.shape[0] - batch_size)
        
#         Generate a minibatch.
        batch_data = small_train_dataset[offset:(offset + batch_size), :]
        batch_labels = small_train_labels[offset:(offset + batch_size), :]
        
#         Prepare a dictionary telling the session where to feed the minibatch.
#         The key of the dictionary is the placeholder node of the graph to be fed,
#         and the value is the numpy array to feed to it.
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        
        if (step % 2000 == 0):
            print("Minibatch loss at step %d: %f" % (step, l))
            print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
            print("Validation accuracy: %.1f%%" % accuracy(valid_prediction.eval(), valid_labels))
    
    print("Final loss: %f" % l)
    
    test_pred = test_prediction.eval()
    print("Test accuracy: %.1f%%" % accuracy(test_pred, test_labels))
    
    elapsed = timeit.default_timer() - start_time
    print("Time:", elapsed)
    
# PROBLEM 2 FINISH


# In[13]:


# Problem 3
# Introduce Dropout on the hidden layer of the neural network. 
# Remember: Dropout should only be introduced during training, not evaluation, 
# otherwise your evaluation results would be stochastic as well. 
# TensorFlow provides nn.dropout() for that, but you have to make sure it's only inserted during training.
# What happens to our extreme overfitting case?

def get_logits_with_dropout(W_hidden, W_out, b_hidden, b_out, dataset, prob):
    input_hidden = tf.nn.relu(tf.matmul(dataset, W_hidden) + b_hidden)
    dropout = tf.nn.dropout(input_hidden, prob)
    logits = tf.matmul(dropout, W_out) + b_out
    return logits

batch_size = 128 #128
hidden_neurons_amount = 1024
learning_rate = 0.05
regularization_param = 0.00165

graph = tf.Graph()
with graph.as_default():
      
#     Input data. For the training data, we use a placeholder that will be fed
#     at run time with a training minibatch.
    tf_train_dataset = tf.placeholder(tf.float32,shape=(batch_size, image_size * image_size))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)
    keep_prob = tf.placeholder(tf.float32)
    
#     Variables.
    weights_hidden = tf.Variable(tf.truncated_normal([image_size * image_size, hidden_neurons_amount]))
    weights_out = tf.Variable(tf.truncated_normal([hidden_neurons_amount, num_labels]))

    biases_hidden = tf.Variable(tf.zeros([hidden_neurons_amount]))
    biases_out = tf.Variable(tf.zeros([num_labels]))

    
#     Training computation
    logits = get_logits_with_dropout(weights_hidden, weights_out, biases_hidden, biases_out, tf_train_dataset, keep_prob)
#     logits = get_logits(weights_hidden, weights_out, biases_hidden, biases_out, tf_train_dataset)


    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))
    loss += get_l2_loss(weights_hidden, weights_out, biases_hidden, biases_out)
    
#     Optimizer.
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
#     optimizer = tf.train.AdamOptimizer().minimize(loss)
    
#     Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    
    valid_prediction = get_logits(weights_hidden, weights_out, biases_hidden, biases_out, tf_valid_dataset)
    test_prediction = get_logits(weights_hidden, weights_out, biases_hidden, biases_out, tf_test_dataset)


# In[14]:


num_steps = 40001 #3801
prob = 1.0

print_config(batch_size, num_steps, hidden_neurons_amount, learning_rate, regularization_param, prob)

import timeit
start_time = timeit.default_timer()

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print("Initialized")
    print("1LNN with dropout")
    
    for step in range(num_steps):
#         Pick an offset within the training data, which has been randomized.
#         Note: we could use better randomization across epochs.
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        
#         Generate a minibatch.
        batch_data = train_dataset[offset:(offset + batch_size), :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        
#         Prepare a dictionary telling the session where to feed the minibatch.
#         The key of the dictionary is the placeholder node of the graph to be fed,
#         and the value is the numpy array to feed to it.
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels, keep_prob : prob}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        
        if (step % 4000 == 0):
            print("Minibatch loss at step %d: %f" % (step, l))
            print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
            print("Validation accuracy: %.1f%%" % accuracy(valid_prediction.eval(), valid_labels))
    
    print("Final loss: %f" % l)
    
    test_pred = test_prediction.eval()
    print("Test accuracy: %.1f%%" % accuracy(test_pred, test_labels))
    
    elapsed = timeit.default_timer() - start_time
    print("Time:", elapsed)


# In[15]:


few_batch_size = batch_size * 8
small_train_dataset = train_dataset[:few_batch_size, :]
small_train_labels = train_labels[:few_batch_size, :]

num_steps = 10001 #3801
prob = 0.5

import timeit
start_time = timeit.default_timer()

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print("Initialized")
    print("1LNN with dropout, overfit")
    
    for step in range(num_steps):
#         Pick an offset within the training data, which has been randomized.
#         Note: we could use better randomization across epochs.
        offset = (step * batch_size) % (small_train_labels.shape[0] - batch_size)
        
#         Generate a minibatch.
        batch_data = small_train_dataset[offset:(offset + batch_size), :]
        batch_labels = small_train_labels[offset:(offset + batch_size), :]
        
#         Prepare a dictionary telling the session where to feed the minibatch.
#         The key of the dictionary is the placeholder node of the graph to be fed,
#         and the value is the numpy array to feed to it.
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels, keep_prob : prob}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        
        if (step % 2000 == 0):
            print("Minibatch loss at step %d: %f" % (step, l))
            print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
            print("Validation accuracy: %.1f%%" % accuracy(valid_prediction.eval(), valid_labels))
    
    print("Final loss: %f" % l)
    
    test_pred = test_prediction.eval()
    print("Test accuracy: %.1f%%" % accuracy(test_pred, test_labels))
    
    elapsed = timeit.default_timer() - start_time
    print("Time:", elapsed)
    
# PROBLEM 3 FINISH


# In[16]:


# Problem 4
# Try to get the best performance you can using a multi-layer model! 
# The best reported test accuracy using a deep network is 97.1%.

# One avenue you can explore is to add multiple layers.

# Another one is to use learning rate decay:

# global_step = tf.Variable(0)  # count the number of steps taken.
# learning_rate = tf.train.exponential_decay(0.5, global_step, ...)
# optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

def get_logits_with_dropout(W_hidden_one, W_hidden_two, W_hidden_three, W_out, 
                            b_hidden_one, b_hidden_two, b_hidden_three, b_out, 
                            dataset, prob = 1):
    output_hidden_one = tf.nn.relu(tf.matmul(dataset, W_hidden_one) + b_hidden_one)
    output_hidden_two = tf.nn.relu(tf.matmul(output_hidden_one, W_hidden_two) + b_hidden_two)
    output_hidden_two = tf.nn.dropout(output_hidden_two, prob)
    output_hidden_three = tf.nn.relu(tf.matmul(output_hidden_two, W_hidden_three) + b_hidden_three)
    logits = tf.matmul(output_hidden_three, W_out) + b_out
    return logits


def get_logits(W_hidden_one, W_hidden_two, W_hidden_three, W_out, 
               b_hidden_one, b_hidden_two, b_hidden_three, b_out, 
               dataset):
    output_hidden_one = tf.nn.relu(tf.matmul(dataset, W_hidden_one) + b_hidden_one)
    output_hidden_two = tf.nn.relu(tf.matmul(output_hidden_one, W_hidden_two) + b_hidden_two)
    output_hidden_three = tf.nn.relu(tf.matmul(output_hidden_two, W_hidden_three) + b_hidden_three)
    logits = tf.matmul(output_hidden_three, W_out) + b_out
    return logits

def get_l2_loss(W_hidden_one, W_hidden_two, W_hidden_three, W_out, b_hidden_one, b_hidden_two, b_hidden_three, b_out):
    return regularization_param * (tf.nn.l2_loss(W_hidden_one) + tf.nn.l2_loss(b_hidden_one) + 
                                   tf.nn.l2_loss(W_hidden_two) + tf.nn.l2_loss(b_hidden_two) +
                                   tf.nn.l2_loss(W_hidden_three) + tf.nn.l2_loss(b_hidden_three) +
                                   tf.nn.l2_loss(W_out) + tf.nn.l2_loss(b_out))


# In[17]:


batch_size = 128

hidden_one_neurons_amount = 1024
hidden_two_neurons_amount = 512
hidden_three_neurons_amount = 128

learning_rate_start = 0.05
regularization_param = 0.00165

graph = tf.Graph()
with graph.as_default():
      
#     Input data. For the training data, we use a placeholder that will be fed
#     at run time with a training minibatch.
    tf_train_dataset = tf.placeholder(tf.float32,shape=(batch_size, image_size * image_size))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)
    keep_prob = tf.placeholder(tf.float32)
    
#     Variables.
    weights_hidden_one = tf.Variable(tf.truncated_normal([image_size * image_size, hidden_one_neurons_amount], 
                                                         stddev=np.sqrt(2.0 / (image_size * image_size))))
    weights_hidden_two = tf.Variable(tf.truncated_normal([hidden_one_neurons_amount, hidden_two_neurons_amount], 
                                                         stddev=np.sqrt(2.0 / hidden_one_neurons_amount)))
    weights_hidden_three = tf.Variable(tf.truncated_normal([hidden_two_neurons_amount, hidden_three_neurons_amount], 
                                                           stddev=np.sqrt(2.0 / hidden_two_neurons_amount)))
    weights_out = tf.Variable(tf.truncated_normal([hidden_three_neurons_amount, num_labels], 
                                                  stddev=np.sqrt(2.0 / hidden_three_neurons_amount)))

    biases_hidden_one = tf.Variable(tf.zeros([hidden_one_neurons_amount]))
    biases_hidden_two = tf.Variable(tf.zeros([hidden_two_neurons_amount]))
    biases_hidden_three = tf.Variable(tf.zeros([hidden_three_neurons_amount]))
    biases_out = tf.Variable(tf.zeros([num_labels]))
    
    global_step = tf.Variable(0)
    
#     Training computation
#     logits = get_logits_with_dropout(weights_hidden_one, weights_hidden_two, weights_hidden_three, weights_out, 
#                                      biases_hidden_one, biases_hidden_two, biases_hidden_three, biases_out, 
#                                      tf_train_dataset, keep_prob)
    logits = get_logits_with_dropout(weights_hidden_one, weights_hidden_two, weights_hidden_three, weights_out, 
                                     biases_hidden_one, biases_hidden_two, biases_hidden_three, biases_out, 
                                     tf_train_dataset, keep_prob)


    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))
    loss += get_l2_loss(weights_hidden_one, weights_hidden_two, weights_hidden_three, weights_out, 
                        biases_hidden_one, biases_hidden_two, biases_hidden_three, biases_out)
    
#     Optimizer.
#     learning_rate = tf.train.exponential_decay(learning_rate_start, global_step, 1000, 0.96, staircase=True)
#     optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step = global_step)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate_start).minimize(loss, global_step)
#     optimizer = tf.train.AdamOptimizer().minimize(loss)
    
#     Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = get_logits_with_dropout(weights_hidden_one, weights_hidden_two, weights_hidden_three, weights_out, 
                                               biases_hidden_one, biases_hidden_two, biases_hidden_three, biases_out, 
                                               tf_valid_dataset)
    test_prediction = get_logits_with_dropout(weights_hidden_one, weights_hidden_two, weights_hidden_three, weights_out, 
                                              biases_hidden_one, biases_hidden_two, biases_hidden_three, biases_out, 
                                              tf_test_dataset)


# In[18]:


num_steps = 20001 #3801
prob = 0.8

# print_config(batch_size, num_steps, hidden_neurons_amount, learning_rate, regularization_param, prob)

import timeit
start_time = timeit.default_timer()

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print("Initialized")
    print("3LNN")
    
    for step in range(num_steps):
#         Pick an offset within the training data, which has been randomized.
#         Note: we could use better randomization across epochs.
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        
#         Generate a minibatch.
        batch_data = train_dataset[offset:(offset + batch_size), :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        
#         Prepare a dictionary telling the session where to feed the minibatch.
#         The key of the dictionary is the placeholder node of the graph to be fed,
#         and the value is the numpy array to feed to it.
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels, keep_prob : prob}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        
        if (step % 1000 == 0):
            print("Minibatch loss at step %d: %f" % (step, l))
            print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
            print("Validation accuracy: %.1f%%" % accuracy(valid_prediction.eval(), valid_labels))
    
    print("Final loss: %f" % l)
    
    test_pred = test_prediction.eval()
    print("Test accuracy: %.1f%%" % accuracy(test_pred, test_labels))
    
    elapsed = timeit.default_timer() - start_time
    print("Time:", elapsed)
    
# PROBLEM 4 FINISH

