
# coding: utf-8

# In[1]:


# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import os
import numpy as np
import random
import string
import tensorflow as tf
import zipfile
from six.moves import range
from six.moves.urllib.request import urlretrieve


# In[2]:


url = 'http://mattmahoney.net/dc/'

def maybe_download(filename, expected_bytes):
    """Download a file if not present, and make sure it's the right size."""
    if not os.path.exists(filename):
        filename, _ = urlretrieve(url + filename, filename)
        
    statinfo = os.stat(filename)
    if statinfo.st_size == expected_bytes:
        print('Found and verified %s' % filename)
    else:
        print(statinfo.st_size)
        raise Exception('Failed to verify ' + filename + '. Can you get to it with a browser?')
    
    return filename

filename = maybe_download('text8.zip', 31344016)


# In[3]:


def read_data(filename):
    with zipfile.ZipFile(filename) as f:
        name = f.namelist()[0]
        data = tf.compat.as_str(f.read(name))
    return data
  
text = read_data(filename)
print('Data size %d' % len(text))


# In[4]:


valid_size = 1000
valid_text = text[:valid_size]
train_text = text[valid_size:]
train_size = len(train_text)
print(train_size, train_text[:64])
print(valid_size, valid_text[:64])


# In[5]:


vocabulary_size = len(string.ascii_lowercase) + 1 # [a-z] + ' '
first_letter = ord(string.ascii_lowercase[0])

def char2id(char):
    if char in string.ascii_lowercase:
        return ord(char) - first_letter + 1
    elif char == ' ':
        return 0
    else:
        print('Unexpected character: %s' % char)
        return 0
    
def id2char(dictid):
    if dictid > 0:
        return chr(dictid + first_letter - 1)
    else:
        return ' '

print(char2id('a'), char2id('z'), char2id(' '), char2id('ï'))
print(id2char(1), id2char(26), id2char(0))


# In[6]:


batch_size = 64
num_unrollings = 10

class BatchGenerator(object):
    def __init__(self, text, batch_size, num_unrollings):
        self._text = text
        self._text_size = len(text)
        self._batch_size = batch_size
        self._num_unrollings = num_unrollings
        segment = self._text_size // batch_size
        self._cursor = [ offset * segment for offset in range(batch_size)]
        self._last_batch = self._next_batch()
    
    def _next_batch(self):
        """Generate a single batch from the current cursor position in the data."""
        batch = np.zeros(shape=(self._batch_size, vocabulary_size), dtype=np.float)
        for b in range(self._batch_size):
            batch[b, char2id(self._text[self._cursor[b]])] = 1.0
            self._cursor[b] = (self._cursor[b] + 1) % self._text_size
        return batch
    
    def next(self):
        """Generate the next array of batches from the data. The array consists of
        the last batch of the previous array, followed by num_unrollings new ones.
        """
        batches = [self._last_batch]
        for step in range(self._num_unrollings):
            batches.append(self._next_batch())
        self._last_batch = batches[-1]
        return batches

def characters(probabilities):
    """Turn a 1-hot encoding or a probability distribution over the possible
    characters back into its (most likely) character representation."""
    return [id2char(c) for c in np.argmax(probabilities, 1)]

def batches2string(batches):
    """Convert a sequence of batches back into their (most likely) string
    representation."""
    s = [''] * batches[0].shape[0]
    for b in batches:
        s = [''.join(x) for x in zip(s, characters(b))]
    return s

def print_batches(batches):
    for i in range(len(batches)):
#         for j in range(batch_size // 2):
        print((characters(batches[i])), end = ' ')
#             print(batches[i][j], end = ' ')
        print()

train_batches = BatchGenerator(train_text, batch_size, num_unrollings)
valid_batches = BatchGenerator(valid_text, 1, 1)

kek = train_batches.next()

print(len(kek))
print(kek[0].shape)

print_batches(kek)
print(batches2string(kek))
# print(batches2string(train_batches.next()))
# print(batches2string(valid_batches.next()))
# print(batches2string(valid_batches.next()))

print()
for i in range(5):
    print('look at dis: ', batches2string(valid_batches.next()))


# In[7]:


def logprob(predictions, labels):
    """Log-probability of the true labels in a predicted batch."""
    predictions[predictions < 1e-10] = 1e-10
#     print(labels.shape, predictions.shape)
    return np.sum(np.multiply(labels, -np.log(predictions))) / labels.shape[0]

def sample_distribution(distribution):
    """Sample one element from a distribution assumed to be an array of normalized
    probabilities.
    """
    r = random.uniform(0, 1)
    s = 0
    for i in range(len(distribution)):
        s += distribution[i]
        if s >= r:
            return i
    return len(distribution) - 1

def sample(prediction):
    """Turn a (column) prediction into 1-hot encoded samples."""
    p = np.zeros(shape=[1, vocabulary_size], dtype=np.float)
    p[0, sample_distribution(prediction[0])] = 1.0
    return p

def random_distribution():
    """Generate a random column of probabilities."""
    b = np.random.uniform(0.0, 1.0, size=[1, vocabulary_size])
    return b/np.sum(b, 1)[:,None]


# In[8]:


num_nodes = 64

graph = tf.Graph()
with graph.as_default():
    
    # Parameters:
    # Input gate: input, previous output, and bias.
    ix = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -0.1, 0.1))
    im = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    ib = tf.Variable(tf.zeros([1, num_nodes]))
    
    # Forget gate: input, previous output, and bias.
    fx = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -0.1, 0.1))
    fm = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    fb = tf.Variable(tf.zeros([1, num_nodes]))
    
    # Memory cell: input, state and bias.                             
    cx = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -0.1, 0.1))
    cm = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    cb = tf.Variable(tf.zeros([1, num_nodes]))
    
    # Output gate: input, previous output, and bias.
    ox = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -0.1, 0.1))
    om = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    ob = tf.Variable(tf.zeros([1, num_nodes]))
    
    # Variables saving state across unrollings.
    saved_output = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    saved_state = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    
    # Classifier weights and biases.
    w = tf.Variable(tf.truncated_normal([num_nodes, vocabulary_size], -0.1, 0.1))
    b = tf.Variable(tf.zeros([vocabulary_size]))
    
    # Definition of the cell computation.
    def lstm_cell(i, o, state):
        """Create a LSTM cell. See e.g.: http://arxiv.org/pdf/1402.1128v1.pdf
        Note that in this formulation, we omit the various connections between the
        previous state and the gates."""
        print(i.shape, fx.shape, o.shape, fm.shape, fb.shape)
        
        forget_gate = tf.sigmoid(tf.matmul(i, fx) + tf.matmul(o, fm) + fb)
        
        print(forget_gate.shape)
        
        input_gate = tf.sigmoid(tf.matmul(i, ix) + tf.matmul(o, im) + ib)
        update = tf.matmul(i, cx) + tf.matmul(o, cm) + cb
        
        state = forget_gate * state + input_gate * tf.tanh(update)
        
        output_gate = tf.sigmoid(tf.matmul(i, ox) + tf.matmul(o, om) + ob)
        return output_gate * tf.tanh(state), state

    # Input data.
    train_data = list()
    for _ in range(num_unrollings + 1):
        train_data.append(tf.placeholder(tf.float32, shape=[batch_size,vocabulary_size]))
    
    train_inputs = train_data[:num_unrollings]
    train_labels = train_data[1:]  # labels are inputs shifted by one time step.
      
    # Unrolled LSTM loop.
    outputs = list()
    output = saved_output
    state = saved_state
    for i in train_inputs:
        output, state = lstm_cell(i, output, state)
        outputs.append(output)

    # State saving across unrollings.
    with tf.control_dependencies([saved_output.assign(output), saved_state.assign(state)]):
        # Classifier.
        logits = tf.nn.xw_plus_b(tf.concat(outputs, 0), w, b)
        print('logits', logits.shape)
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf.concat(train_labels, 0), logits=logits))

    # Optimizer.
    global_step = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(10.0, global_step, 5000, 0.1, staircase=True)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    
    gradients, v = zip(*optimizer.compute_gradients(loss))
    gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
    optimizer = optimizer.apply_gradients(zip(gradients, v), global_step=global_step)

    # Predictions.
    train_prediction = tf.nn.softmax(logits)
    
    # Sampling and validation eval: batch 1, no unrolling.
    sample_input = tf.placeholder(tf.float32, shape=[1, vocabulary_size])
    saved_sample_output = tf.Variable(tf.zeros([1, num_nodes]))
    saved_sample_state = tf.Variable(tf.zeros([1, num_nodes]))
    reset_sample_state = tf.group(saved_sample_output.assign(tf.zeros([1, num_nodes])), 
                                  saved_sample_state.assign(tf.zeros([1, num_nodes])))
    sample_output, sample_state = lstm_cell(sample_input, saved_sample_output, saved_sample_state)
    
    with tf.control_dependencies([saved_sample_output.assign(sample_output), saved_sample_state.assign(sample_state)]):
        sample_prediction = tf.nn.softmax(tf.nn.xw_plus_b(sample_output, w, b))


# In[9]:


num_steps = 7001
summary_frequency = 100

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    mean_loss = 0
    for step in range(num_steps):
        batches = train_batches.next()
        feed_dict = dict()
        for i in range(num_unrollings + 1):
#             print("batch[i], shape", batches[i], batches[i].shape)
#             print(batches2string(batches))
            feed_dict[train_data[i]] = batches[i]
        
        _, l, predictions, lr = session.run([optimizer, loss, train_prediction, learning_rate], feed_dict=feed_dict)
        mean_loss += l
        
        if step % summary_frequency == 0:
            if step > 0:
                mean_loss = mean_loss / summary_frequency
            
            # The mean loss is an estimate of the loss over the last few batches.
            print('Average loss at step %d: %f learning rate: %f' % (step, mean_loss, lr))
            
            mean_loss = 0
            labels = np.concatenate(list(batches)[1:])
            print('Minibatch perplexity: %.2f' % float(np.exp(logprob(predictions, labels))))
            
            if step % (summary_frequency * 10) == 0:
                # Generate some samples.
                print('=' * 80)
                for _ in range(5):
                    feed = sample(random_distribution())
                    sentence = characters(feed)[0]
                    reset_sample_state.run()
                    for _ in range(79):
                        prediction = sample_prediction.eval({sample_input: feed})
#                         print(prediction.shape)
#                         print(prediction[0])
                        feed = sample(prediction)
#                         print("feed", feed)
                        sentence += characters(feed)[0]
                    print(sentence)
                print('=' * 80)
            
            # Measure validation set perplexity.
            reset_sample_state.run()
            valid_logprob = 0
            for _ in range(valid_size):
                b = valid_batches.next()
                predictions = sample_prediction.eval({sample_input: b[0]})
                valid_logprob = valid_logprob + logprob(predictions, b[1])
            
            print('Validation set perplexity: %.2f' % float(np.exp(valid_logprob / valid_size)))


# Problem 1
# You might have noticed that the definition of the LSTM cell involves 4 matrix multiplications with the input, and 4 matrix multiplications with the output. Simplify the expression by using a single matrix multiply for each, and variables that are 4 times larger.

# In[10]:


num_nodes = 64

graph = tf.Graph()
with graph.as_default():
    
    # Parameters:
    ifco_x = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes], -0.1, 0.1))
    ifco_m = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    ifco_b = tf.Variable(tf.zeros([1, num_nodes]))
    
    # Variables saving state across unrollings.
    saved_output = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    saved_state = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    
    # Classifier weights and biases.
    w = tf.Variable(tf.truncated_normal([num_nodes, vocabulary_size], -0.1, 0.1))
    b = tf.Variable(tf.zeros([vocabulary_size]))
    
    # Definition of the cell computation.
    def lstm_cell(i, o, state, batch_size = batch_size):
        """Create a LSTM cell. See e.g.: http://arxiv.org/pdf/1402.1128v1.pdf
        Note that in this formulation, we omit the various connections between the
        previous state and the gates."""
        ins = tf.concat([i, i, i, i], 0)
        outs = tf.concat([o, o, o, o], 0)
        
#         print(i)        
#         print(ins.shape, ifco_x.shape, outs.shape, ifco_m.shape, ifco_b.shape)
        
        res_one = tf.matmul(ins, ifco_x)
        res_two = tf.matmul(outs, ifco_m)
        res_three = res_one + res_two + ifco_b
        
#         print(res_one.shape, res_two.shape, res_three.shape)
        
        input_gate = tf.sigmoid(res_three[:batch_size, :])
#         print('i', input_gate.shape)
        forget_gate = tf.sigmoid(res_three[batch_size : 2 * batch_size, :])
#         print('f', forget_gate.shape)
        update = res_three[2 * batch_size : 3 * batch_size, :]
#         print('c', update.shape)
        output_gate = tf.sigmoid(res_three[3 * batch_size :, :])   
#         print('o', output_gate.shape)
        
        state = forget_gate * state + input_gate * tf.tanh(update)
        
        return output_gate * tf.tanh(state), state

    # Input data.
    train_data = list()
    for _ in range(num_unrollings + 1):
        train_data.append(tf.placeholder(tf.float32, shape=[batch_size,vocabulary_size]))
    
    train_inputs = train_data[:num_unrollings]
    train_labels = train_data[1:]  # labels are inputs shifted by one time step.
    
    print(train_labels[0].shape)
      
    # Unrolled LSTM loop.
    outputs = list()
    output = saved_output
    state = saved_state
    for i in train_inputs:
        output, state = lstm_cell(i, output, state)
        outputs.append(output)

    # State saving across unrollings.
    with tf.control_dependencies([saved_output.assign(output), saved_state.assign(state)]):
        # Classifier.
        logits = tf.nn.xw_plus_b(tf.concat(outputs, 0), w, b)
        print(logits.shape)
        print((tf.concat(train_labels, 0)).shape)
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf.concat(train_labels, 0), logits=logits))

    # Optimizer.
    global_step = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(10.0, global_step, 5000, 0.1, staircase=True)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    
    gradients, v = zip(*optimizer.compute_gradients(loss))
    gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
    optimizer = optimizer.apply_gradients(zip(gradients, v), global_step=global_step)

    # Predictions.
    train_prediction = tf.nn.softmax(logits)
    
    # Sampling and validation eval: batch 1, no unrolling.
    sample_input = tf.placeholder(tf.float32, shape=[1, vocabulary_size])
    saved_sample_output = tf.Variable(tf.zeros([1, num_nodes]))
    saved_sample_state = tf.Variable(tf.zeros([1, num_nodes]))
    reset_sample_state = tf.group(saved_sample_output.assign(tf.zeros([1, num_nodes])), 
                                  saved_sample_state.assign(tf.zeros([1, num_nodes])))
    sample_output, sample_state = lstm_cell(sample_input, saved_sample_output, saved_sample_state, 1)
    
    with tf.control_dependencies([saved_sample_output.assign(sample_output), saved_sample_state.assign(sample_state)]):
        sample_prediction = tf.nn.softmax(tf.nn.xw_plus_b(sample_output, w, b))


# In[11]:


num_steps = 7001
summary_frequency = 100

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    mean_loss = 0
    for step in range(num_steps):
        batches = train_batches.next()
        feed_dict = dict()
        for i in range(num_unrollings + 1):
#             print("batch[i], shape", batches[i], batches[i].shape)
#             print(batches2string(batches))
            feed_dict[train_data[i]] = batches[i]
        
        _, l, predictions, lr = session.run([optimizer, loss, train_prediction, learning_rate], feed_dict=feed_dict)
        mean_loss += l
        
        if step % summary_frequency == 0:
            if step > 0:
                mean_loss = mean_loss / summary_frequency
            
            # The mean loss is an estimate of the loss over the last few batches.
            print('Average loss at step %d: %f learning rate: %f' % (step, mean_loss, lr))
            
            mean_loss = 0
            labels = np.concatenate(list(batches)[1:])
            print('Minibatch perplexity: %.2f' % float(np.exp(logprob(predictions, labels))))
            
            if step % (summary_frequency * 10) == 0:
                # Generate some samples.
                print('=' * 80)
                for _ in range(5):
                    feed = sample(random_distribution())
                    sentence = characters(feed)[0]
                    reset_sample_state.run()
                    for _ in range(79):
                        prediction = sample_prediction.eval({sample_input: feed})
                        feed = sample(prediction)
                        sentence += characters(feed)[0]
                    print(sentence)
                print('=' * 80)
            
            # Measure validation set perplexity.
            reset_sample_state.run()
            valid_logprob = 0
            for _ in range(valid_size):
                b = valid_batches.next()
                predictions = sample_prediction.eval({sample_input: b[0]})
                valid_logprob = valid_logprob + logprob(predictions, b[1])
            
            print('Validation set perplexity: %.2f' % float(np.exp(valid_logprob / valid_size)))


# Problem 2
# We want to train a LSTM over bigrams, that is pairs of consecutive characters like 'ab' instead of single characters like 'a'. Since the number of possible bigrams is large, feeding them directly to the LSTM using 1-hot encodings will lead to a very sparse representation that is very wasteful computationally.
# 
# a- Introduce an embedding lookup on the inputs, and feed the embeddings to the LSTM cell instead of the inputs themselves.
# 
# b- Write a bigram-based LSTM, modeled on the character LSTM above.
# 
# c- Introduce Dropout. For best practices on how to use Dropout in LSTMs, refer to this article.

# In[12]:


alphabet_size = 27
vocabulary_size = alphabet_size ** 2

def build_dict():
    dictionary = dict()
    
    for i in range(alphabet_size):
        for j in range(alphabet_size):
            dictionary[str(id2char(i)) + str(id2char(j))] = i * alphabet_size + j
            
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys())) 
    
    return dictionary, reverse_dictionary



dictionary, reverse_dictionary = build_dict()

print(dictionary['cb'])
print(reverse_dictionary[83])


# In[13]:


batch_size=64
num_unrollings=10

class BatchGeneratorBigramV2(object):
    def __init__(self, text, batch_size, num_unrollings):
        self._text = text
        self._text_size = len(text)
        self._batch_size = batch_size
        self._num_unrollings = num_unrollings
        segment = self._text_size / batch_size
        # list of offsets within batch
        self._cursor = [ offset * segment for offset in range(batch_size)]
        self._last_batch = self._next_batch()
    
    def _next_batch(self):
        """Generate a single batch from the current cursor position in the data."""
        batch = np.zeros(shape=(self._batch_size), dtype=np.int)  # id of char to be embedded
        for b in range(self._batch_size):
            batch[b] = dictionary[str(self._text[int(self._cursor[b])]) + 
                                  str(self._text[int((self._cursor[b]) + 1) % self._text_size])]
            self._cursor[b] = (self._cursor[b] + 1) % self._text_size  # move cursor
        return batch
  
    def next(self):
#         Generate the next array of batches from the data. The array consists of
#         the last batch of the previous array, followed by num_unrollings new ones.
        batches = [self._last_batch]
        for step in range(self._num_unrollings):
            batches.append(self._next_batch())  # add id of char for 1 to num_unrollings
        self._last_batch = batches[-1]
        return batches

train_batches = BatchGeneratorBigramV2(train_text, batch_size, num_unrollings)
valid_batches = BatchGeneratorBigramV2(valid_text, 1, 1)

kek = train_batches.next()
print('kek', len(kek))

print(kek[0].shape)

def print_ebanie_batches(batches):
    for i in range(len(batches)):
        for j in range(batch_size // 2):
            print(reverse_dictionary[batches[i][j]], end = ' ')
#             print(batches[i][j], end = ' ')
        print()
        
def bigrambatches2string(batches):
#     Convert a sequence of batches back into string
#     representation.
    s = [''] * batches[0].shape[0]
    for b in batches:
        s = [''.join(x) for x in zip(s, [reverse_dictionary[c] for c in b])]
    return s
        
print_ebanie_batches(kek)
print(bigrambatches2string(kek))


# In[14]:


num_nodes = 64
embedding_size = 128
num_sampled = 64
keep_probability = 0.9

graph = tf.Graph()
with graph.as_default():
    
    # Parameters:
    ifco_x = tf.Variable(tf.truncated_normal([embedding_size, num_nodes], -0.1, 0.1))
    ifco_m = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    ifco_b = tf.Variable(tf.zeros([1, num_nodes]))
    
    # Variables saving state across unrollings.
    saved_output = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    saved_state = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    
    # Classifier weights and biases.
    w = tf.Variable(tf.truncated_normal([num_nodes, vocabulary_size], -0.1, 0.1))
    b = tf.Variable(tf.zeros([vocabulary_size]))
    
    # Embeddings
    embeddings = tf.Variable(tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))
    
    # Definition of the cell computation.
    def lstm_cell(i, o, state, batch_size = batch_size, keep_prob = 1.0):
        """Create a LSTM cell. See e.g.: http://arxiv.org/pdf/1402.1128v1.pdf
        Note that in this formulation, we omit the various connections between the
        previous state and the gates."""
        
        o = tf.nn.dropout(o, keep_prob)
        
        ins = tf.concat([i, i, i, i], 0)
        outs = tf.concat([o, o, o, o], 0)
        
#         print(i)        
#         print(ins.shape, ifco_x.shape, outs.shape, ifco_m.shape, ifco_b.shape)
        
        res_one = tf.matmul(ins, ifco_x)
        res_two = tf.matmul(outs, ifco_m)
        res_three = res_one + res_two + ifco_b
        
#         print(res_one.shape, res_two.shape, res_three.shape)
        
        input_gate = tf.sigmoid(res_three[:batch_size, :])
#         print('i', input_gate.shape)
        forget_gate = tf.sigmoid(res_three[batch_size : 2 * batch_size, :])
#         print('f', forget_gate.shape)
        update = res_three[2 * batch_size : 3 * batch_size, :]
#         print('c', update.shape)
        output_gate = tf.sigmoid(res_three[3 * batch_size :, :])   
#         print('o', output_gate.shape)
        
        state = forget_gate * state + input_gate * tf.tanh(update)
        
        return output_gate * tf.tanh(state), state

    # Input data.
    train_data = list()
    for _ in range(num_unrollings + 1):
        train_data.append(tf.placeholder(tf.int32, shape=[batch_size]))
    
    train_inputs = train_data[:num_unrollings]
    train_labels = train_data[1:]  # labels are inputs shifted by one time step.
    
    train_embed = list()
    for i in range(num_unrollings):
        train_embed.append(tf.nn.embedding_lookup(embeddings, train_inputs[i]))
        
        
    # Unrolled LSTM loop.
    outputs = list()
    output = saved_output
    state = saved_state
    for i in train_embed:
        output, state = lstm_cell(i, output, state, keep_prob=keep_probability)
        outputs.append(output)

    # State saving across unrollings.
    with tf.control_dependencies([saved_output.assign(output), saved_state.assign(state)]):
        # Classifier.
        logits = tf.nn.xw_plus_b(tf.concat(outputs, 0), w, b)
#         print('kek')
#         print(logits.shape)
#         print((tf.concat(train_labels, 0)).shape)
        loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=tf.concat(train_labels, 0), logits=logits))
        
#         loss = tf.reduce_mean(tf.nn.sampled_softmax_loss(weights=w, biases=b, inputs=train_embed, 
#                                                          labels=tf.concat(train_labels, 0), num_sampled=num_sampled, 
#                                                          num_classes=vocabulary_size))

    # Optimizer.
    global_step = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(10.0, global_step, 5000, 0.03, staircase=True)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    
    gradients, v = zip(*optimizer.compute_gradients(loss))
    gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
    optimizer = optimizer.apply_gradients(zip(gradients, v), global_step=global_step)

    # Predictions.
    train_prediction = tf.nn.softmax(logits)
    
    # Sampling and validation eval: batch 1, no unrolling.
    sample_input = tf.placeholder(tf.int32, shape=[1])
    sample_input_embed = tf.nn.embedding_lookup(embeddings, sample_input)
    
    saved_sample_output = tf.Variable(tf.zeros([1, num_nodes]))
    saved_sample_state = tf.Variable(tf.zeros([1, num_nodes]))
    reset_sample_state = tf.group(saved_sample_output.assign(tf.zeros([1, num_nodes])), 
                                  saved_sample_state.assign(tf.zeros([1, num_nodes])))
    sample_output, sample_state = lstm_cell(sample_input_embed, saved_sample_output, saved_sample_state, 1)
    
    with tf.control_dependencies([saved_sample_output.assign(sample_output), saved_sample_state.assign(sample_state)]):
        sample_prediction = tf.nn.softmax(tf.nn.xw_plus_b(sample_output, w, b))
#         print('prediction shape', sample_prediction.shape)


# In[15]:


def bigrams(probabilities):
    """Turn a 1-hot encoding or a probability distribution over the possible
    characters back into its (most likely) character representation."""
#     return [reverse_dictionary[c] for c in np.argmax(probabilities, 1)]
    print('bigrams: ', np.argmax(probabilities, 1))
    print('shape: ', (np.argmax(probabilities, 1)).shape)
    return [id2char(c // alphabet_size) for c in np.argmax(probabilities, 1)]


# In[16]:


num_steps = 7001
summary_frequency = 100

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    mean_loss = 0
    for step in range(num_steps):
        batches = train_batches.next()
        feed_dict = dict()
        for i in range(num_unrollings + 1):
#             print("batch[i], shape", batches[i], batches[i].shape)
#             print(batches2string(batches))
            feed_dict[train_data[i]] = batches[i]
        
        _, l, predictions, lr = session.run([optimizer, loss, train_prediction, learning_rate], feed_dict=feed_dict)
        mean_loss += l
        
        if step % summary_frequency == 0:
            if step > 0:
                mean_loss = mean_loss / summary_frequency
            
            # The mean loss is an estimate of the loss over the last few batches.
            print('Average loss at step %d: %f learning rate: %f' % (step, mean_loss, lr))
            
            mean_loss = 0
            
            labels = np.concatenate(list(batches)[1:])
            labels_one_hot = np.zeros(predictions.shape)
            for i, j in enumerate(labels):
#                 print(i, j.shape)
                labels_one_hot[i, j] = 1.0
            
            print('Minibatch perplexity: %.2f' % float(np.exp(logprob(predictions, labels_one_hot))))
            
            if step % (summary_frequency * 10) == 0:
                # Generate some samples.
                print('=' * 80)
                for _ in range(5):
                    rand_distr = random_distribution()
#                     print('rand shape', rand_distr.shape)
                    feed = sample(rand_distr)
                    sentence = bigrams(feed)[0]
                    feed = [np.argmax(feed)]
                    reset_sample_state.run()
                    for _ in range(79):
                        prediction = sample_prediction.eval({sample_input: feed})
                        feed = sample(prediction)
                        sentence += bigrams(feed)[0]
                        feed = [np.argmax(feed)]
                    print(sentence)
                print('=' * 80)
            
            # Measure validation set perplexity.
            reset_sample_state.run()
            valid_logprob = 0
            for _ in range(valid_size):
#                 b = valid_batches.next()
#                 print(len(b))
                predictions = sample_prediction.eval({sample_input: b[0]})
                labels_one_hot = np.zeros(predictions.shape)
                labels_one_hot[0][b[1]] = 1.0
                valid_logprob = valid_logprob + logprob(predictions, labels_one_hot)
            
            print('Validation set perplexity: %.2f' % float(np.exp(valid_logprob / valid_size)))

