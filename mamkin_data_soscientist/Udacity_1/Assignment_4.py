
# coding: utf-8

# Assignment 4
# Previously in 2_fullyconnected.ipynb and 3_regularization.ipynb, we trained fully connected networks to classify notMNIST characters.
# 
# The goal of this assignment is make the neural network convolutional

# In[2]:


# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range


# In[3]:


pickle_file = 'notMNIST.pickle'

with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']
    del save  # hint to help gc free up memory
    
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)


# Reformat into a TensorFlow-friendly shape:
# 
# convolutions need the image data formatted as a cube (width by height by #channels)
# labels as float 1-hot encodings

# In[4]:


image_size = 28
num_labels = 10
num_channels = 1 # grayscale

import numpy as np

def reformat(dataset, labels):
    dataset = dataset.reshape((-1, image_size, image_size, num_channels)).astype(np.float32)
    labels = (np.arange(num_labels) == labels[:,None]).astype(np.float32)
    return dataset, labels

train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)


# In[5]:


def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1)) / predictions.shape[0])


# Let's build a small network with two convolutional layers, followed by one fully connected layer. Convolutional networks are more expensive computationally, so we'll limit its depth and number of fully connected nodes.

# Problem 1
# The convolutional model above uses convolutions with stride 2 to reduce the dimensionality. Replace the strides by a max pooling operation (nn.max_pool()) of stride 2 and kernel size 2

# In[72]:


batch_size = 16
patch_size = 5
depth = 16
num_hidden = 64
start_learning_rate = .015
regularization_param = .00165
regularization_param = .0

graph = tf.Graph()

with graph.as_default():

    # Input data.
    tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size, image_size, num_channels))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)
    
    # Variables.
    layer1_weights = tf.Variable(tf.truncated_normal([patch_size, patch_size, num_channels, depth], stddev=0.1))
    layer1_biases = tf.Variable(tf.zeros([depth]))
    
    layer2_weights = tf.Variable(tf.truncated_normal([patch_size, patch_size, depth, depth], stddev=0.1))
    layer2_biases = tf.Variable(tf.constant(1.0, shape=[depth]))
    
    layer3_weights = tf.Variable(tf.truncated_normal([image_size // 4 * image_size // 4 * depth, num_hidden], stddev=0.1))
    layer3_biases = tf.Variable(tf.constant(1.0, shape=[num_hidden]))
    
    layer4_weights = tf.Variable(tf.truncated_normal([num_hidden, num_labels], stddev=0.1))
    layer4_biases = tf.Variable(tf.constant(1.0, shape=[num_labels]))
    
    # Model.
    def model(data):
        print(data.get_shape().as_list())
        
        conv = tf.nn.conv2d(data, layer1_weights, [1, 1, 1, 1], padding='SAME')
        conv = tf.nn.max_pool(conv, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        hidden = tf.nn.relu(conv + layer1_biases)
        
        print(hidden.get_shape().as_list())
        
        conv = tf.nn.conv2d(hidden, layer2_weights, [1, 1, 1, 1], padding='SAME')
        conv = tf.nn.max_pool(conv, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        hidden = tf.nn.relu(conv + layer2_biases)
        
        shape = hidden.get_shape().as_list()
        reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
        
        print(shape)
        print(reshape.shape)
        
        hidden = tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases)
        
        return tf.matmul(hidden, layer4_weights) + layer4_biases
    
    # Training computation.
    logits = model(tf_train_dataset)
    
    l2_loss = regularization_param * (tf.nn.l2_loss(layer1_weights) + tf.nn.l2_loss(layer1_biases) +
                                      tf.nn.l2_loss(layer2_weights) + tf.nn.l2_loss(layer2_biases) +
                                      tf.nn.l2_loss(layer3_weights) + tf.nn.l2_loss(layer3_biases) +
                                      tf.nn.l2_loss(layer4_weights) + tf.nn.l2_loss(layer4_biases))
    
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))
    loss += l2_loss
      
    # Optimizer.
    optimizer = tf.train.GradientDescentOptimizer(start_learning_rate).minimize(loss)
    
    # Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = tf.nn.softmax(model(tf_valid_dataset))
    test_prediction = tf.nn.softmax(model(tf_test_dataset))


# In[73]:


num_steps = 20001

import timeit
start_time = timeit.default_timer()

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    for step in range(num_steps):
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        batch_data = train_dataset[offset:(offset + batch_size), :, :, :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        if (step % 2000 == 0):
            print('Minibatch loss at step %d: %f' % (step, l))
            print('Minibatch accuracy: %.1f%%' % accuracy(predictions, batch_labels))
            print('Validation accuracy: %.1f%%' % accuracy(valid_prediction.eval(), valid_labels))
    print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
    
elapsed = timeit.default_timer() - start_time
print("Time:", elapsed)


# Problem 2
# Try to get the best performance you can using a convolutional net. Look for example at the classic LeNet5 architecture, adding Dropout, and/or adding learning rate decay.

# In[163]:


batch_size = 32
patch_size = 5
depth = 16

num_hidden5 = 128
num_hidden6 = 64

start_learning_rate = .05
regularization_param = .00165
keep_probability = 1.
# regularization_param = .0

graph = tf.Graph()

with graph.as_default():

    # Input data.
    tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size, image_size, num_channels))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)
    
    # Variables.
    layer1_weights = tf.Variable(tf.truncated_normal([patch_size, patch_size, num_channels, depth], stddev=0.1))
    layer1_biases = tf.Variable(tf.zeros([depth]))
    
    layer2_weights = tf.Variable(tf.truncated_normal([patch_size, patch_size, depth, depth], stddev=0.1))
    layer2_biases = tf.Variable(tf.constant(1.0, shape=[depth]))
    
    layer3_weights = tf.Variable(tf.truncated_normal([patch_size, patch_size, depth, depth], stddev=0.1))
    layer3_biases = tf.Variable(tf.constant(1.0, shape=[depth]))
    
    layer4_weights = tf.Variable(tf.truncated_normal([image_size // 4 * image_size // 4 * depth, num_hidden5], stddev=0.1))
    layer4_biases = tf.Variable(tf.constant(1.0, shape=[num_hidden5]))
    
    layer5_weights = tf.Variable(tf.truncated_normal([num_hidden5, num_hidden6], stddev=0.1))
    layer5_biases = tf.Variable(tf.constant(1.0, shape=[num_hidden6]))
    
    layer6_weights = tf.Variable(tf.truncated_normal([num_hidden6, num_labels], stddev=0.1))
    layer6_biases = tf.Variable(tf.constant(1.0, shape=[num_labels]))
    
    global_step = tf.Variable(0)
    
    # Model.
    def model(data, prob = 1.):
        print(data.get_shape().as_list())
        
        conv1 = tf.nn.conv2d(data, layer1_weights, [1, 1, 1, 1], padding='SAME')
        pool1 = tf.nn.max_pool(conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        hidden1 = tf.nn.relu(pool1 + layer1_biases)
        
        print('hidden1:', hidden1.get_shape().as_list())
        
        conv2 = tf.nn.conv2d(hidden1, layer2_weights, [1, 1, 1, 1], padding='SAME')
#         pool2 = tf.nn.max_pool(conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        hidden2 = tf.nn.relu(conv2 + layer2_biases)
    
        print('hidden2:', hidden2.get_shape().as_list())
        
        conv3 = conv2 = tf.nn.conv2d(hidden2, layer3_weights, [1, 1, 1, 1], padding='SAME')
        pool3 = tf.nn.max_pool(conv3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        hidden3 = tf.nn.relu(pool3 + layer3_biases)
        
        print('hidden3:', hidden3.get_shape().as_list())
        
        shape = hidden3.get_shape().as_list()
        reshape = tf.reshape(hidden3, [shape[0], shape[1] * shape[2] * shape[3]])
        
#         print(reshape.shape)
        
        hidden4 = tf.nn.relu(tf.matmul(reshape, layer4_weights) + layer4_biases)
        hidden4 = tf.nn.dropout(hidden4, prob)

        print('hidden4:', hidden4.get_shape().as_list())
        
        hidden5 = tf.nn.relu(tf.matmul(hidden4, layer5_weights) + layer5_biases)
        hidden5 = tf.nn.dropout(hidden5, prob)
        
        print('hidden5:', hidden5.get_shape().as_list())
        
        return tf.matmul(hidden5, layer6_weights) + layer6_biases
    
    # Training computation.
    logits = model(tf_train_dataset, keep_probability)
    
    l2_loss = regularization_param * (tf.nn.l2_loss(layer1_weights) + tf.nn.l2_loss(layer1_biases) +
                                      tf.nn.l2_loss(layer2_weights) + tf.nn.l2_loss(layer2_biases) +
                                      tf.nn.l2_loss(layer3_weights) + tf.nn.l2_loss(layer3_biases) +
                                      tf.nn.l2_loss(layer4_weights) + tf.nn.l2_loss(layer4_biases) +
                                      tf.nn.l2_loss(layer5_weights) + tf.nn.l2_loss(layer5_biases) +
                                      tf.nn.l2_loss(layer6_weights) + tf.nn.l2_loss(layer6_biases))
    
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))
    loss += l2_loss
      
    # Optimizer.
#     optimizer = tf.train.GradientDescentOptimizer(start_learning_rate).minimize(loss)
    
    learning_rate = tf.train.exponential_decay(start_learning_rate, global_step, 6000, 0.75, staircase=True)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step = global_step)
    
    # Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = tf.nn.softmax(model(tf_valid_dataset))
    test_prediction = tf.nn.softmax(model(tf_test_dataset))


# In[ ]:


num_steps = 50001

import timeit
start_time = timeit.default_timer()

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    
    for step in range(num_steps):
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        batch_data = train_dataset[offset:(offset + batch_size), :, :, :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        
        if (step % 2000 == 0):
            print('Minibatch loss at step %d: %f' % (step, l))
            print('Minibatch accuracy: %.1f%%' % accuracy(predictions, batch_labels))
            print('Validation accuracy: %.1f%%' % accuracy(valid_prediction.eval(), valid_labels))
            
    print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
    
elapsed = timeit.default_timer() - start_time
print("Time:", elapsed)

