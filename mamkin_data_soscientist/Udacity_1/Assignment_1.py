
# coding: utf-8

# In[1]:


# These are all the modules we'll be using later. Make sure you can import them# These 
# before proceeding further.
from __future__ import print_function
import imageio
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import tarfile
from IPython.display import display, Image
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle

# Config the matplotlib backend as plotting inline in IPython
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


# DOWNLOAD START
url = 'https://commondatastorage.googleapis.com/books1000/'
last_percent_reported = None
data_root = '.' # Change me to store data elsewhere

def download_progress_hook(count, blockSize, totalSize):
  """A hook to report the progress of a download. This is mostly intended for users with
  slow internet connections. Reports every 5% change in download progress.
  """
  global last_percent_reported
  percent = int(count * blockSize * 100 / totalSize)

  if last_percent_reported != percent:
    if percent % 5 == 0:
      sys.stdout.write("%s%%" % percent)
      sys.stdout.flush()
    else:
      sys.stdout.write(".")
      sys.stdout.flush()
      
    last_percent_reported = percent
        
def maybe_download(filename, expected_bytes, force=False):
  """Download a file if not present, and make sure it's the right size."""
  dest_filename = os.path.join(data_root, filename)
  if force or not os.path.exists(dest_filename):
    print('Attempting to download:', filename) 
    filename, _ = urlretrieve(url + filename, dest_filename, reporthook=download_progress_hook)
    print('\nDownload Complete!')
  statinfo = os.stat(dest_filename)
  if statinfo.st_size == expected_bytes:
    print('Found and verified', dest_filename)
  else:
    raise Exception(
      'Failed to verify ' + dest_filename + '. Can you get to it with a browser?')
  return dest_filename

train_filename = maybe_download('notMNIST_large.tar.gz', 247336696)
test_filename = maybe_download('notMNIST_small.tar.gz', 8458043)
print("download success")
# DOWNLOAD FINISH


# In[3]:


# EXTRACT FORM ARCHIVE START
num_classes = 10
np.random.seed(133)

def maybe_extract(filename, force=False):
  root = os.path.splitext(os.path.splitext(filename)[0])[0]  # remove .tar.gz
  if os.path.isdir(root) and not force:
    # You may override by setting force=True.
    print('%s already present - Skipping extraction of %s.' % (root, filename))
  else:
    print('Extracting data for %s. This may take a while. Please wait.' % root)
    tar = tarfile.open(filename)
    sys.stdout.flush()
    tar.extractall(data_root)
    tar.close()
  data_folders = [
    os.path.join(root, d) for d in sorted(os.listdir(root))
    if os.path.isdir(os.path.join(root, d))]
  if len(data_folders) != num_classes:
    raise Exception(
      'Expected %d folders, one per class. Found %d instead.' % (
        num_classes, len(data_folders)))
  print(data_folders)
  return data_folders
  
train_folders = maybe_extract(train_filename)
test_folders = maybe_extract(test_filename)
print("extract success")
# EXTRACT FINISH


# In[4]:


#DISPLAY 2 IMG FROM EVERY FOLDER START
from IPython.display import Image

notMNIST_path = './notMNIST_large/'

def get_list_file_name(path):  
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            yield file
            
def get_list_folder_name(path):  
    for folder in os.listdir(path):
        if os.path.isdir(os.path.join(path, folder)):
            yield folder
            
def display_image_from_folder(folder_name):
    print("FOLDER NAME IS: ", folder_name)
    
    counter = 0
    image_amount_to_display = 2
    
    for file_name in get_list_file_name(folder_name): 
        display(Image(filename=os.path.join(folder_name, file_name)))
        counter += 1
        if counter == image_amount_to_display:
            break
        
# for folder_name in get_list_folder_name(notMNIST_path): 
#     print(folder_name, end = ' ')
#     folder_path = os.path.join(notMNIST_path, folder_name)
#     display_image_from_folder(folder_path)
    
print("display success")
#DISPLAY 2 IMG FROM EVERY FOLDER FINISH


# In[5]:


# CONVERT IMG INTO TENSOR START

image_size = 28  # Pixel width and height.
pixel_depth = 255.0  # Number of levels per pixel.

def load_letter(folder, min_num_images):
  """Load the data for a single letter label."""
  image_files = os.listdir(folder)
  dataset = np.ndarray(shape=(len(image_files), image_size, image_size),
                         dtype=np.float32)
  print(folder)
  num_images = 0
  for image in image_files:
    image_file = os.path.join(folder, image)
    try:
      image_data = (imageio.imread(image_file).astype(float) - 
                    pixel_depth / 2) / (pixel_depth / 1)
      if image_data.shape != (image_size, image_size):
        raise Exception('Unexpected image shape: %s' % str(image_data.shape))
      dataset[num_images, :, :] = image_data
      num_images = num_images + 1
    except (IOError, ValueError) as e:
      print('Could not read:', image_file, ':', e, '- it\'s ok, skipping.')
    
  dataset = dataset[0:num_images, :, :]
  if num_images < min_num_images:
    raise Exception('Many fewer images than expected: %d < %d' %
                    (num_images, min_num_images))
    
  print('Full dataset tensor:', dataset.shape)
  print('Mean:', np.mean(dataset))
  print('Standard deviation:', np.std(dataset))
  return dataset
        
def maybe_pickle(data_folders, min_num_images_per_class, force=False):
  dataset_names = []
  for folder in data_folders:
    set_filename = folder + '.pickle'
    dataset_names.append(set_filename)
    if os.path.exists(set_filename) and not force:
      # You may override by setting force=True.
      print('%s already present - Skipping pickling.' % set_filename)
    else:
      print('Pickling %s.' % set_filename)
      dataset = load_letter(folder, min_num_images_per_class)
      try:
        with open(set_filename, 'wb') as f:
          pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
      except Exception as e:
        print('Unable to save data to', set_filename, ':', e)
  
  return dataset_names

train_datasets = maybe_pickle(train_folders, 45000)
test_datasets = maybe_pickle(test_folders, 1800)

print("convert image success")
# CONVERT IMG INTO TENSOR START


# In[6]:


# PROBLEM 2, 3 START
print(type(train_datasets))
print(len(train_datasets))

for pickle_name in train_datasets:
    with open(pickle_name, 'rb') as f: 
        data_new = pickle.load(f)
    print(pickle_name)
#     print(type(data_new))
    print(data_new.shape)
    
    for image_amount in range(2):
        sample_image = data_new[np.random.randint(len(data_new)), :, :]  
        plt.figure()
        plt.imshow(sample_image)

print("problem 2, 3 success")
# PROBLEM 2, 3 FINISH


# In[7]:


# CREATE TRAINING & VALIDATION DATASETS START

def make_arrays(nb_rows, img_size):
  if nb_rows:
    dataset = np.ndarray((nb_rows, img_size, img_size), dtype=np.float32)
    labels = np.ndarray(nb_rows, dtype=np.int32)
  else:
    dataset, labels = None, None
  return dataset, labels

def merge_datasets(pickle_files, train_size, valid_size=0):
  num_classes = len(pickle_files)
  valid_dataset, valid_labels = make_arrays(valid_size, image_size)
  train_dataset, train_labels = make_arrays(train_size, image_size)
  vsize_per_class = valid_size // num_classes
  tsize_per_class = train_size // num_classes
    
  start_v, start_t = 0, 0
  end_v, end_t = vsize_per_class, tsize_per_class
  end_l = vsize_per_class+tsize_per_class
  for label, pickle_file in enumerate(pickle_files):       
    try:
      with open(pickle_file, 'rb') as f:
        letter_set = pickle.load(f)
        # let's shuffle the letters to have random validation and training set
        np.random.shuffle(letter_set)
        if valid_dataset is not None:
          valid_letter = letter_set[:vsize_per_class, :, :]
          valid_dataset[start_v:end_v, :, :] = valid_letter
          valid_labels[start_v:end_v] = label
          start_v += vsize_per_class
          end_v += vsize_per_class
                    
        train_letter = letter_set[vsize_per_class:end_l, :, :]
        train_dataset[start_t:end_t, :, :] = train_letter
        train_labels[start_t:end_t] = label
        start_t += tsize_per_class
        end_t += tsize_per_class
    except Exception as e:
      print('Unable to process data from', pickle_file, ':', e)
      raise
    
  return valid_dataset, valid_labels, train_dataset, train_labels
            
            
train_size = 500000
valid_size = 10000
test_size = 10000

valid_dataset, valid_labels, train_dataset, train_labels = merge_datasets(train_datasets, train_size, valid_size)
_, _, test_dataset, test_labels = merge_datasets(test_datasets, test_size)

print('Training:', train_dataset.shape, train_labels.shape)
print('Validation:', valid_dataset.shape, valid_labels.shape)
print('Testing:', test_dataset.shape, test_labels.shape)
print("success CREATE TRAINING & VALIDATION DATASETS START")

# CREATE TRAINING & VALIDATION DATASETS FINISH


# In[8]:


#CHEKIRYIU PICT & LABEL
for image_amount in range(2):
        index = np.random.randint(len(train_dataset))
        sample_image = train_dataset[index, :, :]  
        print(train_labels[index])
        plt.figure()
        plt.imshow(sample_image) 


# In[9]:


# RANDOMIZE START
def randomize(dataset, labels):
  permutation = np.random.permutation(labels.shape[0])
  shuffled_dataset = dataset[permutation,:,:]
  shuffled_labels = labels[permutation]
  return shuffled_dataset, shuffled_labels
train_dataset, train_labels = randomize(train_dataset, train_labels)
test_dataset, test_labels = randomize(test_dataset, test_labels)
valid_dataset, valid_labels = randomize(valid_dataset, valid_labels)

print("randomize success")
#RANDOMIZE FINISH


# In[10]:


# CHEKIRYIU SHO POSLE RANDA VSO KRYTA
check_amount = 2
for index in range(check_amount):
        sample_image = train_dataset[index, :, :]  
        print(train_labels[index])
        plt.figure()
        plt.imshow(sample_image)
        
print("vso kryta")


# In[11]:


# SAVE DATA FOR REUSE START

pickle_file = os.path.join(data_root, 'notMNIST.pickle')

try:
  f = open(pickle_file, 'wb')
  save = {
    'train_dataset': train_dataset,
    'train_labels': train_labels,
    'valid_dataset': valid_dataset,
    'valid_labels': valid_labels,
    'test_dataset': test_dataset,
    'test_labels': test_labels,
    }
  pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
  f.close()
except Exception as e:
  print('Unable to save data to', pickle_file, ':', e)
  raise
    
print("save data for reuse success")
# SAVE DATA FOR REUSE FINISH


# In[12]:


statinfo = os.stat(pickle_file)
print('Compressed pickle size:', statinfo.st_size)


# In[13]:


# CHESTNO SPIZENO

print(type(test_labels))

from hashlib import sha1 as hasher
def get_count_exact_overlaps(dataset_one, dataset_two):
  dataset_one_hash = [hasher(dataset_one_element).digest() for dataset_one_element in dataset_one]
  dataset_two_hash = [hasher(dataset_two_element).digest() for dataset_two_element in dataset_two]
  return np.intersect1d(dataset_one_hash, dataset_two_hash).size
    
print ("Overlaping between valid and train dataset : ", get_count_exact_overlaps(valid_dataset, train_dataset))
print ("Overlaping between test and train dataset : ", get_count_exact_overlaps(test_dataset, train_dataset))


# In[14]:


# import math

# image_size = 28
# label_amount = len(train_labels)

# weights = np.random.randn(label_amount, image_size ** 2) * math.sqrt(2.0 / image_size ** 2)
# biases = np.random.randn(label_amount, 1) * math.sqrt(2.0 / image_size ** 2)
# print(weights.shape)
# print(biases.shape)


# In[15]:


# def softmax(point):
#     return np.exp(point) / np.sum(np.exp(point), axis = 0)

# def training_step(train_data, train_labels):
#     X = np.reshape(train_data, (len(train_data), image_size ** 2))
#     scores = np.ndarray((len(train_data), label_amount, 1), dtype = np.float32)
#     probabilities = np.ndarray((len(train_data), label_amount, 1), dtype = np.float32)

#     loss = .0
#     for i in range(len(train_dataset)):
#         scores[i] = np.dot(weights, np.reshape(X[i], (image_size ** 2, 1))) + biases
#         probabilities[i] = softmax(scores[i])
#         loss += -math.log(probabilities[i][train_labels[i]])
        
#     return loss / len(train_data)

# print(training_step(train_dataset, train_labels))
# print("succ")


# In[16]:


# CHESTNO SPIZENO

import random

def disp_sample_dataset(dataset, labels, title=None):
#     print(type(dataset))
#     print(type(dataset[0]))
#     print(dataset.shape)
#     print(dataset[0].shape)
    
#     print(type(labels))
#     print(labels.shape)
#     print(type(labels[0]))
#     print(labels[0].shape)

    fig = plt.figure()
    if title: fig.suptitle(title, fontsize=16, fontweight='bold')
    items = random.sample(range(len(labels)), 10)
    for i, item in enumerate(items):
        plt.subplot(2, 5, i + 1)
        plt.axis('off')
        plt.title(chr(ord('A') + labels[item]))
        plt.imshow(dataset[item], cmap='Greys')
    plt.show()
    
def train_and_predict(sample_size):
    print("KEK ", sample_size)
    regr = LogisticRegression()
    X_train = train_dataset[:sample_size].reshape(sample_size, 784)
    y_train = train_labels[:sample_size]
    regr.fit(X_train, y_train)

    X_test = test_dataset.reshape(test_dataset.shape[0], 28 * 28)
    y_test = test_labels

    pred_labels = regr.predict(X_test)

    print('Accuracy:', regr.score(X_test, y_test), 'when sample_size=', sample_size)
    disp_sample_dataset(test_dataset, pred_labels, 'sample_size=' + str(sample_size))
    
for sample_size in [64, 128, 1024, 4096]:
    train_and_predict(sample_size)

