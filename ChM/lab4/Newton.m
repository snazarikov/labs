clc

%%% �������������
eps = 1e-3;
%%%

%%% �������� ������
%%% f - f(x)
%%% f_deriv_first - f'(x)
%%% f_deriv_second - f''(x)
f = @(x) (7 .* x .^ 6 - x .^ 4 + x .^ 2 - 3);
f_deriv_first = @(x) (7 .* 6 .* x .^ 5 - 4 .* x .^ 3 + 2 .* x);
f_deriv_second = @(x) (7 .* 6  .* 5 .* x .^ 4 - 4 .* 3 .* x .^ 2 + 2);
%%%

%%% ��������� ������� [start, finish] � ����� step
step = 0.1;
start = -2;
finish = 2;
seg_amount = ((finish - start) / step);
x = start : step : finish;
%%%

%%% �������� f(x), f'(x), f''(x) � ������ x
fx = f(x);
fd1 = f_deriv_first(x);
fd2 = f_deriv_second(x);
%%%

%%% ��������� ������
%%% root_amount - ���-�� ������
%%% � segs ��������� ������� ��� ������� �� ������� ��������� ������ 
%%% [x(segs(i)); x(segs(i) + 1)]
root_amount = 0;
for i = 1 : seg_amount
    if (sign(fx(i)) * sign(fx(i + 1))) < 0
%         if or((sign(fd1(i)) * sign(fd1(i + 1))) > 0, (sign(fd2(i)) * sign(fd2(i + 1))) > 0)
        if (sign(fd1(i)) * sign(fd1(i + 1))) > 0
            root_amount = root_amount + 1;
            segs(root_amount) = i;
        end
    end
end
fprintf('���-�� ������ = %d\n\n', root_amount);
%%%

%%% �������� �����
for i = 1 : root_amount
    fprintf('������ �� ������� (%f, %f)\n', x(segs(i)), x(segs(i) + 1));
    
    %%% ����� ���������� �����������
    if (sign(fx(segs(i))) * sign(fd2(segs(i)))) > 0
       x_prev = x(segs(i));
    elseif (sign(fx(segs(i) + 1)) * sign(fd2(segs(i) + 1))) > 0
       x_prev = x(segs(i) + 1);
    end
%     fprintf('x_prev = %f\n', x_prev);
    %%%
    
    %%% ���-�� ��������
    it_amount = 1;
    %%%
    
    while true
        %%% ��������� �����������
        x_new = x_prev - f(x_prev) / f_deriv_first(x_prev);
        %%%
        
        %%% ����� �� ���������� �����
        if abs(x_new - x_prev) < eps
            fprintf('���-�� �������� = %f\n', it_amount);
            fprintf('������ = %f\n\n', x_new);
            break;
        end
        %%%
        
        %%% ���������� �������� ��� ��������� ��������
        x_prev = x_new;
        it_amount = it_amount + 1;
        %%%
    end
end
%%%
