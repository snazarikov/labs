function [x] = SLAE_upper_triangular(A, b, n)
%     fprintf("SLAE_upper_triangular\n")
    
    x = zeros(size(b));
    for i = n : -1 : 1
       sum = 0;
       for j = (i + 1) : n
           sum = sum + A(i, j) * x(j);
       end
       x(i) = (b(i) - sum) / A(i, i);
    end
end
