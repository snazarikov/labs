function [value, it] = find_maxmin_eig_val(A, n, type, eps)
    value = 0;
    it = 0;

    if type == 'min'
        f1 = @(x) inv(x); % поправить
        f2 = @(x, y) 1 / sum(x .* y);
    else
        f1 = @(x) x;
        f2 = @(x, y) sum(x .* y);  
    end
    
    x_prev = rand(n, 1);    
    counter = 1;
    L_prev = -1e9;
    while true
        x_prev = x_prev / sqrt((sum(x_prev .* x_prev)));
        x_new = f1(A) * x_prev;                      
        L_new = f2(x_new, x_prev);
        flag = (mod(counter, 50000) == 0);
%         flag = 0;
        if flag == 1
            fprintf('counter = %d\n', counter);
            abs(L_new - L_prev)
            break;
        end
        if counter > 1 && abs(L_new / L_prev - 1) <= eps % поправить
            value = L_new;
            it = counter;
            break;
        end
        L_prev = L_new;
        x_prev = x_new;
        counter = counter + 1;
    end
end

