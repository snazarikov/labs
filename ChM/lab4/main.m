clc

%%% �������������
n = 5;
eps = 1e-3;

test_amount = 1;
%%%

for t = 1 : test_amount
    %%% �������������
    A = rand(n, n);
    A = A' + A;
    eig_vals = eig(A);    
    abs_eig_vals = abs(eig_vals);
    abs_eig_vals
%     fprintf('�������� ������\n')
%     A
    %%% 
    
    fprintf('���� �%d\n', t);
    
    %%%
    fprintf('�������� ��������� Matlab:\n');
    fprintf('max(eig_vals) = %.10f\n', max(abs(eig_vals)));
    fprintf('min(eig_vals) = %.10f\n', min(abs(eig_vals)));
    %%%
        
    %%% ��������� ����� (������� find_max_eig_val)
    fprintf('��������� �����:\n');
    [L1, it_L1] = find_max_eig_val(A, n, eps);
    fprintf('���-�� �������� = %d\n', it_L1);
    fprintf('������������ ����������� �������� = %.10f\n', abs(L1));
    %%%
    
    %%% ����� �������� ��������(������� find_min_eig_val)
    fprintf('����� �������� ��������:\n');
    [Ln, it_Ln] = find_min_eig_val(A, n, eps);
    fprintf('���-�� �������� = %d\n', it_Ln);
    fprintf('����������� ����������� �������� = %.10f\n', abs(Ln));
    %%%
    
    fprintf('\n');
end