function [value, it] = find_min_eig_val(A, n, eps)
    %%% ������������ ��������
    %%% value - ��������� ��������
    %%% it - ���-�� ��������
    value = 0;
    it = 0;
    %%%
    
    %%% �������������
    x_prev = rand(n, 1);    
    counter = 1;
    L_prev = -1e9;
    %%%
    
    while true
        %%% ����������
        x_prev = x_prev / sqrt((sum(x_prev .* x_prev)));
        %%%
        
        %%% ��������� ��������� ������ (� ������� LU - ����������)
%         x_new = inv(A) * x_prev;  
        x_new = LU_decomposition(A, x_prev, n);
        %%%
        
        %%% ��������� �����������
        L_new = 1 / sum(x_new .* x_prev);
        %%%
        
%         flag = (mod(counter, 5000) == 0);
% %         flag = 0;
%         if flag == 1
%             fprintf('counter = %d\n', counter);
%             abs(L_new - L_prev)
%             break;
%         end
        
        %%% ����� �� ���������� �����
        if counter > 1 && abs(L_new / L_prev - 1) <= eps
            value = L_new;
            it = counter;
            break;
        end
        %%%
        
        %%% ���������� �������� ��� ��������� ��������
        L_prev = L_new;
        x_prev = x_new;
        counter = counter + 1;
        %%%
    end
end

