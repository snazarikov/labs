function [y1] = calc_y1(tau, t0, y0, f)
    y1 = y0 + tau .* f(t0 + tau / 2, y0 + tau / 2 .* f(t0, y0));
end

