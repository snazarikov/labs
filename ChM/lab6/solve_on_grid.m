function [y] = solve_on_grid(grid_t, grid_size, tau, y0, f)
    y = zeros(1, grid_size);
%     y
    y(1) = y0;

    y(2) = calc_y1(tau, grid_t(1), y0, f);
    
%     y
    % % % ���������-���������
    for i = 2 : (grid_size - 1)
        prediction = y(i) + tau .* (3 / 2 .* f(grid_t(i), y(i)) - 1 / 2 .* f(grid_t(i - 1), y(i - 1)));
        correction = y(i) + tau .* (1 / 2 .* f(grid_t(i + 1), prediction) + 1 / 2 .* f(grid_t(i), y(i)));
%         correction
        y(i + 1) = correction;
    end
    % % % 
end

