clc

% % %  �������������
eps = 1e-6;

u0 = 1;
f = @(t, y) (-20 .* y .* y);

solution_analytic = @(t) (1 ./ (20 .* t + 1));

t_start = 0;
t_finish = 1;
% % % 


tau = 1e-2;
grid_t = t_start : tau : t_finish;
grid_size = ((t_finish - t_start) / tau) + 1;

[y] = solve_on_grid(grid_t, grid_size, tau, u0, f);
yn_prev = y(grid_size);
counter = 1;

while true
    tau = tau / 2;
    grid_t = t_start : tau : t_finish;
    grid_size = ((t_finish - t_start) / tau) + 1;
    [y] = solve_on_grid(grid_t, grid_size, tau, u0, f);
    yn_curr = y(grid_size);
    
    if (1 - (yn_prev / yn_curr)) < eps
        fprintf('tau = %d\n', tau)
%         fprintf('counter = %d\n', counter)
        break;
    end
    
    yn_prev = yn_curr;
    counter = counter + 1;
end

sol_an = solution_analytic(grid_t);
% max(abs(sol_an - y))

fprintf('max(abs(sol_an - y)) = %d\n', max(abs(sol_an - y)))
fprintf('���-�� ����� = %d\n', grid_size)

% sol_an, y

plot(grid_t, y, '*:r')
hold on
plot(grid_t, sol_an, 'Color', 'b', 'LineWidth', 1.0);
hold off


