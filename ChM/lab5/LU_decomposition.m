function [solution] = LU_decomposition(A, f, n)
    %
%     fprintf('�������� ������\n')
%     A, f

    L = zeros(size(A));
    U = zeros(size(A));
    %

    % ��������� ������������ ��������
    for i = 1 : n
        L(i, i) = 1;
    end
    %

    % ���������� ����������
    for i = 1 : n    
        for j = i : n
            U(i, j) = A(i, j);
            for k = 1 : (i - 1)
               U(i, j) = U(i, j) - L(i, k) * U(k, j); 
            end
        end

        for j = (i + 1) : n
            L(j, i) = A(j, i);
            for k = 1 : (i - 1)
               L(j, i) = L(j, i) - L(j, k) * U(k, i); 
            end
            if (U(i, i) == 0)
                fpritnf('������� �� 0\n')
                return;
            end
            L(j, i) = L(j, i) / U(i, i);
        end
    end
    %

%     fprintf('LU - ����������:\n')
    tmp = max(abs(A - L * U));
%     tmp

    % ������ ���
    y = SLAE_lower_triangular(L, f, n);
    %

    % �������� ���
    x = SLAE_upper_triangular(U, y, n);
    %

%     fprintf('�������:\n')
%     tmp = max(abs(A * x - f));
%     tmp
    solution = x;
end

