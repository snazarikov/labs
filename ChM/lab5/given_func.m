function [value] = given_func(f3, xs, n, point)
    if point < xs(1)
       value = f3(1); 
    elseif point >= xs(n)
        value = f3(n);
    else
        index = fix((point + 0.01) / 0.1) + 15;
        value = f3(index) + ((f3(index + 1) - f3(index)) / (xs(index + 1) - xs(index))) * (point - xs(index));
    end
end

