clc

%%% �������������
test_amount = 1;
n = 26;
x = -1.5 : 0.1 : 1.0;
f = [2.1538 2.2491 2.1052 1.6863 0.9788 0 -1.1951 -2.5088 -3.8008 -4.8951 -5.6 -5.7391 -5.1955 -3.9562 -2.1417 0 2.1417 3.9562 5.1955 5.7391 5.6 4.8951 3.8008 2.5088 1.1951 0];
%%%

%%% ��������� ����������� ��������
min_pow = 1;
max_pow = 9;
if max_pow - min_pow + 1 == 1
    subplot_row = 1;
    subplot_column = 1;
elseif max_pow - min_pow + 1 > 6
    subplot_row = 3;
    subplot_column = 3;
elseif max_pow - min_pow + 1 > 4
    subplot_row = 2;
    subplot_column = 3;
elseif max_pow - min_pow + 1 > 1
    subplot_row = 2;
    subplot_column = 2;    
end
%%%

%%% �s ��� �������
splain_x = [];
for i = 1 : (n - 1)
    splain_x = [splain_x, x(i)];
    splain_x = [splain_x, x(i + 1) - 0.0001];
end
splain_x = [splain_x, x(n)];
%%%

for polynom_pow = min_pow : max_pow
    %%% ������ s1(x)
    splain = [];
    for i = 1 : (2 * n - 1)
        splain = [splain, given_func(f, x, n, splain_x(i))];
    end
    %%%

    %%% ������� ������� polynom_pow
%     polynom_pow = 5;
    sum_x_pow = [n];
    sum_y_by_x_pow = [];

    for i = 1 : (polynom_pow * 2)
        sum_x_pow = [sum_x_pow, sum(x .^ i)];
    end

    for i = 1 : (polynom_pow + 1)
        sum_y_by_x_pow = [sum_y_by_x_pow, sum(f .* (x .^ (polynom_pow + 1 - i)))];
    end

    A = zeros(polynom_pow + 1, polynom_pow + 1);
    for i = 1 : (polynom_pow + 1)
        for j = 1 : (polynom_pow + 1)
            A(i, j) = sum_x_pow(((polynom_pow + 1) * 2 - (i + j)) + 1);
        end
    end

    polynom_coef = LU_decomposition(A, sum_y_by_x_pow, polynom_pow + 1);
    %%%

    %%% ����������� ����������� 
    %%% ������� ���� - ������ s1(x)
    %%% ����� ���� - ������� ������� polynom_pow
    %%% ������� * - �������� ����� (xi, fxi)
%     subplot(max_pow - min_pow + 1, 1, polynom_pow - (min_pow - 1));
    subplot(subplot_row, subplot_column, polynom_pow - (min_pow - 1));
    plot(splain_x, splain, 'Color', 'g', 'LineWidth', 1.2)
    hold on
    t = linspace(-1.5, 1.0);
    plot(t, polyval(polynom_coef, t), 'Color', 'b', 'LineWidth', 1.2);
    hold on
    plot(x, f, '*:r')
    subplot_title = strcat('������� ����������=', num2str(polynom_pow)); 
    title(subplot_title)
    hold off
    %%%

    %%% �������� � ����������� ������
    x_check = [-1.3065 -0.4072 0.7581];

    splain_check = [];
    for i = 1 : 3
        splain_check = [splain_check, given_func(f, x, n, x_check(i))];
    end

    polynom_check = polyval(polynom_coef, x_check);

    fprintf('������� ���������� = %d\n', polynom_pow)
    fprintf('�������� � ����������� ������\n')
    x_check
    splain_check
    polynom_check
    fprintf('\n')
    %%%
end
