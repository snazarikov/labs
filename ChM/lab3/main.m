clc

% % % �������������
n = 500;
eps = 1e-3;

test_amount = 5;

avg_it_SUR = 0;
avg_it_MR = 0;
avg_time_SUR = 0.0;
avg_time_MR = 0.0;
% % %
for t = 1 : test_amount
    % % % �������������
    rand_matrix = rand(n, n);
    A = rand_matrix' * rand_matrix;
    f = rand(n, 1);
%     A
    A = make_matrix_diagonal_prevalence(A, 0.05, n);
%     A
    % % % 
    fprintf('���� �%d\n', t);
%     fprintf('�������� ������\n')
%     A, f

    % % % ����� ���
    step = 0.01;
    w = 1.0;
%     w = find_optimal_w(step, A, f, n, eps);
    tic
    [answer_SUR, it_SUR] = successive_upper_relaxation(A, f, n, eps, w);
    time_SUR = toc;

    fprintf('����� ���\n')
    fprintf('��������� w: %f\n', w);
    fprintf('���-�� ��������: %d\n', it_SUR)
    fprintf('�����: %f secs\n', time_SUR)
    max(abs(A * answer_SUR' - f))
    % % % 

    % % % ����� ����������� �������
    tic
    [answer_MR, it_MR] = minimal_residual(A, f, n, eps);
    time_MR = toc;

    fprintf('����� ����������� �������\n')
    fprintf('���-�� ��������: %d\n', it_MR)
    fprintf('�����: %f secs\n', time_MR)
    max(abs(A * answer_MR' - f))
    % % % 
    
    avg_it_SUR = avg_it_SUR + it_SUR;
    avg_it_MR = avg_it_MR + it_MR;
    avg_time_SUR = avg_time_SUR + time_SUR;
    avg_time_MR = avg_time_MR + time_MR;
    
    fprintf('\n');
end

avg_it_SUR = avg_it_SUR / test_amount;
avg_it_MR = avg_it_MR / test_amount;
avg_time_SUR = avg_time_SUR / test_amount;
avg_time_MR = avg_time_MR / test_amount;

fprintf('avg_it_SUR: %d\n', avg_it_SUR)
fprintf('avg_it_MR: %d\n', avg_it_MR)
fprintf('avg_time_SUR: %f secs\n', avg_time_SUR)
fprintf('avg_time_MR: %f secs\n', avg_time_MR)