function [optimal_w] = find_optimal_w(step, A, f, n, eps)
    optimal_w = 1;
    min_it = 501;
    for i = 1.0 : step : 2.0
        [answer_SUR, it_SUR] = successive_upper_relaxation(A, f, n, eps, i);
%         it_SUR, i
        if (min_it > it_SUR)
           optimal_w = i;
           min_it = it_SUR;
        end
    end
end

