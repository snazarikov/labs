function [answer, it_amount] = successive_upper_relaxation(A, f, n, eps, w)
    counter = 1;
    Xs = [rand(1, n);];

    while true
        x = zeros(1, n);

        for i = 1 : n
            sum = 0.0;

            for j = 1 : (i - 1)
                sum = sum + A(i, j) * x(j);
            end
            for j = (i + 0) : n
                sum = sum + A(i, j) * Xs(counter, j);
            end

            x(i) = Xs(counter, i) + w / A(i, i) * (f(i) - sum);
        end

        Xs(counter + 1, :) = x;

%         if counter >= 2 & max(abs(Xs(counter + 1, :) - Xs(counter, :))) <= eps * max(abs(Xs(counter, :) - Xs(counter - 1, :)))
%             fprintf('Понадобилось %d итераций\n', counter);
%             answer = x;
%             it_amount = counter;
%             break;
%         end

        if max(abs(A * x' - f)) < eps
%             fprintf('Понадобилось %d итераций\n', counter);
            answer = x;
            it_amount = counter;
            break;
        end

        flag = (mod(counter, 500) == 0);
%         flag = 0;
        if flag == 1
%            fprintf('Counter: %d\n', counter); 
%            max(abs(Xs(counter + 1, :) - Xs(counter, :)))
%            eps * max(abs(Xs(counter, :) - Xs(counter - 1, :)))
%            x
           it_amount = counter;
           answer = x;
           break;
        end

        counter = counter + 1;
    end
end

