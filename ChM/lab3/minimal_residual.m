function [answer, it_amount] = minimal_residual(A, f, n, eps)
    counter = 1;
    Xs = [rand(1, n);];

    while true        
        % ���������� ������� ������� - r
        r = zeros(1, n);        
        for i = 1 : n
            for j = 1 : n
                r(i) = r(i) + A(i, j) * Xs(counter, j);
            end
            r(i) = r(i) - f(i);
        end
        %
        
        % ���������� Ar
        Ar = zeros(1, n);        
        for i = 1 : n
            for j = 1 : n
                Ar(i) = Ar(i) + A(i, j) * r(j);
            end
        end
        %
        
        % ���������� ������������ tau
%         tau = sum(Ar .* r) / max(abs(Ar)) ^ 2;
        tau = sum(Ar .* r) / sum(Ar .* Ar);
        %
        
        % ���������� (k + 1)�� �����������
        x = zeros(1, n);
        for i = 1 : n
            x(i) = Xs(counter, i) - tau * r(i);
        end
        %
        
        Xs(counter + 1, :) = x;

        if max(abs(A * x' - f)) < eps
%             fprintf('������������ %d ��������\n', counter);
            answer = x;
            it_amount = counter;
            break;
        end
        
        flag = (mod(counter, 500) == 0);
%         flag = 0;
        if flag == 1
%         if counter == 10000
%            r, Ar, tau
%            fprintf('Counter: %d\n', counter); 
%            max(abs(A * x' - f))
%            max(abs(Xs(counter + 1, :) - Xs(counter, :)))
%            eps * max(abs(Xs(counter, :) - Xs(counter - 1, :)))
%            x
           it_amount = counter;
           answer = x;
           break;
        end

        counter = counter + 1;
    end
end

