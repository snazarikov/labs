function matrix = make_matrix_diagonal_prevalence(A, coefficient, n)
    for i = 1 : n
        for j = 1 : (i - 1)
            A(i, i) = A(i, i) + coefficient * abs(A(i, j));
        end
        for j = (i + 1) : n
            A(i, i) = A(i, i) + coefficient * abs(A(i, j));
        end
    end
    matrix = A;
end

