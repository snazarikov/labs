function [x] = SLAE_lower_triangular(A, b, n)
%     fprintf("SLAE_lower_triangular\n")
    
    x = zeros(size(b));
    for i = 1 : n
       sum = 0;
       for j = 1 : (i - 1)
           sum = sum + A(i, j) * x(j);
       end
       x(i) = (b(i) - sum) / A(i, i); 
    end
end

