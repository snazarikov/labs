/* #19. ���� ������������������ ����������� ����� {Aj}j=1...n (n<=10000). ������� �� ������������������ �����, ���������� ����� 2, � ����� ���������� �������������� �����-����������. */

#include <stdio.h>
#define NMAX 20000

int A[NMAX];

int two (int n) {
	int flag = 0;
	while (n) {
		if (n%10==2) flag = 1;
		n/=10;
	}
	return flag;
}

int palindrom (int n) {
	int replica, pal = 0, flag = 0;
	replica = n;
	while (replica) {
		  pal=pal*10+(replica%10);
		  replica/=10;
	}
	if (pal == n) flag = 1;
	return flag;
}
int main (void) {
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	int n = 0, i, j, counter=0, count;
	while (scanf("%d", A+n)>0) n++;
	count = n;
	for (i=n-1; i>=0; i--)
		if (two(A[i])==1) {
			for (j=i; j<count; j++) A[j]=A[j+1];
			count--;
		}
	n = count;
	for (i=count-1; i>=0; i--)
		if (palindrom(A[i])==1) {
			for (j=n-1; j>=i; j--) A[j+1]=A[j];
			n++;
		}
	for (i=0; i<n; i++) printf ("%d ", A[i]);
	return 0;
}
