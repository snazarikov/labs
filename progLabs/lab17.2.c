#include <stdio.h>
#include <string.h>
#define NMAX 100

char help[NMAX];

char *glas = "����������Ũ�������";
char *sogl = "������������������������������������������";

int isVowel (char c) {
    if(strchr(glas, c)) return 1;
    return 0;
}

int isConsonant (char c) {
    if(strchr(sogl, c)) return 1;
    return 0;
}

int LETTER(char c) {
    if('�' <= c && c <= '�' || '�' <= c && c <= '�' || c=='�' || c=='�') return 1;
    return 0;
}

void toUpperCase (char A[NMAX]) {
	int i;
	for (i=1; i<=A[0]; i++) {
		if (A[i]>='�' && A[i]<='�') A[i]-=32;
		else if (A[i]=='�') A[i] = '�';
	}
}

void toLowerCase (char A[NMAX]) {
	int i;
	for (i=1; i<=A[0]; i++) {
		if (A[i]>='�' && A[i]<='�') A[i]+=32;
		else if (A[i]=='�') A[i] = '�';
	}
}

int main (void) {
	
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int symbol, i, vowel = 0, consonant = 0, length = 0;
	char *p_w;
    
    while((scanf("%c", &symbol)) > 0) {
    	    	
        if (LETTER((char) symbol)) {
        	
            p_w = help+1;
            while(LETTER((char) symbol) && symbol != -1) {
            	*(p_w++) = symbol;
                symbol = getchar();
                length++;
            }
            *p_w = '\0';
            
            vowel = 0;
            consonant = 0;
            help[0]=length;
            
            for (i=1; i<=help[0]; i++) {
            	if (isVowel(help[i])==1) vowel++;
				if (isConsonant(help[i])==1) consonant++;
			}
			
			if ((consonant-vowel)==3) {
				toUpperCase(help);
				for (i=1; i<=help[0]; i++) printf("%c", help[i]);
    			printf("(%d %d)", consonant, vowel);
			}
			
			else for (i=1; i<=help[0]; i++) printf("%c", help[i]);
			
			length = 0;
			
        }
        
		if (LETTER((char) symbol) == 0) printf("%c", symbol);
		
    }
	return 0;
}
