// #19.���� ������������� ������� {Aij}i=1,...,n;j=1,...,m (n,m<=20).    ����� ������� �� ������������ ��������� ��������.

#include <stdio.h>
#define NMAX 20
int A[NMAX][NMAX];
int pr [NMAX];
int main(void) {
	freopen("input9.txt", "r", stdin);
	freopen("output9.txt", "w", stdout);
	int n,m,i,j, min = 1000000001;
	scanf ("%d %d", &n, &m);
	for (i=0; i<n; i++) 
		for (j=0; j<m; j++) scanf ("%d", &A[i][j]);
	for (j=0; j<m; j++) pr[j] = 1;
	for (j=0; j<m; j++) 
		for (i=0; i<n; i++) pr[j]*=A[i][j];
	for (j=0; j<m; j++) 
		if (pr[j]<min) min = pr[j];
	printf ("%d", min);
	return 0;
}
