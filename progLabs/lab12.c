// #19. ���� ������������� ������� {Aij}i=1..n,j=1..m (n,m<=100). ����� ������� � ���������� ������ ��������� � �������� ��� �������� ����� ������� ���� ������.

#include <stdio.h>
#define NMAX 100
int A[NMAX][NMAX];
int sum [NMAX];
int main(void) {
	freopen("input9.txt", "r", stdin);
	freopen("output9.txt", "w", stdout);
	int n,m,i,j, maxSum = -1000000001, number;
	scanf ("%d %d", &n, &m);
	for (i=0; i<n; i++) 
		for (j=0; j<m; j++) scanf ("%d", &A[i][j]);
	for (j=0; j<m; j++) sum[j] = 0;
	for (j=0; j<m; j++) 
		for (i=0; i<n; i++) sum[j]+=A[i][j];
	for (j=0; j<m; j++) 
		if (sum[j]>maxSum) {
			maxSum = sum[j];
			number = j;
		}
	for (i=0; i<n; i++) A[i][number] = maxSum;
	for (i=0; i<n; i++) {
		for (j=0; j<m; j++) printf ("%d ", A[i][j]);
		printf("\n");
	}
	return 0;
}
