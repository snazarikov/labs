//#19. ���� ������������������ ����������� ����� {aj}j=1...n (n<=10000). ���� � ������������������ ���� ���� �� ���� �����, ���������� ����� 5, ����������� ������������������ �� ����������.

#include <stdio.h>
#define NMAX 10000

int A[NMAX]; 
int main(void) {
	//freopen("input11.txt", "r", stdin);
	//freopen("output11.txt", "w", stdout);
	int n = 0, i = 0, j, counter = 0, k;
	while (scanf("%d", A+n)>0) n++;
 	for (i = 0; i<n; i++) {
 		if (counter>0) break;
 		k = A[i];
 		while (k) {
 			if (k % 10==5) counter++;
 			k/=10;
 		}
 	}
	if (counter>0) 
	   for (i = 0; i<n-1; i++) 
	   	   for (j = i+1; j<n; j++) 
	   	   	   if (A[i]>A[j]) {
	   	  	   	  k = A[i];
	  		  	  A[i] = A[j];
 	  	  		  A[j] = k;
		       }
	for (i = 0; i<n; i++) printf ("%d ", A[i]);
	return 0;
}
