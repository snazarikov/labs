/* #19.���� ������������� ������� {Aij}i=1...n;j=1..n , n<=100. ���� �� ��� ������������ �������� ������� ������������, �������� ��� ������� ����� � ������� �� ������������ �������� ��������������� ��������. ������������ ��������� � �������! */

#include <stdio.h>
#define NMAX 100

int A[NMAX][NMAX];
int n;

void getData (int *n, int A[NMAX][NMAX]) {
	scanf("%d", n);
	int i,j;
	for(i=0;i<*n;i++)
	    for(j=0;j<*n;j++) scanf("%d",&A[i][j]);
}

void printData (int n, int A[NMAX][NMAX]) {
	int i,j;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++) printf("%d ",A[i][j]);
		printf("\n");
	}
}

int diagonal (int n, int A[NMAX][NMAX]) {
	int i,j;
	int check=0;
	for (i=0; i<n; i++)
		if (A[i][i]>0) check++;
	if (check==n) return 1;
	else return 0;
}

int  maxInCol (int j,int n, int A[NMAX][NMAX]) {
	int max=-1000000001;
	int i;
	for (i=0; i<n; i++)
		if (A[i][j]>max) max=A[i][j];
	return max;	
}

int prime (int n) {
	int d;
	if (n<2) return 0;
	for (d=2; d*d<=n; d++) 
		if (n%d==0) return 0;
	return 1; 
}

int solve (int n, int A[NMAX][NMAX]) {
	int i,j;
	if (diagonal(n,A)==0)
	   for (i=0; i<n; i++)
	   	   for (j=0; j<n; j++)
	   	   	   if (prime(A[i][j])==1) A[i][j]=maxInCol(j, n, A);
}

int main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	getData(&n, A);
	solve (n, A);
	printData(n, A);
	return 0;
}
