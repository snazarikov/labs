/* #19.������ ������������������ ����������� ����� {Aj}j=1...n (n<=1000). ����������� ������������������ �� ���������� ����������� ����� �����, ����� � ����������� ����������� ������� ������������� ����������� �� ���������� ������������ ���� �����, ����� � ����������� ����������� ������� � ����������� �������������� ���� ������������� ����������� �� ���������� ������ �����. */
#include <stdio.h>
#define NMAX 1000

char LDig[NMAX];
int pr[NMAX];
int A[NMAX]; 
int main(void) {
	freopen("input10.txt", "r", stdin);
	freopen("output10.txt", "w", stdout);
	int n = 0, i = 0, k, j, p;
	char l;
	while (scanf("%d", A+n)>0) n++;
	for (i = 0; i<n; i++) {
		LDig[i] = 10;
		pr[i] = 1;
	}
 	for (i = 0; i<n; i++) {
 		k = A[i];
 		while (k) {
 			if (k % 10<LDig[i]) LDig[i] = k % 10;
 			pr[i]*=k % 10;
 			k/=10;
 		}
 	}
	for (i = 0; i<n-1; i++) {
		for (j = i+1; j<n; j++) {
			if ((LDig[i]>LDig[j]) || (LDig[i]==LDig[j] && pr[i]>pr[j]) || (LDig[i]==LDig[j] && pr[i]==pr[j] && A[i]>A[j])) {
			   k = A[i];
			   A[i] = A[j];
			   A[j] = k;
			   l = LDig[i];
			   LDig[i] = LDig[j];
			   LDig[j] = l;
			   p = pr[i];
			   pr[i] = pr[j];
			   pr[j] = p;
 		   }
		}
	}
	for (i = 0; i<n; i++) printf ("%d ", A[i]);
	return 0;
}
