#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NMAX 10005
#define maximum(a, b) (((a) > (b)) ? (a) : (b))

int groupCount;
int A[NMAX];
int B[NMAX];
int sum[NMAX];
int sumRes[NMAX];
int multRes[NMAX];
int mult[NMAX];
int result[NMAX];

int compare (int* A, int* B) {
	int i;

    if(A[0] > B[0]) return 1;
    if(A[0] < B[0]) return -1;

    for(i = A[0]; i > 0; i--) {
        if(A[i] > B[i]) return 1;
        if(A[i] < B[i]) return -1;
    }

    return 0;
}

void scanNumeral (int* A) {
	char S[NMAX];
	int i, j;

	gets(S);
	A[0] = strlen(S);

	for (i=A[0]-1, j=1; i>=0; i--, j++) {
		A[j] = S[i] - '0';
	}
}

void printNumeral (int* A) {
	int i;

	while (A[A[0]]==0 && A[0]!=1) A[0]--;

	for (i = A[0]; i; i--) {
		printf("%c", A[i]+'0');
	}
	printf("\n");
}

void addition (int* A, int* B, int* Z) {
	int d, i;

    memset(Z, 0, NMAX * sizeof(int));
	Z[0] = maximum(A[0],B[0]);
	d = 0;

	for (i=1; i<=Z[0]; i++) {
		d+=(A[i]+B[i]);
		Z[i] = d%10;
		d/=10;
	}

	if (d) Z[++Z[0]] = d;
}

void divisionOfLongByShort (int* A, int shortNum, int* Z) {
	int d, i;

    memset(Z, 0, NMAX * sizeof(int));
	Z[0] = A[0];
	d = 0;

	for (i=A[0]; i; i--) {
		d=d*10 + A[i];
		Z[i]=d/shortNum;
		d%=shortNum;
	}

	while (Z[0]>1 && !Z[Z[0]]) Z[0]--;
}

void multiplication (int* A, int* B, int* Z) {
	int d, i, j;

    memset(Z, 0, NMAX * sizeof(int));

    for (i=1; i<=A[0]; i++) {
    	for (j=1; j<=B[0]; j++) {
    		Z[i+j-1]+=A[i]*B[j];
		}
	}
    Z[0] = A[0] + B[0] - 1;
    for (i=1; i<=Z[0]; i++) {
    	Z[i+1]+=Z[i]/10;
    	Z[i]%=10;
	}

	d=Z[Z[0]+1];
	while (d) {
		Z[++Z[0]]=d%10;
		d/=10;
	}
}

void longPower (int* A, int pow, int* result) {
	int i;
	int help[NMAX];
	int tmp[NMAX];

	memcpy(tmp, A, NMAX * sizeof(int));
	memset(result, 0, NMAX * sizeof(int));
	result[0] = 1;
	result[1] = 1;

	while(pow) {
		memset(help, 0, NMAX * sizeof(int));
		if (pow % 2 == 0) {
			pow /= 2;
			multiplication(tmp, tmp, help);
			memcpy(tmp, help, NMAX * sizeof(int));
		}
		else {
			pow--;
			multiplication(result, tmp, help);
			memcpy(result, help, NMAX * sizeof(int));
		}
	}
}

void longRoot (int* num, int* A, int* B, int* result) {
	int help1[NMAX];
	int help2[NMAX];
	int C[NMAX];
	int tmp[NMAX];

	longPower(A, groupCount, help1);

	while (1) {
		memset(tmp, 0, NMAX * sizeof(int));
		addition(A, B, help1);
		divisionOfLongByShort(help1, 2, C);
		memcpy(help2, C, NMAX * sizeof(int));

		if (compare(C, A) == 0 || compare(C, B) == 0) {
			memcpy(result, A, NMAX * sizeof(int));
			return;
		}

		longPower(C, groupCount, tmp);

		if ((compare(help1, num) == 1 && compare(tmp, num) == 1) || ((compare(help1, num) == -1 && compare(tmp, num) == -1)) || compare(help1, num) == 0 || compare(tmp, num) == 0) {
			memcpy(A, help2, NMAX * sizeof(int));
		}

		else {
			memcpy(B, help2, NMAX * sizeof(int));
		}
	}
}

main (void) {
	freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);

	int i, j, elemCount, k;

	mult[0]=1;
	mult[1]=1;

	scanf("%d", &groupCount);

	for (i=0; i<groupCount; i++) {

		scanf("%d\n", &elemCount);
		memset(sum, 0, NMAX * sizeof(int));

		for (j=0; j<elemCount; j++) {
			scanNumeral(A);
			addition(A,sum,sumRes);
			memset(A, 0, NMAX * sizeof(int));
			memcpy(sum, sumRes, NMAX * sizeof(int));
		}

		divisionOfLongByShort(sum, elemCount, sumRes);
		multiplication(mult, sumRes, multRes);
		memcpy(mult, multRes, NMAX * sizeof(int));
	}

	memset(A, 0, NMAX * sizeof(int));
	memset(B, 0, NMAX * sizeof(int));

	B[0] = multRes[0] / groupCount;
	if (multRes[0] % groupCount != 0) {
		B[0]++;
	}
	B[++B[0]] = 1;
	A[0] = B[0] - 1;
	A[A[0]] = 1;

	if (groupCount == 1) {
		printNumeral(multRes);
	}
	else {
		longRoot (multRes, A, B, result);
		printNumeral(result);
	}


	return 0;
}
