/*
#19. ��� ����, ���������� ������� �����, ������ ������ �� ��������� 1 � ����.
����� � ������ N (N <= 100) ����� ������� ����, �� ���������� �� ����� �� ���� �������� ����.
�������� ��������� ����� � ��������� ���� � ������� �� ����������� �����. 
��� ��������� ����� ������ ���� �������. ����� N ������� �� �����.
*/

#include <stdio.h>
#include <string.h>
#define NMAX 100

char firstLetter, secondLetter, thirdLetter;
int n;

char words[NMAX][NMAX];
char help[NMAX];

int LETTER(char c) {
    if('�' <= c && c <= '�' || '�' <= c && c <= '�' || c=='�' || c=='�')
        return 1;
    return 0;
}

void CLEAR (char A[NMAX]) {
	int i, length;
	length = A[0];
	for (i=0; i<=length; i++) A[i] = 0;
}

int similar (char A[NMAX]) {
	int i;
	for (i=0; i<n; i++)
		if (strcmp(A,words[i])==0) return 1;
	return 0;
}

int wrongLetterIndicator(char A[NMAX]) {
	int i;
	for (i=1; i<=A[0]; i++) 
		if (A[i]==firstLetter || A[i]==secondLetter || A[i]==thirdLetter) return 1;
    return 0;
}

void toLowerCase (char A[NMAX]) {
	int i;
	for (i=1; i<=A[0]; i++) {
		if (A[i]>='�' && A[i]<='�') A[i]+=32;
		else if (A[i]=='�') A[i] = '�';
	}
}

int main (void) {
	
	freopen("input16.txt", "r", stdin);
	freopen("output16.txt", "w", stdout);
	
	int symbol, i, j, length = 0, min = 101, check;
    char *p_w;
    
    scanf("%d", &n);
    scanf("\n");
    scanf("%c ", &firstLetter);
    scanf("%c ", &secondLetter);
    scanf("%c ", &thirdLetter);
    scanf("\n");
    
    if (firstLetter>='�' && firstLetter<='�') firstLetter+=32;
    if (secondLetter>='�' && secondLetter<='�') secondLetter+=32;
    if (thirdLetter>='�' && thirdLetter<='�') thirdLetter+=32;
    
        
    while((scanf("%c", &symbol)) != EOF && symbol != -1)
        if(LETTER((char) symbol)) {
            p_w = help+1;
            while(LETTER((char) symbol) && symbol != -1) {
                *(p_w++) = symbol;
                symbol = getchar();
                length++;
            }
            *p_w = '\0';
            help[0]=length;
            toLowerCase(help);
            for (i=0; i<n; i++) 
            	if (words[i][0]<min) {
            		check = i;
            		min = words[i][0];
				}
            if (help[0]>words[check][0] && similar(help)==0 && wrongLetterIndicator(help)==0) strcpy(words[check], help);
			CLEAR(help);
            length=0;
            min = 101;
        }
      
      
	for (i=0; i<n-1; i++)
		for (j=i+1; j<n; j++) 
			if (words[i][0]<words[j][0]) {
				strcpy(help,words[i]);
				strcpy(words[i],words[j]);
				strcpy(words[j],help);
			}
	
    for (i=0; i<n; i++) {
    	for (j=1; j<=words[i][0]; j++) printf("%c", words[i][j]);
		printf("\n");
	}
	
	return 0;
}
