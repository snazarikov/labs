/*
������ "����� ���������"

	� ��������� ����� INPUT.TXT ������� ���� �������� �������������� ��������� � ��������� �������.
	� ������ ������ �������� ����� N - ������ ��������� �� ��������� (���������� �����) � M - ������ ��������� �� ����������� (���������� ��������) (2 <= N, M <= 200).
	� ��������� N ������� �������� �� M �������� � ������. ������ '.' (ASCII 46) �������� ������ ������ ���������, ������ 'x' (ASCII 120) - �����, ������ 'S' - ��������� ����� ���� � ������ 'T' - �������� ����� ����. ������� ������ �������� � ���� ������� ���.
	�� ����� ������ ������ ����� ���������� � ���� �� ������� �������� ����� (�����, ������, �����, ����), ���� ��� ������ ���������� � �� ������ �������. ������, ���������� ������� S � �, ��������� �������.
	� ��������� ���� OUTPUT.TXT �������� ����� ����������� ���� �� S � T ��� ����� -1, ���� ���� ���.

*/

#include <stdio.h>
#include <string.h>
#define NMAX 200

char A[NMAX][NMAX];
int v[NMAX][NMAX];
int q[3][NMAX*NMAX];
int di[] = {-1, 0, 1, 0};
int dj[] = {0, -1, 0, 1};

int n, m;

main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int i, j, ii, jj, d, stPosX, stPosY, finPosX, finPosY, top = 0, tail = 0;
	
	scanf("%d %d", &n, &m);
	
	for (i=0; i<n; i++) {
		scanf("\n");
		for (j=0; j<m; j++) {
			scanf("%c", &A[i][j]);
			if (A[i][j] == 'S') {
				stPosX = i;
				stPosY = j;
			}
			if (A[i][j] == 'T') {
				finPosX = i;
				finPosY = j;
			}
			v[i][j] = -1;
		}
	}
	
	v[stPosX][stPosY] = 0;
	q[0][tail] = 0;
	q[1][tail] = stPosX;
	q[2][tail] = stPosY;
	tail++;
	
	while (top != tail) {
		for(d = 0; d < 4; d++) {
			ii = q[1][top] + di[d];
			jj = q[2][top] + dj[d];
			if (A[ii][jj] != 'x' && v[ii][jj] == -1 && ii >= 0 && ii < n && jj >= 0 && jj < m) {
				v[ii][jj] = q[0][top] + 1;
				q[0][tail] = q[0][top] + 1;
				q[1][tail] = ii;
				q[2][tail] = jj;
				tail++;
			}
		}
		top++;
	}
	
/*	for (i=0; i<n; i++) {
		for (j=0; j<m; j++) {
			printf ("%d ", v[i][j]);
		}
		printf("\n");
	}*/
	
	printf("%d", v[finPosX][finPosY]);
	
	return 0;
}
