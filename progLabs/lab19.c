
/* 
#19. ������ ������������������ ����������� �����. ���� � ������������������ ��� 
�� ������ �����, ������������� ������ 3, ����������� ������������������ �� �� 
����������� ��������� �����. � ��������� ������ ������� �� ������������������ 
������ �����, ����� ����� 2, � �������������� ������� �����. ������������������ 
������� � ����������� ������. 
*/

#include <stdio.h>
#include <stdlib.h>

int firstDig (int num) {
	int d;
	while (num) {
		d = num%10;
		num /= 10;
	}
	return d;
}

int prime (int num) {
	int d;
	if (num < 2) return 0;
	for (d=2; d*d<=num; d++) {
		if (num%d == 0) return 0;
	}
	return 1;
}

main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int value, flag = 0, tmp;
	
	struct LIST {
		int data;
		struct LIST *next;
	};
	struct LIST *q, *p, *p1, *p2, *tail, *pred, *top = NULL;
	
	while(scanf("%d", &value) != EOF) {
		if (firstDig(value) == 3) flag = 1;
		p = (struct LIST*)malloc(sizeof(struct LIST));
		p->data = value;
		p->next = NULL;
		if (top) {
			tail -> next = p;
		}
		else top = p;
		tail = p;
	}
	
	
	if (flag == 1) {
		while (top && (top->data)%2==0 && (top->data) != 2) {
			top = top->next;
		}
		if (top) {
			pred = top;
			p = top->next;
			while (p) {
				if ((p->data)%2==0 && (p->data) != 2) pred->next = p->next;
				else pred = p;
				p = p->next;
			}
		}
		
		p = top;
		
		while (p) {
			if (prime(p->data) == 1) {
				q = (struct LIST*)malloc(sizeof(struct LIST));
				q->data = p->data;
				q->next = p->next;
				p ->next = q;
				p = q->next;
			}
			else p = p->next;
		}

	}
	
	if (flag == 0) {
		for (p1 = top; p1 != NULL; p1 = p1->next) {
			for (p2 = p1->next; p2 !=NULL; p2 = p2->next) {
				if ((p1->data)%10 < (p2->data)%10) {
					tmp = p1->data;
					p1->data = p2->data;
					p2->data = tmp;
				}
			}
		}
	}
	
	p = top;
	
	while (p) {
		printf("%d ", p->data);
		p = p->next;
	}
	
	return 0;
}
