
/* 
#19. ������ ������������������ ����������� �����. ���� ������������������ �����������
�� �� �������� ��� �� �� ����������� ��������� �����, ������� �� ������������������ 
������� �����, � �������������� ����� ������� 12. � ��������� ������ ����������� 
������������������ �� �� �������� ������ �����. ������������������ ������� � ���������� 
����������� ������ � ��������� ���������. 
*/

#include <stdio.h>
#include <stdlib.h>

struct LIST {
	int data;
	struct LIST *L;
	struct LIST *R;
};
	
struct LIST* make() {
	struct LIST* sent = (struct LIST*)malloc(sizeof(struct LIST));
	sent->L = sent;
	sent->R = sent;
	return sent;
}

struct LIST* insert (int value, struct LIST* q) {
	struct LIST*  p = (struct LIST*)malloc(sizeof(struct LIST));
	p->data = value;
	p->L = q;
	p->R = q->R;
	q->R->L = p;
	q->R = p;
	return p;
}

void del (struct LIST* q) {
	q->L->R = q->R;
	q->R->L = q->L;
}

int firstDig (int num) {
	int d;
	while (num) {
		d = num%10;
		num /= 10;
	}
	return d;
}

int prime (int num) {
	int d;
	if (num < 2) return 0;
	for (d=2; d*d<=num; d++) {
		if (num%d == 0) return 0;
	}
	return 1;
}

main (void) {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int value, flagI = 1, flagD = 1, tmp;
	struct LIST *q, *p, *p1, *p2, *tail, *pred, *top = NULL, *sent;
	
	sent = make();
	while(scanf("%d", &value) != EOF) {
		insert(value, sent);
	}
	
	p = sent->L;
	while (p->L != sent) {
		if ((p->L->data)%10 < (p->data)%10) flagI = 0;
		if ((p->L->data)%10 > (p->data)%10) flagD = 0;
		p = p->L;
	}
	
//	printf("%d %d %d\n", count, flagI, flagD);
	
	if (flagI == 1 || flagD == 1) {
		p = sent->L;
		while (p != sent) {
			if (prime(p->data)) del(p);
			if ((p->data)%12 == 0) insert(p->data, p);
 			p = p->L;
		}
		p = sent->L;
		while (p != sent) {
			printf("%d ", p->data);
			p = p->L;
		}
		return 0;
	}
	else {
		p1 = sent->R;
		while (p1->R != sent) {
			p2 = p1->R;
			while (p2 != sent) {
				if (firstDig(p1->data) > firstDig(p2->data)) {
					tmp = p1->data;
					p1->data = p2->data;
					p2->data = tmp;
				}
				p2 = p2->R;
			}
			p1 = p1->R;
		}
		p = sent->R;
		while (p != sent) {
			printf("%d ", p->data);
			p = p->R;
		}
		return 0;
	}
	
	return 0;
}
