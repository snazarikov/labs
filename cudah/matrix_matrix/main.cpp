//g++ -Wall main.cpp Vector.cpp Vector.hpp

#include <iostream>
#include <typeinfo>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include "cufile.hpp"

using namespace std;

const int NMAX = 1e4 + 5;
const int DEBUG = 0;

int matrixA[NMAX * NMAX];
int matrixB[NMAX * NMAX];
int res_cpu[NMAX * NMAX];

void transpose_1d_matrix(int* matrixA, int* matrixA_t, int _size) {
	int ii, jj;
	for (int i = 0; i < _size * _size; ++i) {
		ii = i / _size;
		jj = i % _size;
		matrixA_t[i] = matrixA[jj * _size + ii];
	}
}

void print_1d_matrix(int* matrix, int _size) {
	for (int i = 0; i < _size * _size; ++i) {
		if (!(i % _size) && i) {
			cout << endl;
		}
		cout << setw(3) << matrix[i] << " ";
	}
	cout << endl << endl;
}

void matrix_matrix_product(int* a, int* b, int* res, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			res[i * n + j] = 0;
			for (int k = 0; k < n; ++k) {
				res[i * n + j] += a[i * n + k] * b[k * n + j];
			}
		}
	}
} 

int main() {
	srand(time(0));
	
	for (int i = 0; i < NMAX * NMAX; ++i) {
        matrixA[i] = rand() % 100;
		matrixB[i] = rand() % 100;
	}
	
	int kostil_n = 1024;
	
	matrix_matrix_product(matrixA, matrixB, res_cpu, kostil_n);
	if (DEBUG) {
		print_1d_matrix(matrixA, kostil_n);
		print_1d_matrix(matrixB, kostil_n);
		print_1d_matrix(res_cpu, kostil_n);
	}
	
	int* res_gpu = matrix_matrix_gpu(matrixA, matrixB, kostil_n);
	
	for (int i = 0; i < kostil_n * kostil_n; ++i) {
		if (res_cpu[i] != res_gpu[i]) {
			cout << "vso ochen' ploho" << endl;
			return 0;
		}		
	}
	cout << "OK" << endl;
	
	return 0;
}
