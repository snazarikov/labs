#include <iostream>
#include <cuda.h>
#include <stdlib.h>
#include <string>
#include <iomanip>
#include <stdio.h>

using namespace std;

enum {MALLOC_ERROR, COPY_ERROR, FREE_ERROR, CREATE_ERROR,
	  RECORD_ERROR, SYNC_ERORR, ELAPSE_ERROR, DESTROY_ERROR};
const char *error_type[] = {"malloc error", "copy error", "free error", "create error", 
							"record error", "sync error", "elapse error", "destroy error"};

const int DEBUG = 0;
const int MAGIC_CONST = 16;

int error_counter = 0;

__host__ void error_handler(cudaError_t err, string msg) { 
	error_counter++;
	if (err != cudaSuccess) {
		cerr << msg << endl;
		cout << msg << " " << error_counter << endl;
		exit(0);
	}
}

__global__ void matrix_matrix_kernel(int* a, int* b, int* res, int n) {  // start from cpu but run on gpu 
	int global_ii = blockDim.y * blockIdx.y + threadIdx.y;
	int global_jj = blockDim.x * blockIdx.x + threadIdx.x;
	
	int local_ii = threadIdx.y;
	int local_jj = threadIdx.x;
	
	if (global_ii >= n || global_jj >= n) {
		return; 
	}
	
	res[global_ii * n + global_jj] = 0;
	
	__shared__ int a_part[MAGIC_CONST][MAGIC_CONST];
	__shared__ int b_part[MAGIC_CONST][MAGIC_CONST];
	
	if (DEBUG) {
		printf("global_ii: %d global_jj: %d local_ii: %d local_jj: %d \n", global_ii, global_jj, local_ii, local_jj);
	}
	
	int sum = 0;
	for (int delta = 0; delta < n; delta += MAGIC_CONST) {
		int a_part_ii = global_ii;
		int a_part_jj = delta + local_jj;
		
		int b_part_ii = delta + local_ii;
		int b_part_jj = global_jj;
		
		a_part[local_ii][local_jj] = a[a_part_ii * n + a_part_jj];
		b_part[local_ii][local_jj] = b[b_part_ii * n + b_part_jj];
		
		__syncthreads();
		
		for (int k = 0; k < MAGIC_CONST; ++k) {
			sum += a_part[local_ii][k] * b_part[k][local_jj];
		}
		
		__syncthreads();
	}
	
	res[global_ii * n + global_jj] = sum;
}

__global__ void matrix_matrix_stupid_kernel(int* a, int* b, int* res, int n) {  // start from cpu but run on gpu 
	int global_ii = blockDim.y * blockIdx.y + threadIdx.y;
	int global_jj = blockDim.x * blockIdx.x + threadIdx.x;
	
	if (global_ii >= n || global_jj >= n) {
		return; 
	}
	
	res[global_ii * n + global_jj] = 0;
	
	if (DEBUG) {
		printf("global_ii: %d global_jj: %d\n", global_ii, global_jj);
	} 
	
	for (int k = 0; k < n; ++k) {
		res[global_ii * n + global_jj] += a[global_ii * n + k] * b[k * n + global_jj];
	}
}

__host__ int* matrix_matrix_gpu(int* matrix_a, int* matrix_b, int _size, bool use_stupid = false) {	
	int* matrix_a_gpu = NULL;
	int* matrix_b_gpu = NULL;
	int* result_gpu = NULL;

	int* result = (int*)malloc(sizeof(int) * _size * _size);
	
	error_handler(cudaMalloc((void**) &matrix_a_gpu, _size * _size * sizeof(int)), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &matrix_b_gpu, _size * _size * sizeof(int)), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &result_gpu, _size * _size * sizeof(int)), error_type[MALLOC_ERROR]);
	
	dim3 threads(MAGIC_CONST, MAGIC_CONST, 1);
	dim3 blocks((_size + threads.x - 1) / threads.x, (_size + threads.y - 1) / threads.y, 1);
	
	if (DEBUG) {
		cout << "threads: " << threads.x << " " << threads.y << " " << threads.z << endl;
		cout << "blocks: " << blocks.x << " " << blocks.y << " " << blocks.z << endl;
	}
	
	error_handler(cudaMemcpy(matrix_a_gpu, matrix_a, _size * _size * sizeof(int), cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	error_handler(cudaMemcpy(matrix_b_gpu, matrix_b, _size * _size * sizeof(int), cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	//
	cudaDeviceSynchronize();
	float elapsed = 0;
	cudaEvent_t start, stop;

	error_handler(cudaEventCreate(&start), error_type[CREATE_ERROR]);
	error_handler(cudaEventCreate(&stop), error_type[CREATE_ERROR]);
	error_handler(cudaEventRecord(start, 0), error_type[RECORD_ERROR]);
	//	
	
	if (use_stupid) {
		matrix_matrix_stupid_kernel<<<blocks, threads>>>(matrix_a_gpu, matrix_b_gpu, result_gpu, _size);
	}
	else {
		matrix_matrix_kernel<<<blocks, threads>>>(matrix_a_gpu, matrix_b_gpu, result_gpu, _size);
	}
		
	//
	error_handler(cudaEventRecord(stop, 0), error_type[RECORD_ERROR]);
	error_handler(cudaEventSynchronize(stop), error_type[SYNC_ERORR]);
	error_handler(cudaEventElapsedTime(&elapsed, start, stop), error_type[ELAPSE_ERROR]);
	error_handler(cudaEventDestroy(start), error_type[DESTROY_ERROR]);
	error_handler(cudaEventDestroy(stop), error_type[DESTROY_ERROR]);
	
	cout << "Computation time gpu: " << elapsed << "ms" << endl;
	//
	
	error_handler(cudaMemcpy(result, result_gpu, _size * _size * sizeof(int), cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(matrix_a_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(matrix_b_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(result_gpu), error_type[FREE_ERROR]);	
	
	return result;
}
