#include <stdio.h>

const int NMAX = 16;
const int block_size = 16;

__global__ void hello(char* a, int* b) {
	a[threadIdx.x] += b[threadIdx.x];
}

int main() {
	char a[NMAX] = "Hello \0\0\0\0\0\0";
	int b[NMAX] = {15, 10, 6, 0, -11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	char* ad;
	int* bd;
	const int csize = NMAX * sizeof(char);
	const int isize = NMAX * sizeof(int);

	printf("%s", a);

	cudaMalloc((void**)&ad, csize);
	cudaMalloc((void**)&bd, isize);
	cudaMemcpy(ad, a, csize, cudaMemcpyHostToDevice);
	cudaMemcpy(bd, b, isize, cudaMemcpyHostToDevice);

	dim3 dim_block(block_size, 1);
	dim3 dim_grid(1, 1);

	hello<<<dim_grid, dim_block>>>(ad, bd);

	cudaMemcpy(a, ad, csize, cudaMemcpyDeviceToHost);
	cudaFree(ad);
	cudaFree(bd);

	printf("%s\n", a);
	char kek;
	scanf("%c", &kek);

	return 0;
}
