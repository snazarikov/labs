//g++ -Wall main.cpp Vector.cpp Vector.hpp

#include <iostream>
#include <typeinfo>
#include <cstdlib>
#include <ctime>
#include "Vector.hpp"

/*
template <typename T> std::string nameofType(const T& v) {
    int     status;
    char   *realname = abi::__cxa_demangle(typeid(v).name(), 0, 0, &status);
    std::string name(realname? realname : "????");
    free(realname);

    return name;
}
*/
 
int main() {
	srand(time(0));
	std::cout << "hello kek" << std::endl;
	
	Vector vec1(3);
	Vector vec2(3);
	Vector vec3(3);
	Vector vec4(vec1);
	Vector vec5(3);
	
	vec5 = vec1;
	
	std::cout << "type: " << typeid(vec1).name() << std::endl;
	std::cout << "vec[1]: " << vec1[1] << std::endl;
	std::cout << "size: " << vec1.get_size() << std::endl;
	
	std::cout << "vec1: "; 
	vec1.print();
	std::cout << "vec2: "; 
	vec2.print();
	std::cout << "vec3: "; 
	vec3.print();
	std::cout << "vec4: "; 
	vec4.print();
	std::cout << "vec5: "; 
	vec5.print();
	
	{
	std::cout << "scalar product: " << vec1.scalar_product(vec2) << std::endl; 
	std::cout << "scalar product gpu: " << vec1.scalar_product(vec2, true) << std::endl; 
	}
	
	{
	Vector vector_product = vec1.vector_product(vec2);
	std::cout << "vector product: ";	
	vector_product.print();
	
	Vector vector_product_gpu = vec1.vector_product(vec2, true);
	std::cout << "vector product gpu: ";	
	vector_product_gpu.print();
	}
	
	{
	std::cout << "mixed product: " << vec1.mixed_product(vec2, vec3) << std::endl; 
	std::cout << "mixed product gpu: " << vec1.mixed_product(vec2, vec3, true) << std::endl; 
	}
	
	//std::cout << nameofType(vec1) << std::endl;
	
	return 0;
}
