#include <iostream>
#include "Vector.hpp"
#include <cstdlib>

using namespace std;

void GG() {
	cout << "polzovatel pochemy-to dyrak" << endl;
	exit(0);
}

Vector::Vector(int size) {
	_size = size;
	_vec.resize(size);
	for (int i = 0; i < _size; ++i) {
		_vec[i] = rand() % 4;
	}
}

Vector::Vector(const Vector& other) {
	_size = other._size;
    _vec = other._vec;
}

Vector::~Vector() {
//    free(_vec);
}

int& Vector::operator[] (int i) {
	return _vec[i];
}

const Vector& Vector::operator=(const Vector &other) {
    if (&other != this) {
        _size = other._size;
        _vec = other._vec;
    }
 
    return *this;
}

int* Vector::get_as_array() {
	return &_vec[0];
}

int Vector::get_size() {
	return _size;
}

void Vector::print() {
	for (int i = 0; i < _size; ++i) {
		cout << _vec[i] << " ";
	}
	cout << endl;
}

int Vector::scalar_product(Vector& other, bool use_gpu) {
	int result = 0;
	
	if (_size != other.get_size()) {
		GG();
	}
	
	if (!use_gpu) {
		for (int i = 0; i < _size; ++i) {
			result += _vec[i] * other[i];
		}	
	}
	else {
		// magic
		result = scalar_product_gpu(&_vec[0], other.get_as_array(), _size);
	}
	return result;
}

Vector Vector::vector_product(Vector& other, bool use_gpu) {
	if (_size != other.get_size() || _size != 3) {
		GG();
	}
	Vector result(_size);
	if (!use_gpu) {
		result[0] = _vec[1] * other[2] - _vec[2] * other[1];
		result[1] = _vec[2] * other[0] - _vec[0] * other[2];
		result[2] = _vec[0] * other[1] - _vec[1] * other[0];
	}
	else {
		// magic
		int* result_gpu = vector_product_gpu(&_vec[0], other.get_as_array(), _size);
		for (int i = 0; i < _size; ++i) {
			result[i] = result_gpu[i];
		}
	}	
	return result;
}

int Vector::mixed_product(Vector& other_1, Vector& other_2, bool use_gpu) {
	if (_size != other_1.get_size() || _size != 3) {
		GG();
	}
	int result = 0;
	if (!use_gpu) {
		Vector vector_product = this->vector_product(other_1);
		result = vector_product.scalar_product(other_2);
	}
	else {
		// magic
		Vector vector_product_gpu = this->vector_product(other_1, true);
		result = vector_product_gpu.scalar_product(other_2, true);
	}
	return result;
}
