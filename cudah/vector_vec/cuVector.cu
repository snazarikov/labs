#include <iostream>
#include <cuda.h>
#include <stdlib.h>
#include <string>

using namespace std;

const string MALLOC_ERROR = "malloc error";
const string COPY_ERROR = "copy error";
const string FREE_ERROR = "free error";

int error_counter = 0;

__host__ void error_handler(cudaError_t err, string msg) {
	error_counter++;
	if (err != cudaSuccess) {
		cerr << msg << endl;
		cout << msg << " " << error_counter << endl;
		exit(0);
	}
}

__global__ void scalar_kernel(int* a, int* b, int* res, int n) {  // start from cpu but run on gpu 
	int idx = blockDim.x * blockIdx.x + threadIdx.x;  // unikalnii index niti
	if (idx < n) {
		res[idx] = a[idx] * b[idx];
	}
}

__global__ void vector_kernel(int* a, int* b, int* res, int n) {  // start from cpu but run on gpu 
	int idx = blockDim.x * blockIdx.x + threadIdx.x;  // unikalnii index niti
	if (idx < n) {
		// solution
		res[idx] = a[(idx + 1) % n] * b[(idx + 2) % n] - a[(idx + 2) % n] * b[(idx + 1) % n];
	}
}

__host__ int scalar_product_gpu(int* vec1, int* vec2, int _size) {
	int* vec1_gpu = NULL;
	int* vec2_gpu = NULL;
	int* result_gpu = NULL;

	int* result = (int*)malloc(sizeof(int) * _size);
	
	error_handler(cudaMalloc((void**) &vec1_gpu, _size * sizeof(int)), MALLOC_ERROR);
	error_handler(cudaMalloc((void**) &vec2_gpu, _size * sizeof(int)), MALLOC_ERROR);
	error_handler(cudaMalloc((void**) &result_gpu, _size * sizeof(int)), MALLOC_ERROR);
	
	dim3 threads = 256;
	dim3 blocks = (_size + threads.x - 1) / threads.x;
	
	error_handler(cudaMemcpy(vec1_gpu, vec1, _size * sizeof(int), cudaMemcpyHostToDevice), COPY_ERROR);
	error_handler(cudaMemcpy(vec2_gpu, vec2, _size * sizeof(int), cudaMemcpyHostToDevice), COPY_ERROR);
	
	scalar_kernel<<<blocks, threads>>>(vec1_gpu, vec2_gpu, result_gpu, _size);
	
	error_handler(cudaMemcpy(result, result_gpu, _size * sizeof(int), cudaMemcpyDeviceToHost), COPY_ERROR);
	
	int answer = 0;
	for (int i = 0; i < _size; ++i) {
		answer += result[i];
	}
	
	error_handler(cudaFree(vec1_gpu), FREE_ERROR);	
	error_handler(cudaFree(vec2_gpu), FREE_ERROR);	
	error_handler(cudaFree(result_gpu), FREE_ERROR);	

	free(result);
	
	return answer;
}

__host__ int* vector_product_gpu(int* vec1, int* vec2, int _size) {
	int* vec1_gpu = NULL;
	int* vec2_gpu = NULL;
	int* result_gpu = NULL;
	
	int* result = (int*)malloc(sizeof(int) * _size);
	
	error_handler(cudaMalloc((void**) &vec1_gpu, _size * sizeof(int)), MALLOC_ERROR);
	error_handler(cudaMalloc((void**) &vec2_gpu, _size * sizeof(int)), MALLOC_ERROR);
	error_handler(cudaMalloc((void**) &result_gpu, _size * sizeof(int)), MALLOC_ERROR);
	
	error_handler(cudaMemcpy(vec1_gpu, vec1, _size * sizeof(int), cudaMemcpyHostToDevice), COPY_ERROR);
	error_handler(cudaMemcpy(vec2_gpu, vec2, _size * sizeof(int), cudaMemcpyHostToDevice), COPY_ERROR);
	
	vector_kernel<<<1, 3>>>(vec1_gpu, vec2_gpu, result_gpu, _size);
	
	error_handler(cudaMemcpy(result, result_gpu, _size * sizeof(int), cudaMemcpyDeviceToHost), COPY_ERROR);
	
	error_handler(cudaFree(vec1_gpu), FREE_ERROR);	
	error_handler(cudaFree(vec2_gpu), FREE_ERROR);	
	error_handler(cudaFree(result_gpu), FREE_ERROR);
	
	return result;
}
