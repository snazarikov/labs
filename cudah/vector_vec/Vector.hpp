#include "cuVector.hpp"
#include <vector>

using namespace std;

class Vector {
public:
    Vector(int size);
	Vector(const Vector& other);
	~Vector();
	
	int& operator[] (int i);
	const Vector &operator=(const Vector& other);
	
	int get_size();
	void print();
	
	int scalar_product(Vector& other, bool use_gpu = false);
	Vector vector_product(Vector& other, bool use_gpu = false);
	int mixed_product(Vector& other_1, Vector& other_2, bool use_gpu = false);
private:
	int* get_as_array();
    int _size;
    vector<int> _vec;
};
