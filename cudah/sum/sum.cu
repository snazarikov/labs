#include <iostream>
#include <cuda.h>
#include <stdlib.h>
#include <string>

using namespace std;

__host__ void error_handler(cudaError_t err, string msg) {
	if (err != cudaSuccess) {
		cerr << msg << endl;
		exit(0);
	}
}

__global__ void init_kernel(int* vector_a, int* vector_b, int* vector_sum, int n) {  // start from cpu but run on gpu 
	int idx = blockDim.x * blockIdx.x + threadIdx.x;  // unikalnii index niti
	if (idx < n) {
		vector_sum[idx] = vector_a[idx] + vector_b[idx];
	}
}

__host__ int main(int argc, char* argv[]) {
	const int n = 10;

	int* gpu_vector_a = NULL;
	int* gpu_vector_b = NULL;
	int* gpu_vector_sum = NULL;

	int* vector_a = NULL;
	int* vector_b = NULL;
	int* vector_sum = NULL;

	const size_t vector_size = sizeof(int) * n; // size of bytes

	vector_a = (int*)malloc(sizeof(int) * n);
	vector_b = (int*)malloc(sizeof(int) * n);
	vector_sum = (int*)malloc(sizeof(int) * n);

	for (int i = 0; i < n; ++i) {
		vector_a[i] = i;
		vector_b[i] = 20 + i;
	}

	const string malloc_error = "cuda malloc error";
	error_handler(cudaMalloc((void**)&gpu_vector_a, vector_size), malloc_error);
	error_handler(cudaMalloc((void**)&gpu_vector_b, vector_size), malloc_error);
	error_handler(cudaMalloc((void**)&gpu_vector_sum, vector_size), malloc_error);

	// calculate start
	dim3 threads = 256; // 256 divisible by warp(32)
	dim3 blocks = (n + threads.x - 1) / threads.x;

	const string copy_error = "cuda copy error";
	error_handler(cudaMemcpy(gpu_vector_a, vector_a, vector_size, cudaMemcpyHostToDevice), copy_error);
	error_handler(cudaMemcpy(gpu_vector_b, vector_b, vector_size, cudaMemcpyHostToDevice), copy_error);

	init_kernel<<<blocks, threads>>>(gpu_vector_a, gpu_vector_b, gpu_vector_sum, n); // <<<n, 1>> n blokov po 1 potoky
	// calculate finish

	error_handler(cudaMemcpy(vector_sum, gpu_vector_sum, vector_size, cudaMemcpyDeviceToHost), copy_error);

	for (int i = 0; i < n; ++i) {
		cout << vector_a[i] << "+"  << vector_b[i] << "=" << vector_sum[i] << endl;
	}

	const string free_error = "cuda free error";
	error_handler(cudaFree(gpu_vector_a), "cuda free error");	
	error_handler(cudaFree(gpu_vector_b), "cuda free error");	
	error_handler(cudaFree(gpu_vector_sum), "cuda free error");	

	free(vector_a);
	free(vector_b);
	free(vector_sum);

	return 0;
}
