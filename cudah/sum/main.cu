#include <iostream>
#include <cuda.h>
#include <stdlib.h>
#include <string>

using namespace std;

__host__ void error_handler(cudaError_t err, string msg) {
	if (err != cudaSuccess) {
		cerr << msg << endl;
		exit(0);
	}
}

__global__ void init_kernel(int* vector, int n) {  // start from cpu but run on gpu 
	int idx = blockDim.x * blockIdx.x + threadIdx.x;  // unikalnii index niti
	if (idx < n) {
		vector[idx] = 147;
	}
}

__host__ int main(int argc, char* argv[]) {
	cudaError_t err = cudaSuccess;

	const int n = 10;

	int* gpu_vector = NULL;
	int* vector = NULL;

	const size_t vector_size = sizeof(int) * n; // size of bytes

	vector = (int*)malloc(sizeof(int) * n);
	err = cudaMalloc((void**)&gpu_vector, vector_size);

	error_handler(err, "cuda malloc error");

	// calculate start
	dim3 threads = 256; // 256 divisible by warp(32)
	dim3 blocks = (n + threads.x - 1) / threads.x;

	init_kernel<<<blocks, threads>>>(gpu_vector, n); // <<<n, 1>> n blokov po 1 potoky

	// calculate finish

	error_handler(cudaMemcpy(vector, gpu_vector, vector_size, cudaMemcpyDeviceToHost), "cuda memcpy error");

	for (int i = 0; i < n; ++i) {
		cout << vector[i] << endl;
	}

	error_handler(cudaFree(gpu_vector), "cuda free error");	
	free(vector);

	return 0;
}
