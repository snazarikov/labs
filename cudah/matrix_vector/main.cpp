//g++ -Wall main.cpp Vector.cpp Vector.hpp

#include <iostream>
#include <typeinfo>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include "cufile.hpp"

using namespace std;

const int NMAX = 1e4 + 5;
const int DEBUG = 0;

int matrixA[NMAX * NMAX];
int matrixA_t[NMAX * NMAX];
int vec[NMAX];
int res_cpu[NMAX];

void transpose_1d_matrix(int* matrixA, int* matrixA_t, int _size) {
	int ii, jj;
	for (int i = 0; i < _size * _size; ++i) {
		ii = i / _size;
		jj = i % _size;
		matrixA_t[i] = matrixA[jj * _size + ii];
	}
}

void print_1d_matrix(int* matrix, int _size) {
	for (int i = 0; i < _size * _size; ++i) {
		if (!(i % _size) && i) {
			cout << endl;
		}
		cout << setw(3) << matrix[i] << " ";
	}
	cout << endl;
}

int main() {
	srand(time(0));

	for (int i = 0; i < NMAX * NMAX; ++i) {
        if (i < NMAX) {
            vec[i] = rand() % 100;
        }
        matrixA[i] = rand() % 10;
	}
	
	int kostil_n = 513;
	
	transpose_1d_matrix(matrixA, matrixA_t, kostil_n);
	
	if (DEBUG) {
		cout << "matrixA:" << endl;
		print_1d_matrix(matrixA, kostil_n);
		cout << "matrixA_t:" << endl;
		print_1d_matrix(matrixA_t, kostil_n);
		
		cout << "vector:" << endl;
		for (int i = 0; i < kostil_n; ++i) {
			cout << vec[i] << " ";
		}
		cout << endl;
	}

    int* res_gpu = matrix_vector_gpu(matrixA_t, vec, kostil_n);

	cout << "matrix_vector_gpu result:" << endl;
    for (int i = 0; i < kostil_n; ++i) {
        cout << res_gpu[i] << " ";
    }
    cout << endl;
	
	cout << "matrix_vector_cpu result:" << endl;
	for (int i = 0; i < kostil_n; ++i) {
		int temp = 0;
		for (int j = 0; j < kostil_n; ++j) {
			temp += vec[j] * matrixA[i * kostil_n + j];
		}
		res_cpu[i] = temp;
		cout << temp << " ";
	}
	cout << endl;
	
	for (int i = 0; i < kostil_n; ++i) {
		if (res_gpu[i] != res_cpu[i]) {
			cout << "vse ochen ploho" << endl;
			return 0;
		}
	}
	cout << "OK" << endl;
	
	return 0;
}
