#include <iostream>
#include <cuda.h>
#include <stdlib.h>
#include <string>
#include <iomanip>
#include <stdio.h>

using namespace std;

enum {MALLOC_ERROR, COPY_ERROR, FREE_ERROR, CREATE_ERROR,
	  RECORD_ERROR, SYNC_ERORR, ELAPSE_ERROR, DESTROY_ERROR};
const char *error_type[] = {"malloc error", "copy error", "free error", "create error", 
							"record error", "sync error", "elapse error", "destroy error"};

const int DEBUG = 0;

int error_counter = 0;

__host__ void error_handler(cudaError_t err, string msg) {
	error_counter++;
	if (err != cudaSuccess) {
		cerr << msg << endl;
		cout << msg << " " << error_counter << endl;
		exit(0);
	}
}

__global__ void matrix_vector_kernel(int* m, int* v, int* res, int n) {  // start from cpu but run on gpu 
	int idx = blockDim.x * blockIdx.x + threadIdx.x;  // unikalnii index niti
//	printf("b_dim_x: %d d_idx_x: %d t_idx_x: %d\n", blockDim.x, blockIdx.x, threadIdx.x);
	if (idx < n) {
		for (int i = 0; i < n; ++i) {
			// SHOK-KONTENT otladka iz kernela
			if (DEBUG) {
				printf("idx: %d i: %d m_ind: %d v[i] %d m[m_ind] %d\n", idx, i, i * n + idx, v[i], m[i * n + idx]);
			}
			
			res[idx] += v[i] * m[i * n + idx];
			
			if (DEBUG) {
				printf("idx: %d result: %d\n", idx, res[idx]);
			}
		}
	}
}

__host__ int* matrix_vector_gpu(int* matrix, int* vec, int _size) {
	if (DEBUG) {
		cout << "matrix on gpu:" << endl;
		for (int i = 0; i < _size * _size; ++i) {
			if (!(i % _size) && i) {
				cout << endl;
			}
			cout << setw(3) << matrix[i] << " ";
		}
		cout << endl;
		
		cout << "vector on gpu:" << endl;
		for (int i = 0; i < _size; ++i) {
			cout << vec[i] << " ";
		}
		cout << endl;
	}
	
	int* matrix_gpu = NULL;
	int* vec_gpu = NULL;
	int* result_gpu = NULL;

	int* result = (int*)malloc(sizeof(int) * _size);
	
	error_handler(cudaMalloc((void**) &matrix_gpu, _size * _size * sizeof(int)), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &vec_gpu, _size * sizeof(int)), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &result_gpu, _size * sizeof(int)), error_type[MALLOC_ERROR]);
	
	dim3 threads = 256;
	dim3 blocks = (_size + threads.x - 1) / threads.x;
	
	if (DEBUG) {
		cout << "threads: " << threads.x << endl;
		cout << "blocks: " << blocks.x << endl;
	}
	
	error_handler(cudaMemcpy(matrix_gpu, matrix, _size * _size * sizeof(int), cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	error_handler(cudaMemcpy(vec_gpu, vec, _size * sizeof(int), cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	//
	cudaDeviceSynchronize();
	float elapsed = 0;
	cudaEvent_t start, stop;

	error_handler(cudaEventCreate(&start), error_type[CREATE_ERROR]);
	error_handler(cudaEventCreate(&stop), error_type[CREATE_ERROR]);
	error_handler(cudaEventRecord(start, 0), error_type[RECORD_ERROR]);
	//	
	
	matrix_vector_kernel<<<blocks, threads>>>(matrix_gpu, vec_gpu, result_gpu, _size);

	//
	error_handler(cudaEventRecord(stop, 0), error_type[RECORD_ERROR]);
	error_handler(cudaEventSynchronize(stop), error_type[SYNC_ERORR]);
	error_handler(cudaEventElapsedTime(&elapsed, start, stop), error_type[ELAPSE_ERROR]);
	error_handler(cudaEventDestroy(start), error_type[DESTROY_ERROR]);
	error_handler(cudaEventDestroy(stop), error_type[DESTROY_ERROR]);
	
	cout << "Computation time gpu: " << elapsed << "ms" << endl;
	//
	
	error_handler(cudaMemcpy(result, result_gpu, _size * sizeof(int), cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(matrix_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(vec_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(result_gpu), error_type[FREE_ERROR]);	
	
	return result;
}
