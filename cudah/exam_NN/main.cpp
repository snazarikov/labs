#include <iostream>
#include <typeinfo>
#include <cstdlib>
#include <ctime>
#include <vector>
//#include <random>
#include <iomanip>
#include "DenseLayer.hpp"

void print_smth(vector<float> A, int row, int column) {
	for (int i = 0; i < row * column; ++i) {
		if (!(i % column) && i) {
			cout << endl;
		}
		cout << fixed << setprecision(3) << setw(7) << A[i] << " ";
	}
	cout << endl << endl;
}

void transpose_smth(vector<float>& A, vector<float>& A_t, int row, int column) {
	int ii, jj;
	for (int i = 0; i < row * column; ++i) {
		ii = i / row;
		jj = i % row;
		A_t[i] = A[jj * column + ii];
	}
}
 
int main() {
	srand(time(0));
	std::cout << "hello kek" << std::endl;
	
	//std::default_random_engine generator;
    //std::normal_distribution<float> distribution(0.0, 1);
	
	int input_size = 5;
	int output_size = 4;
	
	vector<float> input(input_size, 0);
	for (int i = 0; i < input_size; ++i) {
		//input[i] = distribution(generator);
		input[i] = 0.5f - static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	}
	
	DenseLayer layer0(input_size, output_size);
	layer0.print();
	vector<float> output = layer0.forward_pass(input, "sigmoid");
	layer0.print();	
	vector<float> error(output_size, 1);
	vector<float> error_back = layer0.backward_pass(error, 0.1f);	
	layer0.print();
	
	vector<float> error_back_kostil(input_size * output_size, 0);
	for (int i = 0; i < input_size * output_size; ++i) {
		error_back_kostil[i] = error_back[i];
	}
	print_smth(error_back_kostil, 1, output_size);
	
	// Hackathon net	
	DenseLayer layer1(input_size, output_size);
	DenseLayer layer2(output_size, 1);
	
	float lr = 0.1f;
	
	vector<float> output1 = layer1.forward_pass(input, "sigmoid");
	vector<float> output2 = layer2.forward_pass(output1, "sigmoid");
	vector<float> error1_0 = layer2.backward_pass(error, lr);
	layer1.backward_pass(error1_0, lr);
	
	return 0;
}
