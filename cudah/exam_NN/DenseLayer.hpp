#include "cuDenseLayer.hpp"
#include <vector>
#include <string>

using namespace std;

class DenseLayer {
public:
    DenseLayer(int input_size, int output_size);
	~DenseLayer();
	
	vector<float> forward_pass(vector<float> input, string activation_func = "none");
	vector<float> backward_pass(vector<float> error, float learning_rate);
	
	void print();
	void transpose(vector<float>& w_t, int row, int column);
	
	vector<float> get_input();
	vector<float> get_weight();
private:
    int _input_size;
	int _output_size;
    vector<float> _input;
	vector<float> _weight;
	vector<float> _activation;
	string _activation_func;
};
