float* vec_matr_gpu(float* input, float* weight, int input_size, int output_size);
float* sigmoid_gpu(float* data, int size);
float* sigmoid_deriv_gpu(float* data, int size);
float* elementwise_gpu(float* data1, float* data2, int size);
float* vec_t_vec_gpu(float* vec_t, float* vec, int v_t_size, int v_size);
void update_gpu(float* W, float* dW, int size, float learning_rate);