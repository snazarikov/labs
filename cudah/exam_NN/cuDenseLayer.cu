#include <iostream>
#include <cuda.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <stdio.h>

using namespace std;

enum {MALLOC_ERROR, COPY_ERROR, FREE_ERROR, CREATE_ERROR,
	  RECORD_ERROR, SYNC_ERORR, ELAPSE_ERROR, DESTROY_ERROR};
const char *error_type[] = {"malloc error", "copy error", "free error", "create error", 
							"record error", "sync error", "elapse error", "destroy error"};


int error_counter = 0;

__host__ void error_handler(cudaError_t err, string msg) {
	error_counter++;
	if (err != cudaSuccess) {
		cerr << msg << endl;
		cout << msg << " " << error_counter << endl;
		exit(0);
	}
}

__global__ void vec_matr_kernel(float* in, float* w, float* out, int in_size, int out_size) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < out_size) {
		for (int i = 0; i < in_size; ++i) {			
			out[idx] += in[i] * w[i * out_size + idx];
		}
	}
}

__host__ float* vec_matr_gpu(float* input, float* weight, int input_size, int output_size) {	
	float* input_gpu = NULL;
	float* weight_gpu = NULL;
	float* output_gpu = NULL;
	
	size_t w_size = input_size * output_size * sizeof(float);
	size_t i_size = input_size * sizeof(float);
	size_t o_size = output_size * sizeof(float);
	
	float* output = (float*)malloc(o_size);
	
	error_handler(cudaMalloc((void**) &weight_gpu, w_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &input_gpu, i_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &output_gpu, o_size), error_type[MALLOC_ERROR]);
	
	dim3 threads = 256;
	dim3 blocks = (output_size + threads.x - 1) / threads.x;
	
	error_handler(cudaMemcpy(weight_gpu, weight, w_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	error_handler(cudaMemcpy(input_gpu, input, i_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	vec_matr_kernel<<<blocks, threads>>>(input_gpu, weight_gpu, output_gpu, input_size, output_size);
	
	error_handler(cudaMemcpy(output, output_gpu, o_size, cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(weight_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(input_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(output_gpu), error_type[FREE_ERROR]);	
	
	return output;
}


__global__ void sigmoid_kernel(float* data, float* res, int size) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size) {
		res[idx] = 1 / (1 + expf(-data[idx]));
	}
}

__host__ float* sigmoid_gpu(float* data, int size) {	
	float* data_gpu = NULL;
	float* result_gpu = NULL;
	
	size_t d_size = size * sizeof(float);
	
	float* result = (float*)malloc(d_size);
	
	error_handler(cudaMalloc((void**) &data_gpu, d_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &result_gpu, d_size), error_type[MALLOC_ERROR]);
	
	dim3 threads = 256;
	dim3 blocks = (size + threads.x - 1) / threads.x;
	
	error_handler(cudaMemcpy(data_gpu, data, d_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	sigmoid_kernel<<<blocks, threads>>>(data_gpu, result_gpu, size);
	
	error_handler(cudaMemcpy(result, result_gpu, d_size, cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(data_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(result_gpu), error_type[FREE_ERROR]);
	
	return result;
}


__global__ void sigmoid_deriv_kernel(float* data, float* res, int size) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size) {
		res[idx] = (1 / (1 + expf(-data[idx]))) * (1 - (1 / (1 + expf(-data[idx]))));
	}
}

__host__ float* sigmoid_deriv_gpu(float* data, int size) {	
	float* data_gpu = NULL;
	float* result_gpu = NULL;
	
	size_t d_size = size * sizeof(float);
	
	float* result = (float*)malloc(d_size);
	
	error_handler(cudaMalloc((void**) &data_gpu, d_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &result_gpu, d_size), error_type[MALLOC_ERROR]);
	
	dim3 threads = 256;
	dim3 blocks = (size + threads.x - 1) / threads.x;
	
	error_handler(cudaMemcpy(data_gpu, data, d_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	sigmoid_deriv_kernel<<<blocks, threads>>>(data_gpu, result_gpu, size);
	
	error_handler(cudaMemcpy(result, result_gpu, d_size, cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(data_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(result_gpu), error_type[FREE_ERROR]);
	
	return result;
}


__global__ void elementwise_mult_kernel(float* data1, float* data2, float* res, int size) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size) {
		res[idx] = data1[idx] * data2[idx];
	}
}

__host__ float* elementwise_gpu(float* data1, float* data2, int size) {	
	float* data1_gpu = NULL;
	float* data2_gpu = NULL;
	float* result_gpu = NULL;
	
	size_t d_size = size * sizeof(float);
	
	float* result = (float*)malloc(d_size);
	
	error_handler(cudaMalloc((void**) &data1_gpu, d_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &data2_gpu, d_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &result_gpu, d_size), error_type[MALLOC_ERROR]);
	
	dim3 threads = 256;
	dim3 blocks = (size + threads.x - 1) / threads.x;
	
	error_handler(cudaMemcpy(data1_gpu, data1, d_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	error_handler(cudaMemcpy(data2_gpu, data2, d_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	elementwise_mult_kernel<<<blocks, threads>>>(data1_gpu, data2_gpu, result_gpu, size);
	
	error_handler(cudaMemcpy(result, result_gpu, d_size, cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(data1_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(data2_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(result_gpu), error_type[FREE_ERROR]);
	
	return result;
}

__global__ void elementwise_mult_kernel(float* vec_t, float* vec, float* res, int v_t_size, int v_size) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < v_size) {
		for (int i = 0; i < v_t_size; ++i) {			
			res[i * v_size + idx] = vec_t[i] * vec[idx];
		}
	}
}

__host__ float* vec_t_vec_gpu(float* vec_t, float* vec, int v_t_size, int v_size) {
	float* vec_t_gpu = NULL;
	float* vec_gpu = NULL;
	float* result_gpu = NULL;
	
	size_t vt_size = v_t_size * sizeof(float);
	size_t vec_size = v_size * sizeof(float);
	size_t m_size = v_t_size * v_size * sizeof(float);
	
	float* result = (float*)malloc(m_size);
	
	error_handler(cudaMalloc((void**) &vec_gpu, vec_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &vec_t_gpu, vt_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &result_gpu, m_size), error_type[MALLOC_ERROR]);
	
	dim3 threads = 256;
	dim3 blocks = (v_size + threads.x - 1) / threads.x;
	
	error_handler(cudaMemcpy(vec_gpu, vec, vec_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	error_handler(cudaMemcpy(vec_t_gpu, vec_t, vt_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	elementwise_mult_kernel<<<blocks, threads>>>(vec_t_gpu, vec_gpu, result_gpu, v_t_size, v_size);
	
	error_handler(cudaMemcpy(result, result_gpu, m_size, cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(vec_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(vec_t_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(result_gpu), error_type[FREE_ERROR]);
	
	return result;
}


__global__ void update_kernel(float* W, float* dW, int size, float learning_rate) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size) {
		W[idx] -= learning_rate * dW[idx];
	}
}

__host__ void update_gpu(float* W, float* dW, int size, float learning_rate) {	
	float* W_gpu = NULL;
	float* dW_gpu = NULL;
	
	size_t W_size = size * sizeof(float);
	
	float* result = (float*)malloc(W_size);
	
	error_handler(cudaMalloc((void**) &W_gpu, W_size), error_type[MALLOC_ERROR]);
	error_handler(cudaMalloc((void**) &dW_gpu, W_size), error_type[MALLOC_ERROR]);
	
	dim3 threads = 256;
	dim3 blocks = (size + threads.x - 1) / threads.x;
	
	error_handler(cudaMemcpy(W_gpu, W, W_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	error_handler(cudaMemcpy(dW_gpu, dW, W_size, cudaMemcpyHostToDevice), error_type[COPY_ERROR]);
	
	update_kernel<<<blocks, threads>>>(W_gpu, dW_gpu, size, learning_rate);
	
	error_handler(cudaMemcpy(W, W_gpu, W_size, cudaMemcpyDeviceToHost), error_type[COPY_ERROR]);
	
	error_handler(cudaFree(W_gpu), error_type[FREE_ERROR]);	
	error_handler(cudaFree(dW_gpu), error_type[FREE_ERROR]);
}