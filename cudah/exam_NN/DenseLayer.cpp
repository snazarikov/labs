#include <iostream>
#include "DenseLayer.hpp"
#include <cstdlib>
#include <iomanip>

using namespace std;

void GG() {
	cout << "polzovatel pochemy-to dyrak" << endl;
	exit(0);
}

DenseLayer::DenseLayer(int input_size, int output_size) {
	_input_size = input_size;
	_output_size = output_size;
	_input = vector<float>(_input_size, 0);
	_activation = vector<float>(_output_size, 0);
	
	_weight.resize(_input_size * _output_size);
	
	//std::default_random_engine generator;
    //std::normal_distribution<float> distribution(0.0, 1);
	
	for (int i = 0; i < _input_size * _output_size; ++i) {
		//_weight[i] = distribution(generator);
		_weight[i] = 0.5f - static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	}
}

DenseLayer::~DenseLayer() {
//	cout << "destruct";
}

vector<float> DenseLayer::get_input() {
	return _input;
}

vector<float> DenseLayer::get_weight() {
	return _weight;
}

void DenseLayer::print() {
	cout << "input:" << endl;
	for (int i = 0; i < _input_size; ++i) {
		cout << fixed << setprecision(3) << setw(7) << _input[i] << " ";
	}
	cout << endl << endl;
	
	cout << "activation:" << endl;
	for (int i = 0; i < _output_size; ++i) {
		cout << fixed << setprecision(3) << setw(7) << _activation[i] << " ";
	}
	cout << endl << endl;
	
	cout << "weight:" << endl;
	for (int i = 0; i < _input_size * _output_size; ++i) {
		if (!(i % _output_size) && i) {
			cout << endl;
		}
		cout << fixed << setprecision(3) << setw(7) << _weight[i] << " ";
	}
	cout << endl << endl;
}

void DenseLayer::transpose(vector<float>& w_t, int row, int column) {
	int ii, jj;
	for (int i = 0; i < row * column; ++i) {
		ii = i / row;
		jj = i % row;
		w_t[i] = _weight[jj * column + ii];
	}
}

void print_kek(vector<float> A, int row, int column) {
	for (int i = 0; i < row * column; ++i) {
		if (!(i % column) && i) {
			cout << endl;
		}
		cout << fixed << setprecision(3) << setw(7) << A[i] << " ";
	}
	cout << endl << endl;
}

vector<float> DenseLayer::forward_pass(vector<float> input, string activation_func) {
	cout << "forward" << endl;
	if (input.size() != _input_size) {
		GG();
	}
	_input = input;
	_activation_func = activation_func;
	float* output = vec_matr_gpu(&_input[0], &_weight[0], _input_size, _output_size);
	
	if (activation_func == "sigmoid") {
		output = sigmoid_gpu(output, _output_size);
	}
	
	for (int i = 0; i < _output_size; ++i) {
		_activation[i] = output[i];
	}
	
	return _activation;
}

vector<float> DenseLayer::backward_pass(vector<float> error, float learning_rate) {	
	cout << "backward" << endl;
	
	float* activation_deriv = sigmoid_deriv_gpu(&_activation[0], _output_size);
	float* elementwise_res = elementwise_gpu(&error[0], activation_deriv, _output_size);
	
	for (int i = 0; i < _output_size; ++i) {
		cout << fixed << setprecision(3) << setw(7) << error[i] << " ";
	}
	cout << endl;
	
	/*
	cout << endl << endl;
	for (int i = 0; i < _output_size; ++i) {
		cout << fixed << setprecision(3) << setw(7) << activation_deriv[i] << " ";
	}
	cout << endl << endl;
	for (int i = 0; i < _output_size; ++i) {
		cout << fixed << setprecision(3) << setw(7) << elementwise_res[i] << " ";
	}
	cout << endl << endl;
	*/
	
	float* dW = vec_t_vec_gpu(&_input[0], elementwise_res, _input_size, _output_size);
	
	update_gpu(&_weight[0], dW, _input_size * _output_size, learning_rate);
	
	vector<float> w_t(_weight.size(), 0);
	transpose(w_t, _input_size, _output_size);
	
	float* error_back = vec_matr_gpu(elementwise_res, &w_t[0], _output_size, _input_size);
	
	vector<float> kostil(_input_size, 0);
	for (int i = 0; i < kostil.size(); ++i) {
		kostil[i] = error_back[i];
	}
	
	return kostil;
}